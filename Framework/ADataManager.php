<?
namespace Framework;

abstract class ADataManager {
	/* Instance variables */

	public $Id;

	public $Name;

	public $Name2;

	public $DataType;

	public $Connection = null;

	public $Database = null;

	public $Table = null;

	public $PrimaryKey = 'Id';

	public $Slaves = [];

	public $DefaultSlave = null;

	public $UseLog = true;

	public $DataMembers = [];

	public $InsertQuery;

	public $UpdateQuery;

	public $SelectQuery;

	public $References = [];

	/* Magic methods */

	public function __construct(string $Id, string $Name, array $DataType, array $Params) {
		$this->Id = $Id;

		$this->Name = $Name;

		$this->Name2 = str_replace(['»', '-', '/'], ['|', '|', '|'], $this->Id);

		$this->Name2 = explode('|', $this->Name2);

		$this->Name2 = $this->Name2[count($this->Name2) - 1];

		$this->DataType = $DataType;

		$this->Connection = \Framework\GetDatabase($Params['Connection'] ?? '');

		$this->Database = $Params['Database'] ?? null;

		$this->Table = $Params['Table'] ?? $Id;

		foreach($Params['Slaves'] ?? [] as $connection => $params) {
			$this->Slaves[$connection] = new \Framework\ADataManager\Slave($this, $connection, $params);
		}

		if(isset($Params['DefaultSlave'])) {
			$this->DefaultSlave = $this->Slaves[$Params['DefaultSlave']];
		}
		else {
			$this->DefaultSlave = null;
		}

		$this->UseLog = $Params['UseLog'] ?? false;

		$this->DataMembers = $Params['DataMembers'] ?? [];

		$this->References = $Params['References'] ?? [];
	}

	public function __debugInfo() {
		return [
			'ID' => $this->Id,
			'Name' => $this->Name,
			'Connection' => $this->Connection->Name,
			'Database' => $this->Database,
			'Table' => $this->Table,
			'Slaves' => $this->Slaves
		];
	}

	/* Instance initialization methods */

	public abstract function Initialize();

	public function Initialize»DataMembers() {
		foreach($this->DataMembers as $alias => $params) {
			$dataMember = '\\Framework\\'.$params['Type'].'DataMember';

			/*{IF:DEBUG}*/

			unset($params['Type']);

			/*{ENDIF}*/

			$this->DataMembers[$alias] = new $dataMember($this, $alias, $params);
		}
	}

	public abstract function Initialize»InsertQuery(array $Params = null);

	public abstract function Initialize»UpdateQuery(array $Params = null);

	public abstract function Initialize»SelectQuery(array $Params = null);

	public function Initialize»References() {
		foreach($this->References as $name => $params) {
			$this->References[$name] = new \Framework\ADataManager\Reference($this, $name, $params);

			$this->References[$name]->Initialize();
		}
	}

	/* Instance master/slave methods */

	public function CreateSlaveTables() {
		$this->Connection->Lock([
			new \Framework\Database\Source([
				'DataManager' => $this
			]),
			$this->Id => new \Framework\Database\Source([
				'DataManager' => $this
			])
		]);

		foreach($this->Slaves as $slave) {
			if($slave->IsReadOnly) {
				continue;
			}

			$slave->CreateTable();
		}

		$this->Connection->Unlock([
			new \Framework\Database\Source([
				'DataManager' => $this
			]),
			$this->Id => new \Framework\Database\Source([
				'DataManager' => $this
			])
		]);
	}

	public function CreateSlaveTable(string $Name) {
		$this->Connection->Lock([
			new \Framework\Database\Source([
				'DataManager' => $this
			]),
			$this->Id => new \Framework\Database\Source([
				'DataManager' => $this
			])
		]);

		$slave = $this->Slaves[$Name];

		if($slave->IsReadOnly) {
			throw new \Framework\RuntimeException(
				'Unable to sync slave, may not write to slave.',
				['DataManager' => $this->Name, 'Slave' => $Name]
			);
		}

		$slave->CreateTable();

		$this->Connection->Unlock([
			new \Framework\Database\Source([
				'DataManager' => $this
			]),
			$this->Id => new \Framework\Database\Source([
				'DataManager' => $this
			])
		]);
	}

	public function SyncSlaves() {
		$this->Connection->Lock([
			$this->Id => new \Framework\Database\Source([
				'DataManager' => $this
			])
		]);

		foreach($this->Slaves as $slave) {
			if($slave->IsReadOnly) {
				continue;
			}

			$slave->Sync();
		}

		$this->Connection->Unlock([
			$this->Id => new \Framework\Database\Source([
				'DataManager' => $this
			])
		]);
	}

	public function SyncSlave(string $Name) {
		$this->Connection->Lock([
			$this->Id => new \Framework\Database\Source([
				'DataManager' => $this
			])
		]);

		$slave = $this->Slaves[$Name];

		if($slave->IsReadOnly) {
			throw new \Framework\RuntimeException(
				'Unable to sync slave, may not write to slave.',
				['DataManager' => $this->Name, 'Slave' => $Name]
			);
		}

		$slave->Sync();

		$this->Connection->Unlock([
			$this->Id => new \Framework\Database\Source([
				'DataManager' => $this
			])
		]);
	}

	/* Instance data member methods */

	public function HasDataMember(string $DataMember) : bool {
		return array_key_exists($DataMember, $this->DataMembers);
	}

	public function GetDataMember(string $DataMember) : \Framework\ADataMember {
		return $this->DataMembers[$DataMember];
	}

	/* Instance reference methods */

	public function HasReference(string $Reference) : bool {
		return array_key_exists($Reference, $this->References);
	}

	public function GetReference(string $Reference) : \Framework\ADataManager\Reference {
		return $this->References[$Reference];
	}

	/* Instance methods */

	public function GetConnection(string $Name = null, bool $UseSlave = false) : \Framework\Database\AConnection {
		if($Name === null) {
			if(!$UseSlave || $this->DefaultSlave === null) {
				return $this->Connection;
			}
			else {
				return $this->DefaultSlave->Connection;
			}
		}
		else {
			return \Framework\GetDatabase($Name);
		}
	}

	public function TryOnDelete(\Framework\DataType $Item) : bool {
		return true;
	}

	public function OnDelete(\Framework\DataType $Item) {

	}

	public function OnDeleted(\Framework\DataType $Item) {

	}

	private function PrepareInsertQuery($Query, array $Params = null) : \Framework\Database\InsertQuery {
		if($Query === null) {
			$query = $this->InsertQuery->Clone();
		}
		else if($Query instanceof \Framework\Database\InsertQuery) {
			$query = $Query->Clone();
		}
		else {
			$query = $this->InsertQuery->Clone();

			if(array_key_exists('Columns', $Query)) {
				$query->MergeColumns($Query['Columns']);

				/*{IF:DEBUG}*/

				unset($Query['Columns']);

				/*{ENDIF}*/
			}

			if(array_key_exists('Values', $Query)) {
				$query->MergeValues($Query['Values']);

				/*{IF:DEBUG}*/

				unset($Query['Values']);

				/*{ENDIF}*/
			}

			/*{IF:DEBUG}*/

			if(0 < count($Query)) {
				throw new \Framework\RuntimeException(
					'Unable to parse query, trailing data found.',
					['Data' => $Query]
				);
			}

			/*{ENDIF}*/
		}

		if(isset($GLOBALS['Locale'])) {
			$query->SetVariable('Locale', $GLOBALS['Locale']->Id);
		}

		if(isset($Params['Variables'])) {
			foreach($Params['Variables'] as $key => $value) {
				$query->SetVariable($key, $value);
			}
		}

		return $query;
	}

	private function PrepareSelectQuery($Query, array $Params = null) : \Framework\Database\SelectQuery {
		if($Query === null) {
			$query = $this->SelectQuery->Clone();
		}
		else if($Query instanceof \Framework\Database\SelectQuery) {
			$query = $Query->Clone();
		}
		else {
			$query = $this->SelectQuery->Clone();

			if(array_key_exists('Columns', $Query)) {
				$query->SetColumns($Query['Columns']);

				/*{IF:DEBUG}*/

				unset($Query['Columns']);

				/*{ENDIF}*/
			}

			if(array_key_exists('Sources', $Query)) {
				$query->MergeSources($Query['Sources']);

				/*{IF:DEBUG}*/

				unset($Query['Sources']);

				/*{ENDIF}*/
			}

			if(array_key_exists('Condition', $Query)) {
				$query->MergeCondition($Query['Condition']);

				/*{IF:DEBUG}*/

				unset($Query['Condition']);

				/*{ENDIF}*/
			}

			if(array_key_exists('Groups', $Query)) {
				$query->SetGroups($Query['Groups']);

				/*{IF:DEBUG}*/

				unset($Query['Groups']);

				/*{ENDIF}*/
			}

			if(array_key_exists('Having', $Query)) {
				$query->MergeHaving($Query['Having']);

				/*{IF:DEBUG}*/

				unset($Query['Having']);

				/*{ENDIF}*/
			}

			if(array_key_exists('Sorts', $Query)) {
				$query->SetSorts($Query['Sorts']);

				/*{IF:DEBUG}*/

				unset($Query['Sorts']);

				/*{ENDIF}*/
			}

			if(array_key_exists('Limit', $Query)) {
				$query->SetLimit($Query['Limit']);

				/*{IF:DEBUG}*/

				unset($Query['Limit']);

				/*{ENDIF}*/
			}

			/*{IF:DEBUG}*/

			if(0 < count($Query)) {
				throw new \Framework\RuntimeException(
					'Unable to parse query, trailing data found.',
					['Data' => $Query]
				);
			}

			/*{ENDIF}*/
		}

		$this->PrepareSelectQuery»References($query, $Params);

		if(isset($Params['References'])) {
			foreach($Params['References'] as $key => $value) {
				if($value instanceof \Framework\ADataManager) {
					$query->SetSourceAlias($value->Id, $key);
				}
				else if(is_bool($value)) {
					$query->SetSourceAlias($this->References[$key]->TargetDataManager->Id, $key);
				}
			}
		}

		if(isset($GLOBALS['Locale'])) {
			$query->SetVariable('Locale', $GLOBALS['Locale']->Id);
		}

		if(isset($Params['Variables'])) {
			foreach($Params['Variables'] as $key => $value) {
				$query->SetVariable($key, $value);
			}
		}

		return $query;
	}

	private function PrepareSelectQuery»References(\Framework\Database\SelectQuery $Query, array $Params = null) {
		if(isset($Params['References'])) {
			$this->PrepareSelectQuery»References»Columns($Query, $Params);

			$this->PrepareSelectQuery»References»Sources($Query, $Params);
		}
	}

	private function PrepareSelectQuery»References»Columns(\Framework\Database\SelectQuery $Query, array $Params = null) {
		$items = [];

		foreach($Params['References'] as $key => $value) {
			if($value instanceof \Framework\ADataManager) {
				foreach($value->DataMembers as $dataMember) {
					$items[$key.'.'.$dataMember->Name] = new \Framework\Database\Columns\Reference([
						'Source' => $value->Id,
						'Name' => $dataMember->Name
					]);
				}
			}
			else if(is_bool($value)) {
				foreach($this->References[$key]->TargetDataManager->DataMembers as $dataMember) {
					$items[$key.'.'.$dataMember->Name] = new \Framework\Database\Columns\Reference([
						'Source' => $this->References[$key]->Name,
						'Name' => $dataMember->Name
					]);
				}
			}
		}

		$Query->MergeColumns($items);
	}

	private function PrepareSelectQuery»References»Sources(\Framework\Database\SelectQuery $Query, array $Params = null) {
		$items = [];

		foreach($Params['References'] as $key => $value) {
			if(isset($Query->Sources[$this->Id]->Joins[$key])) {
				continue;
			}

			if(is_bool($value)) {
				$items[$key] = new \Framework\Database\Join([
					'IsRequired' => $value,
					'Sources' => $this->References[$key]->SelectQuery->Sources->Clone(),
					'Condition' => $this->References[$key]->SelectQuery->Condition->Clone()
				]);
			}
		}

		$Query->Sources[$this->Id]->MergeJoins($items);
	}

	public function Insert($Query = null, array $Params = null) : int {
		$query = $this->PrepareInsertQuery($Query, $Params);

		$connection = $this->GetConnection($Params['Connection'] ?? null);

		$id = $connection->Insert($query);

		if(0 < count($this->Slaves)) {
			$query->MergeColumns([$this->DataMembers[$this->PrimaryKey]]);

			$query->MergeValues([$id]);

			foreach($this->Slaves as $slave) {
				if($slave->IsReadOnly) {
					continue;
				}

				$slave->Connection->Insert($query);
			}
		}

		$this->OnInserted($this->FindById($id));

		return $id;
	}

	public function OnInserted(\Framework\DataType $Item) {

	}

	public function Update($Query = null, array $Params = null) : int {
		if($Query === null) {
			$query = $this->UpdateQuery;

			if(isset($GLOBALS['Locale'])) {
				$query->SetVariable('Locale', $GLOBALS['Locale']->Id);
			}
		}
		else if($Query instanceof \Framework\Database\UpdateQuery) {
			$query = $Query;
		}
		else {
			$query = $this->UpdateQuery->Clone();

			if(isset($Query['Changes'])) {
				$query->SetChanges($Query['Changes']);
			}

			if(isset($Query['<Changes'])) {
				$query->MergeChanges($Query['<Changes']);
			}

			if(isset($Query['Condition'])) {
				$query->SetCondition($Query['Condition']);
			}

			if(isset($Query['<Condition'])) {
				$query->MergeCondition($Query['<Condition']);
			}

			if(isset($GLOBALS['Locale'])) {
				$query->SetVariable('Locale', $GLOBALS['Locale']->Id);
			}

			if(isset($Query['Variables'])) {
				foreach($Query['Variables'] as $key => $value) {
					$query->SetVariable($key, $value);
				}
			}
		}

		$result = $this->Connection->Update($query);

		foreach($this->Slaves as $slave) {
			if($slave->IsReadOnly) {
				continue;
			}

			$slave->Connection->Update($query);
		}

		return $result;
	}

	public function Delete($Query = null, array $Params = null) : array {
		if($Query instanceof \Framework\DataType) {
			$items = [$Query];
		}
		else if(is_array($Query) && isset($Query[0]) && $Query[0] instanceof \Framework\DataType) {
			$items = $Query;
		}
		else {
			$items = $this->FindAll($Query, $Params);
		}

		$items = array_filter(
			$items,
			function($item) {
				if($item->DataManager !== $this) {
					if($item->Connection->DataManager === $this) {
						$item = $item->Connection;
					}
					else {
						throw new \Framework\RuntimeException(
							'Unable to delete item, invalid data type.',
							['Data manager' => $this, 'Item' => $item]
						);
					}
				}

				if(!$item->TryOnDelete()) {
					return false;
				}

				if(!$this->TryOnDelete($item)) {
					return false;
				}

				$item->OnDelete();

				$this->OnDelete($item);

				$query = new \Framework\Database\DeleteQuery([
					'Targets' => new \Framework\Database\Targets([
						$this->Id => new \Framework\Database\Target([
							'DataManager' => $this,
							'Alias' => $this->Id
						])
					]),
					'Sources' => new \Framework\Database\Sources([
						$this->Id => new \Framework\Database\Source([
							'DataManager' => $this,
							'Alias' => $this->Id
						])
					]),
					'Condition' => new \Framework\Database\Conditions\IsEqualTo([
						'ColumnA' => $this->DataMembers[$this->PrimaryKey],
						'ColumnB' => $item->{$this->PrimaryKey}
					])
				]);

				$result = $this->Connection->Delete($query);

				if($result === 0) {
					return false;
				}

				foreach($this->Slaves as $slave) {
					if($slave->IsReadOnly) {
						continue;
					}

					$slave->Connection->Delete($query);
				}

				$item->OnDeleted();

				$this->OnDeleted($item);

				return true;
			}
		);

		$items = array_map(
			function($item) {
				if($item->DataManager !== $this) {
					$item = $item->Connection;
				}

				return $item->{$this->PrimaryKey};
			},
			$items
		);

		return $items;
	}

	public function Find($Query = null, array $Params = null) {
		$query = $this->PrepareSelectQuery($Query, $Params);

		$connection = $this->GetConnection($Params['Connection'] ?? null, true);

		$result = $connection->Find($query);

		if($result === false) {
			return null;
		}

		if(!$result->FetchAssoc()) {
			return null;
		}

		$item = $this->Instantiate($result, $Params['Extras'] ?? null, $Params['References'] ?? null);

		return $item;
	}

	public function TryFind(&$Item, $Query = null, array $Params = null) : bool {
		$query = $this->PrepareSelectQuery($Query, $Params);

		$connection = $this->GetConnection($Params['Connection'] ?? null, true);

		$result = $connection->Find($query);

		if($result === false) {
			return false;
		}

		if(!$result->FetchAssoc()) {
			return false;
		}

		$Item = $this->Instantiate($result, $Params['Extras'] ?? null, $Params['References'] ?? null);

		return true;
	}

	public function FindAll($Query = null, array $Params = null) : array {
		$query = $this->PrepareSelectQuery($Query, $Params);

		$connection = $this->GetConnection($Params['Connection'] ?? null, true);

		$result = $connection->FindAll($query);

		if($result === false) {
			return [];
		}

		$items = [];

		while($result->FetchAssoc()) {
			$items[] = $this->Instantiate($result, $Params['Extras'] ?? null, $Params['References'] ?? null);
		}

		return $items;
	}

	public function Count($Query = null, array $Params = null) : int {
		$query = $this->PrepareSelectQuery($Query, $Params);

		$query->SetColumns([
			new \Framework\Database\Columns\Count()
		]);

		$connection = $this->GetConnection($Params['Connection'] ?? null, true);

		$value = $connection->Count($query);

		return $value;
	}

	public function Contains($Query = null, array $Params = null) : bool {
		return 0 < $this->Count($Query, $Params);
	}

	public function DoesNotContain($Query = null, array $Params = null) : bool {
		return $this->Count($Query, $Params) === 0;
	}

	public function SumFloat($Column, $Query = null, array $Params = null) : float {
		$query = $this->Sum»GenerateQuery($Column, $Query, $Params);

		$connection = $this->GetConnection($Params['Connection'] ?? null, true);

		$value = $connection->SumFloat($query);

		return $value;
	}

	public function SumInt($Column, $Query = null, array $Params = null) : int {
		$query = $this->Sum»GenerateQuery($Column, $Query, $Params);

		$connection = $this->GetConnection($Params['Connection'] ?? null, true);

		$value = $connection->SumInt($query);

		return $value;
	}

	private function Sum»GenerateQuery($Column, $Query, array $Params = null) : \Framework\Database\SelectQuery {
		if($Query === null) {
			$query = $this->SelectQuery->Clone();
		}
		else if($Query instanceof \Framework\Database\SelectQuery) {
			$query = $Query->Clone();
		}
		else {
			$query = $this->SelectQuery->Clone();

			if(array_key_exists('Sources', $Query)) {
				$query->MergeSources($Query['Sources']);
			}

			if(array_key_exists('Condition', $Query)) {
				if($query->Condition === null) {
					$query->SetCondition($Query['Condition']);
				}
				else {
					$query->SetCondition(
						new \Framework\Database\Conditions\All([
							'Conditions' => [
								$query->Condition,
								$Query['Condition']
							]
						])
					);
				}
			}

			if(array_key_exists('Sorts', $Query)) {
				$query->SetSorts($Query['Sorts']);
			}

			if(array_key_exists('Limit', $Query)) {
				$query->SetLimit($Query['Limit']);
			}
		}

		$query->SetColumns([
			new \Framework\Database\Columns\Sum([
				'Column' => $Column
			])
		]);

		if(isset($Params['References'])) {
			$items = [];

			foreach($Params['References'] as $key => $value) {
				if(is_bool($value)) {
					$items[$key] = new \Framework\Database\Join([
						'IsRequired' => $value,
						'Sources' => $this->References[$key]->SelectQuery->Sources,
						'Condition' => $this->References[$key]->SelectQuery->Condition
					]);
				}
			}

			$query->Sources[$this->Id]->MergeJoins($items);
		}

		if(isset($GLOBALS['Locale'])) {
			$query->SetVariable('Locale', $GLOBALS['Locale']->Id);
		}

		if(isset($Params['Variables'])) {
			foreach($Params['Variables'] as $key => $value) {
				$query->SetVariable($key, $value);
			}
		}

		return $query;
	}

	public function Truncate() {
		$source = new \Framework\Database\Source(['DataManager' => $this]);

		$this->Connection->Truncate($source);

		foreach($this->Slaves as $slave) {
			if($slave->IsReadOnly) {
				continue;
			}

			$slave->Connection->Truncate($source);
		}
	}

	public function Lock() {
		$this->Connection->Lock([new \Framework\Database\Source(['DataManager' => $this])]);
	}

	public function Unlock() {
		$this->Connection->Unlock([new \Framework\Database\Source(['DataManager' => $this])]);
	}

	public function Instantiate(\Framework\Database\AResult $Result, array $Extras = null, array $References = null) : \Framework\DataType {
		$item = $this->DataType['Class'];

		$item = new $item($this, null, $Result->Row, $Extras);

		if($References !== null) {
			foreach($References as $key => $value) {
				if($value instanceof \Framework\ADataManager) {
					if($Result->Row[$key.'.'.$value->PrimaryKey] === null) {
						$item->$key = null;
					}
					else {
						$item->$key = $value->DataType['Class'];

						$item->$key = new $item->$key($value, $key, $Result->Row);
					}
				}
				else if(is_bool($value)) {
					if(array_key_exists($key.'.Id', $Result->Row) && $Result->Row[$key.'.Id'] === null) {
						$item->$key = null;
					}
					else if(array_key_exists($key.'.id', $Result->Row) && $Result->Row[$key.'.id'] === null) {
						$item->$key = null;
					}
					else {
						$item->$key = $this->References[$key]->TargetDataManager->DataType['Class'];

						$item->$key = new $item->$key($this->References[$key]->TargetDataManager, $key, $Result->Row);
					}
				}
				else {
					$item->$key = $value;
				}
			}
		}

		return $item;
	}
}
?>