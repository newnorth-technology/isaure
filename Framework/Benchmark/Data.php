<?
namespace Framework\Benchmark;

class Data {
	/* Static variables */

	public $Parent;

	public $Backtrace;

	public $Data;

	public $Time;

	/* Magic methods */

	public function __construct(\Framework\Benchmark\Process $Parent = null, array $Data = []) {
		$this->Parent = $Parent;

		if($this->Parent !== null) {
			$this->Parent->Children[] = $this;
		}

		$this->Backtrace = array_slice(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS), 2);

		$this->Data = $Data;

		$this->Time = microtime(true);
	}

	/* Instance methods */

	public function Print(string $Key) {
		echo '<tr style="background-color:#e0e0e0;"><td>', $Key, '</td>';

		$this->Print»Backtrace();

		$this->Print»Data();

		echo '<td></td><td>', number_format($this->Time - $_SERVER['REQUEST_TIME_FLOAT'], 2, ',', ' '), '</td></tr>';
	}

	public function Print»Backtrace() {
		echo '<td>';

		foreach($this->Backtrace as $i => $trace) {
			if(0 < $i) {
				echo '<br />';
			}

			if(isset($trace['class'])) {
				echo $trace['class'].$trace['type'].$trace['function'];
			}
			else {
				echo $trace['function'];
			}
		}

		echo '</td>';
	}

	public function Print»Data() {
		echo '<td>';

		foreach($this->Data as $key => $value) {
			echo '<p><b>', $key, ':</b><br />', $value, '</p>';
		}

		echo '</td>';
	}
}
?>