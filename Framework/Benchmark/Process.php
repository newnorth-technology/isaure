<?
namespace Framework\Benchmark;

class Process {
	/* Static variables */

	public $Parent;

	public $Backtrace;

	public $Data;

	public $StartTime;

	public $FinishTime;

	public $ElapsedTime;

	public $Children = [];

	/* Magic methods */

	public function __construct(\Framework\Benchmark\Process $Parent = null, array $Data = []) {
		$this->Parent = $Parent;

		if($this->Parent !== null) {
			$this->Parent->Children[] = $this;
		}

		$this->Backtrace = array_slice(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS), 2);

		$this->Data = $Data;

		$this->StartTime = microtime(true);
	}

	/* Instance methods */

	public function Finish() {
		$this->FinishTime = microtime(true);

		$this->ElapsedTime = $this->FinishTime - $this->StartTime;

		$parent = $this->Parent;
	}

	public function Print(string $Key) {
		if($this->ElapsedTime < 0.05) {
			echo '<tr style="background-color:#c0c0c0;">';
		}
		else {
			echo '<tr style="background-color:#c08080;">';
		}

		echo '<td>', $Key, '</td>';

		$this->Print�Backtrace();

		$this->Print�Data();

		echo '<td>', number_format($this->ElapsedTime, 2, ',', ' '), '</td><td>', number_format($this->StartTime - $_SERVER['REQUEST_TIME_FLOAT'], 2, ',', ' '), '</td></tr>';

		foreach($this->Children as $i => $child) {
			$child->Print($Key.($i + 1).'.');
		}
	}

	public function Print�Backtrace() {
		echo '<td>';

		foreach($this->Backtrace as $i => $trace) {
			if(0 < $i) {
				echo '<br />';
			}

			if(isset($trace['class'])) {
				echo $trace['class'].$trace['type'].$trace['function'];
			}
			else {
				echo $trace['function'];
			}
		}

		echo '</td>';
	}

	public function Print�Data() {
		echo '<td>';

		foreach($this->Data as $key => $value) {
			if(is_int($key)) {
				echo '<p><b>', $value, '</b></p>';
			}
			else {
				echo '<p><b>', $key, ':</b><br />', $value, '</p>';
			}
		}

		echo '</td>';
	}
}
?>