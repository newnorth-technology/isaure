<?
namespace Framework;

class Locale {
	/* Instance variables */

	public $Id;

	public $Conventions;

	/* Magic methods */

	public function __construct(array $Param) {
		foreach($Param as $key => $value) {
			$this->$key = $value;
		}
	}

	/* Instance methods */

	public function Activate() {
		setlocale(LC_ALL, $this->Id);

		$this->Conventions = localeconv();
	}

	public function FormatNumber(float $Number, int $Decimals) : string {
		return number_format($Number, $Decimals, $this->DecimalPoint, $this->ThousandsSeparator);
	}
}
?>