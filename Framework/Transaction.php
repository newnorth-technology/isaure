<?
namespace Framework;

class Transaction {
	/* Instance variables */

	public $Parent;

	public $System;

	public $Source;

	/* Magic methods */

	public function __construct(\Framework\Transaction $Parent = null, string $System, array $Source) {
		$this->Parent = $Parent;

		$this->System = $System;

		$this->Source = $Source;
	}
}

function StartTransaction(string $System = null, array $Source = []) {
	$GLOBALS['Transaction'] = new \Framework\Transaction(
		$GLOBALS['Transaction'],
		$System ?? $GLOBALS['Transaction']->System,
		MergeArrays($GLOBALS['Transaction']->Source, $Source)
	);
}

function StopTransaction() {
	$GLOBALS['Transaction'] = $GLOBALS['Transaction']->Parent;
}
?>