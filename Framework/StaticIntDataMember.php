<?
namespace Framework;

class StaticIntDataMember extends \Framework\AStaticDataMember {
	/* Static methods */

	public static function ParseFromDbValue(\Framework\StaticIntDataMember $DataMember = null, $Value) {
		/*{IF:DEBUG}*/

		if($Value === null) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, value may not be null.',
				['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name]
			);
		}

		if(!is_int($Value) && !is_string($Value)) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, invalid value type.',
				['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name, 'Value' => $Value]
			);
		}

		/*{ENDIF}*/

		return (int)$Value;
	}

	public static function ParseValue(\Framework\StaticIntDataMember $DataMember = null, $Value) {
		/*{IF:DEBUG}*/

		if($Value === null) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, value may not be null.',
				['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name]
			);
		}

		if(!is_int($Value)) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, invalid value type.',
				['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name, 'Value' => $Value]
			);
		}

		/*{ENDIF}*/

		return $Value;
	}

	public static function ParseToDbValue(\Framework\StaticIntDataMember $DataMember = null, $Value) {
		return $Value;
	}
}
?>