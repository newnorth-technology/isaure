<?
namespace Framework;

function OnShutdown() {
	$Error = error_get_last();

	if($Error === null) {
		return;
	}

	if(0 < ob_get_level()) {
		ob_end_clean();
	}

	ob_start();

	echo '<pre>', "\n\n";

	echo '<b>FATAL ERROR</b>', "\n", '------------------------------', "\n";

	echo '<b>Message:</b><br />', "\n", $Error['message'], '<br />', "\n";

	echo '<b>File:</b><br />', "\n", utf8_encode($Error['file']), '<br />', "\n";

	echo '<b>Line:</b><br />', "\n", $Error['line'], '<br />', "\n";

	echo '</pre>';

	$html = ob_get_contents();

	ob_end_clean();

	if(GetConfig('System.ShowErrors', true)) {
		echo $html;
	}

	(new \Framework\Email('system.exception'))->Send([
		'Locale' => $GLOBALS['Locale']->Id,
		'To' => 'james@newnorth.technology',
		'subject' => 'Fatal error: '.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],
		'html' => $html
	]);

	exit();
}
?>