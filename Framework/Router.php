<?
namespace Framework;

class Router {
	/* Static methods */

	public static function Reroute(string $Path, array $RouteParams = [], array $RequestParams = []) {
		$url = \Framework\CreateUrl($Path, null, $RouteParams);

		$url = substr($url, 1);

		Router::ParseUrl($url, $route, $params);

		throw new \Framework\RerouteException($route, $params, $RequestParams);
	}

	public static function RerouteBadRequestPage() {
		header('HTTP/1.0 400 Bad Request');

		/*Router::GetRoute(null, '/BadRequest', $Route);

		$GLOBALS['Route'] = $Route;

		$Params = $Route->Params;

		$Params['Page'] = $Route->FullName;

		foreach($GLOBALS['Params'] as $Key => $Value) {
			if(!isset($Params[$Key])) {
				$Params[$Key] = $Value;
			}
		}

		if(!isset($Params['Locale']) && Config»IsNotEmpty('/Defaults/Locale')) {
			$Params['Locale'] = \Framework\GetConfig('Defaults.Locale');
		}

		throw new RerouteException($Params);*/
	}

	public static function RerouteForbiddenPage() {
		header('HTTP/1.0 403 Forbidden');

		Router::GetRoute(null, '/Forbidden', $Route);

		$GLOBALS['Route'] = $Route;

		$Params = $Route->Params;

		$Params['Page'] = $Route->FullName;

		foreach($GLOBALS['Params'] as $Key => $Value) {
			if(!isset($Params[$Key])) {
				$Params[$Key] = $Value;
			}
		}

		if(!isset($Params['Locale']) && Config»IsNotEmpty('/Defaults/Locale')) {
			$Params['Locale'] = \Framework\GetConfig('Defaults.Locale');
		}

		throw new RerouteException($Params);
	}

	public static function RerouteNotFoundPage() {
		header('HTTP/1.0 404 Not Found');

		exit();

		/*Router::GetRoute(null, '/NotFound', $Route);

		$GLOBALS['Route'] = $Route;

		$Params = $Route->Params;

		$Params['Page'] = $Route->FullName;

		foreach($GLOBALS['Params'] as $Key => $Value) {
			if(!isset($Params[$Key])) {
				$Params[$Key] = $Value;
			}
		}

		if(!isset($Params['Locale']) && Config»IsNotEmpty('Defaults/Locale')) {
			$Params['Locale'] = \Framework\GetConfig('Defaults.Locale');
		}

		throw new RerouteException($Params);*/
	}

	public static function RerouteErrorPage($Exception) {
		header('HTTP/1.0 500 Internal Server Error');die();

		/*Router::GetRoute(null, '/Error', $Route);

		$GLOBALS['Route'] = $Route;

		$Params = $Route->Params;

		$Params['Page'] = $Route->FullName;

		$Params['Error'] = $Exception;

		foreach($GLOBALS['Params'] as $Key => $Value) {
			if(!isset($Params[$Key])) {
				$Params[$Key] = $Value;
			}
		}

		if(!isset($Params['Locale']) && Config»IsNotEmpty('Defaults/Locale')) {
			$Params['Locale'] = \Framework\GetConfig('Defaults.Locale');
		}

		throw new RerouteException($Params);*/
	}

	public static function RedirectPath(string $Path = '', string $Locale = null, array $Params = null, array $QueryString = null) {
		throw new RedirectException(Router::CreateUrl($Path, $Locale, $Params, $QueryString));
	}

	public static function RedirectUrl(string $Url = null, array $QueryString = []) {
		$Url = $Url ?? $_SERVER['REQUEST_URI'];

		if(count($QueryString) === 0) {
			throw new \Framework\RedirectException($Url);
		}
		else if(strpos($Url, '?') === false) {
			throw new \Framework\RedirectException($Url.'?'.$QueryString);
		}
		else {
			throw new \Framework\RedirectException($Url.'&'.$QueryString);
		}
	}
}
?>