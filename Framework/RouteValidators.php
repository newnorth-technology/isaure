<?
namespace Framework;

class RouteValidators {
	/* Instance variables */

	public $Validators = [];

	/* Magic methods */

	public function __construct(array $Params) {
		foreach($Params as $params) {
			$this->Validators[] = new \Framework\RouteValidator($params);
		}
	}

	public function __debugInfo() {
		return $this->Validators;
	}

	/* Instance methods */

	public function Validate(array $pv_params) : bool {
		foreach($this->Validators as $validator) {
			if(!$validator->Validate($pv_params)) {
				return false;
			}
		}

		return true;
	}
}
?>