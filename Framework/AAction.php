<?
namespace Framework;

abstract class AAction {
	/* Static methods */

	public static function Instantiate(\Framework\AController $Owner, string $Path, string $Name = null, array $Params = []) {
		/*{IF:DEBUG}*/

		if(!\Framework\IsConfigSet('Actions.'.$Path)) {
			throw new \Framework\RuntimeException(
				'Unable to instantiate action, declaration not found.',
				['Path' => $Path, 'Name' => $Name, 'Params' => $Params]
			);
		}

		/*{ENDIF}*/

		$Params = MergeArrays(\Framework\GetConfig('Actions.'.$Path), $Params);

		$delimiter = strrpos($Path, '/');

		if($delimiter === false) {
			$id = $Path;

			$name = $Name ?? $Path;

			$class = $Path;

			$directory = '';

			$file = $Path.'.php';
		}
		else {
			$id = $Path;

			$name = $Name ?? substr($Path, $delimiter + 1);

			$class = str_replace('/', '\\', $Path);

			$directory = substr($Path, 0, $delimiter + 1);

			$file = substr($Path, $delimiter + 1).'.php';
		}

		if(self::Instantiate»LoadClass($directory, $file, $class)) {
			if(!class_exists($class, false)) {
				throw new \Framework\RuntimeException(
					'Unable to instantiate action, class not found.',
					['Directory' => $directory, 'File' => $file, 'Class' => $class]
				);
			}
		}
		else {
			throw new \Framework\RuntimeException(
				'Unable to instantiate action, file not found.',
				['Directory' => $directory, 'File' => $file, 'Class' => $class]
			);
		}

		$Params = NormalizeArray($Params);

		$Params['Id'] = $id;

		$Params['Name'] = $name;

		$Params['Directory'] = $directory;

		$Params['File'] = $file;

		$action = new $class($Owner, $Params);

		return $action;
	}

	private static function Instantiate»LoadClass(string & $pv_directory, string $pv_file, string $pv_class) : bool {
		foreach(\Framework\GetConfig('System.Directories.Actions', ['']) as $directory) {
			if(file_exists($directory.$pv_directory.$pv_file)) {
				if(!class_exists($pv_class, false)) {
					@include($directory.$pv_directory.$pv_file);
				}

				$pv_directory = $directory.$pv_directory;

				return true;
			}
		}

		foreach($GLOBALS['modules'] as $module) {
			$path = $module->directory.'actions/'.$pv_directory.$pv_file;

			if(file_exists($path)) {
				if(!class_exists($pv_class, false)) {
					@include($path);
				}

				$pv_directory = $module->directory.'actions/'.$pv_directory;

				return true;
			}
		}

		return false;
	}

	/* Instance variables */

	public $Id;

	public $Name;

	public $Owner;

	public $Model;

	public $method;

	public $Variables;

	public $PreValidators;

	public $PreFormatters = [];

	public $Locks = [];

	public $Validators = [];

	public $Formatters = [];

	public $WasSuccessful = null;

	public $Messages = [];

	/* Magic methods */

	public function __construct(\Framework\AController $Owner, array $Params) {
		$this->Id = $Params['Id'];

		$this->Name = $Params['Name'];

		$this->Owner = $Owner;

		$this->Model = new \Framework\Model($Params);

		$this->method = $Params['method'] ?? null;

		$this->Variables = new \Framework\AAction\Variables($Params['Variables']);

		$this->PreValidators = new \Framework\AAction\PreValidators($Params['PreValidators']);

		$this->PreFormatters = $Params['PreFormatters'] ?? [];

		$this->Initialize»PreFormatters();

		$this->Locks = $Params['Locks'] ?? [];

		$this->Validators = $Params['Validators'] ?? [];

		$this->Initialize»Validators();

		$this->Formatters = $Params['Formatters'] ?? [];

		$this->Initialize»Formatters();
	}

	/* Instance life cycle methods */

	public function Initialize»PreFormatters() {
		foreach($this->PreFormatters as $i => &$params) {
			if(!isset($params['Method'])) {
				throw new \Framework\ConfigException(
					'Pre formatter method is not set.',
					[
						'Owner' => $this->Owner->Id,
						'Action' => $this->Name,
						'Pre formatter' => $i
					]
				);
			}

			$params['Controller'] = $params['Controller'] ?? null;

			$params['Params'] = $params['Params'] ?? [];
		}
	}

	public function Initialize»Validators() {
		foreach($this->Validators as $i => &$group) {
			if(!is_array($group)) {
				throw new \Framework\ConfigException(
					'Invalid validator group, should be an array.',
					[
						'Owner' => $this->Owner->Id,
						'Action' => $this->Name,
						'Group' => $i
					]
				);
			}

			foreach($group as $j => &$params) {
				if(!isset($params['Method'])) {
					throw new \Framework\ConfigException(
						'Validator method is not set.',
						[
							'Owner' => $this->Owner->Id,
							'Action' => $this->Name,
							'Validator' => $j
						]
					);
				}

				$params['Controller'] = $params['Controller'] ?? null;

				$params['Params'] = $params['Params'] ?? [];
			}
		}
	}

	public function Initialize»Formatters() {
		foreach($this->Formatters as $i => &$params) {
			if(!isset($params['Method'])) {
				throw new ConfigException(
					'Formatter method is not set.',
					[
						'Owner' => $this->Owner->__toString(),
						'Action' => $this->Name,
						'Formatter' => $i,
					]
				);
			}

			$params['Controller'] = $params['Controller'] ?? null;

			$params['Params'] = $params['Params'] ?? [];
		}
	}

	public function PreFormat() {
		foreach($this->PreFormatters as $i => $params) {
			if($params['Controller'] === null) {
				$owner = $this;
			}
			else if(!$this->Owner->TryGetRelController($params['Controller'], $owner)) {
				throw new \Framework\RuntimeException(
					'Unable to find pre formatter controller.',
					[
						'Owner' => $this->Owner->Id,
						'Action' => $this->Name,
						'Formatter' => $i,
						'Controller' => $params['Controller']
					]
				);
			}

			$method = 'Format»'.$this->Name.'»'.$params['Method'];

			if(!method_exists($owner, $method)) {
				$method = 'Format»'.$params['Method'];
			}

			if(!method_exists($owner, $method)) {
				throw new \Framework\RuntimeException(
					'Unable to find pre formatter method.',
					[
						'Owner' => $this->Owner->Id,
						'Action' => $this->Name,
						'Formatter' => $i,
						'Owner' => $owner,
						'Method' => $params['Method']
					]
				);
			}

			$owner->$method($params['Params']);
		}
	}

	public function Lock() {
		foreach($this->Locks as $Database => $Sources) {
			\Framework\GetDatabase($Database)->Lock($Sources);
		}
	}

	public function Validate() : bool {
		foreach($this->Validators as $validators) {
			$is_valid = true;

			foreach($validators as $validator) {
				if(array_key_exists('Variable', $validator['Params'])) {
					if($this->Variables->IsVariableTemplate($validator['Params']['Variable'])) {
						$count = $this->Variables->GetVariableTemplateCount($validator['Params']['Variable']);

						for($i = 0; $i < $count; ++$i) {
							$is_valid = $this->Validate»Validator($validator, $i) && $is_valid;
						}
					}
					else {
						$is_valid = $this->Validate»Validator($validator, null) && $is_valid;
					}
				}
				else {
					$is_valid = $this->Validate»Validator($validator, null) && $is_valid;
				}
			}

			if(!$is_valid) {
				$this->WasSuccessful = false;

				return false;
			}
		}

		$this->WasSuccessful = true;

		return true;
	}

	private function Validate»Validator(array $pv_params, int $pv_template = null) : bool {
		$controller = null;

		if(isset($pv_params['Controller'])) {
			if($pv_template === null) {
				if(!$this->Owner->TryGetRelController($pv_params['Controller'], $controller)) {
					throw new \Framework\RuntimeException(
						'Unable to find controller.',
						['Action' => $this->Id, 'Owner' => $this->Owner->Id, 'Controller' => $pv_params['Controller']]
					);
				}
			}
			else {
				if(!$this->Owner->TryGetRelController(str_replace('#', $pv_template, $pv_params['Controller']), $controller)) {
					throw new \Framework\RuntimeException(
						'Unable to find controller.',
						['Action' => $this->Id, 'Owner' => $this->Owner->Id, 'Controller' => str_replace('#', $pv_template, $pv_params['Controller'])]
					);
				}
			}
		}

		$locale = null;

		if(method_exists($this, 'Validate»'.$pv_params['Method'])) {
			if($this->{'Validate»'.$pv_params['Method']}($pv_params['Params'], $pv_template, $locale)) {
				return true;
			}
		}
		else {
			throw new \Framework\RuntimeException(
				'Unable to find method.',
				['Action' => $this->Id, 'Method' => $pv_params['Method']]
			);
		}

		if(array_key_exists('ErrorMessage', $pv_params)) {
			if(isset($pv_params['ErrorMessage'][0])) {
				if($controller === null) {
					$this->Messages[] = \Framework\Translator::Translate($pv_params['ErrorMessage'], ['Action' => '%["Actions.'.$this->Id.'.%$0%"%,$1...%]%']);
				}
				else {
					$controller->Model->ErrorMessages[] = \Framework\Translator::Translate($pv_params['ErrorMessage'], ['Action' => '%["Actions.'.$this->Id.'.%$0%"%,$1...%]%']);
				}
			}
		}
		else if(method_exists($this, 'ErrorMessage»'.$pv_params['Method'])) {
			$message = $this->{'ErrorMessage»'.$pv_params['Method']}($pv_params['Params'], $locale);

			if($controller === null) {
				$this->Messages[] = \Framework\Translator::Translate($message, ['Action' => '%["Actions.'.$this->Id.'.%$0%"%,$1...%]%']);
			}
			else {
				$controller->Model->ErrorMessages[] = \Framework\Translator::Translate($message, ['Action' => '%["Actions.'.$this->Id.'.%$0%"%,$1...%]%']);
			}
		}

		return false;
	}

	public function Format() {
		foreach($this->Formatters as $i => $params) {
			if($params['Controller'] === null) {
				$owner = $this;
			}
			else if(!$this->Owner->TryGetRelController($params['Controller'], $owner)) {
				throw new \Framework\RuntimeException(
					'Unable to find formatter controller.',
					[
						'Owner' => $this->Owner->Id,
						'Action' => $this->Name,
						'Formatter' => $i,
						'Controller' => $params['Controller']
					]
				);
			}

			$method = 'Format»'.$this->Name.'»'.$params['Method'];

			if(!method_exists($owner, $method)) {
				$method = 'Format»'.$params['Method'];
			}

			if(!method_exists($owner, $method)) {
				throw new \Framework\RuntimeException(
					'Unable to find formatter method.',
					[
						'Owner' => $this->Owner->Id,
						'Action' => $this->Name,
						'Formatter' => $i,
						'Owner' => $owner,
						'Method' => $params['Method']
					]
				);
			}

			$owner->$method($params['Params']);
		}
	}

	public function Initialize() {

	}

	public function Load() {

	}

	public function Execute() {

	}

	public function Unlock() {
		if(is_array($this->Locks)) {
			foreach($this->Locks as $Database => $Sources) {
				\Framework\GetDatabase($Database)->Unlock($Sources);
			}
		}
	}

	/* Instance validation methods */

	public function Validate»IsUploaded(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		if($pv_template !== null) {
			$variable = $this->Variables[$pv_params['Variable']][$pv_template];
		}
		else if($this->Variables->IsVariableTemplate($pv_params['Variable'])) {
			$count = $this->Variables->GetVariableTemplateCount($pv_params['Variable']);

			for($i = 0; $i < $count; ++$i) {
				$variable = $this->Variables[$pv_params['Variable']][$i] ?? null;

				if($variable !== null) {
					if(!isset($variable['name'][0])) {
						return false;
					}
				}
			}

			return true;
		}
		else if($this->Variables->IsVariableTranslation($pv_params['Variable'])) {
			foreach(\Framework\GetConfig('Locales') as $locale) {
				$variable = $this->Variables[$locale['Id']][$pv_params['Variable']] ?? null;

				if($variable !== null) {
					if(!isset($variable['name'][0])) {
						$pv_locale = $locale;

						return false;
					}
				}
			}

			return true;
		}
		else {
			$variable = $this->Variables[$pv_params['Variable']];
		}

		if($variable === null) {
			return $pv_params['Default'] ?? true;
		}
		else {
			return isset($variable['name'][0]);
		}
	}

	public function Validate»IsNotUploaded(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		if($this->Variables->IsVariableTranslation($pv_params['Variable'])) {
			foreach(\Framework\GetConfig('Locales') as $locale) {
				$variable = $this->Variables[$locale['Id']][$pv_params['Variable']] ?? null;

				if($variable !== null) {
					if(isset($variable['name'][0])) {
						$pv_locale = $locale;

						return false;
					}
				}
			}

			return true;
		}

		$variable = $this->Variables[$pv_params['Variable']] ?? null;

		if($variable === null) {
			return $pv_params['Default'] ?? true;
		}

		return !isset($variable['name'][0]);
	}

	public function Validate»IsChecked(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		if($this->Variables->IsVariableTranslation($pv_params['Variable'])) {
			foreach(\Framework\GetConfig('Locales') as $locale) {
				if(!isset($this->Variables[$locale['Id']][$pv_params['Variable']])) {
					$pv_locale = $locale;

					return false;
				}
			}

			return true;
		}

		return isset($this->Variables[$pv_params['Variable']]);
	}

	public function Validate»IsNotChecked(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		if($this->Variables->IsVariableTranslation($pv_params['Variable'])) {
			foreach(\Framework\GetConfig('Locales') as $locale) {
				if(isset($this->Variables[$locale['Id']][$pv_params['Variable']])) {
					$pv_locale = $locale;

					return false;
				}
			}

			return true;
		}

		return !isset($this->Variables[$pv_params['Variable']]);
	}

	public function Validate»IsDateTime(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		if($pv_template !== null) {
			$variable = $this->Variables[$pv_params['Variable']][$pv_template];
		}
		else if($this->Variables->IsVariableTranslation($pv_params['Variable'])) {
			foreach(\Framework\GetConfig('Locales') as $locale) {
				$variable = $this->Variables[$locale['Id']][$pv_params['Variable']] ?? null;

				if(isset($variable[0])) {
					$dt = \DateTime::createFromFormat($pv_params['Format'], $variable);

					if($dt === false || $dt->format($pv_params['Format']) !== $variable) {
						$pv_locale = $locale;

						return false;
					}
				}
			}

			return true;
		}
		else {
			$variable = $this->Variables[$pv_params['Variable']];
		}

		if(isset($variable[0])) {
			$dt = \DateTime::createFromFormat($pv_params['Format'], $variable);

			return $dt !== false && $dt->format($pv_params['Format']) === $variable;
		}

		return true;
	}

	public function Validate»IsEmailAddress(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		if($pv_template !== null) {
			$variable = $this->Variables[$pv_params['Variable']][$pv_template];
		}
		else if($this->Variables->IsVariableTranslation($pv_params['Variable'])) {
			foreach(\Framework\GetConfig('Locales') as $locale) {
				$variable = $this->Variables[$locale['Id']][$pv_params['Variable']] ?? null;

				if($variable !== null) {
					if(!filter_var($variable, FILTER_VALIDATE_EMAIL)) {
						$pv_locale = $locale;

						return false;
					}
				}
			}

			return true;
		}
		else {
			$variable = $this->Variables[$pv_params['Variable']];
		}

		if($variable === null) {
			return $pv_params['Default'] ?? true;
		}
		else {
			return filter_var($variable, FILTER_VALIDATE_EMAIL);
		}
	}

	public function Validate»IsNotEmailAddress(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		if($this->Variables->IsVariableTranslation($pv_params['Variable'])) {
			foreach(\Framework\GetConfig('Locales') as $locale) {
				$variable = $this->Variables[$locale['Id']][$pv_params['Variable']] ?? null;

				if($variable !== null) {
					if(filter_var($variable, FILTER_VALIDATE_EMAIL)) {
						$pv_locale = $locale;

						return false;
					}
				}
			}

			return true;
		}

		$variable = $this->Variables[$pv_params['Variable']] ?? null;

		if($variable === null) {
			return true;
		}

		return !filter_var($variable, FILTER_VALIDATE_EMAIL);
	}

	public function Validate»IsEmpty(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		if($this->Variables->IsVariableTranslation($pv_params['Variable'])) {
			foreach(\Framework\GetConfig('Locales') as $locale) {
				$variable = $this->Variables[$locale['Id']][$pv_params['Variable']] ?? null;

				if($variable !== null) {
					if(is_array($variable)) {
						if(0 < count($variable)) {
							$pv_locale = $locale;

							return false;
						}
					}
					else if(isset($variable[0])) {
						$pv_locale = $locale;

						return false;
					}
				}
			}

			return true;
		}

		$variable = $this->Variables[$pv_params['Variable']] ?? null;

		if($variable === null) {
			return $pv_params['Default'] ?? true;
		}

		if(is_array($variable)) {
			return count($variable) === 0;
		}

		return !isset($variable[0]);
	}

	public function Validate»IsNotEmpty(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		if($pv_template !== null) {
			$variable = $this->Variables[$pv_params['Variable']][$pv_template];
		}
		else if($this->Variables->IsVariableTemplate($pv_params['Variable'])) {
			$count = $this->Variables->GetVariableTemplateCount($pv_params['Variable']);

			for($i = 0; $i < $count; ++$i) {
				$variable = $this->Variables[$pv_params['Variable']][$i] ?? null;

				if($variable !== null) {
					if(is_array($variable)) {
						if(count($variable) === 0) {
							return false;
						}
					}
					else if(!isset($variable[0])) {
						return false;
					}
				}
			}

			return true;
		}
		else if($this->Variables->IsVariableTranslation($pv_params['Variable'])) {
			foreach(\Framework\GetConfig('Locales') as $locale) {
				$variable = $this->Variables[$locale['Id']][$pv_params['Variable']] ?? null;

				if($variable !== null) {
					if(is_array($variable)) {
						if(count($variable) === 0) {
							$pv_locale = $locale;

							return false;
						}
					}
					else if(!isset($variable[0])) {
						$pv_locale = $locale;

						return false;
					}
				}
			}

			return true;
		}
		else {
			$variable = $this->Variables[$pv_params['Variable']];
		}

		if($variable === null) {
			return $pv_params['Default'] ?? true;
		}
		else if(is_array($variable)) {
			return 0 < count($variable);
		}
		else {
			return isset($variable[0]);
		}
	}

	public function Validate»IsLocale(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		if($this->Variables->IsVariableTranslation($pv_params['Variable'])) {
			foreach(\Framework\GetConfig('Locales') as $locale) {
				$variable = $this->Variables[$locale['Id']][$pv_params['Variable']] ?? null;

				if($variable !== null) {
					if(!\Framework\IsConfigSet('Locales.'.$variable)) {
						$pv_locale = $locale;

						return false;
					}
				}
			}

			return true;
		}

		$variable = $this->Variables[$pv_params['Variable']] ?? null;

		if($variable === null) {
			return $pv_params['Default'] ?? true;
		}

		return \Framework\IsConfigSet('Locales.'.$variable);
	}

	public function Validate»IsNotLocale(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		if($this->Variables->IsVariableTranslation($pv_params['Variable'])) {
			foreach(\Framework\GetConfig('Locales') as $locale) {
				$variable = $this->Variables[$locale['Id']][$pv_params['Variable']] ?? null;

				if($variable !== null) {
					if(\Framework\IsConfigSet('Locales.'.$variable)) {
						$pv_locale = $locale;

						return false;
					}
				}
			}

			return true;
		}

		$variable = $this->Variables[$pv_params['Variable']] ?? null;

		if($variable === null) {
			return $pv_params['Default'] ?? true;
		}

		return !\Framework\IsConfigSet('Locales.'.$variable);
	}

	public function Validate»IsPassword(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		if($this->Variables->IsVariableTranslation($pv_params['Variable'])) {
			foreach(\Framework\GetConfig('Locales') as $locale) {
				$variable = $this->Variables[$locale['Id']][$pv_params['Variable']] ?? null;

				if($variable !== null) {
					if(preg_match('/[0-9]/', $variable) === 0 || preg_match('/[A-Z]/', $variable) === 0 || preg_match('/[a-z]/', $variable) === 0 || preg_match('/[^0-9A-Za-z]/', $variable) === 0 || !isset($variable[7])) {
						$pv_locale = $locale;

						return false;
					}
				}
			}

			return true;
		}

		$variable = $this->Variables[$pv_params['Variable']] ?? null;

		if($variable === null) {
			return true;
		}

		return preg_match('/[0-9]/', $variable) === 1 && preg_match('/[A-Z]/', $variable) === 1 && preg_match('/[a-z]/', $variable) === 1 && preg_match('/[^0-9A-Za-z]/', $variable) === 1 && isset($variable[7]);
	}

	public function Validate»IsNotPassword(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		if($this->Variables->IsVariableTranslation($pv_params['Variable'])) {
			foreach(\Framework\GetConfig('Locales') as $locale) {
				$variable = $this->Variables[$locale['Id']][$pv_params['Variable']] ?? null;

				if($variable !== null) {
					if(preg_match('/[0-9]/', $variable) === 1 && preg_match('/[A-Z]/', $variable) === 1 && preg_match('/[a-z]/', $variable) === 1 && preg_match('/[^0-9A-Za-z]/', $variable) === 1 && isset($variable[7])) {
						$pv_locale = $locale;

						return false;
					}
				}
			}

			return true;
		}

		$variable = $this->Variables[$pv_params['Variable']] ?? null;

		if($variable === null) {
			return $pv_params['Default'] ?? true;
		}

		return preg_match('/[0-9]/', $variable) === 0 || preg_match('/[A-Z]/', $variable) === 0 || preg_match('/[a-z]/', $variable) === 0 || preg_match('/[^0-9A-Za-z]/', $variable) === 0 || !isset($variable[7]);
	}

	public function Validate»DropDownList»IsValidValue(array $pv_params, int $pv_template = null, array &$pv_locale = null) {
		if($this->Variables->IsVariableTemplate($pv_params['Variable'])) {
			$count = $this->Variables->GetVariableTemplateCount($pv_params['Variable']);

			for($i = 0; $i < $count; ++$i) {
				if(!$this->Owner->TryGetRelController(str_replace('#', $i, $pv_params['Controller']), $controller)) {
					throw new \Framework\RuntimeException(
						'Unable to find controller.',
						['Action' => $this->Id, 'Owner' => $this->Owner->Id, 'Controller' => str_replace('#', $i, $pv_params['Controller'])]
					);
				}
				else if($this->Variables[$pv_params['Variable']][$i] !== null) {
					if(!$controller->ContainsValue($this->Variables[$pv_params['Variable']][$i])) {
						return false;
					}
				}
			}

			return true;
		}
		else if($this->Variables->IsVariableTranslation($pv_params['Variable'])) {
			foreach(\Framework\GetConfig('Locales') as $locale) {
				if(!$this->Owner->TryGetRelController(str_replace('%', $locale['Id'], $pv_params['Controller']), $controller)) {
					throw new \Framework\RuntimeException(
						'Unable to find controller.',
						['Action' => $this->Id, 'Owner' => $this->Owner->Id, 'Controller' => str_replace('%', $locale['Id'], $pv_params['Controller'])]
					);
				}
				else if($this->Variables[$locale['Id']][$pv_params['Variable']] !== null) {
					if(!$controller->ContainsValue($this->Variables[$locale['Id']][$pv_params['Variable']])) {
						return false;
					}
				}
			}

			return true;
		}
		else if(!$this->Owner->TryGetRelController($pv_params['Controller'], $controller)) {
			throw new \Framework\RuntimeException(
				'Unable to find controller.',
				['Action' => $this->Id, 'Owner' => $this->Owner->Id, 'Controller' => $pv_params['Controller']]
			);
		}
		else if($this->Variables[$pv_params['Variable']] !== null) {
			return $controller->ContainsValue($this->Variables[$pv_params['Variable']]);
		}
		else {
			return true;
		}
	}

	public function Validate»DropDownList»IsInvalidValue(array $pv_params, int $pv_template = null, array &$pv_locale = null) {
		if($this->Variables->IsVariableTemplate($pv_params['Variable'])) {
			$count = $this->Variables->GetVariableTemplateCount($pv_params['Variable']);

			for($i = 0; $i < $count; ++$i) {
				if(!$this->Owner->TryGetRelController(str_replace('#', $i, $pv_params['Controller']), $controller)) {
					throw new \Framework\RuntimeException(
						'Unable to find controller.',
						['Action' => $this->Id, 'Owner' => $this->Owner->Id, 'Controller' => str_replace('#', $i, $pv_params['Controller'])]
					);
				}
				else if($this->Variables[$pv_params['Variable']][$i] !== null) {
					if($controller->ContainsValue($this->Variables[$pv_params['Variable']][$i])) {
						return false;
					}
				}
			}

			return true;
		}
		else if($this->Variables->IsVariableTranslation($pv_params['Variable'])) {
			foreach(\Framework\GetConfig('Locales') as $locale) {
				if(!$this->Owner->TryGetRelController(str_replace('%', $locale['Id'], $pv_params['Controller']), $controller)) {
					throw new \Framework\RuntimeException(
						'Unable to find controller.',
						['Action' => $this->Id, 'Owner' => $this->Owner->Id, 'Controller' => str_replace('%', $locale['Id'], $pv_params['Controller'])]
					);
				}
				else if($this->Variables[$locale['Id']][$pv_params['Variable']] !== null) {
					if($controller->ContainsValue($this->Variables[$locale['Id']][$pv_params['Variable']])) {
						return false;
					}
				}
			}

			return true;
		}
		else if(!$this->Owner->TryGetRelController($pv_params['Controller'], $controller)) {
			throw new \Framework\RuntimeException(
				'Unable to find controller.',
				['Action' => $this->Id, 'Owner' => $this->Owner->Id, 'Controller' => $pv_params['Controller']]
			);
		}
		else if($this->Variables[$pv_params['Variable']] !== null) {
			return !$controller->ContainsValue($this->Variables[$pv_params['Variable']]);
		}
		else {
			return true;
		}
	}

	/* * * * */

	public function Validate»IsArray(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		foreach($pv_params as $variable) {
			if($this->Variables[$variable] !== null && !is_array($this->Variables[$variable])) {
				return false;
			}
		}

		return true;
	}

	public function Validate»IsInArray(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		return $this->Variables[$pv_params['Variable']] === null || in_array($this->Variables[$pv_params['Variable']], $pv_params['Values']);
	}

	public function Validate»IsAllDateTime(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		foreach($pv_params['Variables'] as $variable) {
			$dt = \DateTime::createFromFormat($pv_params['Format'], $this->Variables[$variable]);

			if($this->Variables[$variable] === null) {
				return $pv_params['Default'] ?? true;
			}

			if($dt === false || $dt->format($pv_params['Format']) !== $this->Variables[$variable] && $this->Variables[$pv_params['Variable']] !== null) {
				return false;
			}
		}

		return true;
	}

	public function Validate»IsAnyDateTime(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		foreach($pv_params['Variables'] as $variable) {
			$dt = \DateTime::createFromFormat($pv_params['Format'], $this->Variables[$variable]);

			if($this->Variables[$variable] === null) {
				return $pv_params['Default'] ?? true;
			}

			if($dt !== false && $dt->format($pv_params['Format']) === $this->Variables[$variable]) {
				return true;
			}
		}

		return false;
	}

	public function Validate»IsEqualToVariable(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		return $this->Variables[$pv_params['A']] === $this->Variables[$pv_params['B']];
	}

	public function Validate»IsFalse(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		foreach($pv_params as $variable) {
			if(!$this->Variables[$variable]) {
				return true;
			}
		}

		return false;
	}

	public function Validate»IsFormat(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		if($this->Variables[$pv_params['Variable']] === null) {
			return $pv_params['Default'] ?? true;
		}
		else if(is_array($this->Variables[$pv_params['Variable']])) {
			foreach($this->Variables[$pv_params['Variable']] as $value) {
				if(preg_match($pv_params['Format'], $value) !== 1) {
					return false;
				}
			}

			return true;
		}
		else {
			return preg_match($pv_params['Format'], $this->Variables[$pv_params['Variable']]) === 1;
		}
	}

	public function Validate»IsAllFormat(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		foreach($pv_params['Variables'] as $variable) {
			if($this->Variables[$variable] === null) {
				if(!($pv_params['Default'] ?? true)) {
					return false;
				}
			}
			else if(is_array($this->Variables[$variable])) {
				foreach($this->Variables[$variable] as $value) {
					if(preg_match($pv_params['Format'], $value) !== 1) {
						return false;
					}
				}
			}
			else if(preg_match($pv_params['Format'], $this->Variables[$variable]) !== 1) {
				return false;
			}
		}

		return true;
	}

	public function Validate»IsAnyFormat(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		foreach($pv_params['Variables'] as $variable) {
			if($this->Variables[$variable] === null) {
				if($pv_params['Default'] ?? true) {
					return true;
				}
			}
			else if(is_array($this->Variables[$variable])) {
				foreach($this->Variables[$variable] as $value) {
					if(preg_match($pv_params['Format'], $value) === 1) {
						return true;
					}
				}
			}
			else if(preg_match($pv_params['Format'], $this->Variables[$variable]) === 1) {
				return true;
			}
		}

		return false;
	}

	public function Validate»IsInt(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		foreach($pv_params as $variable) {
			if($this->Variables[$variable] === null || preg_match('/^(?:-?[0-9]+)?$/', $this->Variables[$variable]) === 1) {
				return true;
			}
		}

		return false;
	}

	public function Validate»IsNotInt(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		foreach($pv_params as $variable) {
			if($this->Variables[$variable] === null || !preg_match('/^(?:-?[0-9]+)?$/', $this->Variables[$variable]) === 1) {
				return true;
			}
		}

		return false;
	}

	public function Validate»IsUInt(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		foreach($pv_params as $variable) {
			if($this->Variables[$variable] === null || preg_match('/^(?:[0-9]+)?$/', $this->Variables[$variable]) === 1) {
				return true;
			}
		}

		return false;
	}

	public function Validate»IsNotUInt(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		foreach($pv_params as $variable) {
			if($this->Variables[$variable] === null || !preg_match('/^(?:[0-9]+)?$/', $this->Variables[$variable]) === 1) {
				return true;
			}
		}

		return false;
	}

	public function Validate»IsAllInt(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		foreach($pv_params['Variables'] as $variable) {
			if($this->Variables[$variable] !== null && preg_match('/^(?:-?[0-9]+)?$/', $this->Variables[$variable]) === 0) {
				return false;
			}
		}

		return true;
	}

	public function Validate»IsAllUInt(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		foreach($pv_params['Variables'] as $variable) {
			if($this->Variables[$variable] !== null && preg_match('/^(?:[0-9]+)?$/', $this->Variables[$variable]) === 0) {
				return false;
			}
		}

		return true;
	}

	public function Validate»IsFloat(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		foreach($pv_params as $variable) {
			if($this->Variables[$variable] === null || preg_match('/^(?:-?[0-9]+(?:(?:\.|,)[0-9]+)?)?$/', $this->Variables[$variable]) === 1) {
				return true;
			}
		}

		return false;
	}

	public function Validate»IsNotFloat(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		foreach($pv_params as $variable) {
			if($this->Variables[$variable] === null || !preg_match('/^(?:-?[0-9]+(?:(?:\.|,)[0-9]+)?)?$/', $this->Variables[$variable]) === 1) {
				return true;
			}
		}

		return false;
	}

	public function Validate»IsNotArray(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		foreach($pv_params as $variable) {
			if(is_array($this->Variables[$variable])) {
				return false;
			}
		}

		return true;
	}

	public function Validate»IsNotInArray(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		return $this->Variables[$pv_params['Variable']] === null || !in_array($this->Variables[$pv_params['Variable']], $pv_params['Values']);
	}

	public function Validate»IsAllNotEmpty(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		foreach($pv_params['Variables'] as $variable) {
			if($this->Variables->IsVariableTranslation($variable)) {
				foreach(\Framework\GetConfig('Locales') as $locale) {
					if(isset($this->Variables[$locale['Id']][$variable])) {
						if(is_array($this->Variables[$locale['Id']][$variable])) {
							if(count($this->Variables[$locale['Id']][$variable]) === 0) {
								return false;
							}
						}
						else if(is_string($this->Variables[$locale['Id']][$variable])) {
							if(!isset($this->Variables[$locale['Id']][$variable][0])) {
								return false;
							}
						}
					}
				}
			}
			else if(isset($this->Variables[$variable])) {
				if(is_array($this->Variables[$variable])) {
					if(count($this->Variables[$variable]) === 0) {
						return false;
					}
				}
				else if(is_string($this->Variables[$variable])) {
					if(!isset($this->Variables[$variable][0])) {
						return false;
					}
				}
			}
		}

		return true;
	}

	public function Validate»IsAnyNotEmpty(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		if(count($pv_params['Variables']) === 0) {
			return true;
		}

		foreach($pv_params['Variables'] as $variable) {
			if($this->Variables->IsVariableTranslation($variable)) {
				foreach(\Framework\GetConfig('Locales') as $locale) {
					if(isset($this->Variables[$locale['Id']][$variable])) {
						if(is_array($this->Variables[$locale['Id']][$variable])) {
							if(0 < count($this->Variables[$locale['Id']][$variable])) {
								return true;
							}
						}
						else if(is_string($this->Variables[$locale['Id']][$variable])) {
							if(isset($this->Variables[$locale['Id']][$variable][0])) {
								return true;
							}
						}
						else {
							return true;
						}
					}
				}
			}
			else if(isset($this->Variables[$variable])) {
				if(is_array($this->Variables[$variable])) {
					if(0 < count($this->Variables[$variable])) {
						return true;
					}
				}
				else if(is_string($this->Variables[$variable])) {
					if(isset($this->Variables[$variable][0])) {
						return true;
					}
				}
				else {
					return true;
				}
			}
		}

		return false;
	}

	public function Validate»IsNotEqualToVariable(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		return $this->Variables[$pv_params['A']] !== $this->Variables[$pv_params['B']];
	}

	public function Validate»IsNotNull(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		if($this->Variables->IsVariableTranslation($pv_params['Variable'])) {
			foreach(\Framework\GetConfig('Locales') as $locale) {
				$variable = $this->Variables[$locale['Id']][$pv_params['Variable']] ?? null;

				if($variable === null) {
					$pv_locale = $locale;

					return false;
				}
			}

			return true;
		}

		if($this->Variables[$pv_params['Variable']] === null) {
			return false;
		}

		return true;
	}

	public function Validate»IsNotSet(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		foreach($pv_params as $variable) {
			if(!isset($this->Variables[$variable])) {
				return true;
			}
		}

		return false;
	}

	public function Validate»IsAllNotSet(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		foreach($pv_params['Variables'] as $variable) {
			if(isset($this->Variables[$variable])) {
				return false;
			}
		}

		return true;
	}

	public function Validate»IsNull(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		if($this->Variables->IsVariableTranslation($pv_params['Variable'])) {
			foreach(\Framework\GetConfig('Locales') as $locale) {
				$variable = $this->Variables[$locale['Id']][$pv_params['Variable']] ?? null;

				if($variable !== null) {
					$pv_locale = $locale;

					return false;
				}
			}

			return true;
		}

		if($this->Variables[$pv_params['Variable']] !== null) {
			return false;
		}

		return true;
	}

	public function Validate»IsSet(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		foreach($pv_params as $variable) {
			if(isset($this->Variables[$variable])) {
				return true;
			}
		}

		return false;
	}

	public function Validate»IsAllSet(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		foreach($pv_params['Variables'] as $variable) {
			if($this->Variables->IsVariableTemplate($variable)) {
				$count = $this->Variables->GetVariableTemplateCount($variable);

				for($i = 0; $i < $count; ++$i) {
					if(!isset($this->Variables[$variable][$i])) {
						return false;
					}
				}
			}
			else if($this->Variables->IsVariableTranslation($variable)) {
				foreach(\Framework\GetConfig('Locales') as $locale) {
					if(!isset($this->Variables[$locale['Id']][$variable])) {
						return false;
					}
				}
			}
			else if(!isset($this->Variables[$variable])) {
				return false;
			}
		}

		return true;
	}

	public function Validate»IsTrue(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		foreach($pv_params as $variable) {
			if($this->Variables[$variable]) {
				return true;
			}
		}

		return false;
	}

	public function Validate»IsValidUrl(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		$flags = 0;

		if(isset($pv_params['ReqPath']) && $pv_params['ReqPath'] === true) {
			$flags |= FILTER_FLAG_PATH_REQUIRED;
		}

		if(isset($pv_params['ReqQuery']) && $pv_params['ReqQuery'] === true) {
			$flags |= FILTER_FLAG_QUERY_REQUIRED;
		}

		return $this->Variables[$pv_params['Variable']] === null || filter_var($this->Variables[$pv_params['Variable']], FILTER_VALIDATE_URL, $flags) !== false;
	}

	public function Validate»IsAllValidUrl(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		foreach($this->Variables[$pv_params['Variable']] as $variable) {
			$flags = 0;

			if(isset($pv_params['ReqPath']) && $pv_params['ReqPath'] === true) {
				$flags |= FILTER_FLAG_PATH_REQUIRED;
			}

			if(isset($pv_params['ReqQuery']) && $pv_params['ReqQuery'] === true) {
				$flags |= FILTER_FLAG_QUERY_REQUIRED;
			}

			if($variable === null || filter_var($variable, FILTER_VALIDATE_URL, $flags) !== false) {
				continue;
			}

			return false;
		}

		return true;
	}

	public function Validate»IsNotValidUrl(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		$flags = 0;

		if(isset($pv_params['ReqPath']) && $pv_params['ReqPath'] === true) {
			$flags |= FILTER_FLAG_PATH_REQUIRED;
		}

		if(isset($pv_params['ReqQuery']) && $pv_params['ReqQuery'] === true) {
			$flags |= FILTER_FLAG_QUERY_REQUIRED;
		}

		return $this->Variables[$pv_params['Variable']] === null || filter_var($this->Variables[$pv_params['Variable']], FILTER_VALIDATE_URL, $flags) === false;
	}

	public function Validate»IsValidRecaptcha(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		$handle = curl_init('https://www.google.com/recaptcha/api/siteverify?secret='.urlencode($pv_params['Key']).'&response='.urlencode($this->Variables[$pv_params['Variable']]).'&remoteip='.urlencode($_SERVER['REMOTE_ADDR']));

		curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);

		$response = curl_exec($handle);

		if($response === false) {
			return false;
		}

		curl_close($handle);

		$response = json_decode($response, true);

		return $response['success'];
	}

	public function Validate»IsNotValidRecaptcha(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		$handle = curl_init('https://www.google.com/recaptcha/api/siteverify?secret='.urlencode($pv_params['Key']).'&response='.urlencode($this->Variables[$pv_params['Variable']]).'&remoteip='.urlencode($_SERVER['REMOTE_ADDR']));

		curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);

		$response = curl_exec($handle);

		if($response === false) {
			return false;
		}

		curl_close($handle);

		$response = json_decode($response, true);

		return !$response['success'];
	}

	public function Validate»IsEqualTo(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		return $this->Variables[$pv_params['Variable']] === null || $this->Variables[$pv_params['Variable']] === $pv_params['Value'];
	}

	public function Validate»IsGreaterThan(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		return $this->Variables[$pv_params['Variable']] === null || $pv_params['Value'] < $this->Variables[$pv_params['Variable']];
	}

	public function Validate»IsLessThan(array $pv_params, int $pv_template = null, array &$pv_locale = null) : bool {
		return $this->Variables[$pv_params['Variable']] === null || $this->Variables[$pv_params['Variable']] < $pv_params['Value'];
	}

	/* Instance formatting methods */

	public function Format»Explode(array $pv_params) {
		$value = &$this->Variables[$pv_params['Variable']];

		$value = isset($value[0]) ? explode($pv_params['Separator'] ?? ',', $value) : $pv_params['Default'];
	}

	public function Format»MakeBool(array $pv_params) {
		if(is_array($this->Variables[$pv_params['Variable']])) {
			$values = &$this->Variables[$pv_params['Variable']];

			foreach($values as &$value) {
				$value = isset($value[0]) ? boolval($value) : $pv_params['Default'];
			}
		}
		else {
			$value = &$this->Variables[$pv_params['Variable']];

			$value = isset($value[0]) ? boolval($value) : $pv_params['Default'];
		}
	}

	public function Format»MakeAllBool(array $pv_params) {
		foreach($pv_params['Variables'] as $variable) {
			if(is_array($this->Variables[$variable])) {
				$values = &$this->Variables[$variable];

				foreach($values as &$value) {
					$value = isset($value[0]) ? boolval($value) : $pv_params['Default'];
				}
			}
			else {
				$value = &$this->Variables[$variable];

				$value = isset($value[0]) ? boolval($value) : $pv_params['Default'];
			}
		}
	}

	public function Format»MakeFloat(array $pv_params) {
		$variable = $pv_params['Variable'];

		if(is_array($this->Variables[$variable])) {
			$values = &$this->Variables[$variable];

			foreach($values as &$value) {
				$value = isset($value[0]) ? floatval($value) : $pv_params['Default'];
			}
		}
		else {
			$value = &$this->Variables[$variable];

			$value = isset($value[0]) ? floatval($value) : $pv_params['Default'];
		}
	}

	public function Format»MakeAllFloat(array $pv_params) {
		foreach($pv_params['Variables'] as $variable) {
			if(is_array($this->Variables[$variable])) {
				$values = &$this->Variables[$variable];

				foreach($values as &$value) {
					$value = isset($value[0]) ? floatval($value) : $pv_params['Default'];
				}
			}
			else {
				$value = &$this->Variables[$variable];

				$value = isset($value[0]) ? floatval($value) : $pv_params['Default'];
			}
		}
	}

	public function Format»MakeInt(array $pv_params) {
		$variable = $pv_params['Variable'];

		if(is_array($this->Variables[$variable])) {
			$values = &$this->Variables[$variable];

			foreach($values as &$value) {
				$value = isset($value[0]) ? intval($value) : $pv_params['Default'];
			}
		}
		else {
			$value = &$this->Variables[$variable];

			$value = isset($value[0]) ? intval($value) : $pv_params['Default'];
		}
	}

	public function Format»MakeAllInt(array $pv_params) {
		foreach($pv_params['Variables'] as $variable) {
			if(is_array($this->Variables[$variable])) {
				$values = &$this->Variables[$variable];

				foreach($values as &$value) {
					$value = isset($value[0]) ? intval($value) : $pv_params['Default'];
				}
			}
			else {
				$value = &$this->Variables[$variable];

				$value = isset($value[0]) ? intval($value) : $pv_params['Default'];
			}
		}
	}

	public function Format»MakeUnixTimestamp(array $pv_params) {
		$variable = $pv_params['Variable'];

		if($this->Variables->IsVariableTranslation($variable)) {
			foreach(\Framework\GetConfig('Locales') as $locale) {
				if(is_array($this->Variables[$locale['Id']][$variable])) {
					foreach($this->Variables[$locale['Id']][$variable] as $key => $value) {
						$dt = \DateTime::createFromFormat('!'.$pv_params['Format'], $value);

						if($dt === false || $dt->format($pv_params['Format']) !== $value) {
							$this->Variables[$locale['Id']][$variable][$key] = $pv_params['Default'];
						}
						else {
							$this->Variables[$locale['Id']][$variable][$key] = $dt->getTimestamp();
						}
					}
				}
				else {
					$dt = \DateTime::createFromFormat('!'.$pv_params['Format'], $this->Variables[$locale['Id']][$variable]);

					if($dt === false || $dt->format($pv_params['Format']) !== $this->Variables[$locale['Id']][$variable]) {
						$this->Variables[$locale['Id']][$variable] = $pv_params['Default'];
					}
					else {
						$this->Variables[$locale['Id']][$variable] = $dt->getTimestamp();
					}
				}
			}
		}
		else if(is_array($this->Variables[$variable])) {
			foreach($this->Variables[$variable] as $key => $value) {
				$dt = \DateTime::createFromFormat('!'.$pv_params['Format'], $value);

				if($dt === false || $dt->format($pv_params['Format']) !== $value) {
					$this->Variables[$variable][$key] = $pv_params['Default'];
				}
				else {
					$this->Variables[$variable][$key] = $dt->getTimestamp();
				}
			}
		}
		else {
			$dt = \DateTime::createFromFormat('!'.$pv_params['Format'], $this->Variables[$variable]);

			if($dt === false || $dt->format($pv_params['Format']) !== $this->Variables[$variable]) {
				$this->Variables[$variable] = $pv_params['Default'];
			}
			else {
				$this->Variables[$variable] = $dt->getTimestamp();
			}
		}
	}

	public function Format»MakeAllUnixTimestamp(array $pv_params) {
		foreach($pv_params['Variables'] as $variable) {
			if($this->Variables->IsVariableTranslation($variable)) {
				foreach(\Framework\GetConfig('Locales') as $locale) {
					if(is_array($this->Variables[$locale['Id']][$variable])) {
						foreach($this->Variables[$locale['Id']][$variable] as $key => $value) {
							$dt = \DateTime::createFromFormat('!'.$pv_params['Format'], $value);

							if($dt === false || $dt->format($pv_params['Format']) !== $value) {
								$this->Variables[$locale['Id']][$variable][$key] = $pv_params['Default'];
							}
							else {
								$this->Variables[$locale['Id']][$variable][$key] = $dt->getTimestamp();
							}
						}
					}
					else {
						$dt = \DateTime::createFromFormat('!'.$pv_params['Format'], $this->Variables[$locale['Id']][$variable]);

						if($dt === false || $dt->format($pv_params['Format']) !== $this->Variables[$locale['Id']][$variable]) {
							$this->Variables[$locale['Id']][$variable] = $pv_params['Default'];
						}
						else {
							$this->Variables[$locale['Id']][$variable] = $dt->getTimestamp();
						}
					}
				}
			}
			else if(is_array($this->Variables[$variable])) {
				foreach($this->Variables[$variable] as $key => $value) {
					$dt = \DateTime::createFromFormat('!'.$pv_params['Format'], $value);

					if($dt === false || $dt->format($pv_params['Format']) !== $value) {
						$this->Variables[$variable][$key] = $pv_params['Default'];
					}
					else {
						$this->Variables[$variable][$key] = $dt->getTimestamp();
					}
				}
			}
			else {
				$dt = \DateTime::createFromFormat('!'.$pv_params['Format'], $this->Variables[$variable]);

				if($dt === false || $dt->format($pv_params['Format']) !== $this->Variables[$variable]) {
					$this->Variables[$variable] = $pv_params['Default'];
				}
				else {
					$this->Variables[$variable] = $dt->getTimestamp();
				}
			}
		}
	}

	public function Format»SetEmptyToNull(array $pv_params) {
		if($this->Variables->IsVariableTranslation($pv_params['Variable'])) {
			foreach(\Framework\GetConfig('Locales') as $locale) {
				if(is_array($this->Variables[$locale['Id']][$pv_params['Variable']])) {
					foreach($this->Variables[$locale['Id']][$pv_params['Variable']] as &$value) {
						if(!isset($value[0])) {
							$value = null;
						}
					}
				}
				else if(!isset($this->Variables[$locale['Id']][$pv_params['Variable']][0])) {
					$this->Variables[$locale['Id']][$pv_params['Variable']] = null;
				}
			}
		}
		else if(is_array($this->Variables[$pv_params['Variable']])) {
			foreach($this->Variables[$pv_params['Variable']] as &$value) {
				if(!isset($value[0])) {
					$value = null;
				}
			}
		}
		else if(!isset($this->Variables[$pv_params['Variable']][0])) {
			$this->Variables[$pv_params['Variable']] = null;
		}
	}

	public function Format»SetAllEmptyToNull(array $pv_params) {
		foreach($pv_params['Variables'] as $variable) {
			if($this->Variables->IsVariableTranslation($variable)) {
				foreach(\Framework\GetConfig('Locales') as $locale) {
					if(is_array($this->Variables[$locale['Id']][$variable])) {
						foreach($this->Variables[$locale['Id']][$variable] as &$value) {
							if(!isset($value[0])) {
								$value = null;
							}
						}
					}
					else if(!isset($this->Variables[$locale['Id']][$variable][0])) {
						$this->Variables[$locale['Id']][$variable] = null;
					}
				}
			}
			else if(is_array($this->Variables[$variable])) {
				foreach($this->Variables[$variable] as &$value) {
					if(!isset($value[0])) {
						$value = null;
					}
				}
			}
			else if(!isset($this->Variables[$variable][0])) {
				$this->Variables[$variable] = null;
			}
		}
	}

	public function Format»Split(array $pv_params) {
		$value = &$this->Variables[$pv_params['Variable']];

		$value = explode($pv_params['Separator'], $value);
	}

	public function Format»MakeUnique(array $pv_params) {
		$value = &$this->Variables[$pv_params['Variable']];

		$value = array_unique($value);
	}

	public function Format»Trim(array $pv_params) {
		if(isset($pv_params['Mask'])) {
			if(is_array($this->Variables[$pv_params['Variable']])) {
				$values = &$this->Variables[$pv_params['Variable']];

				foreach($values as &$value) {
					$value = trim($value, $pv_params['Mask']);
				}
			}
			else {
				$value = &$this->Variables[$pv_params['Variable']];

				$value = trim($value, $pv_params['Mask']);
			}
		}
		else {
			if(is_array($this->Variables[$pv_params['Variable']])) {
				$values = &$this->Variables[$pv_params['Variable']];

				foreach($values as &$value) {
					$value = trim($value);
				}
			}
			else {
				$value = &$this->Variables[$pv_params['Variable']];

				$value = trim($value);
			}
		}
	}

	public function Format»TrimAll(array $pv_params) {
		if(isset($pv_params['Mask'])) {
			foreach($pv_params['Variables'] as $variable) {
				if($this->Variables->IsVariableTranslation($variable)) {
					foreach(\Framework\GetConfig('Locales') as $locale) {
						if(is_array($this->Variables[$locale['Id']][$variable])) {
							foreach($this->Variables[$locale['Id']][$variable] as $key => $value) {
								if(is_string($this->Variables[$locale['Id']][$variable][$key])) {
									$this->Variables[$locale['Id']][$variable][$key] = trim($value, $pv_params['Mask']);
								}
							}
						}
						else if(is_string($this->Variables[$locale['Id']][$variable])) {
							$this->Variables[$locale['Id']][$variable] = trim($this->Variables[$locale['Id']][$variable], $pv_params['Mask']);
						}
					}
				}
				else if(is_array($this->Variables[$variable])) {
					foreach($this->Variables[$variable] as $key => $value) {
						if(is_string($this->Variables[$variable][$key])) {
							$this->Variables[$variable][$key] = trim($value, $pv_params['Mask']);
						}
					}
				}
				else if(is_string($this->Variables[$variable])) {
					$this->Variables[$variable] = trim($this->Variables[$variable], $pv_params['Mask']);
				}
			}
		}
		else {
			foreach($pv_params['Variables'] as $variable) {
				if($this->Variables->IsVariableTranslation($variable)) {
					foreach(\Framework\GetConfig('Locales') as $locale) {
						if(is_array($this->Variables[$locale['Id']][$variable])) {
							foreach($this->Variables[$locale['Id']][$variable] as $key => $value) {
								if(is_string($this->Variables[$locale['Id']][$variable][$key])) {
									$this->Variables[$locale['Id']][$variable][$key] = trim($value);
								}
							}
						}
						else if(is_string($this->Variables[$locale['Id']][$variable])) {
							$this->Variables[$locale['Id']][$variable] = trim($this->Variables[$locale['Id']][$variable]);
						}
					}
				}
				else if(is_array($this->Variables[$variable])) {
					foreach($this->Variables[$variable] as $key => $value) {
						if(is_string($this->Variables[$variable][$key])) {
							$this->Variables[$variable][$key] = trim($value);
						}
					}
				}
				else if(is_string($this->Variables[$variable])) {
					$this->Variables[$variable] = trim($this->Variables[$variable]);
				}
			}
		}
	}

	public function Format»ToLower(array $pv_params) {
		$value = &$this->Variables[$pv_params['Variable']];

		$value = isset($pv_params['Encoding']) ? mb_strtolower($value, $pv_params['Encoding']) : mb_strtolower($value);
	}

	public function Format»ToUpper(array $pv_params) {
		$value = &$this->Variables[$pv_params['Variable']];

		$value = isset($pv_params['Encoding']) ? mb_strtoupper($value, $pv_params['Encoding']) : mb_strtoupper($value);
	}

	/* Instance error message generation methods */

	public function ErrorMessage»IsNull(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotNull"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotNull', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsNotNull(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNull"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNull', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsUploaded(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotUploaded"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotUploaded', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsNotUploaded(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsUploaded"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsUploaded', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsChecked(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotChecked"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotChecked', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsNotChecked(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsChecked"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsChecked', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsDateTime(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotDateTime"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotDateTime', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsNotDateTime(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsDateTime"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsDateTime', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsEmailAddress(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotEmailAddress"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotEmailAddress', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsNotEmailAddress(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsEmailAddress"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsEmailAddress', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsEmpty(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotEmpty"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotEmpty', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsNotEmpty(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsEmpty"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsEmpty', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsFloat(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotFloat"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotFloat', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsNotFloat(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsFloat"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsFloat', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsFormat(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotFormat"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotFormat', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsNotFormat(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsFormat"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsFormat', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsInt(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotInt"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotInt', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsNotInt(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsInt"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsInt', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsUInt(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotUInt"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotUInt', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsNotUInt(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsUInt"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsUInt', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsLocale(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotLocale"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotLocale', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsNotLocale(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsLocale"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsLocale', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsPassword(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotPassword"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotPassword', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsNotPassword(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsPassword"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsPassword', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsValidUrl(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotValidUrl"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsNotValidUrl', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}

	public function ErrorMessage»IsNotValidUrl(array $pv_params, array $pv_locale = null) : string {
		if($pv_locale === null) {
			return '%["Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsValidUrl"]%';
		}
		else {
			return '%'.json_encode([['Actions.'.$this->Id.'.'.$pv_params['Variable'].'IsValidUrl', $pv_locale['Id'], $pv_locale['Name']]]).'%';
		}
	}
}
?>