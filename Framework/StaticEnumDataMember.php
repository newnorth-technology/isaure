<?
namespace Framework;

class StaticEnumDataMember extends \Framework\AStaticDataMember {
	/* Static methods */

	public static function ParseFromDbValue(\Framework\StaticEnumDataMember $DataMember = null, $Value) {
		/*{IF:DEBUG}*/

		if($Value === null) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, value may not be null.',
				['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name]
			);
		}

		if(!is_string($Value)) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, invalid value type.',
				['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name, 'Value' => $Value]
			);
		}

		if(!in_array($Value, $DataMember->Values, true)) {
			throw new RuntimeException(
				'Failed to parse data member value, invalid value.',
				['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name, 'Value' => $Value, 'Valid values' => $DataMember->Values]
			);
		}

		/*{ENDIF}*/

		return $Value;
	}

	public static function ParseValue(\Framework\StaticEnumDataMember $DataMember = null, $Value) {
		/*{IF:DEBUG}*/

		if($Value === null) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, value may not be null.',
				['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name]
			);
		}

		if(!is_string($Value)) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, invalid value type.',
				['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name, 'Value' => $Value]
			);
		}

		if(!in_array($Value, $DataMember->Values, true)) {
			throw new RuntimeException(
				'Failed to parse data member value, invalid value.',
				['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name, 'Value' => $Value, 'Valid values' => $DataMember->Values]
			);
		}

		/*{ENDIF}*/

		return $Value;
	}

	public static function ParseToDbValue(\Framework\StaticEnumDataMember $DataMember = null, $Value) {
		return $Value;
	}

	/* Instance variables */

	public $Values = [];

	/* Magic methods */

	public function __construct(\Framework\ADataManager $DataManager, string $Name, array $Params) {
		if(isset($Params['Values'])) {
			$this->Values = $Params['Values'];

			/*{IF:DEBUG}*/

			unset($Params['Values']);

			/*{ENDIF}*/
		}

		parent::__construct($DataManager, $Name, $Params);
	}
}
?>