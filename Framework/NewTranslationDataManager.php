<?
namespace Framework;

class NewTranslationDataManager extends \Framework\ADataManager {
	/* Magic methods */

	public function __construct(string $Id, string $Name, array $DataType, array $Params) {
		$this->PrimaryKey = 'id';

		parent::__construct($Id, $Name, $DataType, $Params);
	}

	/* Instance methods */

	public function Initialize() {

	}

	public function Initialize»DataMembers() {
		$this->DataMembers['id'] = [
			'Type' => 'StaticInt',
			'UseLogInsert' => true,
			'UseLogUpdate' => false,
			'UseLogDelete' => true
		];

		$this->DataMembers['parent_id'] = [
			'Type' => 'StaticInt',
			'UseLogInsert' => true,
			'UseLogUpdate' => false,
			'UseLogDelete' => false
		];

		$this->DataMembers['locale'] = [
			'Type' => 'StaticString',
			'UseLogInsert' => true,
			'UseLogUpdate' => false,
			'UseLogDelete' => false
		];

		parent::Initialize»DataMembers();
	}

	public function Initialize»References() {
		parent::Initialize»References();
	}

	public function Initialize»InsertQuery(array $Params = null) {
		$this->InsertQuery = new \Framework\Database\InsertQuery([
			'Target' => new \Framework\Database\Target([
				'DataManager' => $this
			])
		]);
	}

	public function Initialize»UpdateQuery(array $Params = null) {
		$this->UpdateQuery = new \Framework\Database\UpdateQuery([
			'Sources' => new \Framework\Database\Sources([
				$this->Id => new \Framework\Database\Source([
					'DataManager' => $this,
					'Alias' => $this->Id
				])
			])
		]);
	}

	public function Initialize»SelectQuery(array $Params = null) {
		$this->SelectQuery = new \Framework\Database\SelectQuery([
			'Columns' => $this->DataMembers,
			'Sources' => new \Framework\Database\Sources([
				$this->Id => new \Framework\Database\Source([
					'DataManager' => $this,
					'Alias' => $this->Id
				])
			])
		]);
	}

	/* Instance find methods */

	public function FindById(int $Id, array $Params = null) {
		return $this->Find(
			[
				'Condition' => new \Framework\Database\Conditions\IsEqualTo([
					'ColumnA' => $this->DataMembers['id'],
					'ColumnB' => $Id
				])
			],
			$Params
		);
	}

	public function TryFindById(&$Item, int $Id, array $Params = null) : bool {
		$Item = $this->Find(
			[
				'Condition' => new \Framework\Database\Conditions\IsEqualTo([
					'ColumnA' => $this->DataMembers['id'],
					'ColumnB' => $Id
				])
			],
			$Params
		);

		return $Item !== null;
	}

	public function FindByLocale(int $ParentId, string $Locale, array $Params = null) {
		return $this->Find(
			[
				'Condition' => new \Framework\Database\Conditions\All([
					'Columns' => [
						new \Framework\Database\Conditions\IsEqualTo([
							'ColumnA' => $this->DataMembers['parent_id'],
							'ColumnB' => $ParentId
						]),
						new \Framework\Database\Conditions\IsEqualTo([
							'ColumnA' => $this->DataMembers['locale'],
							'ColumnB' => $Locale
						])
					]
				])
			],
			$Params
		);
	}

	public function TryFindByLocale(&$Item, int $ParentId, string $Locale, array $Params = null) : bool {
		$Item = $this->Find(
			[
				'Condition' => new \Framework\Database\Conditions\All([
					'Columns' => [
						new \Framework\Database\Conditions\IsEqualTo([
							'ColumnA' => $this->DataMembers['parent_id'],
							'ColumnB' => $ParentId
						]),
						new \Framework\Database\Conditions\IsEqualTo([
							'ColumnA' => $this->DataMembers['locale'],
							'ColumnB' => $Locale
						])
					]
				])
			],
			$Params
		);

		return $Item !== null;
	}

	/* Instance contains methods */

	public function ContainsById(int $Id, array $Params = null) : bool {
		return $this->Contains(
			[
				'Condition' => new \Framework\Database\Conditions\IsEqualTo([
					'ColumnA' => $this->DataMembers['id'],
					'ColumnB' => $Id
				])
			],
			$Params
		);
	}

	public function ContainsByLocale(int $ParentId, string $Locale, array $Params = null) : bool {
		return $this->Contains(
			[
				'Condition' => new \Framework\Database\Conditions\All([
					'Columns' => [
						new \Framework\Database\Conditions\IsEqualTo([
							'ColumnA' => $this->DataMembers['parent_id'],
							'ColumnB' => $ParentId
						]),
						new \Framework\Database\Conditions\IsEqualTo([
							'ColumnA' => $this->DataMembers['locale'],
							'ColumnB' => $Locale
						])
					]
				])
			],
			$Params
		);
	}
}
?>