<?
namespace Framework\Controllers;

class JsonAction extends \Framework\Controllers\AJson {
	/* Instance life cycle methods */

	public function Execute() {
		parent::Execute();

		$wasSuccessful = null;

		$this->Model->SuccessMessages = [];

		$this->Model->ErrorMessages = [];

		foreach($this->Actions->Items as $action) {
			if($action->WasSuccessful === true) {
				$wasSuccessful = $this->Model->WasSuccessful ?? true;

				$this->Model->SuccessMessages = array_merge($this->Model->SuccessMessages, $action->Messages);
			}
			else if($action->WasSuccessful === false) {
				$wasSuccessful = false;

				$this->Model->ErrorMessages = array_merge($this->Model->ErrorMessages, $action->Messages);
			}
		}

		if($wasSuccessful === true) {
			http_response_code(200);
		}
		else if($wasSuccessful === false) {
			http_response_code(400);
		}
		else {
			http_response_code(404);
		}
	}
}
?>