<?
namespace Framework\Views;

class Json extends \Framework\AView {
	/* Instance variables */

	public $ContentType = 'application/json';

	/* Magic methods */

	public function __construct(array $Params) {

	}

	/* Instance methods */

	public function Render(\Framework\Model $Model, bool $Return, array $Params = []) {
		$output = get_object_vars($Model);

		$output = $this->Render»Translate($output, $Params['Locale'] ?? null);

		$output = json_encode($output);

		if($Return) {
			return $output;
		}
		else {
			echo $output;
		}
	}

	private function Render»Translate(array $Input, string $Locale = null) {
		$output = [];

		foreach($Input as $key => $value) {
			if(is_string($value)) {
				$output[$key] = \Framework\Translator::Translate($value, $this->Translations, $Locale);
			}
			else if(is_array($value)) {
				$output[$key] = $this->Render»Translate($value, $Locale);
			}
			else {
				$output[$key] = $value;
			}
		}

		return $output;
	}
}
?>