<?
namespace Framework\Views;

class ZipArchive extends \Framework\AView {
	/* Instance variables */

	public $ContentType = 'application/zip';

	/* Magic methods */

	public function __construct(array $Params) {

	}

	/* Instance methods */

	public function Render(\Framework\Model $Model = null, bool $Return, array $Params = []) {
		$path = tempnam(null, 'tiles.'.$_SERVER['REQUEST_TIME'].'.zip');

		$archive = new \ZipArchive();

		$archive->open($path, \ZipArchive::CREATE);

		foreach($Model->Items as $itemPath => $itemData) {
			$archive->addFromString($itemPath, $itemData);
		}

		$archive->close();

		$output = file_get_contents($path);

		if($Return) {
			return $output;
		}
		else {
			echo $output;
		}
	}
}
?>