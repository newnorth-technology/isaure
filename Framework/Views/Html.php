<?
namespace Framework\Views;

class Html extends \Framework\AView {
	/* Static methods */

	private static function Render»File(string $Directory, string $File, \Framework\Model $Model = null) {
		global $Locale, $Route;

		include($Directory.$File);
	}

	/* Instance variables */

	public $ContentType = 'text/html';

	public $Directory;

	public $File;

	/* Magic methods */

	public function __construct(array $Params) {
		$this->Directory = $Params['Directory'];

		$this->File = $Params['File'].'.phtml';
	}

	/* Instance methods */

	public function Render(\Framework\Model $Model = null, bool $Return, array $Params = []) {
		ob_start();

		self::Render»File($this->Directory, $this->File, $Model);

		$output = ob_get_clean();

		$output = $this->Render»Translate($output, $Params['Locale'] ?? null);

		if(\Framework\GetConfig('System.MinimizeOutput', false)) {
			$output = $this->Render»Minimize($output);
		}

		if($Return) {
			return $output;
		}
		else {
			echo $output;
		}
	}

	private function Render»Translate(string $Output, string $Locale = null) : string {
		return \Framework\Translator::Translate($Output, $this->Translations, $Locale);
	}

	private function Render»Minimize(string $Output) : string {
		return preg_replace(
			['/[^\S ]+/', '/(?: )+/'],
			['', ' '],
			$Output
		);
	}
}
?>