<?
namespace Framework\Views;

class ImagePng extends \Framework\AView {
	/* Instance variables */

	public $ContentType = 'image/png';

	/* Magic methods */

	public function __construct(array $Params) {

	}

	/* Instance methods */

	public function Render(\Framework\Model $Model = null, bool $Return, array $Params = []) {
		ob_start();

		imagepng(
			$Model->Image->Source,
			null,
			$Model->Quality ?? 0,
			$Model->Filters ?? PNG_NO_FILTER
		);

		if($Return) {
			return ob_get_clean();
		}
		else {
			echo $output;
		}
	}
}
?>