<?
namespace Framework;

class Benchmark {
	/* Static variables */

	private static $Items = [];

	private static $Process = null;

	/* Static methods */

	public static function StartProcess(array $Data = []) {
		if(isset($_GET['__BENCHMARK__'])) {
			if(self::$Process === null) {
				self::$Items[] = self::$Process = new \Framework\Benchmark\Process(null, $Data);
			}
			else {
				self::$Process = new \Framework\Benchmark\Process(self::$Process, $Data);
			}
		}
	}

	public static function LogData(array $Data = []) {
		if(isset($_GET['__BENCHMARK__'])) {
			if(self::$Process === null) {
				self::$Items[] = new \Framework\Benchmark\Data(null, $Data);
			}
			else {
				new \Framework\Benchmark\Data(self::$Process, $Data);
			}
		}
	}

	public static function FinishProcess() {
		if(isset($_GET['__BENCHMARK__'])) {
			self::$Process->Finish();

			self::$Process = self::$Process->Parent;
		}
	}

	public static function Print() {
		echo '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head><body>';

		echo '<table style="width:100%;"><tr style="background-color:#808080;"><td style="width:100px;">Key</td><td style="width:400px;">Backtrace</td><td>Data</td><td style="width:100px;">Delta time</td><td style="width:100px;">Time</td></tr>';

		foreach(self::$Items as $i => $process) {
			$process->Print(($i + 1).'.');
		}

		echo '</table></body></html>';
	}
}
?>