<?
namespace Framework;

abstract class AStaticNullableDataMember extends \Framework\ADataMember {
	/* Magic methods */

	public function __construct(\Framework\ADataManager $DataManager, string $Name, array $Params) {
		parent::__construct($DataManager, $Name, $Params);
	}

	/* Instance methods */

	public function Set(int $Id, $Value) {
		throw new \Framework\RuntimeException(
			'Failed to set data member value, data member is static.',
			[
				'Data member' => $this->Name
			]
		);
	}

	public function Increment(int $Id, $Amount) {
		throw new RuntimeException(
			'Failed to increment data member value, data member is static.',
			[
				'Data member' => $this->Name
			]
		);
	}

	public function Decrement(int $Id, $Amount) {
		throw new RuntimeException(
			'Failed to increment data member value, data member is static.',
			[
				'Data member' => $this->Name
			]
		);
	}
}
?>