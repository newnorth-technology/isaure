<?
namespace Framework\Database;

abstract class AColumn {
	/* Instance methods */

	public abstract function IsEmpty() : bool;

	public abstract function IsNotEmpty() : bool;

	public abstract function Set($Params);

	public abstract function Merge($Params);

	public abstract function Clone() : \Framework\Database\AColumn;

	public abstract function SetSourceAlias(string $OldAlias, string $NewAlias);

	public abstract function SetVariable(string $Key, $Value);

	public abstract function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection);
}

function ParseColumn($pv_params) {
	if($pv_params === null) {
		return new \Framework\Database\Columns\Value(['Value' => null]);
	}
	else if($pv_params instanceof \Framework\Database\AColumn) {
		return $pv_params;
	}
	else if($pv_params instanceof \Framework\ADataMember) {
		return new \Framework\Database\Columns\Reference(['Source' => $pv_params->DataManager->Id, 'Name' => $pv_params->Name]);
	}
	else if(is_array($pv_params)) {
		if(array_key_exists('Type', $pv_params)) {
			$column = '\\Framework\\Database\\Columns\\'.$pv_params['Type'];

			return new $column($pv_params);
		}
		else {
			if(is_bool($pv_params[0]) || is_float($pv_params[0]) || is_int($pv_params[0]) || is_string($pv_params[0])) {
				return new \Framework\Database\Columns\Value(['Value' => $pv_params]);
			}
			else {
				$columns = [];

				foreach($pv_params as $params) {
					$columns[] = ParseColumn($params);
				}

				return $columns;
			}
		}
	}
	else if(is_bool($pv_params) || is_float($pv_params) || is_int($pv_params) || is_string($pv_params)) {
		return new \Framework\Database\Columns\Value(['Value' => $pv_params]);
	}
	else {
		throw new \Framework\RuntimeException('Unable to parse "Params" as a "\\Framework\\Database\\AColumn".', ['Data' => $pv_params, 'Type' => gettype($pv_params)]);
	}
}
?>