<?
namespace Framework\Database;

class Target {
	/* Instance variables */

	public $DataManager;

	public $Alias;

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		return [
			'DataManager' => $this->DataManager,
			'Alias' => $this->Alias
		];
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return $this->DataManager === null;
	}

	public function IsNotEmpty() : bool {
		return $this->DataManager !== null;
	}

	public function Set($Params) {
		if($Params instanceof \Framework\Database\Target) {
			$this->DataManager = $Params->DataManager;

			$this->Alias = $Params->Alias;
		}
		else {
			$this->DataManager = $Params['DataManager'] ?? null;

			$this->Alias = $Params['Alias'] ?? null;
		}
	}

	public function Merge($Params) {
		if($Params instanceof \Framework\Database\Target) {
			$this->DataManager = $Params->DataManager;

			$this->Alias = $Params->Alias;
		}
		else {
			if(array_key_exists('DataManager', $Params)) {
				$this->DataManager = $Params['DataManager'];
			}

			if(array_key_exists('Alias', $Params)) {
				$this->Alias = $Params['Alias'];
			}
		}
	}

	public function Clone() : \Framework\Database\Target {
		return new \Framework\Database\Target([
			'DataManager' => $this->DataManager,
			'Alias' => $this->Alias
		]);
	}

	public function SetSourceAlias(string $OldAlias, string $NewAlias) {
		if($this->Alias === null) {
			if($this->DataManager !== null && $this->DataManager->Id === $OldAlias) {
				$this->Alias = $NewAlias;
			}
		}
		else if($this->Alias === $OldAlias) {
			$this->Alias = $NewAlias;
		}
	}

	public function SetVariable(string $Key, $Value) {

	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection) {
		if($this->DataManager === null) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "DataManager" is null.',
				[
					'Object' => $this
				]
			);
		}

		if($this->Alias === null) {
			if($this->DataManager instanceof \Framework\ADataManager) {
				$dm = $this->DataManager;
			}
			else {
				$dm = \Framework\GetDataManager($this->DataManager);
			}

			if($dm->Connection === $Connection) {
				$database = $dm->Database;

				$table = $dm->Table;
			}
			else if(isset($dm->Slaves[$Connection->Name])) {
				$database = $dm->Slaves[$Connection->Name]->Database;

				$table = $dm->Slaves[$Connection->Name]->Table;
			}
			else {
				throw new \Framework\RuntimeException(
					'Unable to convert to MySQL query string, unsupported connection for data manager.',
					[
						'Object' => $this
					]
				);
			}

			if($database === null) {
				return '`'.$table.'`';
			}
			else {
				return '`'.$database.'`.`'.$table.'`';
			}
		}
		else {
			return '`'.$this->Alias.'`';
		}
	}
}

function ParseTarget($Params) : \Framework\Database\Target {
	if($Params === null) {
		throw new \Framework\RuntimeException(
			'Unable to parse "Params" as a "\\Framework\\Database\\Target", "Params" are null.'
		);
	}
	else if($Params instanceof \Framework\Database\Target) {
		return $Params;
	}
	else if($Params instanceof \Framework\ADataManager) {
		return new \Framework\Database\Target([
			'DataManager' => $Params
		]);
	}
	else if(is_array($Params)) {
		return new \Framework\Database\Target($Params);
	}
	else {
		throw new \Framework\RuntimeException(
			'Unable to parse "Params" as a "\\Fraemwork\\Database\\Target".',
			[
				'Params' => $Params,
				'Type' => gettype($Params)
			]
		);
	}
}
?>