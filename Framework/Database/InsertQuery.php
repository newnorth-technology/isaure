<?
namespace Framework\Database;

class InsertQuery {
	/* Instance variables */

	public $Target;

	public $Columns;

	public $Values;

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		return [
			'Target' => $this->Target === null ? null : $this->Target->__debugInfo(),
			'Columns' => $this->Columns === null ? null : $this->Columns->__debugInfo(),
			'Values' => $this->Values === null ? null : $this->Values->__debugInfo()
		];
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return $this->Target === null || $this->Columns === null || $this->Values === null || $this->Target->IsEmpty() || $this->Columns->IsEmpty() || $this->Values->IsEmpty();
	}

	public function IsNotEmpty() : bool {
		return $this->Target !== null && $this->Columns !== null && $this->Values !== null && $this->Target->IsNotEmpty() && $this->Columns->IsNotEmpty() && $this->Values->IsNotEmpty();
	}

	public function Set($Params) {
		if($Params instanceof \Framework\Database\DeleteQuery) {
			$this->Target = $Params->Target;

			$this->Columns = $Params->Columns;

			$this->Values = $Params->Values;
		}
		else {
			$this->SetTarget($Params['Target'] ?? null);

			$this->SetColumns($Params['Columns'] ?? null);

			if(array_key_exists('<Columns', $Params)) {
				$this->MergeColumns($Params['<Columns']);
			}

			$this->SetValues($Params['Values'] ?? null);

			if(array_key_exists('<Values', $Params)) {
				$this->MergeValues($Params['<Values']);
			}
		}
	}

	public function Merge($Params) {
		if($Params instanceof \Framework\Database\DeleteQuery) {
			$this->Target = $Params->Target;

			$this->MergeColumns($Params->Columns);

			$this->MergeValues($Params->Values);
		}
		else {
			if(array_key_exists('Target', $Params)) {
				$this->SetTarget($Params['Target']);
			}

			if(array_key_exists('Columns', $Params)) {
				$this->MergeColumns($Params['Columns']);
			}

			if(array_key_exists('<Columns', $Params)) {
				$this->MergeColumns($Params['<Columns']);
			}

			if(array_key_exists('Values', $Params)) {
				$this->MergeValues($Params['Values']);
			}

			if(array_key_exists('<Values', $Params)) {
				$this->MergeValues($Params['<Values']);
			}
		}
	}

	public function SetTarget($Params) {
		if($Params === null) {
			$this->Target = null;
		}
		else {
			$this->Target = \Framework\Database\ParseTarget($Params);
		}
	}

	public function SetColumns($Params) {
		if($Params === null) {
			$this->Columns = null;
		}
		else if($Params instanceof \Framework\Database\Columns) {
			$this->Columns = $Params;
		}
		else {
			$this->Columns = new \Framework\Database\Columns($Params);
		}
	}

	public function MergeColumns($Params) {
		if($Params === null) {
			return;
		}

		if($this->Columns === null) {
			if($Params instanceof \Framework\Database\Columns) {
				$this->Columns = $Params;
			}
			else {
				$this->Columns = new \Framework\Database\Columns($Params);
			}
		}
		else {
			$this->Columns->Merge($Params);
		}
	}

	public function SetValues($Params) {
		if($Params === null) {
			$this->Values = null;
		}
		else if($Params instanceof \Framework\Database\Values) {
			$this->Values = $Params;
		}
		else {
			$this->Values = new \Framework\Database\Values($Params);
		}
	}

	public function MergeValues($Params) {
		if($Params === null) {
			return;
		}

		if($this->Values === null) {
			if($Params instanceof \Framework\Database\Values) {
				$this->Values = $Params;
			}
			else {
				$this->Values = new \Framework\Database\Values($Params);
			}
		}
		else {
			$this->Values->Merge($Params);
		}
	}

	public function Clone() : \Framework\Database\InsertQuery {
		return new \Framework\Database\InsertQuery([
			'Target' => $this->Target === null ? null : $this->Target->Clone(),
			'Columns' => $this->Columns === null ? null : $this->Columns->Clone(),
			'Values' => $this->Values === null ? null : $this->Values->Clone()
		]);
	}

	public function SetSourceAlias(string $OldAlias, string $NewAlias) {
		if($this->Target !== null) {
			$this->Target->SetSourceAlias($OldAlias, $NewAlias);
		}

		if($this->Columns !== null) {
			$this->Columns->SetSourceAlias($OldAlias, $NewAlias);
		}

		if($this->Values !== null) {
			$this->Values->SetSourceAlias($OldAlias, $NewAlias);
		}
	}

	public function SetVariable(string $Key, $Value) {
		if($this->Values !== null) {
			$this->Values->SetVariable($Key, $Value);
		}
	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection) {
		if($this->Target === null || $this->Target->IsEmpty()) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Target" is null or empty.',
				[
					'Object' => $this
				]
			);
		}

		if($this->Columns === null || $this->Columns->IsEmpty()) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Columns" is null or empty.',
				[
					'Object' => $this
				]
			);
		}

		if($this->Values === null || $this->Values->IsEmpty()) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Values" is null or empty.',
				[
					'Object' => $this
				]
			);
		}

		return 'INSERT INTO '.$this->Target->ToMySqlQueryString($Connection).' ('.$this->Columns->ToMySqlQueryString($Connection, false).') VALUES '.$this->Values->ToMySqlQueryString($Connection);
	}
}
?>