<?
namespace Framework\Database;

class DeleteQuery {
	/* Instance variables */

	public $Targets;

	public $Sources;

	public $Condition;

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		return [
			'Targets' => $this->Targets === null ? null : $this->Targets->__debugInfo(),
			'Sources' => $this->Sources === null ? null : $this->Sources->__debugInfo(),
			'Condition' => $this->Condition === null ? null : $this->Condition->__debugInfo()
		];
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return $this->Targets === null || $this->Sources === null || $this->Targets->IsEmpty() || $this->Sources->IsEmpty();
	}

	public function IsNotEmpty() : bool {
		return $this->Targets !== null && $this->Sources !== null && $this->Targets->IsNotEmpty() && $this->Sources->IsNotEmpty();
	}

	public function Set($Params) {
		if($Params instanceof \Framework\Database\DeleteQuery) {
			$this->Targets = $Params->Targets;

			$this->Sources = $Params->Sources;

			$this->Condition = $Params->Condition;
		}
		else {
			$this->SetTargets($Params['Targets'] ?? null);

			if(array_key_exists('<Targets', $Params)) {
				$this->MergeTargets($Params['<Targets']);
			}

			$this->SetSources($Params['Sources'] ?? null);

			if(array_key_exists('<Sources', $Params)) {
				$this->MergeSources($Params['<Sources']);
			}

			$this->SetCondition($Params['Condition'] ?? null);
		}
	}

	public function Merge($Params) {
		if($Params instanceof \Framework\Database\DeleteQuery) {
			$this->Targets = $Params->Targets;

			$this->Sources = $Params->Sources;

			$this->Condition = $Params->Condition;
		}
		else {
			if(array_key_exists('Targets', $Params)) {
				$this->SetTargets($Params['Targets']);
			}

			if(array_key_exists('<Targets', $Params)) {
				$this->MergeTargets($Params['<Targets']);
			}

			if(array_key_exists('Sources', $Params)) {
				$this->SetSources($Params['Sources']);
			}

			if(array_key_exists('<Sources', $Params)) {
				$this->MergeSources($Params['<Sources']);
			}

			if(array_key_exists('Condition', $Params)) {
				$this->SetCondition($Params['Condition']);
			}
		}
	}

	public function SetTargets($Params) {
		if($Params === null) {
			$this->Targets = null;
		}
		else if($Params instanceof \Framework\Database\Targets) {
			$this->Targets = $Params;
		}
		else {
			$this->Targets = new \Framework\Database\Targets($Params);
		}
	}

	public function MergeTargets($Params) {
		if($Params === null) {
			return;
		}

		if($this->Targets === null) {
			if($Params instanceof \Framework\Database\Targets) {
				$this->Targets = $Params;
			}
			else {
				$this->Targets = new \Framework\Database\Targets($Params);
			}
		}
		else {
			$this->Targets->Merge($Params);
		}
	}

	public function SetSources($Params) {
		if($Params === null) {
			$this->Sources = null;
		}
		else if($Params instanceof \Framework\Database\Sources) {
			$this->Sources = $Params;
		}
		else {
			$this->Sources = new \Framework\Database\Sources($Params);
		}
	}

	public function MergeSources($Params) {
		if($Params === null) {
			return;
		}

		if($this->Sources === null) {
			if($Params instanceof \Framework\Database\Sources) {
				$this->Sources = $Params;
			}
			else {
				$this->Sources = new \Framework\Database\Sources($Params);
			}
		}
		else {
			$this->Sources->Merge($Params);
		}
	}

	public function SetCondition($Params) {
		if($Params === null) {
			$this->Condition = null;
		}
		else {
			$this->Condition = \Framework\Database\ParseCondition($Params);
		}
	}

	public function Clone() : \Framework\Database\DeleteQuery {
		return new \Framework\Database\DeleteQuery([
			'Targets' => $this->Targets === null ? null : $this->Targets->Clone(),
			'Sources' => $this->Sources === null ? null : $this->Sources->Clone(),
			'Condition' => $this->Condition === null ? null : $this->Condition->Clone()
		]);
	}

	public function SetVariable(string $Key, $Value) {
		if($this->Sources !== null) {
			$this->Sources->SetVariable($Key, $Value);
		}

		if($this->Condition !== null) {
			$this->Condition->SetVariable($Key, $Value);
		}
	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection) {
		if($this->Sources === null || $this->Sources->IsEmpty()) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Sources" is null or empty.',
				[
					'Object' => $this
				]
			);
		}

		if($this->Targets === null) {
			if($this->Condition === null) {
				return 'DELETE FROM '.$this->Sources->ToMySqlQueryString($Connection);
			}
			else {
				return 'DELETE FROM '.$this->Sources->ToMySqlQueryString($Connection).' WHERE '.$this->Condition->ToMySqlQueryString($Connection);
			}
		}
		else {
			if($this->Condition === null) {
				return 'DELETE '.$this->Targets->ToMySqlQueryString($Connection).' FROM '.$this->Sources->ToMySqlQueryString($Connection);
			}
			else {
				return 'DELETE '.$this->Targets->ToMySqlQueryString($Connection).' FROM '.$this->Sources->ToMySqlQueryString($Connection).' WHERE '.$this->Condition->ToMySqlQueryString($Connection);
			}
		}
	}
}
?>