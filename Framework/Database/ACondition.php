<?
namespace Framework\Database;

abstract class ACondition {
	/* Instance methods */

	public abstract function IsEmpty() : bool;

	public abstract function IsNotEmpty() : bool;

	public abstract function Set($Params);

	public abstract function Merge($Params);

	public abstract function Clone() : \Framework\Database\ACondition;

	public abstract function SetSourceAlias(string $OldAlias, string $NewAlias);

	public abstract function SetVariable(string $Key, $Value);

	public abstract function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection);
}

function ParseCondition($Params) : \Framework\Database\ACondition {
	if($Params === null) {
		throw new \Framework\RuntimeException(
			'Unable to parse "Params" as a "\\Framework\\Database\\ACondition", "Params" are null.'
		);
	}
	else if($Params instanceof \Framework\Database\ACondition) {
		return $Params;
	}
	else if(is_array($Params) && array_key_exists('Type', $Params)) {
		$condition = '\\Framework\\Database\\Conditions\\'.$Params['Type'];

		return new $condition($Params);
	}
	else {
		throw new \Framework\RuntimeException(
			'Unable to parse "Params" as a "\\Framework\\Database\\ACondition".',
			[
				'Params' => $Params,
				'Type' => gettype($Params)
			]
		);
	}
}
?>