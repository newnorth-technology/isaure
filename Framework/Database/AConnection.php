<?
namespace Framework\Database;

abstract class AConnection {
	/* Instance variables */

	public $Name;

	/* Magic methods */

	public function __construct(string $Name, array $Params) {
		$this->Name = $Name;
	}

	/* Instance methods */

	public abstract function Connect();

	public abstract function Insert(\Framework\Database\InsertQuery $Query) : int;

	public abstract function Update(\Framework\Database\UpdateQuery $Query) : int;

	public abstract function Delete(\Framework\Database\DeleteQuery $Query) : int;

	public abstract function Find(\Framework\Database\SelectQuery $Query) : \Framework\Database\AResult;

	public abstract function FindAll(\Framework\Database\SelectQuery $Query) : \Framework\Database\AResult;

	public abstract function Count(\Framework\Database\SelectQuery $Query) : int;

	public abstract function SumFloat(\Framework\Database\SelectQuery $Query) : float;

	public abstract function SumInt(\Framework\Database\SelectQuery $Query) : int;

	public abstract function Truncate(\Framework\Database\Source $Source);

	public abstract function Lock(array $Sources);

	public abstract function Unlock(array $Sources);
}
?>