<?
namespace Framework\Database;

class Values implements \ArrayAccess {
	/* Instance variables */

	public $Columns = [];

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		return array_map(
			function($Column) {
				return $Column->__debugInfo();
			},
			$this->Columns
		);
	}

	/* Instance ArrayAccess methods */

	public function offsetSet($Key, $Value) {
		throw new \Framework\NotImplementedException();
	}

	public function offsetExists($Key) {
		return array_key_exists($Key, $this->Columns);
	}

	public function offsetUnset($Key) {
		throw new \Framework\NotImplementedException();
	}

	public function offsetGet($Key) {
		return $this->Columns[$Key];
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return count($this->Columns) === 0;
	}

	public function IsNotEmpty() : bool {
		return 0 < count($this->Columns);
	}

	public function Set($Params) {
		if($Params === null) {
			$this->Columns = [];
		}
		else if($Params instanceof \Framework\Database\Values) {
			$this->Columns = $Params->Columns;
		}
		else {
			$this->Columns = [];

			foreach($Params as $params) {
				$this->Columns[] = \Framework\Database\ParseColumn($params);
			}
		}
	}

	public function Merge($Params) {
		if($Params !== null) {
			if($Params instanceof \Framework\Database\Values) {
				foreach($Params->Columns as $item) {
					$this->Columns[] = $item;
				}
			}
			else {
				foreach($Params as $params) {
					$this->Columns[] = \Framework\Database\ParseColumn($params);
				}
			}
		}
	}

	public function Clone() : \Framework\Database\Values {
		return new \Framework\Database\Values(
			array_map(
				function($Column) {
					return $Column->Clone();
				},
				$this->Columns
			)
		);
	}

	public function SetSourceAlias(string $OldAlias, string $NewAlias) {
		foreach($this->Columns as $column) {
			$column->SetSourceAlias($OldAlias, $NewAlias);
		}
	}

	public function SetVariable(string $Key, $Value) {
		foreach($this->Columns as $column) {
			$column->SetVariable($Key, $Value);
		}
	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection) {
		if(count($this->Columns) === 0) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Columns" is empty.',
				[
					'Object' => $this
				]
			);
		}

		$columns = [];

		foreach($this->Columns as $column) {
			$columns[] = $column->ToMySqlQueryString($Connection);
		}

		return '('.implode(', ', $columns).')';
	}
}
?>