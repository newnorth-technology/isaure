<?
namespace Framework\Database;

class Limit {
	/* Instance variables */

	public $Offset;

	public $Count;

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		return [
			'Offset' => $this->Offset,
			'Count' => $this->Count
		];
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return $this->Count === null;
	}

	public function IsNotEmpty() : bool {
		return $this->Count !== null;
	}

	public function Set($Params) {
		if($Params instanceof \Framework\Database\Limit) {
			$this->Offset = $Params->Offset;

			$this->Count = $Params->Count;
		}
		else {
			$this->Offset = $Params['Offset'] ?? null;

			$this->Count = $Params['Count'] ?? null;
		}
	}

	public function Merge($Params) {
		if($Params instanceof \Framework\Database\Limit) {
			$this->Offset = $Params->Offset;

			$this->Count = $Params->Count;
		}
		else {
			if(array_key_exists('Offset', $Params)) {
				$this->Offset = $Params['Offset'];
			}

			if(array_key_exists('Count', $Params)) {
				$this->Count = $Params['Count'];
			}
		}
	}

	public function Clone() : \Framework\Database\Limit {
		return new \Framework\Database\Limit([
			'Offset' => $this->Offset,
			'Count' => $this->Count
		]);
	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection) {
		if($this->Count === null) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Count" is null.',
				[
					'Object' => $this
				]
			);
		}

		if($this->Offset === null) {
			return $this->Count;
		}
		else {
			return $this->Offset.', '.$this->Count;
		}
	}
}
?>