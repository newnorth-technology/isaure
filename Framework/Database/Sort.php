<?
namespace Framework\Database;

class Sort {
	/* Instance variables */

	public $Column;

	public $Direction;

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		return [
			'Column' => $this->Column === null ? null : $this->Column->__debugInfo(),
			'Direction' => $this->Direction
		];
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return $this->Column === null;
	}

	public function IsNotEmpty() : bool {
		return $this->Column !== null;
	}

	public function Set($Params) {
		if($Params instanceof \Framework\Database\Sort) {
			$this->Column = $Params->Column;

			$this->Direction = $Params->Direction;
		}
		else {
			$this->SetColumn($Params['Column'] ?? null);

			$this->Direction = $Params['Direction'] ?? null;
		}
	}

	public function Merge($Params) {
		if($Params instanceof \Framework\Database\Sort) {
			$this->Column = $Params->Column;

			$this->Direction = $Params->Direction;
		}
		else {
			if(array_key_exists('Column', $Params)) {
				$this->SetColumn($Params['Column']);
			}

			if(array_key_exists('Direction', $Params)) {
				$this->Direction = $Params['Direction'];
			}
		}
	}

	public function SetColumn($Params) {
		if($Params === null) {
			$this->Column = null;
		}
		else {
			$this->Column = \Framework\Database\ParseColumn($Params);
		}
	}

	public function Clone() : \Framework\Database\Sort {
		return new \Framework\Database\Sort([
			'Column' => $this->Column === null ? null : $this->Column->Clone(),
			'Direction' => $this->Direction
		]);
	}

	public function SetSourceAlias(string $OldAlias, string $NewAlias) {
		if($this->Column !== null) {
			$this->Column->SetSourceAlias($OldAlias, $NewAlias);
		}
	}

	public function SetVariable(string $Key, $Value) {
		if($this->Column !== null) {
			$this->Column->SetVariable($Key, $Value);
		}
	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection) {
		if($this->Column === null || $this->Column->IsEmpty()) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Column" is null or empty.',
				[
					'Object' => $this
				]
			);
		}

		if($this->Direction === null) {
			return $this->Column->ToMySqlQueryString($Connection);
		}
		else {
			return $this->Column->ToMySqlQueryString($Connection).' '.$this->Direction;
		}
	}
}
?>