<?
namespace Framework\Database;

class SelectQuery {
	/* Instance variables */

	public $Columns;

	public $Sources;

	public $Condition;

	public $Groups;

	public $Having;

	public $Sorts;

	public $Limit;

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		return [
			'Columns' => $this->Columns === null ? null : $this->Columns->__debugInfo(),
			'Sources' => $this->Sources === null ? null : $this->Sources->__debugInfo(),
			'Condition' => $this->Condition === null ? null : $this->Condition->__debugInfo(),
			'Groups' => $this->Groups === null ? null : $this->Groups->__debugInfo(),
			'Having' => $this->Having === null ? null : $this->Having->__debugInfo(),
			'Sorts' => $this->Sorts === null ? null : $this->Sorts->__debugInfo(),
			'Limit' => $this->Limit === null ? null : $this->Limit->__debugInfo()
		];
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return ($this->Columns === null || $this->Columns->IsEmpty()) && ($this->Sources === null || $this->Sources->IsEmpty());
	}

	public function IsNotEmpty() : bool {
		return ($this->Columns !== null && $this->Columns->IsNotEmpty()) || ($this->Sources !== null && $this->Sources->IsNotEmpty());
	}

	public function Set($Params) {
		if($Params instanceof \Framework\Database\Selectquery) {
			$this->Columns = $Params->Columns;

			$this->Sources = $Params->Sources;

			$this->Condition = $Params->Condition;

			$this->Groups = $Params->Groups;

			$this->Having = $Params->Having;

			$this->Sorts = $Params->Sorts;

			$this->Limit = $Params->Limit;
		}
		else {
			$this->SetColumns($Params['Columns'] ?? null);

			if(array_key_exists('<Columns', $Params)) {
				$this->MergeColumns($Params['<Columns']);
			}

			$this->SetSources($Params['Sources'] ?? null);

			if(array_key_exists('<Sources', $Params)) {
				$this->MergeSources($Params['<Sources']);
			}

			$this->SetCondition($Params['Condition'] ?? null);

			$this->SetGroups($Params['Groups'] ?? null);

			if(array_key_exists('<Groups', $Params)) {
				$this->MergeGroups($Params['<Groups']);
			}

			$this->SetHaving($Params['Having'] ?? null);

			$this->SetSorts($Params['Sorts'] ?? null);

			if(array_key_exists('<Sorts', $Params)) {
				$this->MergeSorts($Params['<Sorts']);
			}

			$this->SetLimit($Params['Limit'] ?? null);
		}
	}

	public function Merge($Params) {
		if($Params instanceof \Framework\Database\Selectquery) {
			$this->Columns = $Params->Columns;

			$this->Sources = $Params->Sources;

			$this->Condition = $Params->Condition;

			$this->Groups = $Params->Groups;

			$this->Having = $Params->Having;

			$this->Sorts = $Params->Sorts;

			$this->Limit = $Params->Limit;
		}
		else {
			if(array_key_exists('Columns', $Params)) {
				$this->MergeColumns($Params['Columns']);
			}

			if(array_key_exists('<Columns', $Params)) {
				$this->MergeColumns($Params['<Columns']);
			}

			if(array_key_exists('Sources', $Params)) {
				$this->MergeSources($Params['Sources']);
			}

			if(array_key_exists('<Sources', $Params)) {
				$this->MergeSources($Params['<Sources']);
			}

			if(array_key_exists('Condition', $Params)) {
				if($this->Condition === null) {
					$this->Condition = \Framework\Database\ParseCondition($Params['Condition']);
				}
				else if($this->Condition instanceof \Framework\Database\Conditions\All) {
					$this->Condition->Conditions[] = \Framework\Database\ParseCondition($Params['Condition']);
				}
				else {
					$this->Condition = new \Framework\Database\Conditions\All([
						'Conditions' => [
							$this->Condition,
							$Params['Condition']
						]
					]);
				}
			}

			if(array_key_exists('<Condition', $Params)) {
				if($this->Condition === null) {
					$this->Condition = \Framework\Database\ParseCondition($Params['<Condition']);
				}
				else if($this->Condition instanceof \Framework\Database\Conditions\All) {
					$this->Condition->Conditions[] = \Framework\Database\ParseCondition($Params['<Condition']);
				}
				else {
					$this->Condition = new \Framework\Database\Conditions\All([
						'Conditions' => [
							$this->Condition,
							$Params['<Condition']
						]
					]);
				}
			}

			if(array_key_exists('Groups', $Params)) {
				$this->MergeGroups($Params['Groups']);
			}

			if(array_key_exists('<Groups', $Params)) {
				$this->MergeGroups($Params['<Groups']);
			}

			if(array_key_exists('Having', $Params)) {
				if($this->Having === null) {
					$this->Having = \Framework\Database\ParseCondition($Params['Having']);
				}
				else if($this->Having instanceof \Framework\Database\Conditions\All) {
					$this->Having->Conditions[] = \Framework\Database\ParseCondition($Params['Having']);
				}
				else {
					$this->Having = new \Framework\Database\Conditions\All([
						'Conditions' => [
							$this->Having,
							$Params['Having']
						]
					]);
				}
			}

			if(array_key_exists('<Having', $Params)) {
				if($this->Having === null) {
					$this->Having = \Framework\Database\ParseCondition($Params['<Having']);
				}
				else if($this->Having instanceof \Framework\Database\Conditions\All) {
					$this->Having->Conditions[] = \Framework\Database\ParseCondition($Params['<Having']);
				}
				else {
					$this->Having = new \Framework\Database\Conditions\All([
						'Conditions' => [
							$this->Having,
							$Params['<Having']
						]
					]);
				}
			}

			if(array_key_exists('Sorts', $Params)) {
				$this->MergeSorts($Params['Sorts']);
			}

			if(array_key_exists('<Sorts', $Params)) {
				$this->MergeSorts($Params['<Sorts']);
			}

			if(array_key_exists('Limit', $Params)) {
				$this->SetLimit($Params['Limit']);
			}
		}
	}

	public function SetColumns($Params) {
		if($Params === null) {
			$this->Columns = null;
		}
		else if($Params instanceof \Framework\Database\Columns) {
			$this->Columns = $Params;
		}
		else {
			$this->Columns = new \Framework\Database\Columns($Params);
		}
	}

	public function MergeColumns($Params) {
		if($Params === null) {
			return;
		}

		if($this->Columns === null) {
			if($Params instanceof \Framework\Database\Columns) {
				$this->Columns = $Params;
			}
			else {
				$this->Columns = new \Framework\Database\Columns($Params);
			}
		}
		else {
			$this->Columns->Merge($Params);
		}
	}

	public function AddColumn(string $Key, $Params) {
		if($this->Columns === null) {
			$this->Columns = new \Framework\Database\Columns([$Key => $Params]);
		}
		else {
			$this->Columns->AddColumn($Key, $Params);
		}
	}

	public function SetSources($Params) {
		if($Params === null) {
			$this->Sources = null;
		}
		else if($Params instanceof \Framework\Database\Sources) {
			$this->Sources = $Params;
		}
		else {
			$this->Sources = new \Framework\Database\Sources($Params);
		}
	}

	public function MergeSources($Params) {
		if($Params === null) {
			return;
		}

		if($this->Sources === null) {
			if($Params instanceof \Framework\Database\Sources) {
				$this->Sources = $Params;
			}
			else {
				$this->Sources = new \Framework\Database\Sources($Params);
			}
		}
		else {
			$this->Sources->Merge($Params);
		}
	}

	public function AddSource(string $Key, $Params) {
		if($this->Sources === null) {
			$this->Sources = new \Framework\Database\Sources([$Key => $Params]);
		}
		else {
			$this->Sources->AddSource($Key, $Params);
		}
	}

	public function SetCondition($Params) {
		if($Params === null) {
			$this->Condition = null;
		}
		else {
			$this->Condition = \Framework\Database\ParseCondition($Params);
		}
	}

	public function MergeCondition($Params) {
		$condition = \Framework\Database\ParseCondition($Params);

		if($this->Condition === null) {
			$this->Condition = $condition;
		}
		else if($this->Condition instanceof \Framework\Database\Conditions\All) {
			if($condition instanceof \Framework\Database\Conditions\All) {
				$this->Condition->Merge($condition);
			}
			else {
				$this->Condition->Add($condition);
			}
		}
		else if($condition instanceof \Framework\Database\Conditions\All) {
			$this->Condition = new \Framework\Database\Conditions\All([
				'Conditions' => array_merge([$this->Condition], $condition->Conditions)
			]);
		}
		else {
			$this->Condition = new \Framework\Database\Conditions\All([
				'Conditions' => [$this->Condition, $condition]
			]);
		}
	}

	public function SetGroups($Params) {
		if($Params === null) {
			$this->Groups = null;
		}
		else if($Params instanceof \Framework\Database\Groups) {
			$this->Groups = $Params;
		}
		else {
			$this->Groups = new \Framework\Database\Groups($Params);
		}
	}

	public function MergeGroups($Params) {
		if($Params === null) {
			return;
		}

		if($this->Groups === null) {
			if($Params instanceof \Framework\Database\Groups) {
				$this->Groups = $Params;
			}
			else {
				$this->Groups = new \Framework\Database\Groups($Params);
			}
		}
		else {
			$this->Groups->Merge($Params);
		}
	}

	public function SetHaving($Params) {
		if($Params === null) {
			$this->Having = null;
		}
		else {
			$this->Having = \Framework\Database\ParseCondition($Params);
		}
	}

	public function MergeHaving($Params) {
		$condition = \Framework\Database\ParseCondition($Params);

		if($this->Having === null) {
			$this->Having = $condition;
		}
		else if($this->Having instanceof \Framework\Database\Conditions\All) {
			if($condition instanceof \Framework\Database\Conditions\All) {
				$this->Having->Merge($condition);
			}
			else {
				$this->Having->Add($condition);
			}
		}
		else if($condition instanceof \Framework\Database\Conditions\All) {
			$this->Having = new \Framework\Database\Conditions\All([
				'Conditions' => array_merge([$this->Having], $condition->Conditions)
			]);
		}
		else {
			$this->Having = new \Framework\Database\Conditions\All([
				'Conditions' => [$this->Having, $condition]
			]);
		}
	}

	public function SetSorts($Params) {
		if($Params === null) {
			$this->Sorts = null;
		}
		else if($Params instanceof \Framework\Database\Sorts) {
			$this->Sorts = $Params;
		}
		else {
			$this->Sorts = new \Framework\Database\Sorts($Params);
		}
	}

	public function MergeSorts($Params) {
		if($Params === null) {
			return;
		}

		if($this->Sorts === null) {
			if($Params instanceof \Framework\Database\Sorts) {
				$this->Sorts = $Params;
			}
			else {
				$this->Sorts = new \Framework\Database\Sorts($Params);
			}
		}
		else {
			$this->Sorts->Merge($Params);
		}
	}

	public function AddSort($Params) {
		if($this->Sorts === null) {
			$this->Sorts = new \Framework\Database\Sorts([$Params]);
		}
		else {
			$this->Sorts->Add($Params);
		}
	}

	public function SetLimit($Params) {
		if($Params === null) {
			$this->Limit = null;
		}
		else if($Params instanceof \Framework\Database\Limit) {
			$this->Limit = $Params;
		}
		else {
			$this->Limit = new \Framework\Database\Limit($Params);
		}
	}

	public function Clone(array $pv_params = []) : \Framework\Database\SelectQuery {
		$params = [
			'Columns' => $this->Columns === null ? null : $this->Columns->Clone(),
			'Groups' => $this->Groups === null ? null : $this->Groups->Clone(),
			'Having' => $this->Having === null ? null : $this->Having->Clone(),
			'Limit' => $this->Limit === null ? null : $this->Limit->Clone()
		];

		if(array_key_exists('sources', $pv_params)) {
			$params['Sources'] = new \Framework\Database\Sources($pv_params['sources']);
		}
		else if($this->Sources !== null) {
			$params['Sources'] = $this->Sources->Clone();
		}
		else {
			$params['Sources'] = new \Framework\Database\Sources();
		}

		if(array_key_exists('+sources', $pv_params)) {
			foreach($pv_params['+sources'] as $name => $source) {
				if($name[0] === '+') {
					$name = substr($name, 1);

					if(isset($params['Sources'][$name])) {
						$params['Sources'][$name] = $params['Sources'][$name]->Clone($source);
					}
					else {
						$params['Sources'][$name] = $source;
					}
				}
				else {
					$params['Sources'][$name] = $source;
				}
			}
		}

		if(array_key_exists('condition', $pv_params)) {
			$params['Condition'] = \Framework\Database\ParseCondition($pv_params['condition']);
		}
		else if($this->Condition !== null) {
			$params['Condition'] = $this->Condition->Clone();
		}

		if(array_key_exists('sorts', $pv_params)) {
			$params['Sorts'] = new \Framework\Database\Sorts($pv_params['sorts']);
		}
		else if($this->Sorts !== null) {
			$params['Sorts'] = $this->Sorts->Clone();
		}

		if(array_key_exists('limit', $pv_params)) {
			$params['Limit'] = new \Framework\Database\Limit($pv_params['limit']);
		}
		else if($this->Limit !== null) {
			$params['Limit'] = $this->Limit->Clone();
		}

		return new \Framework\Database\SelectQuery($params);
	}

	public function SetSourceAlias(string $OldAlias, string $NewAlias) {
		if($this->Columns !== null) {
			$this->Columns->SetSourceAlias($OldAlias, $NewAlias);
		}

		if($this->Sources !== null) {
			$this->Sources->SetSourceAlias($OldAlias, $NewAlias);
		}

		if($this->Condition !== null) {
			$this->Condition->SetSourceAlias($OldAlias, $NewAlias);
		}

		if($this->Groups !== null) {
			$this->Groups->SetSourceAlias($OldAlias, $NewAlias);
		}

		if($this->Having !== null) {
			$this->Having->SetSourceAlias($OldAlias, $NewAlias);
		}

		if($this->Sorts !== null) {
			$this->Sorts->SetSourceAlias($OldAlias, $NewAlias);
		}
	}

	public function SetVariable(string $Key, $Value) {
		if($this->Columns !== null) {
			$this->Columns->SetVariable($Key, $Value);
		}

		if($this->Sources !== null) {
			$this->Sources->SetVariable($Key, $Value);
		}

		if($this->Condition !== null) {
			$this->Condition->SetVariable($Key, $Value);
		}

		if($this->Groups !== null) {
			$this->Groups->SetVariable($Key, $Value);
		}

		if($this->Having !== null) {
			$this->Having->SetVariable($Key, $Value);
		}

		if($this->Sorts !== null) {
			$this->Sorts->SetVariable($Key, $Value);
		}
	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection) {
		if(($this->Columns === null || $this->Columns->IsEmpty()) && ($this->Sources === null || $this->Sources->IsEmpty())) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Columns" and "Sources" is null or empty.',
				[
					'Object' => $this
				]
			);
		}

		$query = 'SELECT';

		if($this->Columns === null || $this->Columns->IsEmpty()) {
			$query .= ' *';
		}
		else {
			$query .= ' '.$this->Columns->ToMySqlQueryString($Connection, true);
		}

		if($this->Sources !== null && $this->Sources->IsNotEmpty()) {
			$query .= ' FROM '.$this->Sources->ToMySqlQueryString($Connection);

			if($this->Condition !== null && $this->Condition->IsNotEmpty()) {
				$query .= ' WHERE '.$this->Condition->ToMySqlQueryString($Connection);
			}

			if($this->Groups !== null && $this->Groups->IsNotEmpty()) {
				$query .= ' GROUP BY '.$this->Groups->ToMySqlQueryString($Connection);
			}

			if($this->Having !== null && $this->Having->IsNotEmpty()) {
				$query .= ' HAVING '.$this->Having->ToMySqlQueryString($Connection);
			}

			if($this->Sorts !== null && $this->Sorts->IsNotEmpty()) {
				$query .= ' ORDER BY '.$this->Sorts->ToMySqlQueryString($Connection);
			}

			if($this->Limit !== null && $this->Limit->IsNotEmpty()) {
				$query .= ' LIMIT '.$this->Limit->ToMySqlQueryString($Connection);
			}
		}

		return $query;
	}
}
?>