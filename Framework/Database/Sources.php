<?
namespace Framework\Database;

class Sources implements \ArrayAccess, \Iterator {
	/* Instance variables */

	public $Items = [];

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		return array_map(
			function($Item) {
				return $Item->__debugInfo();
			},
			$this->Items
		);
	}

	/* Instance ArrayAccess methods */

	public function offsetSet($Key, $Value) {
		if($Key[0] === '<') {
			$Key = substr($Key, 1);

			if(array_key_exists($Key, $this->Items)) {
				$this->Items[$Key]->Merge($Value);
			}
			else {
				$this->Items[$Key] = \Framework\Database\ParseSource($Value);
			}
		}
		else {
			$this->Items[$Key] = \Framework\Database\ParseSource($Value);
		}
	}

	public function offsetExists($Key) {
		return array_key_exists($Key, $this->Items);
	}

	public function offsetUnset($Key) {
		throw new \Framework\NotImplementedException();
	}

	public function offsetGet($Key) {
		return $this->Items[$Key];
	}

	/* Instance Iterator methods */

	public function current() {
		return current($this->Items);
	}

	public function key() {
		return key($this->Items);
	}

	public function next() {
		return next($this->Items);
	}

	public function rewind() {
		return reset($this->Items);
	}

	public function valid() {
		return key($this->Items) !== null;
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return count($this->Items) === 0;
	}

	public function IsNotEmpty() : bool {
		return 0 < count($this->Items);
	}

	public function Set($Params) {
		if($Params === null) {
			$this->Items = [];
		}
		else if($Params instanceof \Framework\Database\Sources) {
			$this->Items = $Params->Items;
		}
		else {
			$this->Items = [];

			foreach($Params as $key => $params) {
				if(is_int($key)) {
					$item = \Framework\Database\ParseSource($params);

					$this->Items[$item->Alias] = $item;
				}
				else if($key[0] === '<') {
					$key = substr($key, 1);

					if(array_key_exists($key, $this->Items)) {
						$this->Items[$key]->Merge($params);
					}
					else {
						$this->Items[$key] = \Framework\Database\ParseSource($params);
					}
				}
				else {
					$this->Items[$key] = \Framework\Database\ParseSource($params);
				}
			}
		}
	}

	public function Merge($Params) {
		if($Params !== null) {
			if($Params instanceof \Framework\Database\Sources) {
				foreach($Params->Items as $key => $item) {
					$this->Items[$key] = $item;
				}
			}
			else {
				foreach($Params as $key => $params) {
					if($key[0] === '<') {
						$key = substr($key, 1);
					}

					if(array_key_exists($key, $this->Items)) {
						$this->Items[$key]->Merge($params);
					}
					else {
						$this->Items[$key] = \Framework\Database\ParseSource($params);
					}
				}
			}
		}
	}

	public function AddSource(string $Key, $Params) {
		$this->Items[$Key] = \Framework\Database\ParseSource($Params);
	}

	public function Clone() : \Framework\Database\Sources {
		return new \Framework\Database\Sources(
			array_map(
				function($Item) {
					return $Item->Clone();
				},
				$this->Items
			)
		);
	}

	public function SetSourceAlias(string $OldAlias, string $NewAlias) {
		if($OldAlias !== $NewAlias) {
			if(isset($this->Items[$OldAlias])) {
				$this->Items[$NewAlias] = $this->Items[$OldAlias];

				unset($this->Items[$OldAlias]);
			}

			foreach($this->Items as $item) {
				$item->SetSourceAlias($OldAlias, $NewAlias);
			}
		}
	}

	public function SetVariable(string $Key, $Value) {
		foreach($this->Items as $item) {
			$item->SetVariable($Key, $Value);
		}
	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection) {
		if(count($this->Items) === 0) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Items" is empty.',
				[
					'Object' => $this
				]
			);
		}

		if(1 < count($this->Items)) {
			$items = [];

			foreach($this->Items as $item) {
				$items[] = $item->ToMySqlQueryString($Connection);
			}

			return '('.implode(', ', $items).')';
		}
		else {
			return reset($this->Items)->ToMySqlQueryString($Connection);
		}
	}
}
?>