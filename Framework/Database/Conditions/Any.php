<?
namespace Framework\Database\Conditions;

class Any extends \Framework\Database\ACondition {
	/* Instance variables */

	public $Conditions = [];

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		return [
			'Type' => 'Any',
			'Conditions' => array_map(
				function($Condition) {
					return $Condition->__debugInfo();
				},
				$this->Conditions
			)
		];
	}

	/* Instance ArrayAccess methods */

	public function offsetSet($Key, $Value) {
		throw new \Framework\NotImplementedException();
	}

	public function offsetExists($Key) {
		return array_key_exists($Key, $this->Conditions);
	}

	public function offsetUnset($Key) {
		throw new \Framework\NotImplementedException();
	}

	public function offsetGet($Key) {
		return $this->Conditions[$Key];
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return count($this->Conditions) === 0;
	}

	public function IsNotEmpty() : bool {
		return 0 < count($this->Conditions);
	}

	public function Set($Params) {
		if($Params === null) {
			$this->Conditions = [];
		}
		else if($Params instanceof \Framework\Database\Conditions\Any) {
			$this->Conditions = $Params->Conditions;
		}
		else {
			$this->Conditions = [];

			if(array_key_exists('Conditions', $Params)) {
				foreach($Params['Conditions'] as $params) {
					$this->Conditions[] = \Framework\Database\ParseCondition($params);
				}
			}
		}
	}

	public function Merge($Params) {
		if($Params !== null) {
			if($Params instanceof \Framework\Database\Conditions\Any) {
				foreach($Params->Conditions as $condition) {
					$this->Conditions[] = $condition;
				}
			}
			else {
				if(array_key_exists('Conditions', $Params)) {
					foreach($Params['Conditions'] as $params) {
						$this->Conditions[] = \Framework\Database\ParseCondition($params);
					}
				}
			}
		}
	}

	public function Clone() : \Framework\Database\ACondition {
		return new \Framework\Database\Conditions\Any([
			'Conditions' => array_map(
				function($Condition) {
					return $Condition->Clone();
				},
				$this->Conditions
			)
		]);
	}

	public function SetSourceAlias(string $OldAlias, string $NewAlias) {
		foreach($this->Conditions as $condition) {
			$condition->SetSourceAlias($OldAlias, $NewAlias);
		}
	}

	public function SetVariable(string $Key, $Value) {
		foreach($this->Conditions as $condition) {
			$condition->SetVariable($Key, $Value);
		}
	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection) {
		if(count($this->Conditions) === 0) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Conditions" is empty.',
				[
					'Object' => $this
				]
			);
		}

		$conditions = [];

		foreach($this->Conditions as $alias => $condition) {
			$conditions[] = $condition->ToMySqlQueryString($Connection);
		}

		return '('.implode(' OR ', $conditions).')';
	}
}
?>