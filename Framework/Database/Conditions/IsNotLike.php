<?
namespace Framework\Database\Conditions;

class IsNotLike extends \Framework\Database\ACondition {
	/* Instance variables */

	public $ColumnA;

	public $ColumnB;

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		return [
			'Type' => 'IsNotLike',
			'ColumnA' => $this->ColumnA === null ? null : $this->ColumnA->__debugInfo(),
			'ColumnB' => $this->ColumnB === null ? null : $this->ColumnB->__debugInfo()
		];
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return $this->ColumnA === null || $this->ColumnB === null || $this->ColumnA->IsEmpty() || $this->ColumnB->IsEmpty();
	}

	public function IsNotEmpty() : bool {
		return $this->ColumnA !== null && $this->ColumnB !== null && $this->ColumnA->IsNotEmpty() && $this->ColumnB->IsNotEmpty();
	}

	public function Set($Params) {
		if($Params instanceof \Framework\Database\Conditions\IsNotLike) {
			$this->ColumnA = $Params->ColumnA;

			$this->ColumnB = $Params->ColumnB;
		}
		else {
			$this->SetColumnA($Params['ColumnA'] ?? null);

			$this->SetColumnB($Params['ColumnB'] ?? null);
		}
	}

	public function Merge($Params) {
		if($Params instanceof \Framework\Database\Conditions\IsNotLike) {
			$this->ColumnA = $Params->ColumnA;

			$this->ColumnB = $Params->ColumnB;
		}
		else {
			if(array_key_exists('ColumnA', $Params)) {
				$this->SetColumnA($Params['ColumnA']);
			}

			if(array_key_exists('ColumnB', $Params)) {
				$this->SetColumnB($Params['ColumnB']);
			}
		}
	}

	public function SetColumnA($Params) {
		if($Params === null) {
			$this->ColumnA = null;
		}
		else {
			$this->ColumnA = \Framework\Database\ParseColumn($Params);
		}
	}

	public function SetColumnB($Params) {
		if($Params === null) {
			$this->ColumnB = null;
		}
		else {
			$this->ColumnB = \Framework\Database\ParseColumn($Params);
		}
	}

	public function Clone() : \Framework\Database\ACondition {
		return new \Framework\Database\Conditions\IsNotLike([
			'ColumnA' => $this->ColumnA === null ? null : $this->ColumnA->Clone(),
			'ColumnB' => $this->ColumnB === null ? null : $this->ColumnB->Clone()
		]);
	}

	public function SetSourceAlias(string $OldAlias, string $NewAlias) {
		if($this->ColumnA !== null) {
			$this->ColumnA->SetSourceAlias($OldAlias, $NewAlias);
		}

		if($this->ColumnB !== null) {
			$this->ColumnB->SetSourceAlias($OldAlias, $NewAlias);
		}
	}

	public function SetVariable(string $Key, $Value) {
		if($this->ColumnA !== null) {
			$this->ColumnA->SetVariable($Key, $Value);
		}

		if($this->ColumnB !== null) {
			$this->ColumnB->SetVariable($Key, $Value);
		}
	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection) {
		if($this->ColumnA === null || $this->ColumnA->IsEmpty()) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "ColumnA" is null or empty.',
				[
					'Object' => $this
				]
			);
		}

		if($this->ColumnB === null || $this->ColumnB->IsEmpty()) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "ColumnB" is null or empty.',
				[
					'Object' => $this
				]
			);
		}

		$columnA = $this->ColumnA->ToMySqlQueryString($Connection);

		$columnB = $this->ColumnB->ToMySqlQueryString($Connection);

		return $columnA.' NOT LIKE CONCAT("%", '.$columnB.', "%")';
	}
}
?>