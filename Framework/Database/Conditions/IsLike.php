<?
namespace Framework\Database\Conditions;

class IsLike extends \Framework\Database\ACondition {
	/* Instance variables */

	public $ColumnA;

	public $ColumnB;

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		return [
			'Type' => 'IsLike',
			'ColumnA' => $this->ColumnA === null ? null : $this->ColumnA->__debugInfo(),
			'ColumnB' => $this->ColumnB === null ? null : $this->ColumnB->__debugInfo()
		];
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return $this->ColumnA === null || $this->ColumnB === null || $this->ColumnA->IsEmpty() || $this->ColumnB->IsEmpty();
	}

	public function IsNotEmpty() : bool {
		return $this->ColumnA !== null && $this->ColumnB !== null && $this->ColumnA->IsNotEmpty() && $this->ColumnB->IsNotEmpty();
	}

	public function Set($Params) {
		if($Params instanceof \Framework\Database\Conditions\IsLike) {
			$this->ColumnA = $Params->ColumnA;

			$this->ColumnB = $Params->ColumnB;
		}
		else {
			$this->SetColumnA($Params['ColumnA'] ?? null);

			$this->SetColumnB($Params['ColumnB'] ?? null);
		}
	}

	public function Merge($Params) {
		if($Params instanceof \Framework\Database\Conditions\IsLike) {
			$this->ColumnA = $Params->ColumnA;

			$this->ColumnB = $Params->ColumnB;
		}
		else {
			if(array_key_exists('ColumnA', $Params)) {
				$this->SetColumnA($Params['ColumnA']);
			}

			if(array_key_exists('ColumnB', $Params)) {
				$this->SetColumnB($Params['ColumnB']);
			}
		}
	}

	public function SetColumnA($Params) {
		if($Params === null) {
			$this->ColumnA = null;
		}
		else {
			$this->ColumnA = \Framework\Database\ParseColumn($Params);
		}
	}

	public function SetColumnB($Params) {
		if($Params === null) {
			$this->ColumnB = null;
		}
		else {
			$this->ColumnB = \Framework\Database\ParseColumn($Params);
		}
	}

	public function Clone() : \Framework\Database\ACondition {
		if(is_array($this->ColumnA)) {
			$column_a = [];

			foreach($this->ColumnA as $column) {
				$column_a[] = $column === null ? null : $column->Clone();
			}
		}
		else {
			$column_a = $this->ColumnA === null ? null : $this->ColumnA->Clone();
		}

		if(is_array($this->ColumnB)) {
			$column_b = [];

			foreach($this->ColumnB as $column) {
				$column_b[] = $column === null ? null : $column->Clone();
			}
		}
		else {
			$column_b = $this->ColumnB === null ? null : $this->ColumnB->Clone();
		}

		return new \Framework\Database\Conditions\IsLike([
			'ColumnA' => $column_a,
			'ColumnB' => $column_b
		]);
	}

	public function SetSourceAlias(string $OldAlias, string $NewAlias) {
		if(is_array($this->ColumnA)) {
			foreach($this->ColumnA as $column) {
				$column->SetSourceAlias($OldAlias, $NewAlias);
			}
		}
		else if($this->ColumnA !== null) {
			$this->ColumnA->SetSourceAlias($OldAlias, $NewAlias);
		}

		if(is_array($this->ColumnB)) {
			foreach($this->ColumnB as $column) {
				$column->SetSourceAlias($OldAlias, $NewAlias);
			}
		}
		else if($this->ColumnB !== null) {
			$this->ColumnB->SetSourceAlias($OldAlias, $NewAlias);
		}
	}

	public function SetVariable(string $Key, $Value) {
		if(is_array($this->ColumnA)) {
			foreach($this->ColumnA as $column) {
				$column->SetVariable($Key, $Value);
			}
		}
		else if($this->ColumnA !== null) {
			$this->ColumnA->SetVariable($Key, $Value);
		}

		if(is_array($this->ColumnB)) {
			foreach($this->ColumnB as $column) {
				$column->SetVariable($Key, $Value);
			}
		}
		else if($this->ColumnB !== null) {
			$this->ColumnB->SetVariable($Key, $Value);
		}
	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $pv_connection) {
		$columns_a = [];

		if(is_array($this->ColumnA)) {
			foreach($this->ColumnA as $column) {
				if($column !== null && !$column->IsEmpty()) {
					$columns_a[] = $column->ToMySqlQueryString($pv_connection);
				}
			}
		}
		else if($this->ColumnA !== null && !$this->ColumnA->IsEmpty()) {
			$columns_a[] = $this->ColumnA->ToMySqlQueryString($pv_connection);
		}

		if(count($columns_a) === 0) {
			throw new \Framework\RuntimeException('Unable to convert to MySQL query string, "ColumnA" is null or empty.', ['Object' => $this]);
		}

		$columns_b = [];

		if(is_array($this->ColumnB)) {
			foreach($this->ColumnB as $column) {
				if($column !== null && !$column->IsEmpty()) {
					$columns_b[] = $column->ToMySqlQueryString($pv_connection);
				}
			}
		}
		else if($this->ColumnB !== null && !$this->ColumnB->IsEmpty()) {
			$columns_b[] = $this->ColumnB->ToMySqlQueryString($pv_connection);
		}

		if(count($columns_b) === 0) {
			throw new \columns_b\RuntimeException('Unable to convert to MySQL query string, "ColumnB" is null or empty.', ['Object' => $this]);
		}

		$conditions = [];

		foreach($columns_a as $column_a) {
			foreach($columns_b as $column_b) {
				$conditions[] = $column_a.' LIKE CONCAT("%", '.$column_b.', "%")';
			}
		}

		if(count($conditions) === 1) {
			return $conditions[0];
		}
		else {
			return '('.implode(' OR ', $conditions).')';
		}
	}
}
?>