<?
namespace Framework\Database\Conditions;

class IsNotBetween extends \Framework\Database\ACondition {
	/* Instance variables */

	public $Column;

	public $From;

	public $To;

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		return [
			'Type' => 'IsNotBetween',
			'Column' => $this->Column === null ? null : $this->Column->__debugInfo(),
			'From' => $this->From === null ? null : $this->From->__debugInfo(),
			'To' => $this->To === null ? null : $this->To->__debugInfo()
		];
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return $this->Column === null || $this->From === null || $this->To === null || $this->Column->IsEmpty() || $this->From->IsEmpty() || $this->To->IsEmpty();
	}

	public function IsNotEmpty() : bool {
		return $this->Column !== null && $this->From !== null && $this->To !== null && $this->Column->IsNotEmpty() && $this->From->IsNotEmpty() && $this->To->IsNotEmpty();
	}

	public function Set($Params) {
		if($Params instanceof \Framework\Database\Conditions\IsNotBetween) {
			$this->Column = $Params->Column;

			$this->From = $Params->From;

			$this->To = $Params->To;
		}
		else {
			$this->SetColumn($Params['Column'] ?? null);

			$this->SetFrom($Params['From'] ?? null);

			$this->SetTo($Params['To'] ?? null);
		}
	}

	public function Merge($Params) {
		if($Params instanceof \Framework\Database\Conditions\IsNotBetween) {
			$this->Column = $Params->Column;

			$this->From = $Params->From;

			$this->To = $Params->To;
		}
		else {
			if(array_key_exists('Column', $Params)) {
				$this->SetColumn($Params['Column']);
			}

			if(array_key_exists('From', $Params)) {
				$this->SetFrom($Params['From']);
			}

			if(array_key_exists('To', $Params)) {
				$this->SetTo($Params['To']);
			}
		}
	}

	public function SetColumn($Params) {
		if($Params === null) {
			$this->Column = null;
		}
		else {
			$this->Column = \Framework\Database\ParseColumn($Params);
		}
	}

	public function SetFrom($Params) {
		if($Params === null) {
			$this->From = null;
		}
		else {
			$this->From = \Framework\Database\ParseColumn($Params);
		}
	}

	public function SetTo($Params) {
		if($Params === null) {
			$this->To = null;
		}
		else {
			$this->To = \Framework\Database\ParseColumn($Params);
		}
	}

	public function Clone() : \Framework\Database\ACondition {
		return new \Framework\Database\Conditions\IsNotBetween([
			'Column' => $this->Column === null ? null : $this->Column->Clone(),
			'From' => $this->From === null ? null : $this->From->Clone(),
			'To' => $this->To === null ? null : $this->To->Clone()
		]);
	}

	public function SetSourceAlias(string $OldAlias, string $NewAlias) {
		if($this->Column !== null) {
			$this->Column->SetSourceAlias($OldAlias, $NewAlias);
		}

		if($this->From !== null) {
			$this->From->SetSourceAlias($OldAlias, $NewAlias);
		}

		if($this->To !== null) {
			$this->To->SetSourceAlias($OldAlias, $NewAlias);
		}
	}

	public function SetVariable(string $Key, $Value) {
		if($this->Column !== null) {
			$this->Column->SetVariable($Key, $Value);
		}

		if($this->From !== null) {
			$this->From->SetVariable($Key, $Value);
		}

		if($this->To !== null) {
			$this->To->SetVariable($Key, $Value);
		}
	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection) {
		if($this->Column === null || $this->Column->IsEmpty()) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Column" is null or empty.',
				[
					'Object' => $this
				]
			);
		}

		if($this->From === null || $this->From->IsEmpty()) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "From" is null or empty.',
				[
					'Object' => $this
				]
			);
		}

		if($this->To === null || $this->To->IsEmpty()) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "To" is null or empty.',
				[
					'Object' => $this
				]
			);
		}

		return $this->Column->ToMySqlQueryString($Connection).' NOT BETWEEN '.$this->From->ToMySqlQueryString($Connection).' AND '.$this->To->ToMySqlQueryString($Connection);
	}
}
?>