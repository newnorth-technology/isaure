<?
namespace Framework\Database\Columns;

class Reference extends \Framework\Database\AColumn {
	/* Instance variables */

	public $Source;

	public $Name;

	public $Variable;

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		if(property_exists($this, 'Value')) {
			return [
				'Type' => 'Reference',
				'Source' => $this->Source,
				'Name' => $this->Name,
				'Variable' => $this->Variable,
				'Value' => $this->Value
			];
		}
		else {
			return [
				'Type' => 'Reference',
				'Source' => $this->Source,
				'Name' => $this->Name,
				'Variable' => $this->Variable
			];
		}
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return $this->Name === null;
	}

	public function IsNotEmpty() : bool {
		return $this->Name !== null;
	}

	public function Set($Params) {
		if($Params instanceof \Framework\Database\Columns\Reference) {
			$this->Source = $Params->Source;

			$this->Name = $Params->Name;

			$this->Variable = $Params->Variable;

			if(property_exists($Params, 'Value')) {
				$this->Value = $Params->Value;
			}
		}
		else {
			$this->Source = $Params['Source'] ?? null;

			$this->Name = $Params['Name'] ?? null;

			$this->Variable = $Params['Variable'] ?? null;

			if(array_key_exists('Value', $Params)) {
				$this->Value = $Params['Value'];
			}
		}
	}

	public function Merge($Params) {
		if($Params instanceof \Framework\Database\Columns\Reference) {
			$this->Source = $Params->Source;

			$this->Name = $Params->Name;

			$this->Variable = $Params->Variable;

			if(property_exists($Params, 'Value')) {
				$this->Value = $Params->Value;
			}
		}
		else {
			if(array_key_exists('Source', $Params)) {
				$this->Source = $Params['Source'];
			}

			if(array_key_exists('Name', $Params)) {
				$this->Name = $Params['Name'];
			}

			if(array_key_exists('Variable', $Params)) {
				$this->Variable = $Params['Variable'];
			}

			if(array_key_exists('Value', $Params)) {
				$this->Value = $Params['Value'];
			}
		}
	}

	public function Clone() : \Framework\Database\AColumn {
		if(property_exists($this, 'Value')) {
			return new \Framework\Database\Columns\Reference([
				'Source' => $this->Source,
				'Name' => $this->Name,
				'Variable' => $this->Variable,
				'Value' => $this->Value
			]);
		}
		else {
			return new \Framework\Database\Columns\Reference([
				'Source' => $this->Source,
				'Name' => $this->Name,
				'Variable' => $this->Variable
			]);
		}
	}

	public function SetSourceAlias(string $OldAlias, string $NewAlias) {
		if($this->Source === $OldAlias) {
			$this->Source = $NewAlias;
		}
	}

	public function SetVariable(string $Key, $Value) {
		if($this->Variable === $Key) {
			$this->Value = $Value;
		}
	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection, bool $UseSource = true) {
		if($this->Name === null) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Name" is null.',
				[
					'Object' => $this
				]
			);
		}

		if(property_exists($this, 'Value')) {
			if($this->Value === null) {
				return 'NULL';
			}
			else if(is_array($this->Value)) {
				$values = [];

				foreach($this->Value as $value) {
					if($value === null) {
						$values[] = 'NULL';
					}
					else if(is_bool($value)) {
						$values[] = $value ? '1' : '0';
					}
					else if(is_string($value)) {
						$values[] = '"'.$Connection->EscapeString($value).'"';
					}
					else {
						$values[] = (string)$value;
					}
				}

				return '('.implode(', ', $values).')';
			}
			else if(is_bool($this->Value)) {
				return $this->Value ? '1' : '0';
			}
			else if(is_string($this->Value)) {
				return '"'.$Connection->EscapeString($this->Value).'"';
			}
			else {
				return (string)$this->Value;
			}
		}
		else if($this->Source === null || !$UseSource) {
			return '`'.$this->Name.'`';
		}
		else {
			return '`'.$this->Source.'`.`'.$this->Name.'`';
		}
	}
}
?>