<?
namespace Framework\Database\Columns;

class Condition extends \Framework\Database\AColumn {
	/* Instance variables */

	public $Condition;

	public $Then;

	public $Else;

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		return [
			'Type' => 'Condition',
			'Condition' => $this->Condition,
			'Then' => $this->Then,
			'Else' => $this->Else
		];
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return $this->Then === null || $this->Else === null || $this->Then->IsEmpty() || $this->Else->IsEmpty();
	}

	public function IsNotEmpty() : bool {
		return $this->Then !== null && $this->Else !== null && $this->Then->IsNotEmpty() && $this->Else->IsNotEmpty();
	}

	public function Set($Params) {
		if($Params instanceof \Framework\Database\Columns\Condition) {
			$this->Condition = $Params->Condition;

			$this->Then = $Params->Then;

			$this->Else = $Params->Else;
		}
		else {
			$this->SetCondition($Params['Condition'] ?? null);

			$this->SetThen($Params['Then'] ?? null);

			$this->SetElse($Params['Else'] ?? null);
		}
	}

	public function Merge($Params) {
		if($Params instanceof \Framework\Database\Columns\Condition) {
			$this->Condition = $Params->Condition;

			$this->Then = $Params->Then;

			$this->Else = $Params->Else;
		}
		else {
			if(array_key_exists('Condition', $Params)) {
				$this->SetCondition($Params['Condition']);
			}

			if(array_key_exists('Then', $Params)) {
				$this->SetThen($Params['Then']);
			}

			if(array_key_exists('Else', $Params)) {
				$this->SetElse($Params['Else']);
			}
		}
	}

	public function SetCondition($Params) {
		if($Params === null) {
			$this->Condition = null;
		}
		else {
			$this->Condition = \Framework\Database\ParseCondition($Params);
		}
	}

	public function SetThen($Params) {
		if($Params === null) {
			$this->Then = null;
		}
		else {
			$this->Then = \Framework\Database\ParseColumn($Params);
		}
	}

	public function SetElse($Params) {
		if($Params === null) {
			$this->Else = null;
		}
		else {
			$this->Else = \Framework\Database\ParseColumn($Params);
		}
	}

	public function Clone() : \Framework\Database\AColumn {
		return new \Framework\Database\Columns\Condition([
			'Condition' => $this->Condition,
			'Then' => $this->Then,
			'Else' => $this->Else
		]);
	}

	public function SetSourceAlias(string $OldAlias, string $NewAlias) {
		if($this->Condition !== null) {
			$this->Condition->SetSourceAlias($OldAlias, $NewAlias);
		}

		if($this->Then !== null) {
			$this->Then->SetSourceAlias($OldAlias, $NewAlias);
		}

		if($this->Else !== null) {
			$this->Else->SetSourceAlias($OldAlias, $NewAlias);
		}
	}

	public function SetVariable(string $Key, $Value) {
		if($this->Condition !== null) {
			$this->Condition->SetVariable($Key, $Value);
		}

		if($this->Then !== null) {
			$this->Then->SetVariable($Key, $Value);
		}

		if($this->Else !== null) {
			$this->Else->SetVariable($Key, $Value);
		}
	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection) {
		if($this->Condition === null) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Condition" is null.',
				[
					'Object' => $this
				]
			);
		}

		if($this->Then === null) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Then" is null.',
				[
					'Object' => $this
				]
			);
		}

		if($this->Else === null) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Else" is null.',
				[
					'Object' => $this
				]
			);
		}

		return 'IF('.$this->Condition->ToMySqlQueryString($Connection).', '.$this->Then->ToMySqlQueryString($Connection).', '.$this->Else->ToMySqlQueryString($Connection).')';
	}
}
?>