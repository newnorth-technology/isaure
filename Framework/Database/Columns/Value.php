<?
namespace Framework\Database\Columns;

class Value extends \Framework\Database\AColumn {
	/* Instance variables */

	public $Variable;

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		if(property_exists($this, 'Value')) {
			return [
				'Type' => 'Value',
				'Variable' => $this->Variable,
				'Value' => $this->Value
			];
		}
		else {
			return [
				'Type' => 'Value',
				'Variable' => $this->Variable
			];
		}
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return !property_exists($this, 'Value');
	}

	public function IsNotEmpty() : bool {
		return property_exists($this, 'Value');
	}

	public function Set($Params) {
		if($Params instanceof \Framework\Database\Columns\Value) {
			$this->Variable = $Params->Variable;

			if(property_exists($Params, 'Value')) {
				$this->Value = $Params->Value;
			}
		}
		else {
			$this->Variable = $Params['Variable'] ?? null;

			if(array_key_exists('Value', $Params)) {
				$this->Value = $Params['Value'];
			}
		}
	}

	public function Merge($Params) {
		if($Params instanceof \Framework\Database\Columns\Value) {
			$this->Variable = $Params->Variable;

			if(property_exists($Params, 'Value')) {
				$this->Value = $Params->Value;
			}
		}
		else {
			if(array_key_exists('Variable', $Params)) {
				$this->Variable = $Params['Variable'];
			}

			if(array_key_exists('Value', $Params)) {
				$this->Value = $Params['Value'];
			}
		}
	}

	public function Clone() : \Framework\Database\AColumn {
		if(property_exists($this, 'Value')) {
			return new \Framework\Database\Columns\Value([
				'Variable' => $this->Variable,
				'Value' => $this->Value
			]);
		}
		else {
			return new \Framework\Database\Columns\Value([
				'Variable' => $this->Variable
			]);
		}
	}

	public function SetSourceAlias(string $OldAlias, string $NewAlias) {

	}

	public function SetVariable(string $Key, $Value) {
		if($this->Variable === $Key) {
			$this->Value = $Value;
		}
	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection) {
		if(!property_exists($this, 'Value')) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Value" is not set.',
				[
					'Object' => $this
				]
			);
		}

		if($this->Value === null) {
			return 'NULL';
		}
		else if(is_array($this->Value)) {
			$values = [];

			foreach($this->Value as $value) {
				if($value === null) {
					$values[] = 'NULL';
				}
				else if(is_bool($value)) {
					$values[] = $value ? '1' : '0';
				}
				else if(is_string($value)) {
					$values[] = '"'.$Connection->EscapeString($value).'"';
				}
				else if(is_float($value)) {
					$values[] = str_replace(',', '.', (string)$value);
				}
				else {
					$values[] = (string)$value;
				}
			}

			return '('.implode(', ', $values).')';
		}
		else if(is_bool($this->Value)) {
			return $this->Value ? '1' : '0';
		}
		else if(is_string($this->Value)) {
			return '"'.$Connection->EscapeString($this->Value).'"';
		}
		else if(is_float($this->Value)) {
			return str_replace(',', '.', (string)$this->Value);
		}
		else {
			return (string)$this->Value;
		}
	}
}
?>