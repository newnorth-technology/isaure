<?
namespace Framework\Database\Columns;

class Count extends \Framework\Database\AColumn {
	/* Magic methods */

	public function __construct($Params = null) {

	}

	public function __debugInfo() {
		return [
			'Type' => 'Count'
		];
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return false;
	}

	public function IsNotEmpty() : bool {
		return true;
	}

	public function Set($Params) {

	}

	public function Merge($Params) {

	}

	public function Clone() : \Framework\Database\AColumn {
		return new \Framework\Database\Columns\Count();
	}

	public function SetSourceAlias(string $OldAlias, string $NewAlias) {

	}

	public function SetVariable(string $Key, $Value) {

	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection) {
		return 'COUNT(*)';
	}
}
?>