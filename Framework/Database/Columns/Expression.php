<?
namespace Framework\Database\Columns;

class Expression extends \Framework\Database\AColumn {
	/* Instance variables */

	public $Expression;

	public $Params = [];

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		return [
			'Type' => 'Expression',
			'Expression' => $this->Expression,
			'Params' => $this->Params
		];
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return $this->Expression === null;
	}

	public function IsNotEmpty() : bool {
		return $this->Expression !== null;
	}

	public function Set($Params) {
		if($Params instanceof \Framework\Database\Columns\Expression) {
			$this->Expression = $Params->Expression;

			$this->Params = $Params->Params;
		}
		else {
			$this->Expression = $Params['Expression'];

			$this->Params = $Params['Params'];
		}
	}

	public function Merge($Params) {
		if($Params instanceof \Framework\Database\Columns\Expression) {
			$this->Expression = $Params->Expression;

			$this->Params = array_merge($this->Params, $Params->Params);
		}
		else {
			if(array_key_exists('Expression', $Params)) {
				$this->Expression = $Params['Expression'];
			}

			if(array_key_exists('Params', $Params)) {
				$this->Params = array_merge($this->Params, $Params['Params']);
			}
		}
	}

	public function Clone() : \Framework\Database\AColumn {
		return new \Framework\Database\Columns\Expression([
			'Expression' => $this->Expression,
			'Params' => $this->Params
		]);
	}

	public function SetSourceAlias(string $OldAlias, string $NewAlias) {
		foreach($this->Params as $param) {
			$param->SetSourceAlias($OldAlias, $NewAlias);
		}
	}

	public function SetVariable(string $Key, $Value) {
		foreach($this->Params as $param) {
			$param->SetVariable($Key, $Value);
		}
	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection) {
		if(empty($this->Expression)) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Expression" is null or empty.',
				[
					'Object' => $this
				]
			);
		}

		$expression = $this->Expression;

		foreach($this->Params as $key => $object) {
			$expression = str_replace('?'.$key, $object->ToMySqlQueryString($Connection), $expression);
		}

		return $expression;
	}
}
?>