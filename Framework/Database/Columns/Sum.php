<?
namespace Framework\Database\Columns;

class Sum extends \Framework\Database\AColumn {
	/* Instance variables */

	public $Column;

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		return [
			'Type' => 'Sum',
			'Column' => $this->Column === null ? null : $this->Column->__debugInfo()
		];
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return $this->Column === null || $this->Column->IsEmpty();
	}

	public function IsNotEmpty() : bool {
		return $this->Column !== null && $this->Column->IsNotEmpty();
	}

	public function Set($Params) {
		if($Params instanceof \Framework\Database\Columns\Sum) {
			$this->Column = $Params->Column;
		}
		else {
			$this->SetColumn($Params['Column'] ?? null);
		}
	}

	public function Merge($Params) {
		if($Params instanceof \Framework\Database\Columns\Sum) {
			$this->Column = $Params->Column;
		}
		else {
			if(array_key_exists('Column', $Params)) {
				$this->SetColumn($Params['Column']);
			}
		}
	}

	public function SetColumn($Params) {
		if($Params === null) {
			$this->Column = null;
		}
		else {
			$this->Column = \Framework\Database\ParseColumn($Params);
		}
	}

	public function Clone() : \Framework\Database\AColumn {
		return new \Framework\Database\Columns\Sum([
			'Column' => $this->Column
		]);
	}

	public function SetSourceAlias(string $OldAlias, string $NewAlias) {
		if($this->Column !== null) {
			$this->Column->SetSourceAlias($OldAlias, $NewAlias);
		}
	}

	public function SetVariable(string $Key, $Value) {
		if($this->Column !== null) {
			$this->Column->SetVariable($Key, $Value);
		}
	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection) {
		if($this->Column === null || $this->Column->IsEmpty()) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Column" is null or empty.',
				[
					'Object' => $this
				]
			);
		}

		return 'SUM('.$this->Column->ToMySqlQueryString($Connection).')';
	}
}
?>