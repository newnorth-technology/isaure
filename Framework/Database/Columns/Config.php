<?
namespace Framework\Database\Columns;

class Config extends \Framework\Database\AColumn {
	/* Instance variables */

	public $Path;

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		return [
			'Type' => 'Config',
			'Path' => $this->Path
		];
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return $this->Path === null;
	}

	public function IsNotEmpty() : bool {
		return $this->Path !== null;
	}

	public function Set($Params) {
		if($Params instanceof \Framework\Database\Columns\Config) {
			$this->Path = $Params->Path;
		}
		else {
			$this->Path = $Params['Path'] ?? null;
		}
	}

	public function Merge($Params) {
		if($Params instanceof \Framework\Database\Columns\Config) {
			$this->Path = $Params->Path;
		}
		else {
			if(array_key_exists('Path', $Params)) {
				$this->Path = $Params['Path'];
			}
		}
	}

	public function Clone() : \Framework\Database\AColumn {
		return new \Framework\Database\Columns\Config([
			'Path' => $this->Path
		]);
	}

	public function SetSourceAlias(string $OldAlias, string $NewAlias) {

	}

	public function SetVariable(string $Key, $Value) {

	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection) {
		if($this->Path === null) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Path" is null.',
				[
					'Object' => $this
				]
			);
		}

		$value = \Framework\GetConfig($this->Path);

		if($value === null) {
			return 'NULL';
		}
		else if(is_array($value)) {
			$values = [];

			foreach($value as $ivalue) {
				if($ivalue === null) {
					$values[] = 'NULL';
				}
				else if(is_bool($ivalue)) {
					$values[] = $ivalue ? '1' : '0';
				}
				else if(is_string($ivalue)) {
					$values[] = '"'.$Connection->EscapeString($ivalue).'"';
				}
				else {
					$values[] = (string)$ivalue;
				}
			}

			return '('.implode(', ', $values).')';
		}
		else if(is_bool($value)) {
			return $value ? '1' : '0';
		}
		else if(is_string($value)) {
			return '"'.$Connection->EscapeString($value).'"';
		}
		else {
			return (string)$value;
		}
	}
}
?>