<?
namespace Framework\Database;

class Sorts implements \ArrayAccess {
	/* Instance variables */

	public $Items = [];

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		return array_map(
			function($Item) {
				return $Item->__debugInfo();
			},
			$this->Items
		);
	}

	/* Instance ArrayAccess methods */

	public function offsetSet($Key, $Value) {
		throw new \Framework\NotImplementedException();
	}

	public function offsetExists($Key) {
		return array_key_exists($Key, $this->Items);
	}

	public function offsetUnset($Key) {
		throw new \Framework\NotImplementedException();
	}

	public function offsetGet($Key) {
		return $this->Items[$Key];
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return count($this->Items) === 0;
	}

	public function IsNotEmpty() : bool {
		return 0 < count($this->Items);
	}

	public function Set($Params) {
		if($Params === null) {
			$this->Items = [];
		}
		else if($Params instanceof \Framework\Database\Sorts) {
			$this->Items = $Params->Items;
		}
		else {
			$this->Items = [];

			foreach($Params as $params) {
				if($params instanceof \Framework\Database\Sort) {
					$this->Items[] = $params;
				}
				else {
					$this->Items[] = new \Framework\Database\Sort($params);
				}
			}
		}
	}

	public function Merge($Params) {
		if($Params !== null) {
			if($Params instanceof \Framework\Database\Sorts) {
				foreach($Params->Items as $item) {
					$this->Items[] = $item;
				}
			}
			else {
				foreach($Params as $params) {
					if($params instanceof \Framework\Database\Sort) {
						$this->Items[] = $params;
					}
					else {
						$this->Items[] = new \Framework\Database\Sort($params);
					}
				}
			}
		}
	}

	public function Add($Params) {
		if($Params !== null) {
			if($Params instanceof \Framework\Database\Sort) {
				$this->Items[] = $Params;
			}
			else {
				$this->Items[] = new \Framework\Database\Sort($Params);
			}
		}
	}

	public function Clone() : \Framework\Database\Sorts {
		return new \Framework\Database\Sorts(
			array_map(
				function($Item) {
					return $Item->Clone();
				},
				$this->Items
			)
		);
	}

	public function SetSourceAlias(string $OldAlias, string $NewAlias) {
		foreach($this->Items as $item) {
			$item->SetSourceAlias($OldAlias, $NewAlias);
		}
	}

	public function SetVariable(string $Key, $Value) {
		foreach($this->Items as $item) {
			$item->SetVariable($Key, $Value);
		}
	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection) {
		if(count($this->Items) === 0) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Items" is empty.',
				[
					'Object' => $this
				]
			);
		}

		$items = [];

		foreach($this->Items as $item) {
			$items[] = $item->ToMySqlQueryString($Connection);
		}

		return implode(', ', $items);
	}
}
?>