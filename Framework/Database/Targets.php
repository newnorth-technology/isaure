<?
namespace Framework\Database;

class Targets implements \ArrayAccess, \Iterator {
	/* Instance variables */

	public $Items = [];

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		return array_map(
			function($Item) {
				return $Item->__debugInfo();
			},
			$this->Items
		);
	}

	/* Instance ArrayAccess methods */

	public function offsetSet($Key, $Value) {
		throw new \Framework\NotImplementedException();
	}

	public function offsetExists($Key) {
		return array_key_exists($Key, $this->Items);
	}

	public function offsetUnset($Key) {
		throw new \Framework\NotImplementedException();
	}

	public function offsetGet($Key) {
		return $this->Items[$Key];
	}

	/* Instance Iterator methods */

	public function current() {
		return current($this->Items);
	}

	public function key() {
		return key($this->Items);
	}

	public function next() {
		return next($this->Items);
	}

	public function rewind() {
		return reset($this->Items);
	}

	public function valid() {
		return key($this->Items) !== null;
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return count($this->Items) === 0;
	}

	public function IsNotEmpty() : bool {
		return 0 < count($this->Items);
	}

	public function Set($Params) {
		if($Params === null) {
			$this->Items = [];
		}
		else if($Params instanceof \Framework\Database\Targets) {
			$this->Items = $Params->Items;
		}
		else {
			$this->Items = [];

			foreach($Params as $key => $params) {
				if($key[0] === '<') {
					$key = substr($key, 1);

					$this->Items[$key] = \Framework\Database\ParseTarget($params);
				}
				else {
					$this->Items[$key] = \Framework\Database\ParseTarget($params);
				}
			}
		}
	}

	public function Merge($Params) {
		if($Params !== null) {
			if($Params instanceof \Framework\Database\Targets) {
				foreach($Params->Items as $key => $item) {
					$this->Items[$key] = $item;
				}
			}
			else {
				foreach($Params as $key => $params) {
					if($key[0] === '<') {
						$key = substr($key, 1);

						if(array_key_exists($key, $this->Items)) {
							$this->Items[$key]->Merge($params);
						}
						else {
							$this->Items[$key] = \Framework\Database\ParseTarget($params);
						}
					}
					else {
						$this->Items[$key] = \Framework\Database\ParseTarget($params);
					}
				}
			}
		}
	}

	public function Clone() : \Framework\Database\Targets {
		return new \Framework\Database\Targets(
			array_map(
				function($Item) {
					return $Item->Clone();
				},
				$this->Items
			)
		);
	}

	public function SetVariable(string $Key, $Value) {
		foreach($this->Items as $item) {
			$item->SetVariable($Key, $Value);
		}
	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection) {
		if(count($this->Items) === 0) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Items" is empty.',
				[
					'Object' => $this
				]
			);
		}

		$items = [];

		foreach($this->Items as $item) {
			$items[] = $item->ToMySqlQueryString($Connection);
		}

		return implode(', ', $items);
	}
}
?>