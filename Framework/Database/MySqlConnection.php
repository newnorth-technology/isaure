<?
namespace Framework\Database;

class MySqlConnection extends \Framework\Database\AConnection {
	/* Instance variables */

	public $BaseConnection;

	public $IsConnected = false;

	public $ConnTimeout = 5;

	public $Hostname = null;

	public $Username = null;

	public $Password = null;

	public $Database = null;

	public $CharSet = null;

	/* Magic methods */

	public function __construct(string $Name, array $Params) {
		parent::__construct($Name, $Params);

		$this->ConnTimeout = $Params['ConnTimeout'] ?? $this->ConnTimeout;

		$this->Hostname = $Params['Hostname'] ?? $this->Hostname;

		$this->Username = $Params['Username'] ?? $this->Username;

		$this->Password = $Params['Password'] ?? $this->Password;

		$this->Database = $Params['Database'] ?? $this->Database;

		$this->CharSet = $Params['CharSet'] ?? $this->CharSet;
	}

	public function __debugInfo() {
		return [
			'IsConnected' => $this->IsConnected,
			'ConnTimeout' => $this->ConnTimeout,
			'Hostname' => $this->Hostname,
			'Username' => $this->Username,
			'CharSet' => $this->CharSet,
		];
	}

	/* Instance \Framework\Database\AConnection methods */

	public function Connect() {
		$this->BaseConnection = mysqli_init();

		$this->BaseConnection->options(MYSQLI_OPT_CONNECT_TIMEOUT, $this->ConnTimeout);

		try {
			$this->BaseConnection->real_connect($this->Hostname, $this->Username, $this->Password, $this->Database);
		}
		catch(\Exception $exception) {
			throw new \Framework\RuntimeException(
				'Unable to connect to MySQL.',
				[
					'ErrorNumber' => $this->BaseConnection->connect_errno,
					'ErrorMessage' => utf8_encode($this->BaseConnection->connect_error),
					'Hostname' => $this->Hostname,
					'Username' => $this->Username
				]
			);
		}

		if($this->CharSet !== null) {
			$this->BaseConnection->set_charset($this->CharSet);
		}

		$this->IsConnected = true;
	}

	public function Insert(\Framework\Database\InsertQuery $Query) : int {
		if(!$this->IsConnected) {
			$this->Connect();
		}

		try {
			$query = $Query->ToMySqlQueryString($this);
		}
		catch(\Framework\RuntimeException $Exception) {
			throw new \Framework\RuntimeException(
				'Unable to convert query to MySQL query string.',
				[
					'Query' => $Query
				],
				$Exception
			);
		}

		\Framework\Benchmark::LogData(['QueryString' => $query]);

		$result = $this->BaseConnection->query($query);

		if($result === false) {
			throw new \Framework\RuntimeException(
				'MySQL error when executing query.',
				[
					'Error number' => $this->BaseConnection->errno,
					'Error messages' => $this->BaseConnection->error,
					'Query' => $Query,
					'Query string' => $query
				]
			);
		}

		return $this->BaseConnection->insert_id;
	}

	public function Update(\Framework\Database\UpdateQuery $Query) : int {
		if(!$this->IsConnected) {
			$this->Connect();
		}

		try {
			$query = $Query->ToMySqlQueryString($this);
		}
		catch(\Framework\RuntimeException $Exception) {
			throw new \Framework\RuntimeException(
				'Unable to convert query to MySQL query string.',
				[
					'Query' => $Query
				],
				$Exception
			);
		}

		\Framework\Benchmark::LogData(['QueryString' => $query]);

		$result = $this->BaseConnection->query($query);

		if($result === false) {
			throw new \Framework\RuntimeException(
				'MySQL error when executing query.',
				[
					'Error number' => $this->BaseConnection->errno,
					'Error messages' => $this->BaseConnection->error,
					'Query' => $Query,
					'Query string' => $query
				]
			);
		}

		return $this->BaseConnection->affected_rows;
	}

	public function Delete(\Framework\Database\DeleteQuery $Query) : int {
		if(!$this->IsConnected) {
			$this->Connect();
		}

		try {
			$query = $Query->ToMySqlQueryString($this);
		}
		catch(\Framework\RuntimeException $Exception) {
			throw new \Framework\RuntimeException(
				'Unable to convert query to MySQL query string.',
				[
					'Query' => $Query
				],
				$Exception
			);
		}

		\Framework\Benchmark::LogData(['QueryString' => $query]);

		$result = $this->BaseConnection->query($query);

		if($result === false) {
			throw new \Framework\RuntimeException(
				'MySQL error when executing query.',
				[
					'Error number' => $this->BaseConnection->errno,
					'Error messages' => $this->BaseConnection->error,
					'Query' => $Query,
					'Query string' => $query
				]
			);
		}

		return $this->BaseConnection->affected_rows;
	}

	public function Find(\Framework\Database\SelectQuery $Query) : \Framework\Database\AResult {
		if(!$this->IsConnected) {
			$this->Connect();
		}

		try {
			$query = $Query->ToMySqlQueryString($this);
		}
		catch(\Framework\RuntimeException $Exception) {
			throw new \Framework\RuntimeException(
				'Unable to convert query to MySQL query string.',
				[
					'Query' => $Query
				],
				$Exception
			);
		}

		\Framework\Benchmark::LogData(['QueryString' => $query]);

		$result = $this->BaseConnection->query($query);

		if($result === false) {
			throw new \Framework\RuntimeException(
				'MySQL error when executing query.',
				[
					'Error number' => $this->BaseConnection->errno,
					'Error messages' => $this->BaseConnection->error,
					'Query string' => $query,
					'Query' => $Query->__debugInfo()
				]
			);
		}

		return new \Framework\Database\MySqlResult($result);
	}

	public function FindAll(\Framework\Database\SelectQuery $Query) : \Framework\Database\AResult {
		if(!$this->IsConnected) {
			$this->Connect();
		}

		try {
			$query = $Query->ToMySqlQueryString($this);
		}
		catch(\Framework\RuntimeException $Exception) {
			throw new \Framework\RuntimeException(
				'Unable to convert query to MySQL query string.',
				[
					'Query' => $Query
				],
				$Exception
			);
		}

		\Framework\Benchmark::LogData(['QueryString' => $query]);

		$result = $this->BaseConnection->query($query);

		if($result === false) {
			throw new \Framework\RuntimeException(
				'MySQL error when executing query.',
				[
					'Error number' => $this->BaseConnection->errno,
					'Error messages' => $this->BaseConnection->error,
					'Query string' => $query,
					'Query' => $Query->__debugInfo()
				]
			);
		}

		return new \Framework\Database\MySqlResult($result);
	}

	public function Count(\Framework\Database\SelectQuery $Query) : int {
		if(!$this->IsConnected) {
			$this->Connect();
		}

		try {
			$query = $Query->ToMySqlQueryString($this);
		}
		catch(\Framework\RuntimeException $Exception) {
			throw new \Framework\RuntimeException(
				'Unable to convert query to MySQL query string.',
				[
					'Query' => $Query
				],
				$Exception
			);
		}

		\Framework\Benchmark::LogData(['QueryString' => $query]);

		$result = $this->BaseConnection->query($query);

		if($result === false) {
			throw new \Framework\RuntimeException(
				'MySQL error when executing query.',
				[
					'Error number' => $this->BaseConnection->errno,
					'Error messages' => $this->BaseConnection->error,
					'Query string' => $query,
					'Query' => $Query->__debugInfo()
				]
			);
		}

		$row = $result->fetch_row();

		if($row === null) {
			return 0;
		}
		else {
			return (int)$row[0];
		}
	}

	public function SumFloat(\Framework\Database\SelectQuery $Query) : float {
		if(!$this->IsConnected) {
			$this->Connect();
		}

		try {
			$query = $Query->ToMySqlQueryString($this);
		}
		catch(\Framework\RuntimeException $Exception) {
			throw new \Framework\RuntimeException(
				'Unable to convert query to MySQL query string.',
				[
					'Query' => $Query
				],
				$Exception
			);
		}

		\Framework\Benchmark::LogData(['QueryString' => $query]);

		$result = $this->BaseConnection->query($query);

		if($result === false) {
			throw new \Framework\RuntimeException(
				'MySQL error when executing query.',
				[
					'Error number' => $this->BaseConnection->errno,
					'Error messages' => $this->BaseConnection->error,
					'Query string' => $query,
					'Query' => $Query->__debugInfo()
				]
			);
		}

		$row = $result->fetch_row();

		if($row === null) {
			return 0;
		}
		else {
			return (float)$row[0];
		}
	}

	public function SumInt(\Framework\Database\SelectQuery $Query) : int {
		if(!$this->IsConnected) {
			$this->Connect();
		}

		try {
			$query = $Query->ToMySqlQueryString($this);
		}
		catch(\Framework\RuntimeException $Exception) {
			throw new \Framework\RuntimeException(
				'Unable to convert query to MySQL query string.',
				[
					'Query' => $Query
				],
				$Exception
			);
		}

		\Framework\Benchmark::LogData(['QueryString' => $query]);

		$result = $this->BaseConnection->query($query);

		if($result === false) {
			throw new \Framework\RuntimeException(
				'MySQL error when executing query.',
				[
					'Error number' => $this->BaseConnection->errno,
					'Error messages' => $this->BaseConnection->error,
					'Query string' => $query,
					'Query' => $Query->__debugInfo()
				]
			);
		}

		$row = $result->fetch_row();

		if($row === null) {
			return 0;
		}
		else {
			return (int)$row[0];
		}
	}

	public function Truncate(\Framework\Database\Source $Source) {
		if(!$this->IsConnected) {
			$this->Connect();
		}

		try {
			$source = $Source->ToMySqlQueryString($this);
		}
		catch(\Framework\RuntimeException $Exception) {
			throw new \Framework\RuntimeException(
				'Unable to convert source to MySQL query string.',
				[
					'Source' => $Source
				],
				$Exception
			);
		}

		$query = 'TRUNCATE '.$source;

		$result = $this->BaseConnection->query($query);

		if($result === false) {
			throw new \Framework\RuntimeException(
				'MySQL error when truncating table.',
				[
					'Error number' => $this->BaseConnection->errno,
					'Error messages' => $this->BaseConnection->error,
					'Source' => $Source,
					'Query string' => $query
				]
			);
		}
	}

	public function Lock(array $Sources) {
		if(!$this->IsConnected) {
			$this->Connect();
		}

		$sources = array_map(
			function(\Framework\Database\Source $Source, $Alias) {
				try {
					$source = $Source->ToMySqlQueryString($this);

					if(is_int($Alias)) {
						return $source.' WRITE';
					}
					else {
						return $source.' AS `'.$Alias.'` WRITE';
					}
				}
				catch(\Framework\RuntimeException $Exception) {
					throw new \Framework\RuntimeException(
						'Unable to convert source to MySQL query string.',
						[
							'Source' => $Source
						],
						$Exception
					);
				}
			},
			$Sources,
			array_keys($Sources)
		);

		$query = 'LOCK TABLES '.implode(', ', $sources);

		$result = $this->BaseConnection->query($query);

		if($result === false) {
			throw new \Framework\RuntimeException(
				'MySQL error when locking table(s).',
				[
					'Error number' => $this->BaseConnection->errno,
					'Error messages' => $this->BaseConnection->error,
					'Sources' => $Sources
				]
			);
		}
	}

	public function Unlock(array $Sources) {
		if(!$this->IsConnected) {
			$this->Connect();
		}

		$result = $this->BaseConnection->query('UNLOCK TABLES');

		if($result === false) {
			throw new \Framework\RuntimeException(
				'MySQL error when unlocking table(s).',
				[
					'Error number' => $this->BaseConnection->errno,
					'Error messages' => $this->BaseConnection->error,
					'Sources' => $Sources
				]
			);
		}
	}

	/* Instance methods */

	public function Query(string $query) {
		if(!$this->IsConnected) {
			$this->Connect();
		}

		\Framework\Benchmark::LogData(['QueryString' => $query]);

		$result = $this->BaseConnection->query($query);

		if($result === false) {
			throw new \Framework\RuntimeException(
				'MySQL error when executing query.',
				[
					'Error number' => $this->BaseConnection->errno,
					'Error messages' => $this->BaseConnection->error,
					'Query string' => $query
				]
			);
		}

		if($result === true) {
			return true;
		}
		else {
			return new \Framework\Database\MySqlResult($result);
		}
	}

	public function EscapeString(string $String) : string {
		if(!$this->IsConnected) {
			$this->Connect();
		}

		return $this->BaseConnection->real_escape_string($String);
	}

	public function LastInsertId() : int {
		return $this->BaseConnection->insert_id;
	}

	public function AffectedRows() : int {
		return $this->BaseConnection->affected_rows;
	}
}
?>