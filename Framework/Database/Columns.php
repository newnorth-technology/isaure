<?
namespace Framework\Database;

class Columns implements \ArrayAccess, \Iterator {
	/* Instance variables */

	public $Items = [];

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		return array_map(
			function($Item) {
				return $Item->__debugInfo();
			},
			$this->Items
		);
	}

	/* Instance ArrayAccess methods */

	public function offsetSet($Key, $Value) {
		throw new \Framework\NotImplementedException();
	}

	public function offsetExists($Key) {
		return array_key_exists($Key, $this->Items);
	}

	public function offsetUnset($Key) {
		throw new \Framework\NotImplementedException();
	}

	public function offsetGet($Key) {
		return $this->Items[$Key];
	}

	/* Instance Iterator methods */

	public function current() {
		return current($this->Items);
	}

	public function key() {
		return key($this->Items);
	}

	public function next() {
		return next($this->Items);
	}

	public function rewind() {
		return reset($this->Items);
	}

	public function valid() {
		return key($this->Items) !== null;
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return count($this->Items) === 0;
	}

	public function IsNotEmpty() : bool {
		return 0 < count($this->Items);
	}

	public function Set($Params) {
		if($Params === null) {
			$this->Items = [];
		}
		else if($Params instanceof \Framework\Database\Columns) {
			$this->Items = $Params->Items;
		}
		else {
			$this->Items = [];

			foreach($Params as $key => $params) {
				$this->Items[$key] = \Framework\Database\ParseColumn($params);
			}
		}
	}

	public function Merge($Params) {
		if($Params !== null) {
			if($Params instanceof \Framework\Database\Columns) {
				foreach($Params->Items as $key => $item) {
					if(is_int($key)) {
						$this->Items[] = $item;
					}
					else {
						$this->Items[$key] = $item;
					}
				}
			}
			else {
				foreach($Params as $key => $params) {
					if(is_int($key)) {
						$this->Items[] = \Framework\Database\ParseColumn($params);
					}
					else {
						$this->Items[$key] = \Framework\Database\ParseColumn($params);
					}
				}
			}
		}
	}

	public function AddColumn(string $Key, $Params) {
		$this->Items[$Key] = \Framework\Database\ParseColumn($Params);
	}

	public function Clone() : \Framework\Database\Columns {
		return new \Framework\Database\Columns(
			array_map(
				function($Item) {
					return $Item->Clone();
				},
				$this->Items
			)
		);
	}

	public function SetSourceAlias(string $OldAlias, string $NewAlias) {
		foreach($this->Items as $item) {
			$item->SetSourceAlias($OldAlias, $NewAlias);
		}
	}

	public function SetVariable(string $Key, $Value) {
		foreach($this->Items as $item) {
			$item->SetVariable($Key, $Value);
		}
	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection, bool $UseAliases = true) {
		if(count($this->Items) === 0) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Items" is empty.',
				[
					'Object' => $this
				]
			);
		}

		$items = [];

		if($UseAliases) {
			foreach($this->Items as $alias => $item) {
				if(is_int($alias)) {
					$items[] = $item->ToMySqlQueryString($Connection, true);
				}
				else if($item instanceof \Framework\Database\Columns\Reference && $item->Name === $alias) {
					$items[] = $item->ToMySqlQueryString($Connection, true);
				}
				else {
					$items[] = $item->ToMySqlQueryString($Connection, true).' AS `'.$alias.'`';
				}
			}
		}
		else {
			foreach($this->Items as $alias => $item) {
				$items[] = $item->ToMySqlQueryString($Connection, false);
			}
		}

		return implode(', ', $items);
	}
}
?>