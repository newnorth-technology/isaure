<?
namespace Framework\Database;

class UpdateQuery {
	/* Instance variables */

	public $Sources;

	public $Changes;

	public $Condition;

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		return [
			'Sources' => $this->Sources === null ? null : $this->Sources->__debugInfo(),
			'Changes' => $this->Changes === null ? null : $this->Changes->__debugInfo(),
			'Condition' => $this->Condition === null ? null : $this->Condition->__debugInfo()
		];
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return $this->Sources === null || $this->Changes === null || $this->Sources->IsEmpty() || $this->Changes->IsEmpty();
	}

	public function IsNotEmpty() : bool {
		return $this->Sources !== null && $this->Changes !== null && $this->Sources->IsNotEmpty() && $this->Changes->IsNotEmpty();
	}

	public function Set($Params) {
		if($Params instanceof \Framework\Database\UpdateQuery) {
			$this->Sources = $Params->Sources;

			$this->Changes = $Params->Changes;

			$this->Condition = $Params->Condition;
		}
		else {
			$this->SetSources($Params['Sources'] ?? null);

			if(array_key_exists('<Sources', $Params)) {
				$this->MergeSources($Params['<Sources']);
			}

			$this->SetChanges($Params['Changes'] ?? null);

			if(array_key_exists('<Changes', $Params)) {
				$this->MergeChanges($Params['<Changes']);
			}

			$this->SetCondition($Params['Condition'] ?? null);
		}
	}

	public function Merge($Params) {
		if($Params instanceof \Framework\Database\UpdateQuery) {
			$this->Sources = $Params->Sources;

			$this->Changes = $Params->Changes;

			$this->Condition = $Params->Condition;
		}
		else {
			if(array_key_exists('Sources', $Params)) {
				$this->SetSources($Params['Sources']);
			}

			if(array_key_exists('<Sources', $Params)) {
				$this->MergeSources($Params['<Sources']);
			}

			if(array_key_exists('Changes', $Params)) {
				$this->SetChanges($Params['Changes']);
			}

			if(array_key_exists('<Changes', $Params)) {
				$this->MergeChanges($Params['<Changes']);
			}

			if(array_key_exists('Condition', $Params)) {
				$this->SetCondition($Params['Condition']);
			}
		}
	}

	public function SetSources($Params) {
		if($Params === null) {
			$this->Sources = null;
		}
		else if($Params instanceof \Framework\Database\Sources) {
			$this->Sources = $Params;
		}
		else {
			$this->Sources = new \Framework\Database\Sources($Params);
		}
	}

	public function MergeSources($Params) {
		if($Params === null) {
			return;
		}

		if($this->Sources === null) {
			if($Params instanceof \Framework\Database\Sources) {
				$this->Sources = $Params;
			}
			else {
				$this->Sources = new \Framework\Database\Sources($Params);
			}
		}
		else {
			$this->Sources->Merge($Params);
		}
	}

	public function SetChanges($Params) {
		if($Params === null) {
			$this->Changes = null;
		}
		else if($Params instanceof \Framework\Database\Changes) {
			$this->Changes = $Params;
		}
		else {
			$this->Changes = new \Framework\Database\Changes($Params);
		}
	}

	public function MergeChanges($Params) {
		if($Params === null) {
			return;
		}

		if($this->Changes === null) {
			if($Params instanceof \Framework\Database\Changes) {
				$this->Changes = $Params;
			}
			else {
				$this->Changes = new \Framework\Database\Changes($Params);
			}
		}
		else {
			$this->Changes->Merge($Params);
		}
	}

	public function SetCondition($Params) {
		if($Params === null) {
			$this->Condition = null;
		}
		else {
			$this->Condition = \Framework\Database\ParseCondition($Params);
		}
	}

	public function Clone() : \Framework\Database\UpdateQuery {
		return new \Framework\Database\UpdateQuery([
			'Sources' => $this->Sources === null ? null : $this->Sources->Clone(),
			'Changes' => $this->Changes === null ? null : $this->Changes->Clone(),
			'Condition' => $this->Condition === null ? null : $this->Condition->Clone()
		]);
	}

	public function SetVariable(string $Key, $Value) {
		if($this->Sources !== null) {
			$this->Sources->SetVariable($Key, $Value);
		}

		if($this->Changes !== null) {
			$this->Changes->SetVariable($Key, $Value);
		}

		if($this->Condition !== null) {
			$this->Condition->SetVariable($Key, $Value);
		}
	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection) {
		if($this->Sources === null || $this->Sources->IsEmpty()) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Sources" is null or empty.',
				[
					'Object' => $this
				]
			);
		}

		if($this->Changes === null || $this->Changes->IsEmpty()) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Changes" is null or empty.',
				[
					'Object' => $this
				]
			);
		}

		$query = 'UPDATE '.$this->Sources->ToMySqlQueryString($Connection).' SET '.$this->Changes->ToMySqlQueryString($Connection);

		if($this->Condition !== null && $this->Condition->IsNotEmpty()) {
			$query .= ' WHERE '.$this->Condition->ToMySqlQueryString($Connection);
		}

		return $query;
	}
}
?>