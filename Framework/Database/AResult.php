<?
namespace Framework\Database;

abstract class AResult {
	/* Instance variables */

	public $Rows;

	public $Row;

	/* Instance methods */

	public abstract function Fetch() : bool;

	public abstract function FetchAssoc() : bool;

	public abstract function GetBoolean($Column);

	public abstract function GetFloat($Column);

	public abstract function GetInt($Column);

	public abstract function GetString($Column);

	public abstract function IsFalse($Column) : bool;

	public abstract function IsTrue($Column) : bool;

	public abstract function IsNull($Column) : bool;
}
?>