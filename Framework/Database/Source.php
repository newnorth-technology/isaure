<?
namespace Framework\Database;

class Source {
	/* Instance variables */

	public $DataManager;

	public $Alias;

	public $Joins;

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		return [
			'DataManager' => $this->DataManager === null ? null : $this->DataManager->Id,
			'Alias' => $this->Alias,
			'Joins' => $this->Joins === null ? null : $this->Joins->__debugInfo()
		];
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return $this->DataManager === null;
	}

	public function IsNotEmpty() : bool {
		return $this->DataManager !== null;
	}

	public function Set($Params) {
		if($Params instanceof \Framework\Database\Join) {
			$this->DataManager = $Params->DataManager;

			$this->Alias = $Params->Alias;

			$this->Joins = $Params->Joins;
		}
		else {
			$this->DataManager = $Params['DataManager'] ?? null;

			$this->Alias = $Params['Alias'] ?? null;

			$this->SetJoins($Params['Joins'] ?? null);

			if(array_key_exists('<Joins', $Params)) {
				$this->MergeJoins($Params['<Joins']);
			}
		}
	}

	public function Merge($Params) {
		if($Params instanceof \Framework\Database\Join) {
			$this->DataManager = $Params->DataManager;

			$this->Alias = $Params->Alias;

			$this->Joins = $Params->Joins;
		}
		else {
			if(array_key_exists('DataManager', $Params)) {
				$this->DataManager = $Params['DataManager'];
			}

			if(array_key_exists('Alias', $Params)) {
				$this->Alias = $Params['Alias'];
			}

			if(array_key_exists('Joins', $Params)) {
				$this->MergeJoins($Params['Joins']);
			}

			if(array_key_exists('<Joins', $Params)) {
				$this->MergeJoins($Params['<Joins']);
			}
		}
	}

	public function SetJoins($Params) {
		if($Params === null) {
			$this->Joins = null;
		}
		else if($Params instanceof \Framework\Database\Joins) {
			$this->Joins = $Params;
		}
		else {
			$this->Joins = new \Framework\Database\Joins($Params);
		}
	}

	public function MergeJoins($Params) {
		if($Params !== null) {
			if($this->Joins === null) {
				if($Params instanceof \Framework\Database\Joins) {
					$this->Joins = $Params;
				}
				else {
					$this->Joins = new \Framework\Database\Joins($Params);
				}
			}
			else {
				$this->Joins->Merge($Params);
			}
		}
	}

	public function AddJoin(string $Key, $Params) {
		if($this->Joins === null) {
			$this->Joins = new \Framework\Database\Joins([$Key => $Params]);
		}
		else {
			$this->Joins->AddJoin($Key, $Params);
		}
	}

	public function Clone(array $pv_params = []) : \Framework\Database\Source {
		$params = [
			'DataManager' => $this->DataManager,
			'Alias' => $this->Alias,
			'Joins' => $this->Joins === null ? null : $this->Joins->Clone()
		];

		if(array_key_exists('joins', $pv_params)) {
			$params['Joins'] = new \Framework\Database\Joins($pv_params['joins']);
		}
		else if($this->Joins !== null) {
			$params['Joins'] = $this->Joins->Clone();
		}
		else {
			$params['Joins'] = new \Framework\Database\Joins();
		}

		if(array_key_exists('+joins', $pv_params)) {
			foreach($pv_params['+joins'] as $name => $source) {
				if($name[0] === '+') {
					$name = substr($name, 1);

					if(isset($params['Joins'][$name])) {
						$params['Joins'][$name] = $params['Joins'][$name]->Clone($source);
					}
					else {
						$params['Joins'][$name] = $source;
					}
				}
				else {
					$params['Joins'][$name] = $source;
				}
			}
		}

		return new \Framework\Database\Source($params);
	}

	public function SetSourceAlias(string $OldAlias, string $NewAlias) {
		if($this->Alias === null) {
			if($this->DataManager !== null && $this->DataManager->Id === $OldAlias) {
				$this->Alias = $NewAlias;
			}
		}
		else if($this->Alias === $OldAlias) {
			$this->Alias = $NewAlias;
		}

		if($this->Joins !== null) {
			$this->Joins->SetSourceAlias($OldAlias, $NewAlias);
		}
	}

	public function SetVariable(string $Key, $Value) {
		if($this->Joins !== null) {
			$this->Joins->SetVariable($Key, $Value);
		}
	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection) {
		if($this->DataManager === null) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "DataManager" is null.',
				[
					'Object' => $this
				]
			);
		}

		if($this->DataManager instanceof \Framework\ADataManager) {
			$dm = $this->DataManager;
		}
		else {
			$dm = \Framework\GetDataManager($this->DataManager);
		}

		if($dm->Connection === $Connection) {
			$database = $dm->Database;

			$table = $dm->Table;
		}
		else if(isset($dm->Slaves[$Connection->Name])) {
			$database = $dm->Slaves[$Connection->Name]->Database;

			$table = $dm->Slaves[$Connection->Name]->Table;
		}
		else {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, unsupported connection for data manager.',
				[
					'Object' => $this
				]
			);
		}

		if($database === null) {
			if($this->Alias === null || $this->Alias === $table) {
				$query = '`'.$table.'`';
			}
			else {
				$query = '`'.$table.'` AS `'.$this->Alias.'`';
			}
		}
		else {
			if($this->Alias === null || $this->Alias === $table) {
				$query = '`'.$database.'`.`'.$table.'`';
			}
			else {
				$query = '`'.$database.'`.`'.$table.'` AS `'.$this->Alias.'`';
			}
		}

		if($this->Joins !== null && $this->Joins->IsNotEmpty()) {
			foreach($this->Joins->Items as $join) {
				$query = $query.' '.$join->ToMySqlQueryString($Connection);
			}

			$query = '('.$query.')';
		}

		return $query;
	}
}

function ParseSource($Params) : \Framework\Database\Source {
	if($Params === null) {
		throw new \Framework\RuntimeException(
			'Unable to parse "Params" as a "\\Framework\\Database\\Source", "Params" are null.'
		);
	}
	else if($Params instanceof \Framework\Database\Source) {
		return $Params;
	}
	else if($Params instanceof \Framework\ADataManager) {
		return new \Framework\Database\Source([
			'DataManager' => $Params,
			'Alias' => $Params->Id
		]);
	}
	else if(is_array($Params)) {
		return new \Framework\Database\Source($Params);
	}
	else {
		throw new \Framework\RuntimeException(
			'Unable to parse "Params" as a "\\Framework\\Database\\Source".',
			[
				'Params' => $Params,
				'Type' => gettype($Params)
			]
		);
	}
}
?>