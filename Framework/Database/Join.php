<?
namespace Framework\Database;

class Join {
	/* Instance variables */

	public $IsRequired;

	public $Sources;

	public $Condition;

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		return [
			'IsRequired' => $this->IsRequired,
			'Sources' => $this->Sources === null ? null : $this->Sources->__debugInfo(),
			'Condition' => $this->Condition === null ? null : $this->Condition->__debugInfo()
		];
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return $this->IsRequired === null || $this->Sources === null || $this->Condition === null || $this->Sources->IsEmpty() || $this->Condition->IsEmpty();
	}

	public function IsNotEmpty() : bool {
		return $this->IsRequired !== null && $this->Sources !== null && $this->Condition !== null && $this->Sources->IsNotEmpty() && $this->Condition->IsNotEmpty();
	}

	public function Set($Params) {
		if($Params instanceof \Framework\Database\Join) {
			$this->IsRequired = $Params->IsRequired;

			$this->Sources = $Params->Sources;

			$this->Condition = $Params->Condition;
		}
		else {
			$this->IsRequired = $Params['IsRequired'] ?? null;

			$this->SetSources($Params['Sources'] ?? null);

			if(array_key_exists('<Sources', $Params)) {
				$this->MergeSources($Params['<Sources']);
			}

			$this->SetCondition($Params['Condition'] ?? null);
		}
	}

	public function Merge($Params) {
		if($Params instanceof \Framework\Database\Join) {
			$this->IsRequired = $Params->IsRequired;

			$this->Sources = $Params->Sources;

			$this->Condition = $Params->Condition;
		}
		else {
			if(array_key_exists('IsRequired', $Params)) {
				$this->IsRequired = $Params['IsRequired'];
			}

			if(array_key_exists('Sources', $Params)) {
				$this->SetSources($Params['Sources']);
			}

			if(array_key_exists('<Sources', $Params)) {
				$this->MergeSources($Params['<Sources']);
			}

			if(array_key_exists('Condition', $Params)) {
				$this->SetCondition($Params['Condition']);
			}
		}
	}

	public function SetSources($Params) {
		if($Params === null) {
			$this->Sources = null;
		}
		else if($Params instanceof \Framework\Database\Sources) {
			$this->Sources = $Params;
		}
		else {
			$this->Sources = new \Framework\Database\Sources($Params);
		}
	}

	public function MergeSources($Params) {
		if($Params !== null) {
			if($this->Sources === null) {
				if($Params instanceof \Framework\Database\Sources) {
					$this->Sources = $Params;
				}
				else {
					$this->Sources = new \Framework\Database\Sources($Params);
				}
			}
			else {
				$this->Sources->Merge($Params);
			}
		}
	}

	public function SetCondition($Params) {
		if($Params === null) {
			$this->Condition = null;
		}
		else {
			$this->Condition = \Framework\Database\ParseCondition($Params);
		}
	}

	public function Clone(array $pv_params = []) : \Framework\Database\Join {
		return new \Framework\Database\Join([
			'IsRequired' => $this->IsRequired,
			'Sources' => $this->Sources === null ? null : $this->Sources->Clone(),
			'Condition' => $this->Condition === null ? null : $this->Condition->Clone()
		]);
	}

	public function SetSourceAlias(string $OldAlias, string $NewAlias) {
		if($this->Sources !== null) {
			$this->Sources->SetSourceAlias($OldAlias, $NewAlias);
		}

		if($this->Condition !== null) {
			$this->Condition->SetSourceAlias($OldAlias, $NewAlias);
		}
	}

	public function SetVariable(string $Key, $Value) {
		if($this->Sources !== null) {
			$this->Sources->SetVariable($Key, $Value);
		}

		if($this->Condition !== null) {
			$this->Condition->SetVariable($Key, $Value);
		}
	}

	public function ToMySqlQueryString(\Framework\Database\MySqlConnection $Connection) {
		if($this->IsRequired === null) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "IsRequired" is null.',
				[
					'Object' => $this
				]
			);
		}

		if($this->Sources === null || $this->Sources->IsEmpty()) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Sources" is null or empty.',
				[
					'Object' => $this
				]
			);
		}

		if($this->Condition === null || $this->Condition->IsEmpty()) {
			throw new \Framework\RuntimeException(
				'Unable to convert to MySQL query string, "Condition" is null or empty.',
				[
					'Object' => $this
				]
			);
		}

		return ($this->IsRequired ? 'INNER' : 'LEFT').' JOIN '.$this->Sources->ToMySqlQueryString($Connection).' ON '.$this->Condition->ToMySqlQueryString($Connection);
	}
}

function ParseJoin($Params) : \Framework\Database\Join {
	if($Params === null) {
		throw new \Framework\RuntimeException(
			'Unable to parse "Params" as a "\\Framework\\Database\\Join", "Params" are null.'
		);
	}
	else if($Params instanceof \Framework\Database\Join) {
		return $Params;
	}
	else if(is_array($Params)) {
		return new \Framework\Database\Join($Params);
	}
	else {
		throw new \Framework\RuntimeException(
			'Unable to parse "Params" as a "\\Framework\\Database\\Join".',
			[
				'Params' => $Params,
				'Type' => gettype($Params)
			]
		);
	}
}
?>