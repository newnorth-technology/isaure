<?
namespace Framework\Database;

class Joins implements \ArrayAccess {
	/* Instance variables */

	public $Items = [];

	/* Magic methods */

	public function __construct($Params = null) {
		$this->Set($Params);
	}

	public function __debugInfo() {
		return array_map(
			function($Item) {
				return $Item->__debugInfo();
			},
			$this->Items
		);
	}

	/* Instance ArrayAccess methods */

	public function offsetSet($pv_key, $pv_value) {
		$this->Items[$pv_key] = \Framework\Database\ParseJoin($pv_value);
	}

	public function offsetExists($pv_key) {
		return array_key_exists($pv_key, $this->Items);
	}

	public function offsetUnset($pv_key) {
		throw new \Framework\NotImplementedException();
	}

	public function offsetGet($pv_key) {
		return $this->Items[$pv_key];
	}

	/* Instance methods */

	public function IsEmpty() : bool {
		return count($this->Items) === 0;
	}

	public function IsNotEmpty() : bool {
		return 0 < count($this->Items);
	}

	public function Set($Params) {
		if($Params === null) {
			$this->Items = [];
		}
		else if($Params instanceof \Framework\Database\Joins) {
			$this->Items = $Params->Items;
		}
		else {
			$this->Items = [];

			foreach($Params as $key => $params) {
				if($key[0] === '<') {
					$key = substr($key, 1);

					$this->Items[$key] = \Framework\Database\ParseJoin($params);
				}
				else {
					$this->Items[$key] = \Framework\Database\ParseJoin($params);
				}
			}
		}
	}

	public function Merge($Params) {
		if($Params !== null) {
			if($Params instanceof \Framework\Database\Joins) {
				foreach($Params->Items as $key => $item) {
					$this->Items[$key] = $item;
				}
			}
			else {
				foreach($Params as $key => $params) {
					if(is_int($key)) {
						$this->Items[] = \Framework\Database\ParseJoin($params);
					}
					else if($key[0] === '<') {
						$key = substr($key, 1);

						if(array_key_exists($key, $this->Items)) {
							$this->Items[$key]->Merge($params);
						}
						else {
							$this->Items[$key] = \Framework\Database\ParseJoin($params);
						}
					}
					else {
						$this->Items[$key] = \Framework\Database\ParseJoin($params);
					}
				}
			}
		}
	}

	public function AddJoin(string $Key, $Params) {
		$this->Items[$Key] = \Framework\Database\ParseJoin($Params);
	}

	public function Clone() : \Framework\Database\Joins {
		return new \Framework\Database\Joins(
			array_map(
				function($Item) {
					return $Item->Clone();
				},
				$this->Items
			)
		);
	}

	public function SetSourceAlias(string $OldAlias, string $NewAlias) {
		foreach($this->Items as $item) {
			$item->SetSourceAlias($OldAlias, $NewAlias);
		}
	}

	public function SetVariable(string $Key, $Value) {
		foreach($this->Items as $item) {
			$item->SetVariable($Key, $Value);
		}
	}
}
?>