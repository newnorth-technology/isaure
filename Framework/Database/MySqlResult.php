<?
namespace Framework\Database;

class MySqlResult extends \Framework\Database\AResult {
	/* Instance variables */

	private $Result;

	/* Constructor */

	public function __construct(\mysqli_result $Result) {
		$this->Result = $Result;

		$this->Rows = $Result->num_rows;
	}

	public function __destruct() {
		$this->Result->close();
	}

	/* Instance methods */

	public function Fetch() : bool {
		return ($this->Row = $this->Result->fetch_row()) !== null;
	}

	public function FetchAssoc() : bool {
		return ($this->Row = $this->Result->fetch_assoc()) !== null;
	}

	public function GetBoolean($Column) {
		return $this->Row[$Column] === null ? null : (bool)$this->Row[$Column];
	}

	public function GetFloat($Column) {
		return $this->Row[$Column] === null ? null : (float)$this->Row[$Column];
	}

	public function GetInt($Column) {
		return $this->Row[$Column] === null ? null : (int)$this->Row[$Column];
	}

	public function GetString($Column) {
		return $this->Row[$Column] === null ? null : (string)$this->Row[$Column];
	}

	public function IsFalse($Column) : bool {
		return $this->Row[$Column] === 0;
	}

	public function IsTrue($Column) : bool {
		return $this->Row[$Column] === 1;
	}

	public function IsNull($Column) : bool {
		return $this->Row[$Column] === null;
	}

	public function IsNotNull($Column) : bool {
		return $this->Row[$Column] !== null;
	}
}
?>