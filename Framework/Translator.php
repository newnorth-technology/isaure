<?
namespace Framework;

class Translator {
	/* Static methods */

	public static function TryFindTarget(string $Text, int $Offset, string &$Target = null, int &$Position = -1) : bool {
		while(self::TryFindTarget»FindStart($Text, $Offset, $start)) {
			if(self::TryFindTarget»FindEnd($Text, $start + 2, $end)) {
				$Target = substr($Text, $start + 1, $end - $start - 1);

				$Position = $start;

				return true;
			}
			else {
				$Offset = $start + 2;
			}
		}

		return false;
	}

	private static function TryFindTarget»FindStart(string $Text, int $Offset, int &$Position = null) : bool {
		$position = strpos($Text, '%[', $Offset);

		if($position === false) {
			return false;
		}

		$Position = $position;

		return true;
	}

	private static function TryFindTarget»FindEnd(string $Text, int $Offset, int &$Position = null) : bool {
		$start = strpos($Text, '%[', $Offset);

		$finish = strpos($Text, ']%', $Offset);

		$level = 0;

		while($finish !== false) {
			if($start !== false && $start < $finish) {
				$start = strpos($Text, '%[', $start + 2);

				++$level;
			}
			else if(0 < $level) {
				$finish = strpos($Text, ']%', $finish + 2);

				--$level;
			}
			else {
				$Position = $finish + 1;

				return true;
			}
		}

		return false;
	}

	public static function FindTargets(string $Text) : array {
		$targets = [];

		$offset = 0;

		while(self::TryFindTarget»FindStart($Text, $offset, $start)) {
			if(self::TryFindTarget»FindEnd($Text, $start + 2, $end)) {
				$target = substr($Text, $start + 1, $end - $start - 1);

				if(!in_array($target, $targets, true)) {
					$targets[] = $target;
				}

				$offset = $end + 1;
			}
			else {
				$offset = $start + 2;
			}
		}

		return $targets;
	}

	public static function Translate(string $Contents, array $Translations = null, string $Locale = null) : string {
		$offset = 0;

		while(self::TryFindTarget($Contents, $offset, $target, $offset)) {
			if(($targets = json_decode($target, true)) !== null) {
				if(self::TryGetTranslation($targets, $Translations, $Locale, $translation, $index)) {
					if($index !== null && \Framework\StrStartsWith($translation, '%[') && \Framework\StrEndsWith($translation, ']%')) {
						if(\Framework\StrStartsWith($translation, '%[[') && \Framework\StrEndsWith($translation, ']]%')) {
							array_splice($targets, $index, 1, json_decode(substr($translation, 1, -1), true));
						}
						else {
							$targets[$index] = json_decode(substr($translation, 1, -1), true);
						}

						$Contents = str_replace('%'.$target.'%', '%'.json_encode($targets, JSON_UNESCAPED_UNICODE).'%', $Contents);
					}
					else {
						$Contents = str_replace('%'.$target.'%', $translation, $Contents);
					}
				}
				else {
					++$offset;
				}
			}
			else {
				++$offset;
			}
		}

		return $Contents;
	}

	public static function TryGetTranslation(array $Targets, array $Translations = null, string $Locale = null, string &$Translation = null, int &$Index = null) : bool {
		if(is_array($Targets[0])) {
			$i = 0;

			foreach($Targets as $target) {
				$key = $target[0];

				$params = array_slice($target, 1);

				if(isset($Translations[$key])) {
					$Translation = self::TryGetTranslation»Params($Translations[$key], $params);

					$Index = $i;

					return true;
				}
				else if(\Framework\TryGetTranslation($Locale, $key, $translation)) {
					$Translation = self::TryGetTranslation»Params($translation, $params);

					$Index = $i;

					return true;
				}
				else if(\Framework\TryGetTranslation('', $key, $translation)) {
					$Translation = self::TryGetTranslation»Params($translation, $params);

					$Index = $i;

					return true;
				}

				++$i;
			}
		}
		else {
			$key = $Targets[0];

			$params = array_slice($Targets, 1);

			if(isset($Translations[$key])) {
				$Translation = self::TryGetTranslation»Params($Translations[$key], $params);

				$Index = null;

				return true;
			}
			else if(\Framework\TryGetTranslation($Locale, $key, $translation)) {
				$Translation = self::TryGetTranslation»Params($translation, $params);

				$Index = null;

				return true;
			}
			else if(\Framework\TryGetTranslation('', $key, $translation)) {
				$Translation = self::TryGetTranslation»Params($translation, $params);

				$Index = null;

				return true;
			}
		}

		return false;
	}

	public static function TryGetTranslation»Params(string $Translation, array $Params) : string {
		while(true) {
			if(preg_match('/%\\$([0-9]+)\\.\\.\\.%/', $Translation, $match)) {
				$index = (int)$match[1];

				$params = array_slice($Params, $index);

				if(0 < count($params)) {
					$Translation = str_replace('%$'.$index.'...%', substr(json_encode($params), 1, -1), $Translation);
				}
				else {
					$Translation = str_replace('%$'.$index.'...%', '', $Translation);
				}
			}
			else if(preg_match('/%,\\$([0-9]+)\\.\\.\\.%/', $Translation, $match)) {
				$index = (int)$match[1];

				$params = array_slice($Params, $index);

				if(0 < count($params)) {
					$Translation = str_replace('%,$'.$index.'...%', ','.substr(json_encode($params), 1, -1), $Translation);
				}
				else {
					$Translation = str_replace('%,$'.$index.'...%', '', $Translation);
				}
			}
			else if(preg_match('/%\\$([0-9]+)%/', $Translation, $match)) {
				$index = (int)$match[1];

				$Translation = str_replace('%$'.$index.'%', $Params[$index] ?? '%&dollar;'.$index.'%', $Translation);
			}
			else if(preg_match('/%\\$([^ %]+)%/', $Translation, $match)) {
				$index = $match[1];

				$Translation = str_replace('%$'.$index.'%', $Params[$index] ?? '%&dollar;'.$index.'%', $Translation);
			}
			else if(preg_match('/%\\$\\.\\.\\.%/', $Translation, $match)) {
				if(0 < count($Params)) {
					$Translation = str_replace('%$...%', substr(json_encode($Params), 1, -1), $Translation);
				}
				else {
					$Translation = str_replace('%$...%', '', $Translation);
				}
			}
			else if(preg_match('/%,\\$\\.\\.\\.%/', $Translation, $match)) {
				if(0 < count($Params)) {
					$Translation = str_replace('%,$...%', ','.substr(json_encode($Params), 1, -1), $Translation);
				}
				else {
					$Translation = str_replace('%,$...%', '', $Translation);
				}
			}
			else {
				break;
			}
		}

		return $Translation;
	}
}
?>