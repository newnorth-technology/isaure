<?
namespace Framework;

class Email {
	/* Instance variables */

	public $Name;

	public $SMTP;

	public $From;

	public $ReplyTo;

	public $To;

	public $Directory;

	public $File;

	public $HtmlView;

	public $TextView;

	/* Instance constructors */

	public function __construct(string $Name) {
		$this->Name = $Name;

		$this->SMTP = \Framework\GetConfig('Emails.'.$Name.'.SMTP') ?? \Framework\GetConfig('System.Email.SMTP');

		$this->From = \Framework\GetConfig('Emails.'.$Name.'.From') ?? \Framework\GetConfig('System.Email.From');

		$this->ReplyTo = \Framework\GetConfig('Emails.'.$Name.'.ReplyTo') ?? \Framework\GetConfig('System.Email.NoReply');

		$this->To = \Framework\GetConfig('Emails.'.$Name.'.To') ?? \Framework\GetConfig('System.Email.To');

		$delimiter = strrpos($Name, '/');

		if($delimiter === false) {
			$this->Directory = '';

			$this->File = $Name;
		}
		else {
			$this->Directory = substr($Name, 0, $delimiter + 1);

			$this->File = substr($Name, $delimiter + 1);
		}

		foreach(\Framework\GetConfig('System.Directories.Emails', ['']) as $directory) {
			if(file_exists($directory.$this->Directory.$this->File.'.html.phtml')) {
				$this->HtmlView = new \Framework\Views\Html([
					'Directory' => $directory.$this->Directory,
					'File' => $this->File.'.html'
				]);
			}
		}

		if($this->HtmlView !== null) {
			$this->HtmlView->SetTranslation('Email', '%["Emails.'.$Name.'.%$0%"%,$1...%]%');

			$this->HtmlView->SetTranslation('Variable', '%["Variables.%$0%"]%');
		}

		foreach(\Framework\GetConfig('System.Directories.Emails', ['']) as $directory) {
			if(file_exists($directory.$this->Directory.$this->File.'.text.phtml')) {
				$this->TextView = new \Framework\Views\Html([
					'Directory' => $directory.$this->Directory,
					'File' => $this->File.'.text'
				]);
			}
		}

		if($this->TextView !== null) {
			$this->TextView->SetTranslation('Email', '%["Emails.'.$Name.'.%$0%"%,$1...%]%');

			$this->TextView->SetTranslation('Variable', '%["Variables.%$0%"]%');
		}
	}

	/* Instance methods */

	public function Send(array $Params = []) {
		$sendgrid = new \SendGrid('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');

		$Params['Locale'] = $Params['Locale'] ?? null;

		$Params['Variables'] = $Params['Variables'] ?? [];

		$mail = new \SendGrid\Mail\Mail();

		if(array_key_exists('From', $Params)) {
			if(is_array($Params['From'])) {
				$mail->setFrom($Params['From']['EmailAddress'], $Params['From']['Name'] ?? null);
			}
			else {
				$mail->setFrom($Params['From'], null);
			}
		}
		else if(is_array($this->From)) {
			$mail->setFrom($this->From['EmailAddress'], $this->From['Name'] ?? null);
		}
		else if($this->From !== null) {
			$mail->setFrom($this->From, null);
		}

		if(array_key_exists('ReplyTo', $Params)) {
			if(is_array($Params['ReplyTo'])) {
				$mail->setReplyTo($Params['ReplyTo']['EmailAddress'], $Params['ReplyTo']['Name'] ?? null);
			}
			else {
				$mail->setReplyTo($Params['ReplyTo'], null);
			}
		}
		else if(is_array($this->ReplyTo)) {
			$mail->setReplyTo($this->ReplyTo['EmailAddress'], $this->ReplyTo['Name'] ?? null);
		}
		else if($this->ReplyTo !== null) {
			$mail->setReplyTo($this->ReplyTo, null);
		}

		$model = new \Framework\Model(['Model' => $Params['Variables']]);

		if(array_key_exists('subject', $Params)) {
			$mail->setSubject(\Framework\Translator::Translate($Params['subject'], $Params['Variables'], $Params['Locale']));
		}
		else {
			$mail->setSubject(\Framework\Translator::Translate('%["Emails.'.$this->Name.'.Subject"]%', $Params['Variables'], $Params['Locale']));
		}

		if(array_key_exists('html', $Params)) {
			$mail->addContent(
				"text/html",
				\Framework\Translator::Translate($Params['html'], $Params['Variables'], $Params['Locale'])
			);
		}
		else if($this->HtmlView !== null) {
			if(array_key_exists('Variables', $Params)) {
				foreach($Params['Variables'] as $key => $value) {
					$this->HtmlView->SetTranslation('Variables.'.$key, $value);
				}
			}

			if(array_key_exists('HtmlVariables', $Params)) {
				foreach($Params['HtmlVariables'] as $key => $value) {
					$this->HtmlView->SetTranslation('Variables.'.$key, $value);
				}
			}

			$mail->addContent(
				"text/html",
				$this->HtmlView->Render($model, true, ['Locale' => $Params['Locale']])
			);
		}

		if(array_key_exists('text', $Params)) {
			$mail->addContent(
				"text/plain",
				\Framework\Translator::Translate($Params['text'], $Params['Variables'], $Params['Locale'])
			);
		}
		else if($this->TextView !== null) {
			if(array_key_exists('Variables', $Params)) {
				foreach($Params['Variables'] as $key => $value) {
					$this->TextView->SetTranslation('Variables.'.$key, $value);
				}
			}

			if(array_key_exists('TextVariables', $Params)) {
				foreach($Params['TextVariables'] as $key => $value) {
					$this->TextView->SetTranslation('Variables.'.$key, $value);
				}
			}

			$mail->addContent(
				"text/plain",
				$this->TextView->Render($model, true, ['Locale' => $Params['Locale']])
			);
		}

		if(array_key_exists('To', $Params)) {
			if(is_array($Params['To'])) {
				foreach($Params['To'] as $to) {
					$mail->addTo($to, null);
				}
			}
			else {
				$mail->addTo($Params['To'], null);
			}
		}
		else if(is_array($this->To)) {
			foreach($this->To as $to) {
				$mail->addTo($to, null);
			}
		}
		else {
			$mail->addTo($this->To, null);
		}

		$response = $sendgrid->send($mail);

		return true;
		/*print $response->statusCode() . "\n";
		print_r($response->headers());
		print $response->body() . "\n";
		} catch (Exception $e) {
		echo 'Caught exception: '. $e->getMessage() ."\n";
		}*/
	}

	public function Send2(array $Params = []) {
		$Params['Locale'] = $Params['Locale'] ?? null;

		$Params['Variables'] = $Params['Variables'] ?? [];

		$mail = new \PHPMailer();

		$mail->Timeout = 5;

		if($this->SMTP === null) {
			$mail->isMail();
		}
		else if(is_array($this->SMTP)) {
			if(0 < count($this->SMTP)) {
				$smtp = $this->SMTP;

				$mail->isSMTP();

				if(array_key_exists('Host', $smtp)) {
					$mail->Host = $smtp['Host'];

					/*{IF:DEBUG}*/

					unset($smtp['Host']);

					/*{ENDIF}*/
				}

				if(array_key_exists('Port', $smtp)) {
					$mail->Port = $smtp['Port'];

					/*{IF:DEBUG}*/

					unset($smtp['Port']);

					/*{ENDIF}*/
				}

				if(array_key_exists('Username', $smtp) && array_key_exists('Password', $smtp)) {
					$mail->SMTPAuth = true;

					$mail->Username = $smtp['Username'];

					$mail->Password = $smtp['Password'];

					/*{IF:DEBUG}*/

					unset($smtp['Username']);

					unset($smtp['Password']);

					/*{ENDIF}*/
				}

				if(array_key_exists('UseSSL', $smtp)) {
					if($smtp['UseSSL']) {
						$mail->SMTPSecure = "ssl";
					}

					/*{IF:DEBUG}*/

					unset($smtp['UseSSL']);

					/*{ENDIF}*/
				}

				/*{IF:DEBUG}*/

				if(0 < count($smtp)) {
					throw new \Framework\RuntimeException(
						'Unable to send e-mail, trailing SMTP data found.',
						[
							'SMTP' => $this->SMTP
						]
					);
				}

				/*{ENDIF}*/
			}
			else {
				$mail->isMail();
			}
		}
		else {
			throw new \Framework\RuntimeException(
				'Unable to send e-mail, invalid SMTP data specified.',
				[
					'SMTP' => $this->SMTP
				]
			);
		}

		$mail->CharSet = 'UTF-8';

		if(array_key_exists('From', $Params)) {
			if(is_array($Params['From'])) {
				$mail->From = $Params['From']['EmailAddress'];

				$mail->FromName = $Params['From']['Name'] ?? null;
			}
			else {
				$mail->From = $Params['From'];

				$mail->FromName = null;
			}
		}
		else if(is_array($this->From)) {
			$mail->From = $this->From['EmailAddress'];

			$mail->FromName = $this->From['Name'] ?? null;
		}
		else if($this->From !== null) {
			$mail->From = $this->From;

			$mail->FromName = null;
		}

		if(array_key_exists('ReplyTo', $Params)) {
			if(is_array($Params['ReplyTo'])) {
				if(array_key_exists('EmailAddress', $Params['ReplyTo'])) {
					$mail->addReplyTo($Params['ReplyTo']['EmailAddress'], $Params['ReplyTo']['Name'] ?? null);
				}
				else {
					foreach($Params['ReplyTo'] as $replyTo) {
						$mail->addReplyTo($replyTo['EmailAddress'], $replyTo['Name'] ?? null);
					}
				}
			}
			else {
				$mail->addReplyTo($Params['ReplyTo']);
			}
		}
		else if(is_array($this->ReplyTo)) {
			if(array_key_exists('EmailAddress', $this->ReplyTo)) {
				$mail->addReplyTo($this->ReplyTo['EmailAddress'], $this->ReplyTo['Name'] ?? null);
			}
			else {
				foreach($this->ReplyTo as $replyTo) {
					$mail->addReplyTo($replyTo['EmailAddress'], $replyTo['Name'] ?? null);
				}
			}
		}
		else if($this->ReplyTo !== null) {
			$mail->addReplyTo($this->ReplyTo);
		}

		$model = new \Framework\Model(['Model' => $Params['Variables']]);

		if(array_key_exists('subject', $Params)) {
			$mail->Subject = \Framework\Translator::Translate($Params['subject'], $Params['Variables'], $Params['Locale']);
		}
		else {
			$mail->Subject = \Framework\Translator::Translate('%["Emails.'.$this->Name.'.Subject"]%', $Params['Variables'], $Params['Locale']);
		}

		if(array_key_exists('html', $Params)) {
			$mail->msgHTML(\Framework\Translator::Translate($Params['html'], $Params['Variables'], $Params['Locale']));
		}
		else if($this->HtmlView !== null) {
			if(array_key_exists('Variables', $Params)) {
				foreach($Params['Variables'] as $key => $value) {
					$this->HtmlView->SetTranslation('Variables.'.$key, $value);
				}
			}

			if(array_key_exists('HtmlVariables', $Params)) {
				foreach($Params['HtmlVariables'] as $key => $value) {
					$this->HtmlView->SetTranslation('Variables.'.$key, $value);
				}
			}

			$mail->msgHTML($this->HtmlView->Render($model, true, ['Locale' => $Params['Locale']]));
		}

		if(array_key_exists('text', $Params)) {
			$mail->msgHTML(\Framework\Translator::Translate($Params['text'], $Params['Variables'], $Params['Locale']));
		}
		else if($this->TextView !== null) {
			if(array_key_exists('Variables', $Params)) {
				foreach($Params['Variables'] as $key => $value) {
					$this->TextView->SetTranslation('Variables.'.$key, $value);
				}
			}

			if(array_key_exists('TextVariables', $Params)) {
				foreach($Params['TextVariables'] as $key => $value) {
					$this->TextView->SetTranslation('Variables.'.$key, $value);
				}
			}

			$mail->AltBody = $this->TextView->Render($model, true, ['Locale' => $Params['Locale']]);
		}

		if(array_key_exists('Attachments', $Params)) {
			foreach($Params['Attachments'] as $name => $values) {
				$mail->AddAttachment(
					$values['Path'],
					$name,
					$values['Encoding'] ?? 'base64',
					$values['Type'] ?? ''
				);
			}
		}

		if(array_key_exists('To', $Params)) {
			if(is_array($Params['To'])) {
				foreach($Params['To'] as $to) {
					$mail->AddAddress($to);
				}
			}
			else {
				$mail->AddAddress($Params['To']);
			}
		}
		else if(is_array($this->To)) {
			foreach($this->To as $to) {
				$mail->AddAddress($to);
			}
		}
		else {
			$mail->AddAddress($this->To);
		}

		return $mail->Send();
	}
}
?>