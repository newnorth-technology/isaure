<?
namespace Framework;

function OnException($Exception) {
	if(0 < ob_get_level()) {
		ob_end_clean();
	}

	ob_start();

	echo '<pre>', "\n\n";

	$backtrace = $Exception->getTrace();

	$isInnerException = false;

	while(true) {
		if($isInnerException) {
			echo '<b>INNER EXCEPTION</b>', "\n", '------------------------------', "\n";
		}
		else {
			echo '<b>EXCEPTION</b>', "\n", '------------------------------', "\n";
		}

		$file = utf8_encode($Exception->getFile());

		$line = $Exception->getLine();

		if(substr($file, -22) === '\Framework\OnError.php') {
			$trace = array_shift($backtrace);

			$file = $trace['file'] ?? null;

			$line = $trace['line'] ?? null;
		}

		echo '<b>Message:</b>', "\n", $Exception->getMessage(), "\n\n";

		if($file !== null) {
			echo '<b>File:</b>', "\n", $file, "\n\n";
		}

		if($line !== null) {
			echo '<b>Line:</b>', "\n", $line, "\n\n";
		}

		if(isset($Exception->Data)) {
			echo '<b>DATA</b>', "\n", '------------------------------', "\n";

			foreach($Exception->Data as $key => $value) {
				if(is_array($value)) {
					echo '<b>', $key, ':</b>', "\n", print_r($value, true), "\n";
				}
				else if(is_object($value)) {
					echo '<b>', $key, ':</b>', "\n", print_r($value, true), "\n";
				}
				else {
					echo '<b>', $key, ':</b>', "\n", $value, "\n\n";
				}
			}
		}

		if(!isset($Exception->InnerException)) {
			break;
		}
		else if($Exception->InnerException === null) {
			break;
		}
		else {
			$Exception = $Exception->InnerException;

			$isInnerException = true;
		}
	}

	$backtrace = $Exception->getTrace();

	foreach($backtrace as $i => $trace) {
		echo '<b>TRACE #', $i + 1, '</b>', "\n", '------------------------------', "\n";

		if(isset($trace['file'])) {
			echo '<b>File:</b>', "\n", $trace['file'], "\n\n";
		}

		if(isset($trace['line'])) {
			echo '<b>Line:</b>', "\n", $trace['line'], "\n\n";
		}

		if(isset($trace['class'])) {
			echo '<b>Function:</b>', "\n", $trace['class'].$trace['type'].$trace['function'], "\n\n";
		}
		else {
			echo '<b>Function:</b>', "\n", $trace['function'], "\n\n";
		}
	}

	echo '</pre>';

	$html = ob_get_contents();

	ob_end_clean();

	if(GetConfig('System.ShowErrors', true)) {
		echo $html;
	}

	/*(new \Framework\Email('system.exception'))->Send([
		'Locale' => $GLOBALS['Locale']->Id,
		'To' => 'james@newnorth.technology',
		'subject' => 'Exception: '.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],
		'html' => $html
	]);*/

	exit();
}
?>