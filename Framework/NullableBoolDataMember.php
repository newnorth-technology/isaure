<?
namespace Framework;

class NullableBoolDataMember extends \Framework\ADynamicNullableDataMember {
	/* Static methods */

	public static function ParseFromDbValue(\Framework\NullableBoolDataMember $DataMember = null, $Value) {
		if($Value === null) {
			return null;
		}

		/*{IF:DEBUG}*/

		if(!is_bool($Value) && !is_int($Value) && !is_string($Value)) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, invalid value type.',
				['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name, 'Value' => $Value]
			);
		}

		if(is_string($Value)) {
			if($Value !== '0' && $Value !== '1') {
				throw new \Framework\RuntimeException(
					'Failed to parse data member value, invalid string value.',
					['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name, 'Value' => $Value]
				);
			}
		}

		if(is_int($Value)) {
			if($Value !== 0 && $Value !== 1) {
				throw new \Framework\RuntimeException(
					'Failed to parse data member value, invalid int value.',
					['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name, 'Value' => $Value]
				);
			}
		}

		/*{ENDIF}*/

		return (bool)$Value;
	}

	public static function ParseValue(\Framework\NullableBoolDataMember $DataMember = null, $Value) {
		/*{IF:DEBUG}*/

		if($Value !== null && !is_bool($Value)) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, invalid value type.',
				['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name, 'Value' => $Value]
			);
		}

		/*{ENDIF}*/

		return $Value;
	}

	public static function ParseToDbValue(\Framework\NullableBoolDataMember $DataMember = null, $Value) {
		return $Value;
	}
}
?>