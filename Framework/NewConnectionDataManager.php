<?
namespace Framework;

class NewConnectionDataManager extends \Framework\ADataManager {
	/* Instance variables */

	public $UseTranslation;

	/* Magic methods */

	public function __construct(string $Id, string $Name, array $DataType, array $Params) {
		$this->PrimaryKey = 'id';

		$this->UseTranslation = $Params['UseTranslation'] ?? false;

		parent::__construct($Id, $Name, $DataType, $Params);
	}

	/* Instance initialization methods */

	public function Initialize() {
		if($this->UseTranslation) {
			if(!\Framework\TryGetDataManager($this->Id.'/Translation', $this->TranslationDataManager)) {
				throw new \Framework\RuntimeException(
					'Translation data manager not found for standard data manager.',
					[
						'Standard data manager' => $this->Id,
						'Translation data manager' => $this->Id.'/Translation'
					]
				);
			}

			foreach($this->TranslationDataManager->DataMembers as $dataMember) {
				if($dataMember->Name === 'id') {
					continue;
				}

				if($dataMember->Name === 'parent_id') {
					continue;
				}

				if($dataMember->Name === 'locale') {
					continue;
				}

				$class = get_class($dataMember);

				$this->DataMembers[$dataMember->Name] = new $class(
					$this->TranslationDataManager,
					$dataMember->Name,
					[
						'IsDynamic' => $dataMember->IsDynamic,
						'IsNullable' => $dataMember->IsNullable,
						'UseLogInsert' => $dataMember->UseLogInsert,
						'UseLogUpdate' => $dataMember->UseLogUpdate,
						'UseLogDelete' => $dataMember->UseLogDelete
					]
				);
			}
		}
	}

	public function Initialize»DataMembers() {
		$this->DataMembers['id'] = [
			'Type' => 'StaticInt',
			'UseLogInsert' => true,
			'UseLogUpdate' => false,
			'UseLogDelete' => true
		];

		parent::Initialize»DataMembers();
	}

	public function Initialize»References() {
		if($this->UseTranslation) {
			$this->References['Translation'] = [
				'TargetDataManager' => $this->Id.'/Translation',
				'SelectQuery' => [
					'Condition' => \Framework\Database\Conditions\All([
						'Conditions' => [
							\Framework\Database\Conditions\IsEqualTo([
								'ColumnA' => \Framework\Database\Columns\Reference(['Source' => $this->Id.'/Translation', 'Name' => 'parent_id']),
								'ColumnB' => \Framework\Database\Columns\Value(['Variable' => $this->Id.'.id'])
							]),
							\Framework\Database\Conditions\IsEqualTo([
								'ColumnA' => \Framework\Database\Columns\Reference(['Source' => $this->Id.'/Translation', 'Name' => 'locale']),
								'ColumnB' => \Framework\Database\Columns\Value(['Variable' => 'Locale'])
							])
						]
					])
				]
			];

			$this->References['Translations'] = [
				'TargetDataManager' => $this->Id.'/Translation',
				'OnDeleteAction' => 'Delete',
				'SelectQuery' => [
					'Condition' => \Framework\Database\Conditions\IsEqualTo([
						'ColumnA' => \Framework\Database\Columns\Reference(['Source' => $this->Id.'/Translation', 'Name' => 'parent_id']),
						'ColumnB' => \Framework\Database\Columns\Value(['Variable' => $this->Id.'.id'])
					])
				]
			];
		}

		parent::Initialize»References();
	}

	public function Initialize»InsertQuery(array $Params = null) {
		$this->InsertQuery = new \Framework\Database\InsertQuery([
			'Target' => new \Framework\Database\Target([
				'DataManager' => $this
			])
		]);
	}

	public function Initialize»UpdateQuery(array $Params = null) {
		$this->UpdateQuery = new \Framework\Database\UpdateQuery([
			'Sources' => new \Framework\Database\Sources([
				$this->Id => new \Framework\Database\Source([
					'DataManager' => $this,
					'Alias' => $this->Id
				])
			])
		]);
	}

	public function Initialize»SelectQuery(array $Params = null) {
		$this->SelectQuery = new \Framework\Database\SelectQuery([
			'Columns' => $this->DataMembers,
			'Sources' => new \Framework\Database\Sources([
				$this->Id => new \Framework\Database\Source([
					'DataManager' => $this,
					'Alias' => $this->Id
				])
			])
		]);
	}

	/* Instance find methods */

	public function FindById(int $Id, array $Params = null) {
		return $this->Find(
			[
				'Condition' => new \Framework\Database\Conditions\IsEqualTo([
					'ColumnA' => $this->DataMembers['id'],
					'ColumnB' => $Id
				])
			],
			$Params
		);
	}

	public function TryFindById(&$Item, int $Id, array $Params = null) : bool {
		$Item = $this->Find(
			[
				'Condition' => new \Framework\Database\Conditions\IsEqualTo([
					'ColumnA' => $this->DataMembers['id'],
					'ColumnB' => $Id
				])
			],
			$Params
		);

		return $Item !== null;
	}

	/* Instance contains methods */

	public function ContainsById(int $Id, array $Params = null) : bool {
		return $this->Contains(
			[
				'Condition' => new \Framework\Database\Conditions\IsEqualTo([
					'ColumnA' => $this->DataMembers['id'],
					'ColumnB' => $Id
				])
			],
			$Params
		);
	}
}
?>