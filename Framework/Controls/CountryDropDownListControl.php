<?
namespace Framework\Controls;

class CountryDropDownListControl extends \Framework\Controls\DropDownListControl {
	/* Instance life cycle methods */

	public function Initialize() {
		if(!isset($this->Model->Items)) {
			$this->Model->Items = [];
		}

		$this->Model->Items[] = ['Value' => 'AF', 'Text' => 'Afghanistan'];

		$this->Model->Items[] = ['Value' => 'AL', 'Text' => 'Albania'];

		$this->Model->Items[] = ['Value' => 'DZ', 'Text' => 'Algeria'];

		$this->Model->Items[] = ['Value' => 'AS', 'Text' => 'American Samoa'];

		$this->Model->Items[] = ['Value' => 'AD', 'Text' => 'Andorra'];

		$this->Model->Items[] = ['Value' => 'AO', 'Text' => 'Angola'];

		$this->Model->Items[] = ['Value' => 'AI', 'Text' => 'Anguilla'];

		$this->Model->Items[] = ['Value' => 'AQ', 'Text' => 'Antarctica'];

		$this->Model->Items[] = ['Value' => 'AG', 'Text' => 'Antigua and Barbuda'];

		$this->Model->Items[] = ['Value' => 'AR', 'Text' => 'Argentina'];

		$this->Model->Items[] = ['Value' => 'AM', 'Text' => 'Armenia'];

		$this->Model->Items[] = ['Value' => 'AW', 'Text' => 'Aruba'];

		$this->Model->Items[] = ['Value' => 'AU', 'Text' => 'Australia'];

		$this->Model->Items[] = ['Value' => 'AT', 'Text' => 'Austria'];

		$this->Model->Items[] = ['Value' => 'AZ', 'Text' => 'Azerbaijan'];

		$this->Model->Items[] = ['Value' => 'BS', 'Text' => 'Bahamas'];

		$this->Model->Items[] = ['Value' => 'BH', 'Text' => 'Bahrain'];

		$this->Model->Items[] = ['Value' => 'BD', 'Text' => 'Bangladesh'];

		$this->Model->Items[] = ['Value' => 'BB', 'Text' => 'Barbados'];

		$this->Model->Items[] = ['Value' => 'BY', 'Text' => 'Belarus'];

		$this->Model->Items[] = ['Value' => 'BE', 'Text' => 'Belgium'];

		$this->Model->Items[] = ['Value' => 'BZ', 'Text' => 'Belize'];

		$this->Model->Items[] = ['Value' => 'BJ', 'Text' => 'Benin'];

		$this->Model->Items[] = ['Value' => 'BM', 'Text' => 'Bermuda'];

		$this->Model->Items[] = ['Value' => 'BT', 'Text' => 'Bhutan'];

		$this->Model->Items[] = ['Value' => 'BO', 'Text' => 'Bolivia'];

		$this->Model->Items[] = ['Value' => 'BA', 'Text' => 'Bosnia and Herzegovina'];

		$this->Model->Items[] = ['Value' => 'BW', 'Text' => 'Botswana'];

		$this->Model->Items[] = ['Value' => 'BV', 'Text' => 'Bouvet Island'];

		$this->Model->Items[] = ['Value' => 'BR', 'Text' => 'Brazil'];

		$this->Model->Items[] = ['Value' => 'IO', 'Text' => 'British Indian Ocean Territory'];

		$this->Model->Items[] = ['Value' => 'BN', 'Text' => 'Brunei Darussalam'];

		$this->Model->Items[] = ['Value' => 'BG', 'Text' => 'Bulgaria'];

		$this->Model->Items[] = ['Value' => 'BF', 'Text' => 'Burkina Faso'];

		$this->Model->Items[] = ['Value' => 'BI', 'Text' => 'Burundi'];

		$this->Model->Items[] = ['Value' => 'KH', 'Text' => 'Cambodia'];

		$this->Model->Items[] = ['Value' => 'CM', 'Text' => 'Cameroon'];

		$this->Model->Items[] = ['Value' => 'CA', 'Text' => 'Canada'];

		$this->Model->Items[] = ['Value' => 'CV', 'Text' => 'Cape Verde'];

		$this->Model->Items[] = ['Value' => 'KY', 'Text' => 'Cayman Islands'];

		$this->Model->Items[] = ['Value' => 'CF', 'Text' => 'Central African Republic'];

		$this->Model->Items[] = ['Value' => 'TD', 'Text' => 'Chad'];

		$this->Model->Items[] = ['Value' => 'CL', 'Text' => 'Chile'];

		$this->Model->Items[] = ['Value' => 'CN', 'Text' => 'China'];

		$this->Model->Items[] = ['Value' => 'CX', 'Text' => 'Christmas Island'];

		$this->Model->Items[] = ['Value' => 'CC', 'Text' => 'Cocos (Keeling) Islands'];

		$this->Model->Items[] = ['Value' => 'CO', 'Text' => 'Colombia'];

		$this->Model->Items[] = ['Value' => 'KM', 'Text' => 'Comoros'];

		$this->Model->Items[] = ['Value' => 'CG', 'Text' => 'Congo'];

		$this->Model->Items[] = ['Value' => 'CD', 'Text' => 'Congo, the Democratic Republic of the'];

		$this->Model->Items[] = ['Value' => 'CK', 'Text' => 'Cook Islands'];

		$this->Model->Items[] = ['Value' => 'CR', 'Text' => 'Costa Rica'];

		$this->Model->Items[] = ['Value' => 'CI', 'Text' => 'Cote D\'Ivoire'];

		$this->Model->Items[] = ['Value' => 'HR', 'Text' => 'Croatia'];

		$this->Model->Items[] = ['Value' => 'CU', 'Text' => 'Cuba'];

		$this->Model->Items[] = ['Value' => 'CY', 'Text' => 'Cyprus'];

		$this->Model->Items[] = ['Value' => 'CZ', 'Text' => 'Czech Republic'];

		$this->Model->Items[] = ['Value' => 'DK', 'Text' => 'Denmark'];

		$this->Model->Items[] = ['Value' => 'DJ', 'Text' => 'Djibouti'];

		$this->Model->Items[] = ['Value' => 'DM', 'Text' => 'Dominica'];

		$this->Model->Items[] = ['Value' => 'DO', 'Text' => 'Dominican Republic'];

		$this->Model->Items[] = ['Value' => 'EC', 'Text' => 'Ecuador'];

		$this->Model->Items[] = ['Value' => 'EG', 'Text' => 'Egypt'];

		$this->Model->Items[] = ['Value' => 'SV', 'Text' => 'El Salvador'];

		$this->Model->Items[] = ['Value' => 'GQ', 'Text' => 'Equatorial Guinea'];

		$this->Model->Items[] = ['Value' => 'ER', 'Text' => 'Eritrea'];

		$this->Model->Items[] = ['Value' => 'EE', 'Text' => 'Estonia'];

		$this->Model->Items[] = ['Value' => 'ET', 'Text' => 'Ethiopia'];

		$this->Model->Items[] = ['Value' => 'FK', 'Text' => 'Falkland Islands (Malvinas)'];

		$this->Model->Items[] = ['Value' => 'FO', 'Text' => 'Faroe Islands'];

		$this->Model->Items[] = ['Value' => 'FJ', 'Text' => 'Fiji'];

		$this->Model->Items[] = ['Value' => 'FI', 'Text' => 'Finland'];

		$this->Model->Items[] = ['Value' => 'FR', 'Text' => 'France'];

		$this->Model->Items[] = ['Value' => 'GF', 'Text' => 'French Guiana'];

		$this->Model->Items[] = ['Value' => 'PF', 'Text' => 'French Polynesia'];

		$this->Model->Items[] = ['Value' => 'TF', 'Text' => 'French Southern Territories'];

		$this->Model->Items[] = ['Value' => 'GA', 'Text' => 'Gabon'];

		$this->Model->Items[] = ['Value' => 'GM', 'Text' => 'Gambia'];

		$this->Model->Items[] = ['Value' => 'GE', 'Text' => 'Georgia'];

		$this->Model->Items[] = ['Value' => 'DE', 'Text' => 'Germany'];

		$this->Model->Items[] = ['Value' => 'GH', 'Text' => 'Ghana'];

		$this->Model->Items[] = ['Value' => 'GI', 'Text' => 'Gibraltar'];

		$this->Model->Items[] = ['Value' => 'GR', 'Text' => 'Greece'];

		$this->Model->Items[] = ['Value' => 'GL', 'Text' => 'Greenland'];

		$this->Model->Items[] = ['Value' => 'GD', 'Text' => 'Grenada'];

		$this->Model->Items[] = ['Value' => 'GP', 'Text' => 'Guadeloupe'];

		$this->Model->Items[] = ['Value' => 'GU', 'Text' => 'Guam'];

		$this->Model->Items[] = ['Value' => 'GT', 'Text' => 'Guatemala'];

		$this->Model->Items[] = ['Value' => 'GN', 'Text' => 'Guinea'];

		$this->Model->Items[] = ['Value' => 'GW', 'Text' => 'Guinea-Bissau'];

		$this->Model->Items[] = ['Value' => 'GY', 'Text' => 'Guyana'];

		$this->Model->Items[] = ['Value' => 'HT', 'Text' => 'Haiti'];

		$this->Model->Items[] = ['Value' => 'HM', 'Text' => 'Heard Island and Mcdonald Islands'];

		$this->Model->Items[] = ['Value' => 'VA', 'Text' => 'Holy See (Vatican City State)'];

		$this->Model->Items[] = ['Value' => 'HN', 'Text' => 'Honduras'];

		$this->Model->Items[] = ['Value' => 'HK', 'Text' => 'Hong Kong'];

		$this->Model->Items[] = ['Value' => 'HU', 'Text' => 'Hungary'];

		$this->Model->Items[] = ['Value' => 'IS', 'Text' => 'Iceland'];

		$this->Model->Items[] = ['Value' => 'IN', 'Text' => 'India'];

		$this->Model->Items[] = ['Value' => 'ID', 'Text' => 'Indonesia'];

		$this->Model->Items[] = ['Value' => 'IR', 'Text' => 'Iran, Islamic Republic of'];

		$this->Model->Items[] = ['Value' => 'IQ', 'Text' => 'Iraq'];

		$this->Model->Items[] = ['Value' => 'IE', 'Text' => 'Ireland'];

		$this->Model->Items[] = ['Value' => 'IL', 'Text' => 'Israel'];

		$this->Model->Items[] = ['Value' => 'IT', 'Text' => 'Italy'];

		$this->Model->Items[] = ['Value' => 'JM', 'Text' => 'Jamaica'];

		$this->Model->Items[] = ['Value' => 'JP', 'Text' => 'Japan'];

		$this->Model->Items[] = ['Value' => 'JO', 'Text' => 'Jordan'];

		$this->Model->Items[] = ['Value' => 'KZ', 'Text' => 'Kazakhstan'];

		$this->Model->Items[] = ['Value' => 'KE', 'Text' => 'Kenya'];

		$this->Model->Items[] = ['Value' => 'KI', 'Text' => 'Kiribati'];

		$this->Model->Items[] = ['Value' => 'KP', 'Text' => 'Korea, Democratic People\'s Republic of'];

		$this->Model->Items[] = ['Value' => 'KR', 'Text' => 'Korea, Republic of'];

		$this->Model->Items[] = ['Value' => 'KW', 'Text' => 'Kuwait'];

		$this->Model->Items[] = ['Value' => 'KG', 'Text' => 'Kyrgyzstan'];

		$this->Model->Items[] = ['Value' => 'LA', 'Text' => 'Lao People\'s Democratic Republic'];

		$this->Model->Items[] = ['Value' => 'LV', 'Text' => 'Latvia'];

		$this->Model->Items[] = ['Value' => 'LB', 'Text' => 'Lebanon'];

		$this->Model->Items[] = ['Value' => 'LS', 'Text' => 'Lesotho'];

		$this->Model->Items[] = ['Value' => 'LR', 'Text' => 'Liberia'];

		$this->Model->Items[] = ['Value' => 'LY', 'Text' => 'Libyan Arab Jamahiriya'];

		$this->Model->Items[] = ['Value' => 'LI', 'Text' => 'Liechtenstein'];

		$this->Model->Items[] = ['Value' => 'LT', 'Text' => 'Lithuania'];

		$this->Model->Items[] = ['Value' => 'LU', 'Text' => 'Luxembourg'];

		$this->Model->Items[] = ['Value' => 'MO', 'Text' => 'Macao'];

		$this->Model->Items[] = ['Value' => 'MK', 'Text' => 'Macedonia, the Former Yugoslav Republic of'];

		$this->Model->Items[] = ['Value' => 'MG', 'Text' => 'Madagascar'];

		$this->Model->Items[] = ['Value' => 'MW', 'Text' => 'Malawi'];

		$this->Model->Items[] = ['Value' => 'MY', 'Text' => 'Malaysia'];

		$this->Model->Items[] = ['Value' => 'MV', 'Text' => 'Maldives'];

		$this->Model->Items[] = ['Value' => 'ML', 'Text' => 'Mali'];

		$this->Model->Items[] = ['Value' => 'MT', 'Text' => 'Malta'];

		$this->Model->Items[] = ['Value' => 'MH', 'Text' => 'Marshall Islands'];

		$this->Model->Items[] = ['Value' => 'MQ', 'Text' => 'Martinique'];

		$this->Model->Items[] = ['Value' => 'MR', 'Text' => 'Mauritania'];

		$this->Model->Items[] = ['Value' => 'MU', 'Text' => 'Mauritius'];

		$this->Model->Items[] = ['Value' => 'YT', 'Text' => 'Mayotte'];

		$this->Model->Items[] = ['Value' => 'MX', 'Text' => 'Mexico'];

		$this->Model->Items[] = ['Value' => 'FM', 'Text' => 'Micronesia, Federated States of'];

		$this->Model->Items[] = ['Value' => 'MD', 'Text' => 'Moldova, Republic of'];

		$this->Model->Items[] = ['Value' => 'MC', 'Text' => 'Monaco'];

		$this->Model->Items[] = ['Value' => 'MN', 'Text' => 'Mongolia'];

		$this->Model->Items[] = ['Value' => 'ME', 'Text' => 'Montenegro'];

		$this->Model->Items[] = ['Value' => 'MS', 'Text' => 'Montserrat'];

		$this->Model->Items[] = ['Value' => 'MA', 'Text' => 'Morocco'];

		$this->Model->Items[] = ['Value' => 'MZ', 'Text' => 'Mozambique'];

		$this->Model->Items[] = ['Value' => 'MM', 'Text' => 'Myanmar'];

		$this->Model->Items[] = ['Value' => 'NA', 'Text' => 'Namibia'];

		$this->Model->Items[] = ['Value' => 'NR', 'Text' => 'Nauru'];

		$this->Model->Items[] = ['Value' => 'NP', 'Text' => 'Nepal'];

		$this->Model->Items[] = ['Value' => 'NL', 'Text' => 'Netherlands'];

		$this->Model->Items[] = ['Value' => 'AN', 'Text' => 'Netherlands Antilles'];

		$this->Model->Items[] = ['Value' => 'NC', 'Text' => 'New Caledonia'];

		$this->Model->Items[] = ['Value' => 'NZ', 'Text' => 'New Zealand'];

		$this->Model->Items[] = ['Value' => 'NI', 'Text' => 'Nicaragua'];

		$this->Model->Items[] = ['Value' => 'NE', 'Text' => 'Niger'];

		$this->Model->Items[] = ['Value' => 'NG', 'Text' => 'Nigeria'];

		$this->Model->Items[] = ['Value' => 'NU', 'Text' => 'Niue'];

		$this->Model->Items[] = ['Value' => 'NF', 'Text' => 'Norfolk Island'];

		$this->Model->Items[] = ['Value' => 'MP', 'Text' => 'Northern Mariana Islands'];

		$this->Model->Items[] = ['Value' => 'NO', 'Text' => 'Norway'];

		$this->Model->Items[] = ['Value' => 'OM', 'Text' => 'Oman'];

		$this->Model->Items[] = ['Value' => 'PK', 'Text' => 'Pakistan'];

		$this->Model->Items[] = ['Value' => 'PW', 'Text' => 'Palau'];

		$this->Model->Items[] = ['Value' => 'PS', 'Text' => 'Palestinian Territory, Occupied'];

		$this->Model->Items[] = ['Value' => 'PA', 'Text' => 'Panama'];

		$this->Model->Items[] = ['Value' => 'PG', 'Text' => 'Papua New Guinea'];

		$this->Model->Items[] = ['Value' => 'PY', 'Text' => 'Paraguay'];

		$this->Model->Items[] = ['Value' => 'PE', 'Text' => 'Peru'];

		$this->Model->Items[] = ['Value' => 'PH', 'Text' => 'Philippines'];

		$this->Model->Items[] = ['Value' => 'PN', 'Text' => 'Pitcairn'];

		$this->Model->Items[] = ['Value' => 'PL', 'Text' => 'Poland'];

		$this->Model->Items[] = ['Value' => 'PT', 'Text' => 'Portugal'];

		$this->Model->Items[] = ['Value' => 'PR', 'Text' => 'Puerto Rico'];

		$this->Model->Items[] = ['Value' => 'QA', 'Text' => 'Qatar'];

		$this->Model->Items[] = ['Value' => 'RE', 'Text' => 'Reunion'];

		$this->Model->Items[] = ['Value' => 'RO', 'Text' => 'Romania'];

		$this->Model->Items[] = ['Value' => 'RU', 'Text' => 'Russian Federation'];

		$this->Model->Items[] = ['Value' => 'RW', 'Text' => 'Rwanda'];

		$this->Model->Items[] = ['Value' => 'SH', 'Text' => 'Saint Helena'];

		$this->Model->Items[] = ['Value' => 'KN', 'Text' => 'Saint Kitts and Nevis'];

		$this->Model->Items[] = ['Value' => 'LC', 'Text' => 'Saint Lucia'];

		$this->Model->Items[] = ['Value' => 'PM', 'Text' => 'Saint Pierre and Miquelon'];

		$this->Model->Items[] = ['Value' => 'VC', 'Text' => 'Saint Vincent and the Grenadines'];

		$this->Model->Items[] = ['Value' => 'WS', 'Text' => 'Samoa'];

		$this->Model->Items[] = ['Value' => 'SM', 'Text' => 'San Marino'];

		$this->Model->Items[] = ['Value' => 'ST', 'Text' => 'Sao Tome and Principe'];

		$this->Model->Items[] = ['Value' => 'SA', 'Text' => 'Saudi Arabia'];

		$this->Model->Items[] = ['Value' => 'SN', 'Text' => 'Senegal'];

		$this->Model->Items[] = ['Value' => 'RS', 'Text' => 'Serbia'];

		$this->Model->Items[] = ['Value' => 'SC', 'Text' => 'Seychelles'];

		$this->Model->Items[] = ['Value' => 'SL', 'Text' => 'Sierra Leone'];

		$this->Model->Items[] = ['Value' => 'SG', 'Text' => 'Singapore'];

		$this->Model->Items[] = ['Value' => 'SK', 'Text' => 'Slovakia'];

		$this->Model->Items[] = ['Value' => 'SI', 'Text' => 'Slovenia'];

		$this->Model->Items[] = ['Value' => 'SB', 'Text' => 'Solomon Islands'];

		$this->Model->Items[] = ['Value' => 'SO', 'Text' => 'Somalia'];

		$this->Model->Items[] = ['Value' => 'ZA', 'Text' => 'South Africa'];

		$this->Model->Items[] = ['Value' => 'GS', 'Text' => 'South Georgia and the South Sandwich Islands'];

		$this->Model->Items[] = ['Value' => 'ES', 'Text' => 'Spain'];

		$this->Model->Items[] = ['Value' => 'LK', 'Text' => 'Sri Lanka'];

		$this->Model->Items[] = ['Value' => 'SD', 'Text' => 'Sudan'];

		$this->Model->Items[] = ['Value' => 'SR', 'Text' => 'Suriname'];

		$this->Model->Items[] = ['Value' => 'SJ', 'Text' => 'Svalbard and Jan Mayen'];

		$this->Model->Items[] = ['Value' => 'SZ', 'Text' => 'Swaziland'];

		$this->Model->Items[] = ['Value' => 'SE', 'Text' => 'Sweden'];

		$this->Model->Items[] = ['Value' => 'CH', 'Text' => 'Switzerland'];

		$this->Model->Items[] = ['Value' => 'SY', 'Text' => 'Syrian Arab Republic'];

		$this->Model->Items[] = ['Value' => 'TW', 'Text' => 'Taiwan, Province of China'];

		$this->Model->Items[] = ['Value' => 'TJ', 'Text' => 'Tajikistan'];

		$this->Model->Items[] = ['Value' => 'TZ', 'Text' => 'Tanzania, United Republic of'];

		$this->Model->Items[] = ['Value' => 'TH', 'Text' => 'Thailand'];

		$this->Model->Items[] = ['Value' => 'TL', 'Text' => 'Timor-Leste'];

		$this->Model->Items[] = ['Value' => 'TG', 'Text' => 'Togo'];

		$this->Model->Items[] = ['Value' => 'TK', 'Text' => 'Tokelau'];

		$this->Model->Items[] = ['Value' => 'TO', 'Text' => 'Tonga'];

		$this->Model->Items[] = ['Value' => 'TT', 'Text' => 'Trinidad and Tobago'];

		$this->Model->Items[] = ['Value' => 'TN', 'Text' => 'Tunisia'];

		$this->Model->Items[] = ['Value' => 'TR', 'Text' => 'Turkey'];

		$this->Model->Items[] = ['Value' => 'TM', 'Text' => 'Turkmenistan'];

		$this->Model->Items[] = ['Value' => 'TC', 'Text' => 'Turks and Caicos Islands'];

		$this->Model->Items[] = ['Value' => 'TV', 'Text' => 'Tuvalu'];

		$this->Model->Items[] = ['Value' => 'UG', 'Text' => 'Uganda'];

		$this->Model->Items[] = ['Value' => 'UA', 'Text' => 'Ukraine'];

		$this->Model->Items[] = ['Value' => 'AE', 'Text' => 'United Arab Emirates'];

		$this->Model->Items[] = ['Value' => 'GB', 'Text' => 'United Kingdom'];

		$this->Model->Items[] = ['Value' => 'US', 'Text' => 'United States'];

		$this->Model->Items[] = ['Value' => 'UM', 'Text' => 'United States Minor Outlying Islands'];

		$this->Model->Items[] = ['Value' => 'UY', 'Text' => 'Uruguay'];

		$this->Model->Items[] = ['Value' => 'UZ', 'Text' => 'Uzbekistan'];

		$this->Model->Items[] = ['Value' => 'VU', 'Text' => 'Vanuatu'];

		$this->Model->Items[] = ['Value' => 'VE', 'Text' => 'Venezuela'];

		$this->Model->Items[] = ['Value' => 'VN', 'Text' => 'Viet Nam'];

		$this->Model->Items[] = ['Value' => 'VG', 'Text' => 'Virgin Islands, British'];

		$this->Model->Items[] = ['Value' => 'VI', 'Text' => 'Virgin Islands, U.s.'];

		$this->Model->Items[] = ['Value' => 'WF', 'Text' => 'Wallis and Futuna'];

		$this->Model->Items[] = ['Value' => 'EH', 'Text' => 'Western Sahara'];

		$this->Model->Items[] = ['Value' => 'YE', 'Text' => 'Yemen'];

		$this->Model->Items[] = ['Value' => 'ZM', 'Text' => 'Zambia'];

		$this->Model->Items[] = ['Value' => 'ZW', 'Text' => 'Zimbabwe'];

		parent::Initialize();
	}
}
?>