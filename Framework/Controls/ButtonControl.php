<?
namespace Framework\Controls;

class ButtonControl extends \Framework\Controllers\AHtml {
	/* Magic methods */

	public function __construct(array $Params) {
		parent::__construct($Params);

		$this->Model->RootElement = $this->Model->ContainerElement = new \Framework\HTML\Element('div');

		$this->Model->InputElement = new \Framework\HTML\Element('input');

		$this->Model->InputElement->CreateAttribute('type', 'button');
	}

	/* Instance life cycle methods */

	public function Initialize() {
		$this->SetControlData('Form', null);

		$this->SetControlData('Name', null);

		$this->SetControlData('Id', $this->Model->Form !== null && $this->Model->Name !== null ? $this->Model->Form.'»'.$this->Model->Name : null);

		$this->SetControlData('TabIndex', null);

		$this->SetControlData('Container»CssClass', null);

		$this->SetControlData('Input»CssClass', null);

		$this->SetControlData('Input»Text', '%["Owner","'.$this->Name.'»Text"]%');

		$this->SetControlData('Input»Disabled', null);

		$this->SetControlData('Input»Attributes', []);

		parent::Initialize();
	}

	public function Render() {
		$this->Render»Container();

		$this->Render»Input();

		parent::Render();
	}

	public function Render»Container() {
		if($this->Model->Container»CssClass === null) {
			$this->Model->ContainerElement->CreateAttribute('class', 'i-b');
		}
		else {
			$this->Model->ContainerElement->CreateAttribute('class', 'i-b '.$this->Model->Container»CssClass);
		}
	}

	public function Render»Input() {
		if($this->Model->Id !== null) {
			$this->Model->InputElement->CreateAttribute('id', $this->Model->Id);
		}

		if($this->Model->Form !== null) {
			$this->Model->InputElement->CreateAttribute('form', $this->Model->Form);
		}

		if($this->Model->Name !== null) {
			$this->Model->InputElement->CreateAttribute('name', $this->Model->Name);
		}

		if($this->Model->Input»CssClass !== null) {
			$this->Model->InputElement->CreateAttribute('class', $this->Model->Input»CssClass);
		}

		if($this->Model->Input»Text !== null) {
			$this->Model->InputElement->CreateAttribute('value', $this->Model->Input»Text);
		}

		if($this->Model->TabIndex !== null) {
			$this->Model->InputElement->CreateAttribute('tabindex', $this->Model->TabIndex);
		}

		if($this->Model->Input»Disabled !== null) {
			$this->Model->InputElement->CreateAttribute('disabled', $this->Model->Input»Disabled);
		}

		if($this->Model->Input»Attributes !== null) {
			foreach($this->Model->Input»Attributes as $key => $value) {
				$this->Model->InputElement->CreateAttribute($key, $value);
			}
		}

		if($this->Model->InputElement->Parent === null) {
			$this->Model->ContainerElement->AppendChild($this->Model->InputElement);
		}
	}
}
?>