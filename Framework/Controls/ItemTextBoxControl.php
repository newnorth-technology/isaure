<?
namespace Framework\Controls;

class ItemTextBoxControl extends \Framework\Controllers\AHtml {
	/* Magic methods */

	public function __construct(array $Params) {
		parent::__construct($Params);

		$this->Model->RootElement = $this->Model->ContainerElement = new \Framework\HTML\Element('div');

		$this->Model->InputElement = new \Framework\HTML\Element('input');

		$this->Model->InputElement->CreateAttribute('type', 'hidden');

		$this->Model->SearchInputElement = new \Framework\HTML\Element('input');

		$this->Model->SearchInputElement->CreateAttribute('type', 'text');

		$this->Model->LabelElement = new \Framework\HTML\Element('label');

		$this->Model->SuggestionListElement = new \Framework\HTML\Element('div');

		$this->Model->ErrorMessagesElement = new \Framework\HTML\Element('div');

		$this->Model->ErrorMessages = [];
	}

	/* Instance life cycle methods */

	public function Initialize() {
		$this->SetControlData('Form', null);

		$this->SetControlData('Name', null);

		$this->SetControlData('Id', $this->Model->Form !== null && $this->Model->Name !== null ? $this->Model->Form.'»'.$this->Model->Name : null);

		$this->SetControlData('Value', null);

		$this->SetControlData('Source', null);

		$this->SetControlData('Query', null);

		$this->SetControlData('Delay', null);

		$this->SetControlData('TabIndex', null);

		$this->SetControlData('Container»CssClass', null);

		$this->SetControlData('Input»Attributes', []);

		$this->SetControlData('SearchInput»CssClass', null);

		$this->SetControlData('SearchInput»Placeholder', null);

		$this->SetControlData('SearchInput»MaxLength', null);

		$this->SetControlData('SearchInput»Disabled', null);

		$this->SetControlData('SearchInput»Readonly', null);

		$this->SetControlData('SearchInput»Attributes', []);

		$this->SetControlData('Label»Show', false);

		$this->SetControlData('Label»CssClass', null);

		$this->SetControlData('Label»Text', '%[["Owner","'.$this->Name.'»Label"],["Controller","Label"]]%');

		$this->SetControlData('Label»Html', null);

		$this->SetControlData('Label»Attributes', []);

		$this->SetControlData('SuggestionList»CssClass', null);

		$this->SetControlData('SuggestionList»Attributes', []);

		$this->SetControlData('ErrorMessages»CssClass', null);

		$this->SetControlData('ErrorMessages»CloseButton', null);

		$this->SetControlData('ErrorMessages»Attributes', []);

		parent::Initialize();
	}

	public function Render() {
		$this->Render»Container();

		$this->Render»Input();

		$this->Render»SearchInput();

		$this->Render»Label();

		$this->Render»SuggestionList();

		$this->Render»ErrorMessages();

		parent::Render();
	}

	public function Render»Container() {
		if($this->Model->Container»CssClass === null) {
			$this->Model->ContainerElement->CreateAttribute('class', 'i-tb i-itb');
		}
		else {
			$this->Model->ContainerElement->CreateAttribute('class', 'i-tb i-itb '.$this->Model->Container»CssClass);
		}

		$this->Model->ContainerElement->CreateAttribute('n:itemtextbox');

		if($this->Model->Delay !== null) {
			$this->Model->ContainerElement->CreateAttribute('n:delay', $this->Model->Delay);
		}
	}

	public function Render»Label() {
		if($this->Model->Label»Show) {
			if($this->Model->Label»CssClass !== null) {
				$this->Model->LabelElement->CreateAttribute('class', $this->Model->Label»CssClass);
			}

			if($this->Model->Id !== null) {
				$this->Model->LabelElement->CreateAttribute('for', $this->Model->Id.'_SearchInput');
			}

			if($this->Model->Label»Html !== null) {
				$this->Model->LabelElement->AppendHtml($this->Model->Label»Html);
			}
			else if($this->Model->Label»Text !== null) {
				$this->Model->LabelElement->AppendText($this->Model->Label»Text);
			}

			if($this->Model->Label»Attributes !== null) {
				foreach($this->Model->Label»Attributes as $key => $value) {
					$this->Model->LabelElement->CreateAttribute($key, $value);
				}
			}

			if($this->Model->LabelElement->Parent === null) {
				$this->Model->ContainerElement->AppendChild($this->Model->LabelElement);
			}
		}
	}

	public function Render»Input() {
		if($this->Model->Id !== null) {
			$this->Model->InputElement->CreateAttribute('id', $this->Model->Id);
		}

		if($this->Model->Form !== null) {
			$this->Model->InputElement->CreateAttribute('form', $this->Model->Form);
		}

		if($this->Model->Name !== null) {
			$this->Model->InputElement->CreateAttribute('name', $this->Model->Name);
		}

		if($this->Model->Value !== null) {
			$this->Model->InputElement->CreateAttribute('value', $this->Model->Value);
		}

		if($this->Model->Input»Attributes !== null) {
			foreach($this->Model->Input»Attributes as $key => $value) {
				$this->Model->InputElement->CreateAttribute($key, $value);
			}
		}

		if($this->Model->InputElement->Parent === null) {
			$this->Model->ContainerElement->AppendChild($this->Model->InputElement);
		}
	}

	public function Render»SearchInput() {
		if($this->Model->SearchInput»CssClass !== null) {
			$this->Model->SearchInputElement->CreateAttribute('class', $this->Model->SearchInput»CssClass);
		}

		if($this->Model->SearchInput»Placeholder !== null) {
			$this->Model->SearchInputElement->CreateAttribute('placeholder', $this->Model->SearchInput»Placeholder);
		}

		if($this->Model->SearchInput»MaxLength !== null) {
			$this->Model->SearchInputElement->CreateAttribute('maxlength', $this->Model->SearchInput»MaxLength);
		}

		if($this->Model->TabIndex !== null) {
			$this->Model->SearchInputElement->CreateAttribute('tabindex', $this->Model->TabIndex);
		}

		$this->Model->SearchInputElement->CreateAttribute('autocomplete', 'off');

		if($this->Model->SearchInput»Disabled !== null) {
			$this->Model->SearchInputElement->CreateAttribute('disabled', $this->Model->SearchInput»Disabled);
		}

		if($this->Model->SearchInput»Readonly !== null) {
			$this->Model->SearchInputElement->CreateAttribute('readonly', $this->Model->SearchInput»Readonly);
		}

		if($this->Model->SearchInput»Attributes !== null) {
			foreach($this->Model->SearchInput»Attributes as $key => $value) {
				$this->Model->SearchInputElement->CreateAttribute($key, $value);
			}
		}

		if($this->Model->SearchInputElement->Parent === null) {
			$this->Model->ContainerElement->AppendChild($this->Model->SearchInputElement);
		}
	}

	public function Render»SuggestionList() {
		if($this->Model->SuggestionList»CssClass === null) {
			$this->Model->SuggestionListElement->CreateAttribute('class', 'i-sl slide up');
		}
		else {
			$this->Model->SuggestionListElement->CreateAttribute('class', 'i-sl slide up '.$this->Model->SuggestionList»CssClass);
		}

		$this->Model->SuggestionListElement->CreateAttribute('n:ajaxcontent');

		$this->Model->SuggestionListElement->CreateAttribute('n:method', 'GET');

		if($this->Model->Query !== null) {
			$this->Model->SuggestionListElement->CreateAttribute('n:data', json_encode($this->Model->Query));
		}

		$this->Model->SuggestionListElement->CreateAttribute('n:source', $this->Model->Source);

		$this->Model->SuggestionListElement->CreateAttribute('n:loadoninit', null);

		$this->Model->SuggestionListElement->CreateAttribute('n:onsuccess', 'N(this.parentNode, N.ItemTextBox.OnSuccess, pv_data);');

		$this->Model->SuggestionListElement->CreateAttribute('n:onpostsuccess', 'N(this.parentNode, N.ItemTextBox.OnPostSuccess, pv_data);');

		$this->Model->SuggestionListElement->CreateAttribute('n:onerror', 'N(this.parentNode, N.ItemTextBox.OnError, pv_data);');

		if($this->Model->SuggestionList»Attributes !== null) {
			foreach($this->Model->SuggestionList»Attributes as $key => $value) {
				$this->Model->SuggestionListElement->CreateAttribute($key, $value);
			}
		}

		if($this->Model->SuggestionListElement->Parent === null) {
			$this->Model->ContainerElement->AppendChild($this->Model->SuggestionListElement);
		}
	}

	public function Render»ErrorMessages() {
		if(0 < count($this->Model->ErrorMessages)) {
			if($this->Model->ErrorMessages»CssClass === null) {
				$this->Model->ErrorMessagesElement->CreateAttribute('class', 'error_messages');
			}
			else {
				$this->Model->ErrorMessagesElement->CreateAttribute('class', 'error_messages '.$this->Model->ErrorMessages»CssClass);
			}

			foreach($this->Model->ErrorMessages»Attributes as $key => $value) {
				$this->Model->ErrorMessagesElement->CreateAttribute($key, $value);
			}

			foreach($this->Model->ErrorMessages as $error_message) {
				$this->Model->ErrorMessagesElement->AppendHtml('<div>'.$error_message.'</div>');
			}

			if($this->Model->ErrorMessages»CloseButton !== null) {
				$this->Model->ErrorMessagesElement->AppendHtml($this->Model->ErrorMessages»CloseButton);
			}

			if($this->Model->ErrorMessagesElement->Parent === null) {
				$this->Model->ContainerElement->AppendChild($this->Model->ErrorMessagesElement);
			}
		}
	}
}
?>