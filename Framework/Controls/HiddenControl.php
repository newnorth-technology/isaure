<?
namespace Framework\Controls;

class HiddenControl extends \Framework\Controllers\AHtml {
	/* Magic methods */

	public function __construct(array $Params) {
		parent::__construct($Params);

		$this->Model->RootElement = $this->Model->ContainerElement = new \Framework\HTML\Element();

		$this->Model->InputElement = new \Framework\HTML\Element('input');

		$this->Model->InputElement->CreateAttribute('type', 'hidden');
	}

	/* Instance life cycle methods */

	public function Initialize() {
		$this->SetControlData('Form', null);

		$this->SetControlData('Name', null);

		$this->SetControlData('Id', $this->Model->Form !== null && $this->Model->Name !== null ? $this->Model->Form.'»'.$this->Model->Name : null);

		$this->SetControlData('Value', null);

		$this->SetControlData('Input»Attributes', []);

		parent::Initialize();
	}

	public function Render() {
		$this->Render»Input();

		parent::Render();
	}

	public function Render»Input() {
		if($this->Model->Id !== null) {
			$this->Model->InputElement->CreateAttribute('id', $this->Model->Id);
		}

		if($this->Model->Form !== null) {
			$this->Model->InputElement->CreateAttribute('form', $this->Model->Form);
		}

		if($this->Model->Name !== null) {
			$this->Model->InputElement->CreateAttribute('name', $this->Model->Name);
		}

		if($this->Model->Value !== null) {
			$this->Model->InputElement->CreateAttribute('value', $this->Model->Value);
		}

		if($this->Model->Input»Attributes !== null) {
			foreach($this->Model->Input»Attributes as $key => $value) {
				$this->Model->InputElement->CreateAttribute($key, $value);
			}
		}

		if($this->Model->InputElement->Parent === null) {
			$this->Model->ContainerElement->AppendChild($this->Model->InputElement);
		}
	}
}
?>