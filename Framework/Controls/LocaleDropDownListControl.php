<?
namespace Framework\Controls;

class LocaleDropDownListControl extends \Framework\Controls\DropDownListControl {
	/* Instance life cycle methods */

	public function Initialize() {
		if(!isset($this->Model->Items)) {
			$this->Model->Items = [];
		}

		foreach(\Framework\GetConfig('Locales') as $locale) {
			$this->Model->Items[] = ['Text' => $locale['Name'], 'Value' => $locale['Id']];
		}

		parent::Initialize();
	}
}
?>