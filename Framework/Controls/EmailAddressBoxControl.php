<?
namespace Framework\Controls;

class EmailAddressBoxControl extends \Framework\Controllers\AHtml {
	/* Magic methods */

	public function __construct(array $Params) {
		parent::__construct($Params);

		$this->Model->RootElement = $this->Model->ContainerElement = new \Framework\HTML\Element('div');

		$this->Model->InputElement = new \Framework\HTML\Element('input');

		$this->Model->InputElement->CreateAttribute('type', 'email');

		$this->Model->LabelElement = new \Framework\HTML\Element('label');

		$this->Model->ErrorMessagesElement = new \Framework\HTML\Element('div');

		$this->Model->ErrorMessages = [];
	}

	/* Instance life cycle methods */

	public function Initialize() {
		$this->SetControlData('Form', null);

		$this->SetControlData('Name', null);

		$this->SetControlData('Id', $this->Model->Form !== null && $this->Model->Name !== null ? $this->Model->Form.'»'.$this->Model->Name : null);

		$this->SetControlData('Value', null);

		$this->SetControlData('TabIndex', null);

		$this->SetControlData('Container»CssClass', null);

		$this->SetControlData('Input»CssClass', null);

		$this->SetControlData('Input»Placeholder', '%[["Owner","'.$this->Name.'»Placeholder"],["Controller","Placeholder"]]%');

		$this->SetControlData('Input»MaxLength', null);

		$this->SetControlData('Input»Autocomplete', null);

		$this->SetControlData('Input»Disabled', null);

		$this->SetControlData('Input»Readonly', null);

		$this->SetControlData('Input»Attributes', []);

		$this->SetControlData('Label»Show', false);

		$this->SetControlData('Label»CssClass', null);

		$this->SetControlData('Label»Text', '%[["Owner","'.$this->Name.'»Label"],["Controller","Label"]]%');

		$this->SetControlData('Label»Html', null);

		$this->SetControlData('Label»Attributes', []);

		$this->SetControlData('ErrorMessages»CssClass', null);

		$this->SetControlData('ErrorMessages»CloseButton', null);

		$this->SetControlData('ErrorMessages»Attributes', []);

		parent::Initialize();
	}

	public function Render() {
		$this->Render»Container();

		$this->Render»Input();

		$this->Render»Label();

		$this->Render»ErrorMessages();

		parent::Render();
	}

	public function Render»Container() {
		if($this->Model->Container»CssClass === null) {
			$this->Model->ContainerElement->CreateAttribute('class', 'i-tb i-eab');
		}
		else {
			$this->Model->ContainerElement->CreateAttribute('class', 'i-tb i-eab '.$this->Model->Container»CssClass);
		}
	}

	public function Render»Label() {
		if($this->Model->Label»Show) {
			if($this->Model->Label»CssClass !== null) {
				$this->Model->LabelElement->CreateAttribute('class', $this->Model->Label»CssClass);
			}

			if($this->Model->Id !== null) {
				$this->Model->LabelElement->CreateAttribute('for', $this->Model->Id);
			}

			if($this->Model->Label»Html !== null) {
				$this->Model->LabelElement->AppendHtml($this->Model->Label»Html);
			}
			else if($this->Model->Label»Text !== null) {
				$this->Model->LabelElement->AppendText($this->Model->Label»Text);
			}

			if($this->Model->Label»Attributes !== null) {
				foreach($this->Model->Label»Attributes as $key => $value) {
					$this->Model->LabelElement->CreateAttribute($key, $value);
				}
			}

			if($this->Model->LabelElement->Parent === null) {
				$this->Model->ContainerElement->AppendChild($this->Model->LabelElement);
			}
		}
	}

	public function Render»Input() {
		if($this->Model->Id !== null) {
			$this->Model->InputElement->CreateAttribute('id', $this->Model->Id);
		}

		if($this->Model->Form !== null) {
			$this->Model->InputElement->CreateAttribute('form', $this->Model->Form);
		}

		if($this->Model->Name !== null) {
			$this->Model->InputElement->CreateAttribute('name', $this->Model->Name);
		}

		if($this->Model->Value !== null) {
			$this->Model->InputElement->CreateAttribute('value', $this->Model->Value);
		}

		if($this->Model->Input»CssClass !== null) {
			$this->Model->InputElement->CreateAttribute('class', $this->Model->Input»CssClass);
		}

		if($this->Model->Input»Placeholder !== null) {
			$this->Model->InputElement->CreateAttribute('placeholder', $this->Model->Input»Placeholder);
		}

		if($this->Model->Input»MaxLength !== null) {
			$this->Model->InputElement->CreateAttribute('maxlength', $this->Model->Input»MaxLength);
		}

		if($this->Model->Input»Autocomplete !== null) {
			$this->Model->InputElement->CreateAttribute('autocomplete', $this->Model->Input»Autocomplete);
		}

		if($this->Model->TabIndex !== null) {
			$this->Model->InputElement->CreateAttribute('tabindex', $this->Model->TabIndex);
		}

		if($this->Model->Input»Disabled !== null) {
			$this->Model->InputElement->CreateAttribute('disabled', $this->Model->Input»Disabled);
		}

		if($this->Model->Input»Readonly !== null) {
			$this->Model->InputElement->CreateAttribute('readonly', $this->Model->Input»Readonly);
		}

		if($this->Model->Input»Attributes !== null) {
			foreach($this->Model->Input»Attributes as $key => $value) {
				$this->Model->InputElement->CreateAttribute($key, $value);
			}
		}

		if($this->Model->InputElement->Parent === null) {
			$this->Model->ContainerElement->AppendChild($this->Model->InputElement);
		}
	}

	public function Render»ErrorMessages() {
		if(0 < count($this->Model->ErrorMessages)) {
			if($this->Model->ErrorMessages»CssClass === null) {
				$this->Model->ErrorMessagesElement->CreateAttribute('class', 'error_messages');
			}
			else {
				$this->Model->ErrorMessagesElement->CreateAttribute('class', 'error_messages '.$this->Model->ErrorMessages»CssClass);
			}

			foreach($this->Model->ErrorMessages»Attributes as $key => $value) {
				$this->Model->ErrorMessagesElement->CreateAttribute($key, $value);
			}

			foreach($this->Model->ErrorMessages as $error_message) {
				$this->Model->ErrorMessagesElement->AppendHtml('<div>'.$error_message.'</div>');
			}

			if($this->Model->ErrorMessages»CloseButton !== null) {
				$this->Model->ErrorMessagesElement->AppendHtml($this->Model->ErrorMessages»CloseButton);
			}

			if($this->Model->ErrorMessagesElement->Parent === null) {
				$this->Model->ContainerElement->AppendChild($this->Model->ErrorMessagesElement);
			}
		}
	}
}
?>