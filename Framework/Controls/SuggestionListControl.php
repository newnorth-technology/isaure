<?
namespace Framework\Controls;

class SuggestionListControl extends \Framework\Controllers\AHtml {
	/* Magic methods */

	public function __construct(array $Params) {
		parent::__construct($Params);

		$this->Model->RootElement = $this->Model->ContainerElement = new \Framework\HTML\Element('div');

	}

	/* Instance life cycle methods */

	public function Initialize() {
		$this->SetControlData('Id', null);

		$this->SetControlData('CssClass', null);

		$this->SetControlData('Input', null);

		$this->SetControlData('Source', null);

		$this->SetControlData('Attributes', []);

		parent::Initialize();
	}

	public function Render() {
		$this->Render»Container();

		parent::Render();
	}

	public function Render»Container() {
		if($this->Model->Id !== null) {
			$this->Model->ContainerElement->CreateAttribute('id', $this->Model->Id);
		}

		if($this->Model->CssClass === null) {
			$this->Model->ContainerElement->CreateAttribute('class', 'i-sl slide up');
		}
		else {
			$this->Model->ContainerElement->CreateAttribute('class', 'i-sl slide up '.$this->Model->CssClass);
		}

		$this->Model->ContainerElement->CreateAttribute('n:suggestionlist');

		if($this->Model->Input !== null) {
			$this->Model->ContainerElement->CreateAttribute('n:input', $this->Model->Input);
		}

		if($this->Model->Source !== null) {
			$this->Model->ContainerElement->CreateAttribute('n:source', $this->Model->Source);
		}

		if($this->Model->Attributes !== null) {
			foreach($this->Model->Attributes as $key => $value) {
				$this->Model->InputElement->CreateAttribute($key, $value);
			}
		}
	}
}
?>