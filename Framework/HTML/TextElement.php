<?
namespace Framework\HTML;

class TextElement extends AElement {
	/* Instance variables */

	public $Text;

	/* Constructor */

	public function __construct($Text) {
		$this->Text = $Text;
	}

	/* Magic methods */

	public function __toString() {
		$text = $this->Text;

		$targets = \Framework\Translator::FindTargets($text);

		foreach($targets as $i => $target) {
			$text = preg_replace('/(?<!%)%'.preg_quote($target, '/').'%(?!%)/', '%\\$'.$i.'%', $text);
		}

		$text = htmlspecialchars($text, ENT_QUOTES);

		$text = str_replace("\n", '&#13;', $text);

		$text = str_replace("\r", '&#10;', $text);

		foreach($targets as $i => $target) {
			$text = preg_replace('/(?<!%)%\\$'.$i.'%(?!%)/', '%'.$target.'%', $text);
		}

		return $text;
	}
}
?>