<?
namespace Framework\HTML;

class Element extends AElement {
	/* Instance variables */

	public $Name;

	public $Attributes;

	public $Children;

	/* Constructor */

	public function __construct($Name = null) {
		$this->Name = $Name;

		$this->Attributes = new \Framework\HTML\Attributes();

		$this->Children = new \Framework\HTML\Elements();
	}

	/* Magic methods */

	public function __toString() {
		if($this->Name === null) {
			if(0 < count($this->Children->Items)) {
				return $this->Children->__toString();
			}
			else {
				return '';
			}
		}
		else if(0 < count($this->Attributes->Items)) {
			if(0 < count($this->Children->Items)) {
				return '<'.$this->Name.' '.$this->Attributes.'>'.$this->Children.'</'.$this->Name.'>';
			}
			else if($this->Name === 'input') {
				return '<'.$this->Name.' '.$this->Attributes.'/>';
			}
			else {
				return '<'.$this->Name.' '.$this->Attributes.'></'.$this->Name.'>';
			}
		}
		else if(0 < count($this->Children->Items)) {
			return '<'.$this->Name.'>'.$this->Children.'</'.$this->Name.'>';
		}
		else if($this->Name === 'input') {
			return '<'.$this->Name.'/>';
		}
		else {
			return '<'.$this->Name.'></'.$this->Name.'>';
		}
	}

	/* Instance class methods */

	public function AddClass(string $Class) {
		if($this->Attributes->IsSet('class')) {
			$this->Attributes->Append('class', $Class);
		}
		else {
			$this->Attributes->Create('class', [$Class]);
		}
	}

	public function AddClasses(array $Classes) {
		if($this->Attributes->IsSet('class')) {
			$this->Attributes->Append('class', $Classes);
		}
		else {
			$this->Attributes->Create('class', $Classes);
		}
	}

	/* Instance methods */

	public function CreateAttribute($Key, $Value = null) {
		$this->Attributes->Create($Key, $Value);

		return $this;
	}

	public function HasAttribute($Key) {
		return $this->Attributes->IsSet($Key);
	}

	public function GetAttribute($Key) {
		return $this->Attributes->Get($Key);
	}

	public function SetAttribute($Key, $Value) {
		return $this->Attributes->Set($Key, $Value);
	}

	public function PrependChild(\Framework\HTML\AElement $Element) {
		$Element->Parent = $this;

		$this->Children->Prepend($Element);

		return $this;
	}

	public function PrependHtml($Html) {
		$Element = new \Framework\HTML\HtmlElement($Html);

		$Element->Parent = $this;

		$this->Children->Prepend($Element);

		return $this;
	}

	public function PrependText($Text) {
		$Element = new \Framework\HTML\TextElement($Text);

		$Element->Parent = $this;

		$this->Children->Prepend($Element);

		return $this;
	}

	public function AppendChild(\Framework\HTML\AElement $Element) {
		$Element->Parent = $this;

		$this->Children->Append($Element);

		return $this;
	}

	public function AppendHtml($Html) {
		$Element = new \Framework\HTML\HtmlElement($Html);

		$Element->Parent = $this;

		$this->Children->Append($Element);

		return $this;
	}

	public function AppendText($Text) {
		$Element = new \Framework\HTML\TextElement($Text);

		$Element->Parent = $this;

		$this->Children->Append($Element);

		return $this;
	}

	public function RemoveChild(\Framework\HTML\AElement $Element) {
		$Element->Parent = null;

		$this->Children->Remove($Element);

		return $this;
	}
}
?>