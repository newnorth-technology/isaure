<?
namespace Framework\HTML;

class Attribute {
	/* Instance variables */

	public $Key;

	public $Value;

	/* Constructor */

	public function __construct($Key, $Value) {
		$this->Key = $Key;

		$this->Value = $Value;
	}

	/* Magic methods */

	public function __toString() {
		if($this->Value === null) {
			return $this->Key;
		}
		else if(is_array($this->Value)) {
			$value = implode(' ', $this->Value);

			$targets = \Framework\Translator::FindTargets($value);

			foreach($targets as $i => $target) {
				$value = preg_replace('/(?<!%)%'.preg_quote($target, '/').'%(?!%)/', '%\\$'.$i.'%', $value);
			}

			$value = htmlspecialchars($value, ENT_QUOTES);

			foreach($targets as $i => $target) {
				$value = preg_replace('/(?<!%)%\\$'.$i.'%(?!%)/', '%'.$target.'%', $value);
			}

			return $this->Key.'="'.$value.'"';
		}
		else {
			$value = $this->Value;

			$targets = \Framework\Translator::FindTargets($value);

			foreach($targets as $i => $target) {
				$value = preg_replace('/(?<!%)%'.preg_quote($target, '/').'%(?!%)/', '%\\$'.$i.'%', $value);
			}

			$value = htmlspecialchars($value, ENT_QUOTES);

			foreach($targets as $i => $target) {
				$value = preg_replace('/(?<!%)%\\$'.$i.'%(?!%)/', '%'.$target.'%', $value);
			}

			return $this->Key.'="'.$value.'"';
		}
	}
}
?>