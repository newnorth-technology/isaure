<?
namespace Framework\HTML;

class Attributes {
	/* Instance variables */

	public $Items = [];

	public $ItemsByKey = [];

	/* Magic methods */

	public function __toString() {
		if(0 < count($this->Items)) {
			$HTML = $this->Items[0]->__toString();

			for($I = 1; $I < count($this->Items); ++$I) {
				$HTML .= ' '.$this->Items[$I]->__toString();
			}

			return $HTML;
		}
		else {
			return '';
		}
	}

	/* Instance methods */

	public function Create(string $Key, $Value) {
		$Attribute = new \Framework\HTML\Attribute($Key, $Value);

		$this->Items[] = $Attribute;

		$this->ItemsByKey[$Key] = $Attribute;

		return $this;
	}

	public function IsSet(string $Key) {
		return isset($this->ItemsByKey[$Key]);
	}

	public function Get(string $Key) {
		return $this->ItemsByKey[$Key]->Value;
	}

	public function TryGet(string $Key, &$Value) : bool {
		if(isset($this->ItemsByKey[$Key])) {
			$Value = $this->ItemsByKey[$Key]->Value;

			return true;
		}
		else {
			return false;
		}
	}

	public function Set(string $Key, $Value) {
		$this->ItemsByKey[$Key]->Value = $Value;
	}

	public function Append(string $Key, $Value) {
		if(is_array($Value)) {
			$this->ItemsByKey[$Key]->Value = array_merge($this->ItemsByKey[$Key]->Value, $Value);
		}
		else {
			$this->ItemsByKey[$Key]->Value[] = $Value;
		}
	}
}
?>