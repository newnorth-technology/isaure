<?
namespace Framework;

class NullableFloatDataMember extends \Framework\ADynamicNullableDataMember {
	/* Static methods */

	public static function ParseFromDbValue(\Framework\NullableFloatDataMember $DataMember = null, $Value) {
		if($Value === null) {
			return null;
		}

		/*{IF:DEBUG}*/

		if(!is_float($Value) && !is_int($Value) && !is_string($Value)) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, invalid value type.',
				['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name, 'Value' => $Value]
			);
		}

		/*{ENDIF}*/

		return (float)$Value;
	}

	public static function ParseValue(\Framework\NullableFloatDataMember $DataMember = null, $Value) {
		/*{IF:DEBUG}*/

		if($Value !== null && !is_float($Value) && !is_int($Value)) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, invalid value type.',
				['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name, 'Value' => $Value]
			);
		}

		/*{ENDIF}*/

		return $Value;
	}

	public static function ParseToDbValue(\Framework\NullableFloatDataMember $DataMember = null, $Value) {
		return $Value;
	}
}
?>