<?
namespace Framework\AController;

class Controls implements \ArrayAccess, \Iterator {
	/* Instance variables */

	private $items = [];

	/* Instance \ArrayAccess methods */

	public function offsetSet($pv_offset, $pv_item) {
		if(is_null($pv_offset)) {
			$parent = $pv_item;

			while($parent->Parent !== null) {
				$parent = $parent->Parent;
			}

			$this->items[$parent->Name] = $pv_item;
		} else {
			$this->items[$pv_offset] = $pv_item;
		}
	}

	public function offsetUnset($pv_offset) {
		unset($this->items[$pv_offset]);
	}

	public function offsetExists($pv_offset) {
		return isset($this->items[$pv_offset]);
	}

	public function offsetGet($pv_offset) {
		return $this->items[$pv_offset];
	}

	/* instance \Iterator methods */

	public function rewind() {
		reset($this->items);
	}

	public function current() {
		return current($this->items);
	}

	public function key() {
		return key($this->items);
	}

	public function next() {
		return next($this->items);
	}

	public function valid() {
		return key($this->items) !== NULL;
	}

	/* Instance life cycle methods */

	public function Initialize() {
		foreach($this->items as $control) {
			if(!$control->IsTemplate) {
				$control->Initialize();
			}
		}
	}

	public function Load() {
		foreach($this->items as $control) {
			if(!$control->IsTemplate) {
				$control->Load();
			}
		}
	}

	public function Execute() {
		foreach($this->items as $control) {
			if(!$control->IsTemplate) {
				$control->Execute();
			}
		}
	}

	public function Render() {
		foreach($this->items as $control) {
			if(!$control->IsTemplate) {
				$control->Render();
			}
		}
	}

	/* Instance methods */

	public function Add(\Framework\AController $Control) {
		$parent = $Control;

		while($parent->Parent !== null) {
			$parent = $parent->Parent;
		}

		$this->items[$parent->Name] = $Control;
	}

	public function Remove(string $Name) {
		unset($this->items[$Name]);
	}

	public function Get(string $Name) : \Framework\AController {
		return $this->items[$Name];
	}

	public function TryGet(string $Name, \Framework\AController &$Control = null) : bool {
		if(isset($this->items[$Name])) {
			$Control = $this->items[$Name];

			return true;
		}
		else {
			return false;
		}
	}

	public function Contains(string $Name) : bool {
		return isset($this->items[$Name]);
	}
}
?>