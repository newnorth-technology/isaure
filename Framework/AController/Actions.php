<?
namespace Framework\AController;

class Actions implements \ArrayAccess {
	/* Instance variables */

	public $Items = [];

	/* Instance ArrayAccess methods */

	public function offsetSet($pv_offset, $pv_item) {
		if(is_null($pv_offset)) {
			$this->Items[$pv_item->Name] = $pv_item;
		} else {
			$this->Items[$pv_offset] = $pv_item;
		}
	}

	public function offsetUnset($pv_offset) {
		unset($this->Items[$pv_offset]);
	}

	public function offsetExists($pv_offset) {
		return isset($this->Items[$pv_offset]);
	}

	public function offsetGet($pv_offset) {
		return $this->Items[$pv_offset];
	}

	/* Instance life cycle methods */

	public function Execute(\Framework\AController $Controller, \Framework\Model $Model) {
		foreach($this->Items as $action) {
			$action->Variables->Initialize($Controller, $Model);

			if($action->method !== null && $action->method !== $_SERVER['REQUEST_METHOD']) {
				continue;
			}

			if($action->PreValidators->Validate($Controller, $action)) {
				$action->PreFormat();

				$action->Initialize();

				try {
					$action->Lock();

					$action->Load();

					if($action->Validate()) {
						$action->Format();

						$result = $action->Execute();

						if(is_array($result)) {
							foreach($result as $error_message) {
								$action->Messages[] = \Framework\Translator::Translate($error_message, ['Action' => '%["Actions.'.$action->Id.'.%$0%"%,$1...%]%']);
							}
						}
						else if(is_string($result)) {
							$action->Messages[] = \Framework\Translator::Translate($result, ['Action' => '%["Actions.'.$action->Id.'.%$0%"%,$1...%]%']);
						}
					}
				}
				catch(\Exception $Exception) {
					throw $Exception;
				}
				finally {
					$action->Unlock();
				}
			}
		}
	}

	/* Instance methods */

	public function Add(\Framework\AAction $Action) {
		$this->Items[$Action->Name] = $Action;
	}

	public function Get(string $Name) : \Framework\AAction {
		return $this->Items[$Name];
	}

	public function TryGet(string $Name, \Framework\AAction &$Action = null) : bool {
		if(isset($this->Items[$Name])) {
			$Action = $this->Items[$Name];

			return true;
		}
		else {
			return false;
		}
	}

	public function Contains(string $Name) : bool {
		return isset($this->Items[$Name]);
	}
}
?>