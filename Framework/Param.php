<?
namespace Framework;

function GetParam(string $Key, $DefaultValue = null) {
	return $GLOBALS['Params'][$Key] ?? $DefaultValue;
}

function TryGetParam(string $Key, &$Value) : bool {
	return ($Value = $GLOBALS['Params'][$Key] ?? null) !== null;
}

function SetParam(string $Key, $Value) {
	$GLOBALS['Params'][$Key] = $Value;
}

function IsParamSet(string $Key) : bool {
	return isset($GLOBALS['Params'][$Key]);
}

function IsParamNotSet(string $Key) : bool {
	return !isset($GLOBALS['Params'][$Key]);
}

function IsParamEmpty(string $Key) : bool {
	return !isset($GLOBALS['Params'][$Key][0]);
}

function IsParamNotEmpty(string $Key) : bool {
	return isset($GLOBALS['Params'][$Key][0]);
}
?>