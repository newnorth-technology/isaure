<?
namespace Framework;

class DataManagers {
	/* Static variables */

	public static $Items = [];

	/* Static initialization methods */

	public static function Initialize() {
		/*{IF:BENCHMARK}*/

		\Framework\Benchmark::StartProcess(['Initialize data managers']);

		/*{ENDIF}*/

		$data = \Framework\GetConfig('DataManagers', []);

		ksort($data);

		foreach($data as $id => $params) {
			$dataManager = DataManagers::Initialize»LoadDataManager($id, $params);

			$dataType = DataManagers::Initialize»LoadDataType($id, $params);

			$name = str_replace(['»', '-', '/'], ['|', '|', '|'], $params['Table'] ?? $id);

			$name = explode('|', $name);

			$name = $name[count($name) - 1];

			DataManagers::$Items[$id] = new $dataManager['Class'](
				$id,
				$name,
				$dataType,
				$params
			);
		}

		foreach(DataManagers::$Items as $dm) {
			$dm->Initialize();

			$dm->Initialize»DataMembers();
		}

		foreach($data as $id => $params) {
			$dm = DataManagers::$Items[$id];

			$dm->Initialize»InsertQuery($params['InsertQuery'] ?? null);

			$dm->Initialize»UpdateQuery($params['UpdateQuery'] ?? null);

			$dm->Initialize»SelectQuery($params['SelectQuery'] ?? null);

			if(isset($params['Slaves'])) {
				foreach($params['Slaves'] as $slave_id => $slave_params) {
					$slave = $dm->Slaves[$slave_id];

					$slave->Initialize»SelectQuery($slave_params['SelectQuery'] ?? null);
				}
			}
		}

		foreach(DataManagers::$Items as $dm) {
			$dm->Initialize»References();
		}

		/*{IF:BENCHMARK}*/

		\Framework\Benchmark::FinishProcess();

		/*{ENDIF}*/
	}

	private static function Initialize»LoadDataManager(string $Id, array $Params) : array {
		$file = str_replace(['»', '-'], ['/', '/'], $Id).'DataManager.php';

		$class = '\\'.str_replace(['»', '-', '/'], ['\\', '\\', '\\'], $Id).'DataManager';

		if(DataManagers::Initialize»LoadDataManager»LoadClass($file, $class, $baseDir)) {
			if(!class_exists($class, false)) {
				throw new \Framework\RuntimeException(
					'Data manager class not found.',
					[
						'Id' => $Id,
						'Base directory' => $baseDir,
						'File' => $file,
						'Class' => $class
					]
				);
			}

			return [
				'Id' => $Id,
				'Base directory' => $baseDir,
				'File' => $file,
				'Class' => $class
			];
		}

		$file = 'Framework/'.$Params['Type'].'DataManager.php';

		$class = '\\Framework\\'.$Params['Type'].'DataManager';

		if(DataManagers::Initialize»LoadDataManager»LoadClass($file, $class, $baseDir)) {
			return [
				'Id' => $Id,
				'Base directory' => $baseDir,
				'File' => $file,
				'Class' => $class
			];
		}

		throw new \Framework\RuntimeException(
			'Data manager file not found.',
			[
				'Id' => $Id,
				'File' => $file,
				'Class' => $class
			]
		);
	}

	private static function Initialize»LoadDataManager»LoadClass(string $FilePath, string $Class, string &$BaseDir = null) : bool {
		foreach(\Framework\GetConfig('System.Directories.DataManagers', ['']) as $baseDir) {
			if(file_exists($baseDir.$FilePath)) {
				if(!class_exists($Class, false)) {
					@include($baseDir.$FilePath);
				}

				$BaseDir = $baseDir;

				return true;
			}

			if(file_exists(utf8_decode($baseDir.$FilePath))) {
				if(!class_exists($Class, false)) {
					@include(utf8_decode($baseDir.$FilePath));
				}

				$BaseDir = $baseDir;

				return true;
			}
		}

		return false;
	}

	private static function Initialize»LoadDataType(string $Id, array $Params) : array {
		$file = str_replace(['»', '-'], ['/', '/'], $Id).'DataType.php';

		$class = '\\'.str_replace(['»', '-', '/'], ['\\', '\\', '\\'], $Id).'DataType';

		if(DataManagers::Initialize»LoadDataType»LoadClass($file, $class, $baseDir)) {
			if(!class_exists($class, false)) {
				throw new \Framework\RuntimeException(
					'Data type class not found.',
					[
						'ID' => $Id,
						'Base directory' => $baseDir,
						'File' => $file,
						'Class' => $class
					]
				);
			}

			return [
				'Id' => $Id,
				'Base directory' => $baseDir,
				'File' => $file,
				'Class' => $class
			];
		}

		$file = 'Framework/DataType.php';

		$class = '\\Framework\\DataType';

		if(DataManagers::Initialize»LoadDataType»LoadClass($file, $class, $baseDir)) {
			return [
				'Id' => $Id,
				'Base directory' => $baseDir,
				'File' => $file,
				'Class' => $class
			];
		}

		throw new \Framework\RuntimeException(
			'Data type file not found.',
			[
				'ID' => $Id,
				'File' => $file,
				'Class' => $class
			]
		);
	}

	private static function Initialize»LoadDataType»LoadClass(string $FilePath, string $Class, string &$BaseDir = null) : bool {
		foreach(\Framework\GetConfig('System.Directories.DataTypes', ['']) as $baseDir) {
			if(file_exists($baseDir.$FilePath)) {
				if(!class_exists($Class, false)) {
					@include($baseDir.$FilePath);
				}

				$BaseDir = $baseDir;

				return true;
			}

			if(file_exists(utf8_decode($baseDir.$FilePath))) {
				if(!class_exists($Class, false)) {
					@include(utf8_decode($baseDir.$FilePath));
				}

				$BaseDir = $baseDir;

				return true;
			}
		}

		return false;
	}
}

function GetDataManager(string $Alias) : \Framework\ADataManager {
	/*{IF:DEBUG}*/

	if(!isset(\Framework\DataManagers::$Items[$Alias])) {
		throw new \Framework\RuntimeException(
			'Data manager not found.',
			['Alias' => $Alias]
		);
	}

	/*{ENDIF}*/

	return \Framework\DataManagers::$Items[$Alias];
}

function HasDataManager(string $Alias) : bool {
	return isset(\Framework\DataManagers::$Items[$Alias]);
}

function TryGetDataManager(string $Alias, &$DataManager = null) : bool {
	if(isset(\Framework\DataManagers::$Items[$Alias])) {
		$DataManager = \Framework\DataManagers::$Items[$Alias];

		return true;
	}
	else {
		return false;
	}
}

function GetDataMember(string $DataManagerAlias, string $DataMemberAlias) : \Framework\ADataMember {
	/*{IF:DEBUG}*/

	if(!isset(\Framework\DataManagers::$Items[$DataManagerAlias])) {
		throw new \Framework\RuntimeException(
			'Data manager not found.',
			['Alias' => $DataManagerAlias]
		);
	}

	if(!isset(\Framework\DataManagers::$Items[$DataManagerAlias]->DataMembers[$DataMemberAlias])) {
		throw new \Framework\RuntimeException(
			'Data member not found.',
			['Data manager' => $DataManagerAlias, 'Alias' => $DataMemberAlias]
		);
	}

	/*{ENDIF}*/

	return \Framework\DataManagers::$Items[$DataManagerAlias]->DataMembers[$DataMemberAlias];
}
?>