<?
namespace Framework\ADataManager;

class Reference {
	/* Instance variables */

	public $DataManager;

	public $Name;

	public $DataMember;

	public $DataMemberConnection;

	public $TargetDataManager;

	public $TargetDataMember;

	public $TargetDataMemberConnection;

	public $ConnectionDataManager;

	public $OnDeleteAction;

	public $OnDeletedAction;

	public $InsertQuery;

	public $UpdateQuery;

	public $SelectQuery;

	/* Magic methods */

	public function __construct(\Framework\ADataManager $DataManager, string $Name, array $Params) {
		$this->DataManager = $DataManager;

		$this->Name = $Name;

		$this->DataMember = $Params['DataMember'] ?? null;

		$this->DataMemberConnection = $Params['DataMemberConnection'] ?? null;

		$this->TargetDataManager = $Params['TargetDataManager'];

		$this->TargetDataMember = $Params['TargetDataMember'] ?? null;

		$this->TargetDataMemberConnection = $Params['TargetDataMemberConnection'] ?? null;

		$this->ConnectionDataManager = $Params['ConnectionDataManager'] ?? null;

		$this->OnDeleteAction = $Params['OnDeleteAction'] ?? null;

		$this->OnDeletedAction = $Params['OnDeletedAction'] ?? null;

		$this->InsertQuery = $Params['InsertQuery'] ?? [];

		$this->UpdateQuery = $Params['UpdateQuery'] ?? [];

		$this->SelectQuery = $Params['SelectQuery'] ?? [];
	}

	public function __debugInfo() {
		return [
			'DataManager' => $this->DataManager->Name,
			'Name' => $this->Name,
			'TargetDataManager' => $this->TargetDataManager->Name,
			'ConnectionDataManager' => $this->ConnectionDataManager === null ? null : $this->ConnectionDataManager->Name,
			'OnDeleteAction' => $this->OnDeleteAction,
			'OnDeletedAction' => $this->OnDeletedAction
		];
	}

	/* Instance methods */

	public function Initialize() {
		if(!\Framework\TryGetDataManager($this->TargetDataManager, $this->TargetDataManager)) {
			throw new \Framework\RuntimeException(
				'Unable to initialize reference, target data manager not found.',
				[
					'Reference' => $this->Name,
					'Target data manager' => $this->TargetDataManager
				]
			);
		}

		if($this->ConnectionDataManager !== null) {
			if(!\Framework\TryGetDataManager($this->ConnectionDataManager, $this->ConnectionDataManager)) {
				throw new \Framework\RuntimeException(
					'Unable to initialize reference, connection data manager not found.',
					[
						'Reference' => $this->Name,
						'Connection data manager' => $this->ConnectionDataManager
					]
				);
			}
		}

		$this->Initialize»InsertQuery();

		$this->Initialize»SelectQuery();
	}

	public function Initialize»InsertQuery() {
		if($this->ConnectionDataManager === null) {
			$dm = $this->TargetDataManager;
		}
		else {
			$dm = $this->ConnectionDataManager;
		}

		$query = $dm->InsertQuery->Clone();

		$this->Initialize»InsertQuery»Columns($query, $dm);

		$this->Initialize»InsertQuery»Values($query, $dm);

		$this->InsertQuery = $query;
	}

	public function Initialize»InsertQuery»Columns(\Framework\Database\InsertQuery $Query, \Framework\ADataManager $DataManager) {
		if(array_key_exists('Columns', $this->InsertQuery)) {
			$Query->MergeColumns($this->InsertQuery['Columns']);
		}
		else if($DataManager instanceof \Framework\TranslationDataManager) {
			$Query->MergeColumns([
				'ParentId' => new \Framework\Database\Columns\Reference([
					'Name' => 'ParentId'
				])
			]);
		}
		else if(array_key_exists($this->DataManager->Name.'Id', $DataManager->DataMembers)) {
			$Query->MergeColumns([
				$this->DataManager->Name.'Id' => new \Framework\Database\Columns\Reference([
					'Name' => $this->DataManager->Name.'Id'
				])
			]);
		}

		if(array_key_exists('<Columns', $this->InsertQuery)) {
			$Query->MergeColumns($this->InsertQuery['<Columns']);
		}
	}

	public function Initialize»InsertQuery»Values(\Framework\Database\InsertQuery $Query, \Framework\ADataManager $DataManager) {
		if(array_key_exists('Values', $this->InsertQuery)) {
			$Query->MergeValues($this->InsertQuery['Values']);
		}
		else if($DataManager instanceof \Framework\TranslationDataManager) {
			$Query->MergeValues([
				$this->DataManager->Name.'Id' => new \Framework\Database\Columns\Value([
					'Variable' => $this->DataManager->Id.'.Id'
				])
			]);
		}
		else if(array_key_exists($this->DataManager->Name.'Id', $DataManager->DataMembers)) {
			$Query->MergeValues([
				$this->DataManager->Name.'Id' => new \Framework\Database\Columns\Value([
					'Variable' => $this->DataManager->Id.'.Id'
				])
			]);
		}

		if(array_key_exists('<Values', $this->InsertQuery)) {
			$Query->MergeValues($this->InsertQuery['<Values']);
		}
	}

	public function Initialize»SelectQuery() {
		$query = $this->TargetDataManager->SelectQuery->Clone();

		$this->Initialize»SelectQuery»Columns($query);

		$this->Initialize»SelectQuery»Source($query);

		$this->Initialize»SelectQuery»Condition($query);

		$this->Initialize»SelectQuery»Sorts($query);

		$query->SetSourceAlias($this->TargetDataManager->Id, $this->Name);

		$this->SelectQuery = $query;
	}

	public function Initialize»SelectQuery»Columns(\Framework\Database\SelectQuery $Query) {
		if(isset($this->SelectQuery['Columns'])) {
			$Query->SetColumns($this->SelectQuery['Columns']);
		}

		if(isset($this->SelectQuery['<Columns'])) {
			$Query->MergeColumns($this->SelectQuery['<Columns']);
		}
	}

	public function Initialize»SelectQuery»Source(\Framework\Database\SelectQuery $Query) {
		if($this->ConnectionDataManager !== null) {
			$Query->Sources[$this->ConnectionDataManager->Id] = new \Framework\Database\Source([
				'DataManager' => $this->ConnectionDataManager,
				'Alias' => $this->ConnectionDataManager->Id
			]);
		}
	}

	public function Initialize»SelectQuery»Condition(\Framework\Database\SelectQuery $Query) {
		if($this->DataManager instanceof \Framework\NewConnectionDataManager || $this->DataManager instanceof \Framework\NewStandardDataManager || $this->DataManager instanceof \Framework\NewTranslationDataManager) {
			if($this->TargetDataManager instanceof \Framework\NewConnectionDataManager || $this->TargetDataManager instanceof \Framework\NewStandardDataManager || $this->TargetDataManager instanceof \Framework\NewTranslationDataManager) {
				if($this->ConnectionDataManager === null) {
					if(isset($this->SelectQuery['Condition'])) {
						$condition = $this->SelectQuery['Condition'];
					}
					else {
						if($this->TargetDataMember !== null) {
							$columnA = $this->TargetDataManager->DataMembers[$this->TargetDataMember];
						}
						else if(isset($this->TargetDataManager->DataMembers[$this->DataManager->Name.'_id'])) {
							$columnA = $this->TargetDataManager->DataMembers[$this->DataManager->Name.'_id'];
						}
						else if($this->TargetDataManager instanceof \Framework\NewTranslationDataManager) {
							$columnA = $this->TargetDataManager->DataMembers['parent_id'];
						}
						else {
							$columnA = $this->TargetDataManager->DataMembers['id'];
						}

						if($this->DataMember !== null) {
							$columnB = new \Framework\Database\Columns\Reference([
								'Source' => $this->DataManager->Id,
								'Name' => $this->DataMember,
								'Variable' => $this->DataManager->Id.'.'.$this->DataMember
							]);
						}
						else if(isset($this->DataManager->DataMembers[$this->TargetDataManager->Name.'_id'])) {
							$columnB = new \Framework\Database\Columns\Reference([
								'Source' => $this->DataManager->Id,
								'Name' => $this->TargetDataManager->Name.'_id',
								'Variable' => $this->DataManager->Id.'.'.$this->TargetDataManager->Name.'_id'
							]);
						}
						else if($this->DataManager instanceof \Framework\NewTranslationDataManager) {
							$columnB = new \Framework\Database\Columns\Reference([
								'Source' => $this->DataManager->Id,
								'Name' => 'parent_id',
								'Variable' => $this->DataManager->Id.'.parent_id'
							]);
						}
						else {
							$columnB = new \Framework\Database\Columns\Reference([
								'Source' => $this->DataManager->Id,
								'Name' => 'id',
								'Variable' => $this->DataManager->Id.'.id'
							]);
						}

						$condition = new \Framework\Database\Conditions\IsEqualTo(['ColumnA' => $columnA, 'ColumnB' => $columnB]);
					}
				}
				else if($this->ConnectionDataManager instanceof \Framework\NewConnectionDataManager || $this->ConnectionDataManager instanceof \Framework\NewStandardDataManager || $this->ConnectionDataManager instanceof \Framework\NewTranslationDataManager) {
					if(isset($this->SelectQuery['Condition'])) {
						$condition = new \Framework\Database\Conditions\All([
							'Conditions' => [
								$this->SelectQuery['Condition'],
								new \Framework\Database\Conditions\IsEqualTo([
									'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->TargetDataMemberConnection ?? $this->TargetDataManager->Name.'_id'],
									'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->Name, 'Name' => 'id'])
								]),
								new \Framework\Database\Conditions\IsEqualTo([
									'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->DataMemberConnection ?? $this->DataManager->Name.'_id'],
									'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->DataManager->Id, 'Name' => 'id', 'Variable' => $this->DataManager->Id.'.id'])
								])
							]
						]);
					}
					else {
						$condition = new \Framework\Database\Conditions\All([
							'Conditions' => [
								new \Framework\Database\Conditions\IsEqualTo([
									'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->TargetDataMemberConnection ?? $this->TargetDataManager->Name.'_id'],
									'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->Name, 'Name' => 'id'])
								]),
								new \Framework\Database\Conditions\IsEqualTo([
									'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->DataMemberConnection ?? $this->DataManager->Name.'_id'],
									'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->DataManager->Id, 'Name' => 'id', 'Variable' => $this->DataManager->Id.'.id'])
								])
							]
						]);
					}
				}
				else if(isset($this->SelectQuery['Condition'])) {
					$condition = new \Framework\Database\Conditions\All([
						'Conditions' => [
							$this->SelectQuery['Condition'],
							new \Framework\Database\Conditions\IsEqualTo([
								'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->TargetDataMemberConnection ?? $this->TargetDataManager->Name2.'Id'],
								'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->Name, 'Name' => 'id'])
							]),
							new \Framework\Database\Conditions\IsEqualTo([
								'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->DataMemberConnection ?? $this->DataManager->Name2.'Id'],
								'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->DataManager->Id, 'Name' => 'id', 'Variable' => $this->DataManager->Id.'.id'])
							])
						]
					]);
				}
				else {
					$condition = new \Framework\Database\Conditions\All([
						'Conditions' => [
							new \Framework\Database\Conditions\IsEqualTo([
								'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->TargetDataMemberConnection ?? $this->TargetDataManager->Name2.'Id'],
								'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->Name, 'Name' => 'id'])
							]),
							new \Framework\Database\Conditions\IsEqualTo([
								'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->DataMemberConnection ?? $this->DataManager->Name2.'Id'],
								'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->DataManager->Id, 'Name' => 'id', 'Variable' => $this->DataManager->Id.'.id'])
							])
						]
					]);
				}
			}
			else {
				if($this->ConnectionDataManager === null) {
					if(isset($this->SelectQuery['Condition'])) {
						$condition = $this->SelectQuery['Condition'];
					}
					else {
						if($this->TargetDataMember !== null) {
							$columnA = $this->TargetDataManager->DataMembers[$this->TargetDataMember];
						}
						else if(isset($this->TargetDataManager->DataMembers[$this->DataManager->Name.'Id'])) {
							$columnA = $this->TargetDataManager->DataMembers[$this->DataManager->Name.'Id'];
						}
						else if($this->TargetDataManager instanceof \Framework\TranslationDataManager) {
							$columnA = $this->TargetDataManager->DataMembers['ParentId'];
						}
						else {
							$columnA = $this->TargetDataManager->DataMembers['Id'];
						}

						if($this->DataMember !== null) {
							$columnB = new \Framework\Database\Columns\Reference([
								'Source' => $this->DataManager->Id,
								'Name' => $this->DataMember,
								'Variable' => $this->DataManager->Id.'.'.$this->DataMember
							]);
						}
						else if(isset($this->DataManager->DataMembers[$this->TargetDataManager->Name.'_id'])) {
							$columnB = new \Framework\Database\Columns\Reference([
								'Source' => $this->DataManager->Id,
								'Name' => $this->TargetDataManager->Name.'_id',
								'Variable' => $this->DataManager->Id.'.'.$this->TargetDataManager->Name.'_id'
							]);
						}
						else if($this->DataManager instanceof \Framework\NewTranslationDataManager) {
							$columnB = new \Framework\Database\Columns\Reference([
								'Source' => $this->DataManager->Id,
								'Name' => 'parent_id',
								'Variable' => $this->DataManager->Id.'.parent_id'
							]);
						}
						else {
							$columnB = new \Framework\Database\Columns\Reference([
								'Source' => $this->DataManager->Id,
								'Name' => 'id',
								'Variable' => $this->DataManager->Id.'.id'
							]);
						}

						$condition = new \Framework\Database\Conditions\IsEqualTo(['ColumnA' => $columnA, 'ColumnB' => $columnB]);
					}
				}
				else if($this->ConnectionDataManager instanceof \Framework\NewConnectionDataManager || $this->ConnectionDataManager instanceof \Framework\NewStandardDataManager || $this->ConnectionDataManager instanceof \Framework\NewTranslationDataManager) {
					if(isset($this->SelectQuery['Condition'])) {
						$condition = new \Framework\Database\Conditions\All([
							'Conditions' => [
								$this->SelectQuery['Condition'],
								new \Framework\Database\Conditions\IsEqualTo([
									'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->TargetDataMemberConnection ?? $this->TargetDataManager->Name.'Id'],
									'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->Name, 'Name' => 'id'])
								]),
								new \Framework\Database\Conditions\IsEqualTo([
									'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->DataMemberConnection ?? $this->DataManager->Name.'_id'],
									'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->DataManager->Id, 'Name' => 'id', 'Variable' => $this->DataManager->Id.'.id'])
								])
							]
						]);
					}
					else {
						$condition = new \Framework\Database\Conditions\All([
							'Conditions' => [
								new \Framework\Database\Conditions\IsEqualTo([
									'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->TargetDataMemberConnection ?? $this->TargetDataManager->Name.'Id'],
									'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->Name, 'Name' => 'id'])
								]),
								new \Framework\Database\Conditions\IsEqualTo([
									'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->DataMemberConnection ?? $this->DataManager->Name.'_id'],
									'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->DataManager->Id, 'Name' => 'id', 'Variable' => $this->DataManager->Id.'.id'])
								])
							]
						]);
					}
				}
				else if(isset($this->SelectQuery['Condition'])) {
					$condition = new \Framework\Database\Conditions\All([
						'Conditions' => [
							$this->SelectQuery['Condition'],
							new \Framework\Database\Conditions\IsEqualTo([
								'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->TargetDataMemberConnection ?? $this->TargetDataManager->Name2.'Id'],
								'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->Name, 'Name' => 'id'])
							]),
							new \Framework\Database\Conditions\IsEqualTo([
								'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->DataMemberConnection ?? $this->DataManager->Name2.'Id'],
								'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->DataManager->Id, 'Name' => 'id', 'Variable' => $this->DataManager->Id.'.id'])
							])
						]
					]);
				}
				else {
					$condition = new \Framework\Database\Conditions\All([
						'Conditions' => [
							new \Framework\Database\Conditions\IsEqualTo([
								'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->TargetDataMemberConnection ?? $this->TargetDataManager->Name2.'Id'],
								'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->Name, 'Name' => 'id'])
							]),
							new \Framework\Database\Conditions\IsEqualTo([
								'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->DataMemberConnection ?? $this->DataManager->Name2.'Id'],
								'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->DataManager->Id, 'Name' => 'id', 'Variable' => $this->DataManager->Id.'.id'])
							])
						]
					]);
				}
			}
		}
		else if($this->TargetDataManager instanceof \Framework\NewConnectionDataManager || $this->TargetDataManager instanceof \Framework\NewStandardDataManager || $this->TargetDataManager instanceof \Framework\NewTranslationDataManager) {
			if($this->ConnectionDataManager === null) {
				if(isset($this->SelectQuery['Condition'])) {
					$condition = $this->SelectQuery['Condition'];
				}
				else {
					if($this->TargetDataMember !== null) {
						$columnA = $this->TargetDataManager->DataMembers[$this->TargetDataMember];
					}
					else if(isset($this->TargetDataManager->DataMembers[$this->DataManager->Name.'_id'])) {
						$columnA = $this->TargetDataManager->DataMembers[$this->DataManager->Name.'_id'];
					}
					else if($this->TargetDataManager instanceof \Framework\NewTranslationDataManager) {
						$columnA = $this->TargetDataManager->DataMembers['parent_id'];
					}
					else {
						$columnA = $this->TargetDataManager->DataMembers['id'];
					}

					if($this->DataMember !== null) {
						$columnB = new \Framework\Database\Columns\Reference([
							'Source' => $this->DataManager->Id,
							'Name' => $this->DataMember,
							'Variable' => $this->DataManager->Id.'.'.$this->DataMember
						]);
					}
					else if(isset($this->DataManager->DataMembers[$this->TargetDataManager->Name.'Id'])) {
						$columnB = new \Framework\Database\Columns\Reference([
							'Source' => $this->DataManager->Id,
							'Name' => $this->TargetDataManager->Name.'Id',
							'Variable' => $this->DataManager->Id.'.'.$this->TargetDataManager->Name.'Id'
						]);
					}
					else if($this->DataManager instanceof \Framework\TranslationDataManager) {
						$columnB = new \Framework\Database\Columns\Reference([
							'Source' => $this->DataManager->Id,
							'Name' => 'ParentId',
							'Variable' => $this->DataManager->Id.'.ParentId'
						]);
					}
					else {
						$columnB = new \Framework\Database\Columns\Reference([
							'Source' => $this->DataManager->Id,
							'Name' => 'Id',
							'Variable' => $this->DataManager->Id.'.Id'
						]);
					}

					$condition = new \Framework\Database\Conditions\IsEqualTo(['ColumnA' => $columnA, 'ColumnB' => $columnB]);
				}
			}
			else if($this->ConnectionDataManager instanceof \Framework\NewConnectionDataManager || $this->ConnectionDataManager instanceof \Framework\NewStandardDataManager || $this->ConnectionDataManager instanceof \Framework\NewTranslationDataManager) {
				if(isset($this->SelectQuery['Condition'])) {
					$condition = new \Framework\Database\Conditions\All([
						'Conditions' => [
							$this->SelectQuery['Condition'],
							new \Framework\Database\Conditions\IsEqualTo([
								'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->TargetDataMemberConnection ?? $this->TargetDataManager->Name.'_id'],
								'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->Name, 'Name' => 'Id'])
							]),
							new \Framework\Database\Conditions\IsEqualTo([
								'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->DataMemberConnection ?? $this->DataManager->Name.'_id'],
								'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->DataManager->Id, 'Name' => 'id', 'Variable' => $this->DataManager->Id.'.id'])
							])
						]
					]);
				}
				else {
					$condition = new \Framework\Database\Conditions\All([
						'Conditions' => [
							new \Framework\Database\Conditions\IsEqualTo([
								'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->TargetDataMemberConnection ?? $this->TargetDataManager->Name.'_id'],
								'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->Name, 'Name' => 'id'])
							]),
							new \Framework\Database\Conditions\IsEqualTo([
								'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->DataMemberConnection ?? $this->DataManager->Name.'_id'],
								'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->DataManager->Id, 'Name' => 'id', 'Variable' => $this->DataManager->Id.'.id'])
							])
						]
					]);
				}
			}
			else if(isset($this->SelectQuery['Condition'])) {
				$condition = new \Framework\Database\Conditions\All([
					'Conditions' => [
						$this->SelectQuery['Condition'],
						new \Framework\Database\Conditions\IsEqualTo([
							'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->TargetDataMemberConnection ?? $this->TargetDataManager->Name2.'Id'],
							'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->Name, 'Name' => 'Id'])
						]),
						new \Framework\Database\Conditions\IsEqualTo([
							'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->DataMemberConnection ?? $this->DataManager->Name2.'Id'],
							'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->DataManager->Id, 'Name' => 'id', 'Variable' => $this->DataManager->Id.'.id'])
						])
					]
				]);
			}
			else {
				$condition = new \Framework\Database\Conditions\All([
					'Conditions' => [
						new \Framework\Database\Conditions\IsEqualTo([
							'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->TargetDataMemberConnection ?? $this->TargetDataManager->Name2.'Id'],
							'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->Name, 'Name' => 'id'])
						]),
						new \Framework\Database\Conditions\IsEqualTo([
							'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->DataMemberConnection ?? $this->DataManager->Name2.'Id'],
							'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->DataManager->Id, 'Name' => 'id', 'Variable' => $this->DataManager->Id.'.id'])
						])
					]
				]);
			}
		}
		else {
			if($this->ConnectionDataManager === null) {
				if(isset($this->SelectQuery['Condition'])) {
					$condition = $this->SelectQuery['Condition'];
				}
				else {
					if($this->TargetDataMember !== null) {
						$columnA = $this->TargetDataManager->DataMembers[$this->TargetDataMember];
					}
					else if(isset($this->TargetDataManager->DataMembers[$this->DataManager->Name.'Id'])) {
						$columnA = $this->TargetDataManager->DataMembers[$this->DataManager->Name.'Id'];
					}
					else if($this->TargetDataManager instanceof \Framework\TranslationDataManager) {
						$columnA = $this->TargetDataManager->DataMembers['ParentId'];
					}
					else {
						$columnA = $this->TargetDataManager->DataMembers['Id'];
					}

					if($this->DataMember !== null) {
						$columnB = new \Framework\Database\Columns\Reference([
							'Source' => $this->DataManager->Id,
							'Name' => $this->DataMember,
							'Variable' => $this->DataManager->Id.'.'.$this->DataMember
						]);
					}
					else if(isset($this->DataManager->DataMembers[$this->TargetDataManager->Name.'Id'])) {
						$columnB = new \Framework\Database\Columns\Reference([
							'Source' => $this->DataManager->Id,
							'Name' => $this->TargetDataManager->Name.'Id',
							'Variable' => $this->DataManager->Id.'.'.$this->TargetDataManager->Name.'Id'
						]);
					}
					else if($this->DataManager instanceof \Framework\TranslationDataManager) {
						$columnB = new \Framework\Database\Columns\Reference([
							'Source' => $this->DataManager->Id,
							'Name' => 'ParentId',
							'Variable' => $this->DataManager->Id.'.ParentId'
						]);
					}
					else {
						$columnB = new \Framework\Database\Columns\Reference([
							'Source' => $this->DataManager->Id,
							'Name' => 'Id',
							'Variable' => $this->DataManager->Id.'.Id'
						]);
					}

					$condition = new \Framework\Database\Conditions\IsEqualTo(['ColumnA' => $columnA, 'ColumnB' => $columnB]);
				}
			}
			else if($this->ConnectionDataManager instanceof \Framework\NewConnectionDataManager || $this->ConnectionDataManager instanceof \Framework\NewStandardDataManager || $this->ConnectionDataManager instanceof \Framework\NewTranslationDataManager) {
				if(isset($this->SelectQuery['Condition'])) {
					$condition = new \Framework\Database\Conditions\All([
						'Conditions' => [
							$this->SelectQuery['Condition'],
							new \Framework\Database\Conditions\IsEqualTo([
								'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->TargetDataMemberConnection ?? $this->TargetDataManager->Name.'Id'],
								'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->Name, 'Name' => 'Id'])
							]),
							new \Framework\Database\Conditions\IsEqualTo([
								'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->DataMemberConnection ?? $this->DataManager->Name.'Id'],
								'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->DataManager->Id, 'Name' => 'Id', 'Variable' => $this->DataManager->Id.'.Id'])
							])
						]
					]);
				}
				else {
					$condition = new \Framework\Database\Conditions\All([
						'Conditions' => [
							new \Framework\Database\Conditions\IsEqualTo([
								'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->TargetDataMemberConnection ?? $this->TargetDataManager->Name.'Id'],
								'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->Name, 'Name' => 'Id'])
							]),
							new \Framework\Database\Conditions\IsEqualTo([
								'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->DataMemberConnection ?? $this->DataManager->Name.'Id'],
								'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->DataManager->Id, 'Name' => 'Id', 'Variable' => $this->DataManager->Id.'.Id'])
							])
						]
					]);
				}
			}
			else if(isset($this->SelectQuery['Condition'])) {
				$condition = new \Framework\Database\Conditions\All([
					'Conditions' => [
						$this->SelectQuery['Condition'],
						new \Framework\Database\Conditions\IsEqualTo([
							'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->TargetDataMemberConnection ?? $this->TargetDataManager->Name.'Id'],
							'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->Name, 'Name' => 'Id'])
						]),
						new \Framework\Database\Conditions\IsEqualTo([
							'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->DataMemberConnection ?? $this->DataManager->Name.'Id'],
							'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->DataManager->Id, 'Name' => 'Id', 'Variable' => $this->DataManager->Id.'.Id'])
						])
					]
				]);
			}
			else {
				$condition = new \Framework\Database\Conditions\All([
					'Conditions' => [
						new \Framework\Database\Conditions\IsEqualTo([
							'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->TargetDataMemberConnection ?? $this->TargetDataManager->Name.'Id'],
							'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->Name, 'Name' => 'Id'])
						]),
						new \Framework\Database\Conditions\IsEqualTo([
							'ColumnA' => $this->ConnectionDataManager->DataMembers[$this->DataMemberConnection ?? $this->DataManager->Name.'Id'],
							'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->DataManager->Id, 'Name' => 'Id', 'Variable' => $this->DataManager->Id.'.Id'])
						])
					]
				]);
			}
		}

		if($Query->Condition === null) {
			$Query->SetCondition($condition);
		}
		else {
			$Query->MergeCondition($condition);
		}
	}

	public function Initialize»SelectQuery»Sorts(\Framework\Database\SelectQuery $Query) {
		if(isset($this->SelectQuery['Sorts'])) {
			$Query->SetSorts($this->SelectQuery['Sorts']);
		}

		if(isset($this->SelectQuery['<Sorts'])) {
			$Query->MergeSorts($this->SelectQuery['<Sorts']);
		}
	}

	public function TryOnDelete(\Framework\DataType $Item) : bool {
		switch($this->OnDeleteAction) {
			case 'Block': {
				return $this->Count($Item) === 0;
			}
			case 'Delete': {
				$items = $this->FindAll($Item);

				if($this->ConnectionDataManager === null) {
					foreach($items as $item) {
						if(!$item->TryOnDelete()) {
							return false;
						}
					}
				}
				else {
					foreach($items as $item) {
						if(!$item->Connection->TryOnDelete()) {
							return false;
						}
					}
				}

				return true;
			}
			default: {
				return true;
			}
		}
	}

	public function OnDelete(\Framework\DataType $Item) {
		switch($this->OnDeleteAction) {
			case 'Delete': {
				$this->Delete($Item);

				break;
			}
		}
	}

	public function OnDeleted(\Framework\DataType $Item) {
		switch($this->OnDeletedAction) {
			case 'Delete': {
				$this->Delete($Item);

				break;
			}
		}
	}

	private function PrepareInsertQuery(\Framework\DataType $DataType, $Query, array $Params = null) : \Framework\Database\InsertQuery {
		if($Query === null) {
			$query = $this->InsertQuery->Clone();
		}
		else if($Query instanceof \Framework\Database\InsertQuery) {
			$query = $Query->Clone();
		}
		else {
			$query = $this->InsertQuery->Clone();

			if(array_key_exists('Columns', $Query)) {
				$query->MergeColumns($Query['Columns']);

				/*{IF:DEBUG}*/

				unset($Query['Columns']);

				/*{ENDIF}*/
			}

			if(array_key_exists('Values', $Query)) {
				$query->MergeValues($Query['Values']);

				/*{IF:DEBUG}*/

				unset($Query['Values']);

				/*{ENDIF}*/
			}

			/*{IF:DEBUG}*/

			if(0 < count($Query)) {
				throw new \Framework\RuntimeException(
					'Unable to parse query, trailing data found.',
					['Data' => $Query]
				);
			}

			/*{ENDIF}*/
		}

		if(isset($GLOBALS['Locale'])) {
			$query->SetVariable('Locale', $GLOBALS['Locale']->Id);
		}

		foreach($this->DataManager->DataMembers as $name => $dataMember) {
			$query->SetVariable($this->DataManager->Id.'.'.$name, $DataType->$name);
		}

		if(isset($Params['Variables'])) {
			foreach($Params['Variables'] as $key => $value) {
				$query->SetVariable($key, $value);
			}
		}

		return $query;
	}

	private function PrepareSelectQuery(\Framework\DataType $DataType, $Query, array $Params = null) : \Framework\Database\SelectQuery {
		if($Query === null) {
			$query = $this->SelectQuery->Clone();
		}
		else if($Query instanceof \Framework\Database\SelectQuery) {
			$query = $Query->Clone();
		}
		else {
			$query = $this->SelectQuery->Clone();

			if(array_key_exists('Columns', $Query)) {
				$query->SetColumns($Query['Columns']);

				/*{IF:DEBUG}*/

				unset($Query['Columns']);

				/*{ENDIF}*/
			}

			if(array_key_exists('Sources', $Query)) {
				$query->MergeSources($Query['Sources']);

				/*{IF:DEBUG}*/

				unset($Query['Sources']);

				/*{ENDIF}*/
			}

			if(array_key_exists('Condition', $Query)) {
				$query->MergeCondition($Query['Condition']);

				/*{IF:DEBUG}*/

				unset($Query['Condition']);

				/*{ENDIF}*/
			}

			if(array_key_exists('Groups', $Query)) {
				$query->SetGroups($Query['Groups']);

				/*{IF:DEBUG}*/

				unset($Query['Groups']);

				/*{ENDIF}*/
			}

			if(array_key_exists('Having', $Query)) {
				$query->MergeHaving($Query['Having']);

				/*{IF:DEBUG}*/

				unset($Query['Having']);

				/*{ENDIF}*/
			}

			if(array_key_exists('Sorts', $Query)) {
				$query->SetSorts($Query['Sorts']);

				/*{IF:DEBUG}*/

				unset($Query['Sorts']);

				/*{ENDIF}*/
			}

			if(array_key_exists('Limit', $Query)) {
				$query->SetLimit($Query['Limit']);

				/*{IF:DEBUG}*/

				unset($Query['Limit']);

				/*{ENDIF}*/
			}

			/*{IF:DEBUG}*/

			if(0 < count($Query)) {
				throw new \Framework\RuntimeException(
					'Unable to parse query, trailing data found.',
					['Data' => $Query]
				);
			}

			/*{ENDIF}*/
		}

		$this->PrepareSelectQuery»References($DataType, $query, $Params);

		if(isset($Params['References'])) {
			foreach($Params['References'] as $key => $value) {
				if($value instanceof \Framework\ADataManager) {
					$query->SetSourceAlias($value->Id, $key);
				}
				else if(is_bool($value)) {
					$query->SetSourceAlias($this->TargetDataManager->References[$key]->TargetDataManager->Id, $key);
				}
			}
		}

		if(isset($GLOBALS['Locale'])) {
			$query->SetVariable('Locale', $GLOBALS['Locale']->Id);
		}

		foreach($this->DataManager->DataMembers as $name => $dataMember) {
			if(property_exists($DataType, $name)) {
				$query->SetVariable($this->DataManager->Id.'.'.$name, $DataType->$name);
			}
		}

		if(isset($Params['Variables'])) {
			foreach($Params['Variables'] as $key => $value) {
				$query->SetVariable($key, $value);
			}
		}

		$query->SetSourceAlias($this->TargetDataManager->Id, $this->Name);

		return $query;
	}

	private function PrepareSelectQuery»References(\Framework\DataType $DataType, \Framework\Database\SelectQuery $Query, array $Params = null) {
		if(isset($Params['References'])) {
			$this->PrepareSelectQuery»References»Columns($DataType, $Query, $Params);

			$this->PrepareSelectQuery»References»Sources($DataType, $Query, $Params);
		}
	}

	private function PrepareSelectQuery»References»Columns(\Framework\DataType $DataType, \Framework\Database\SelectQuery $Query, array $Params = null) {
		$items = [];

		foreach($Params['References'] as $key => $value) {
			if($value instanceof \Framework\ADataManager) {
				foreach($value->DataMembers as $dataMember) {
					$items[$key.'.'.$dataMember->Name] = new \Framework\Database\Columns\Reference([
						'Source' => $value->Id,
						'Name' => $dataMember->Name
					]);
				}
			}
			else if(is_bool($value)) {
				foreach($this->TargetDataManager->References[$key]->TargetDataManager->DataMembers as $dataMember) {
					$items[$key.'.'.$dataMember->Name] = new \Framework\Database\Columns\Reference([
						'Source' => $this->TargetDataManager->References[$key]->Name,
						'Name' => $dataMember->Name
					]);
				}
			}
		}

		$Query->MergeColumns($items);
	}

	private function PrepareSelectQuery»References»Sources(\Framework\DataType $DataType, \Framework\Database\SelectQuery $Query, array $Params = null) {
		$items = [];

		foreach($Params['References'] as $key => $value) {
			if(isset($Query->Sources[$this->Name]->Joins[$key])) {
				continue;
			}

			if(is_bool($value)) {
				$items[$key] = new \Framework\Database\Join([
					'IsRequired' => $value,
					'Sources' => $this->TargetDataManager->References[$key]->SelectQuery->Sources->Clone(),
					'Condition' => $this->TargetDataManager->References[$key]->SelectQuery->Condition->Clone()
				]);
			}
		}

		$Query->Sources[$this->Name]->MergeJoins($items);
	}

	public function Insert(\Framework\DataType $DataType, $Query = null, array $Params = null) {
		if($this->ConnectionDataManager === null) {
			$dm = $this->TargetDataManager;
		}
		else {
			$dm = $this->ConnectionDataManager;
		}

		$query = $this->PrepareInsertQuery($DataType, $Query, $Params);

		$connection = $dm->GetConnection($Params['Connection'] ?? null);

		$id = $connection->Insert($query);

		if(0 < count($dm->Slaves)) {
			$query->MergeColumns([$dm->DataMembers['Id']]);

			$query->MergeValues([$id]);

			foreach($dm->Slaves as $slave) {
				$slave->Connection->Insert($query);
			}
		}

		$dm->OnInserted($dm->FindById($id));

		return $id;
	}

	public function Delete(\Framework\DataType $Item, $Query = null, array $Params = null) {
		if($Query instanceof \Framework\DataType) {
			$items = [$Query];
		}
		else if(is_array($Query) && isset($Query[0]) && $Query[0] instanceof \Framework\DataType) {
			$items = $Query;
		}
		else {
			$items = $this->FindAll($Item, $Query, $Params);
		}

		if(isset($items[0])) {
			if($this->ConnectionDataManager === null) {
				return $this->TargetDataManager->Delete($items);
			}
			else {
				return $this->ConnectionDataManager->Delete($items);
			}
		}
	}

	public function Find(\Framework\DataType $DataType, $Query = null, array $Params = null) {
		if($this->ConnectionDataManager !== null) {
			if($Params === null) {
				$Params = ['References' => ['Connection' => $this->ConnectionDataManager]];
			}
			else if(isset($Params['References'])) {
				$Params['References']['Connection'] = $this->ConnectionDataManager;
			}
			else {
				$Params['References'] = ['Connection' => $this->ConnectionDataManager];
			}
		}

		$query = $this->PrepareSelectQuery($DataType, $Query, $Params);

		if($this->ConnectionDataManager === null) {
			$connection = $this->TargetDataManager->GetConnection($Params['Connection'] ?? null);
		}
		else {
			$connection = $this->ConnectionDataManager->GetConnection($Params['Connection'] ?? null);
		}

		$result = $connection->Find($query);

		if($result === false) {
			return null;
		}

		if(!$result->FetchAssoc()) {
			return null;
		}

		$item = $this->TargetDataManager->Instantiate($result, $Params['Extras'] ?? null, $Params['References'] ?? null);

		return $item;
	}

	public function TryFind(\Framework\DataType &$Result = null, \Framework\DataType $DataType, $Query = null, array $Params = null) : bool {
		$result = $this->Find($DataType, $Query, $Params);

		if($result === null) {
			return false;
		}
		else {
			$Result = $result;

			return true;
		}
	}

	public function FindAll(\Framework\DataType $DataType, $Query = null, array $Params = null) : array {
		if($this->ConnectionDataManager !== null) {
			if($Params === null) {
				$Params = ['References' => ['Connection' => $this->ConnectionDataManager]];
			}
			else if(isset($Params['References'])) {
				$Params['References']['Connection'] = $this->ConnectionDataManager;
			}
			else {
				$Params['References'] = ['Connection' => $this->ConnectionDataManager];
			}
		}

		$query = $this->PrepareSelectQuery($DataType, $Query, $Params);

		if($this->ConnectionDataManager === null) {
			$connection = $this->TargetDataManager->GetConnection($Params['Connection'] ?? null);
		}
		else {
			$connection = $this->ConnectionDataManager->GetConnection($Params['Connection'] ?? null);
		}

		$result = $connection->Find($query);

		if($result === false) {
			return null;
		}

		$items = [];

		while($result->FetchAssoc()) {
			$items[] = $this->TargetDataManager->Instantiate($result, $Params['Extras'] ?? null, $Params['References'] ?? null);
		}

		return $items;
	}

	public function TryFindAll(array &$Result = null, \Framework\DataType $DataType, $Query = null, array $Params = null) : bool {
		$result = $this->FindAll($DataType, $Query, $Params);

		if(count($result) === 0) {
			return false;
		}
		else {
			$Result = $result;

			return true;
		}
	}

	public function Count(\Framework\DataType $DataType, $Query = null, array $Params = null) : int {
		if($this->ConnectionDataManager !== null) {
			if($Params === null) {
				$Params = ['References' => ['Connection' => $this->ConnectionDataManager]];
			}
			else if(isset($Params['References'])) {
				$Params['References']['Connection'] = $this->ConnectionDataManager;
			}
			else {
				$Params['References'] = ['Connection' => $this->ConnectionDataManager];
			}
		}

		$query = $this->PrepareSelectQuery($DataType, $Query, $Params);

		$query->SetColumns([
			new \Framework\Database\Columns\Count()
		]);

		if($this->ConnectionDataManager === null) {
			$connection = $this->TargetDataManager->GetConnection($Params['Connection'] ?? null);
		}
		else {
			$connection = $this->ConnectionDataManager->GetConnection($Params['Connection'] ?? null);
		}

		$value = $connection->Count($query);

		return $value;
	}

	public function SumFloat(\Framework\DataType $DataType, $Column, $Query = null, array $Params = null) : float {
		if($this->ConnectionDataManager !== null) {
			if($Params === null) {
				$Params = ['References' => ['Connection' => $this->ConnectionDataManager]];
			}
			else if(isset($Params['References'])) {
				$Params['References']['Connection'] = $this->ConnectionDataManager;
			}
			else {
				$Params['References'] = ['Connection' => $this->ConnectionDataManager];
			}
		}

		$query = $this->PrepareSelectQuery($DataType, $Query, $Params);

		$query->SetColumns([
			new \Framework\Database\Columns\Sum([
				'Column' => $Column
			])
		]);

		if($this->ConnectionDataManager === null) {
			$connection = $this->TargetDataManager->GetConnection($Params['Connection'] ?? null);
		}
		else {
			$connection = $this->ConnectionDataManager->GetConnection($Params['Connection'] ?? null);
		}

		$value = $connection->SumFloat($query);

		return $value;
	}
}
?>