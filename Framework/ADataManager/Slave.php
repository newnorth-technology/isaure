<?
namespace Framework\ADataManager;

class Slave {
	/* Instance variables */

	public $DataManager;

	public $Connection;

	public $Database;

	public $Table;

	public $IsReadOnly;

	private $SelectQuery;

	/* Magic methods */

	public function __construct(\Framework\ADataManager $DataManager, string $Connection, array $Params) {
		$this->DataManager = $DataManager;

		$this->Connection = \Framework\GetDatabase($Connection);

		$this->Database = $Params['Database'] ?? null;

		$this->Table = $Params['Table'] ?? $DataManager->Table;

		$this->IsReadOnly = $Params['IsReadOnly'] ?? true;
	}

	public function __debugInfo() {
		return [
			'DataManager' => $this->DataManager->Id,
			'Connection' => $this->Connection->Name,
			'Database' => $this->Database,
			'Table' => $this->Table
		];
	}

	/* Instance methods */

	public function Initialize»SelectQuery(array $Params = null) {
		$this->SelectQuery = $this->DataManager->SelectQuery->Clone();

		if(isset($Params['Condition'])) {
			$this->SelectQuery->MergeCondition($Params['Condition']);
		}
	}

	public function CreateTable() {
		$query = 'DROP TABLE IF EXISTS `'.$this->Table.'`';

		$result = $this->Connection->Query($query);

		if($result === false) {
			throw new \Framework\RuntimeException(
				'MySQL error when executing query.',
				[
					'Error number' => $this->Connection->BaseConnection->errno,
					'Error messages' => $this->Connection->BaseConnection->error,
					'Query string' => $query,
				]
			);
		}

		if($this->DataManager->Database === null) {
			$query = 'SHOW CREATE TABLE `'.$this->DataManager->Table.'`';
		}
		else {
			$query = 'SHOW CREATE TABLE `'.$this->DataManager->Database.'`.`'.$this->DataManager->Table.'`';
		}

		$query = $this->DataManager->Connection->Query($query);

		$query->Fetch();

		$query = $query->GetString(1);

		$query = str_replace('  ', '', $query);

		$query = str_replace('CREATE TABLE `'.$this->DataManager->Table.'`', 'CREATE TABLE `'.$this->Table.'`', $query);

		$query = str_replace(' AUTO_INCREMENT,', ',', $query);

		$query = preg_replace('/,\nCONSTRAINT.+?(?=,\n|\n)/', '', $query);

		$query = preg_replace('/ AUTO_INCREMENT=[0-9]+/', '', $query);

		$result = $this->Connection->Query($query);

		if($result === false) {
			throw new \Framework\RuntimeException(
				'MySQL error when executing query.',
				[
					'Error number' => $this->Connection->BaseConnection->errno,
					'Error messages' => $this->Connection->BaseConnection->error,
					'Query string' => $query,
				]
			);
		}

		$result = $this->DataManager->Connection->FindAll($this->SelectQuery);

		$insertQuery = new \Framework\Database\InsertQuery([
			'Target' => new \Framework\Database\Target(['DataManager' => $this->DataManager]),
			'Columns' => $this->DataManager->DataMembers,
		]);

		while($result->FetchAssoc()) {
			$insertQuery->SetValues($result->Row);

			$this->Connection->Insert($insertQuery);
		}
	}

	public function Sync() {
		$this->Connection->Truncate(
			new \Framework\Database\Source([
				'DataManager' => $this->DataManager
			])
		);

		$result = $this->DataManager->Connection->FindAll($this->SelectQuery);

		$insertQuery = new \Framework\Database\InsertQuery([
			'Target' => new \Framework\Database\Target(['DataManager' => $this->DataManager]),
			'Columns' => $this->DataManager->DataMembers,
		]);

		while($result->FetchAssoc()) {
			$insertQuery->SetValues($result->Row);

			$this->Connection->Insert($insertQuery);
		}
	}
}
?>