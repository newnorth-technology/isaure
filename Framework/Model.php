<?
namespace Framework;

class Model {
	/* Magic methods */

	public function __construct(array $Params) {
		foreach($Params['Model'] ?? [] as $key => $value) {
			$this->$key = $value;
		}
	}
}
?>