<?
namespace Framework;

class RuntimeException extends Exception {
	/* Magic methods */

	public function __construct($Message, $Data = [], \Exception $InnerException = null) {
		parent::__construct(
			'Runtime exception',
			$Message,
			$Data,
			$InnerException
		);
	}
}
?>