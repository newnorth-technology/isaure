<?
namespace Framework;

class DataTablePage extends \Framework\Controllers\AHtml {
	/* Instance variables */

	public $Renderer = '\\Framework\\Renderers\\Json';

	public $ContentType = 'application/json';

	public $Data = [];

	public $DataManager = null;

	public $Columns = null;

	/* Instance life cycle methods */

	public function Load() {
		$items = $this->DataManager->FindAll();

		foreach($items as $item) {
			$this->Data[] = $this->FormatItem($item);
		}
	}

	public function Render() {
		ob_start();

		$this->Render»Translate($this->Data);

		$this->RenderContent();

		ob_end_flush();
	}

	private function Render»Translate(&$Data) {
		if(is_array($Data)) {
			foreach($Data as &$data) {
				$this->Render»Translate($data);
			}
		}
		else if(is_string($Data)) {
			$Data = \Framework\Translator::Translate($Data);
		}
	}

	/* Instance method methods */

	public function FormatItem(\Framework\DataType $Item) : array {
		$item = [];

		foreach($this->Columns as $column) {
			if(method_exists($this, 'FormatItem»'.$column)) {
				$item[$column] = $this->{'FormatItem»'.$column}($Item);
			}
			else {
				$item[$column] = $Item->$column;
			}
		}

		return $item;
	}
}
?>