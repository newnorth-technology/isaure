<?
namespace Framework\Route;

class Match {
	/* Instance variables */

	public $Url;

	public $rest_url;

	public $LocaleId;

	public $Params;

	/* Magic methods */

	public function __construct(string $pv_url = null, string $pv_rest_url = null, string $pv_locale_id = null, array $pv_params) {
		$this->Url = $pv_url;

		$this->rest_url = $pv_rest_url;

		$this->LocaleId = $pv_locale_id;

		$this->Params = $pv_params;
	}

	public function __debugInfo() {
		return [
			'Url' => $this->Url,
			'rest_url' => $this->rest_url,
			'LocaleId' => $this->LocaleId,
			'Params' => $this->Params,
		];
	}
}
?>