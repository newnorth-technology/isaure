<?
namespace Framework\Route;

class Pattern {
	/* Instance variables */

	public $Route;

	public $LocaleId;

	public $Segments = [];

	/* Magic methods */

	public function __construct(\Framework\Route $Route, string $LocaleId = null, array $Params) {
		$this->Route = $Route;

		$this->LocaleId = $LocaleId;

		foreach($Params as $name => $params) {
			$segment = '\\Framework\\Route\\Pattern\\Segments\\'.$params['Type'];

			$segment = new $segment($this, $params);

			$this->Segments[] = $segment;
		}
	}

	public function __debugInfo() {
		return [
			'LocaleId' => $this->LocaleId,
			'Segments' => $this->Segments,
		];
	}

	/* Instance methods */

	public function match(string $pv_url = null, array $pv_params, array &$pv_matches) : int {
		/*{IF:DEBUG}*/
		if(array_key_exists('debug_route', $_GET)) {
			echo '<div><b>pattern::match(', $pv_url === null ? 'null' : '"'.$pv_url.'"', ', [...], [...])</b></div>';

			echo '<div style="padding-left: 2rem;">';

			echo '<div>- locale_id: "', $this->LocaleId, '"</div>';

			if(count($this->Segments) === 0) {
				$pv_matches[] = new \Framework\Route\Pattern\Match($pv_url, $pv_url, $pv_params);

				$match_count = 1;
			}
			else {
				$match_count = $this->match_continued($pv_url, $pv_url, $pv_params, 0, $pv_matches);
			}

			echo '<div>return ', $match_count, ';</div>';

			echo '</div>';

			return $match_count;
		}
		else if(count($this->Segments) === 0) {
			$pv_matches[] = new \Framework\Route\Pattern\Match($pv_url, $pv_url, $pv_params);

			return 1;
		}
		else {
			return $this->match_continued($pv_url, $pv_url, $pv_params, 0, $pv_matches);
		}
		/*{ELSE}*/
		if(count($this->Segments) === 0) {
			$pv_matches[] = new \Framework\Route\Pattern\Match($pv_url, $pv_url, $pv_params);

			return 1;
		}
		else {
			return $this->match_continued($pv_url, $pv_url, $pv_params, 0, $pv_matches);
		}
		/*{ENDIF}*/
	}

	public function match_continued(string $pv_url = null, string $pv_rest_url = null, array $pv_params, int $pv_segment, array &$pv_matches) : int {
		/*{IF:DEBUG}*/
		if(array_key_exists('debug_route', $_GET)) {
			$segment = $this->Segments[$pv_segment];

			$match_count = 0;

			echo '<div><b>pattern::match_continued(', $pv_url === null ? 'null' : '"'.$pv_url.'"', ', ', $pv_rest_url === null ? 'null' : '"'.$pv_rest_url.'"', ', [...], ', json_encode($pv_segment), ', [...])</b></div>';

			echo '<div style="padding-left: 2rem;">';

			echo '<div>- name: "', $segment->Name, '"</div>';

			echo '<div>- is_optional: ', $segment->IsOptional ? 'true' : 'false', '</div>';

			if($segment->RegExp === null) {
				echo '<div>- regex: null</div>';
			}
			else {
				echo '<div>- regex: "', $segment->RegExp, '" [', preg_match($segment->RegExp, $pv_rest_url), ']</div>';
			}

			if($pv_rest_url === null) {
				echo '<div>$pv_rest_url === null</div>';

				if($segment->IsOptional) {
					if(count($this->Segments) === $pv_segment + 1) {
						$pv_matches[] = new \Framework\Route\Pattern\Match($pv_url, $pv_rest_url, $pv_params);

						$match_count = 1;
					}
					else {
						$match_count = $this->match_continued($pv_url, $pv_rest_url, $pv_params, $pv_segment + 1, $pv_matches);
					}
				}
				else {
					$match_count = 0;
				}
			}
			else if($segment->Match($pv_rest_url, $pv_params, $match)) {
				echo '<div>$segment->Match($pv_rest_url, $pv_params, $match)</div>';

				$params = array_merge($pv_params, $match->Params);

				if($segment->IsOptional) {
					if(count($this->Segments) === $pv_segment + 1) {
						$pv_matches[] = new \Framework\Route\Pattern\Match($pv_url, $match->rest_url, $params);

						$pv_matches[] = new \Framework\Route\Pattern\Match($pv_url, $pv_rest_url, $params);

						$match_count = 2;
					}
					else {
						$match_count = $this->match_continued($pv_url, $match->rest_url, $params, $pv_segment + 1, $pv_matches) + $this->match_continued($pv_url, $pv_rest_url, $params, $pv_segment + 1, $pv_matches);
					}
				}
				else {
					if(count($this->Segments) === $pv_segment + 1) {
						$pv_matches[] = new \Framework\Route\Pattern\Match($pv_url, $match->rest_url, $params);

						$match_count = 1;
					}
					else {
						$match_count = $this->match_continued($pv_url, $match->rest_url, $params, $pv_segment + 1, $pv_matches);
					}
				}
			}
			else if($segment->IsOptional) {
				echo '<div>$segment->IsOptional</div>';

				if(count($this->Segments) === $pv_segment + 1) {
					$pv_matches[] = new \Framework\Route\Pattern\Match($pv_url, $pv_rest_url, $pv_params);

					$match_count = 1;
				}
				else {
					$match_count = $this->match_continued($pv_url, $pv_rest_url, $pv_params, $pv_segment + 1, $pv_matches);
				}
			}

			echo '<div>- match_count: ', $match_count, '</div>';

			echo '</div>';

			return $match_count;
		}
		else {
			$segment = $this->Segments[$pv_segment];

			if($pv_rest_url === null) {
				if($segment->IsOptional) {
					if(count($this->Segments) === $pv_segment + 1) {
						$pv_matches[] = new \Framework\Route\Pattern\Match($pv_url, $pv_rest_url, $pv_params);

						return 1;
					}
					else {
						return $this->match_continued($pv_url, $pv_rest_url, $pv_params, $pv_segment + 1, $pv_matches);
					}
				}
				else {
					return 0;
				}
			}
			else if($segment->Match($pv_rest_url, $pv_params, $match)) {
				$params = array_merge($pv_params, $match->Params);

				if($segment->IsOptional) {
					if(count($this->Segments) === $pv_segment + 1) {
						$pv_matches[] = new \Framework\Route\Pattern\Match($pv_url, $match->rest_url, $params);

						$pv_matches[] = new \Framework\Route\Pattern\Match($pv_url, $pv_rest_url, $params);

						return 2;
					}
					else {
						return $this->match_continued($pv_url, $match->rest_url, $params, $pv_segment + 1, $pv_matches) + $this->match_continued($pv_url, $pv_rest_url, $params, $pv_segment + 1, $pv_matches);
					}
				}
				else {
					if(count($this->Segments) === $pv_segment + 1) {
						$pv_matches[] = new \Framework\Route\Pattern\Match($pv_url, $match->rest_url, $params);

						return 1;
					}
					else {
						return $this->match_continued($pv_url, $match->rest_url, $params, $pv_segment + 1, $pv_matches);
					}
				}
			}
			else if($segment->IsOptional) {
				if(count($this->Segments) === $pv_segment + 1) {
					$pv_matches[] = new \Framework\Route\Pattern\Match($pv_url, $pv_rest_url, $pv_params);

					return 1;
				}
				else {
					return $this->match_continued($pv_url, $pv_rest_url, $pv_params, $pv_segment + 1, $pv_matches);
				}
			}
			else {
				return 0;
			}
		}
		/*{ELSE}*/
		$segment = $this->Segments[$pv_segment];

		if($pv_rest_url === null) {
			if($segment->IsOptional) {
				if(count($this->Segments) === $pv_segment + 1) {
					$pv_matches[] = new \Framework\Route\Pattern\Match($pv_url, $pv_rest_url, $pv_params);

					return 1;
				}
				else {
					return $this->match_continued($pv_url, $pv_rest_url, $pv_params, $pv_segment + 1, $pv_matches);
				}
			}
			else {
				return 0;
			}
		}
		else if($segment->Match($pv_rest_url, $pv_params, $match)) {
			$params = array_merge($pv_params, $match->Params);

			if($segment->IsOptional) {
				if(count($this->Segments) === $pv_segment + 1) {
					$pv_matches[] = new \Framework\Route\Pattern\Match($pv_url, $match->rest_url, $params);

					$pv_matches[] = new \Framework\Route\Pattern\Match($pv_url, $pv_rest_url, $params);

					return 2;
				}
				else {
					return $this->match_continued($pv_url, $match->rest_url, $params, $pv_segment + 1, $pv_matches) + $this->match_continued($pv_url, $pv_rest_url, $params, $pv_segment + 1, $pv_matches);
				}
			}
			else {
				if(count($this->Segments) === $pv_segment + 1) {
					$pv_matches[] = new \Framework\Route\Pattern\Match($pv_url, $match->rest_url, $params);

					return 1;
				}
				else {
					return $this->match_continued($pv_url, $match->rest_url, $params, $pv_segment + 1, $pv_matches);
				}
			}
		}
		else if($segment->IsOptional) {
			if(count($this->Segments) === $pv_segment + 1) {
				$pv_matches[] = new \Framework\Route\Pattern\Match($pv_url, $pv_rest_url, $pv_params);

				return 1;
			}
			else {
				return $this->match_continued($pv_url, $pv_rest_url, $pv_params, $pv_segment + 1, $pv_matches);
			}
		}
		else {
			return 0;
		}
		/*{ENDIF}*/
	}

	public function CreateUrl(array $Params) : string {
		$url = '';

		foreach($this->Segments as $segment) {
			$url .= $segment->CreateUrl($Params);
		}

		return $url;
	}

	public function TryCreateUrl(array $Params, string &$Url = null) : bool {
		$url = '';

		foreach($this->Segments as $segment) {
			if($segment->TryCreateUrl($Params, $segmentUrl)) {
				$url .= $segmentUrl;
			}
			else {
				return false;
			}
		}

		$Url = $url;

		return true;
	}
}
?>