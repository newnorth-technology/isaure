<?
namespace Framework\Route\Pattern;

class Match {
	/* Instance variables */

	public $Url;

	public $rest_url;

	public $Params;

	/* Magic methods */

	public function __construct(string $pv_url = null, string $pv_rest_url = null, array $pv_params) {
		$this->Url = $pv_url;

		$this->rest_url = $pv_rest_url;

		$this->Params = $pv_params;
	}

	public function __debugInfo() {
		return [
			'Url' => $this->Url,
			'rest_url' => $this->rest_url,
			'Params' => $this->Params,
		];
	}
}
?>