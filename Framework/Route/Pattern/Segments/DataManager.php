<?
namespace Framework\Route\Pattern\Segments;

class DataManager extends \Framework\Route\Pattern\ASegment {
	/* Instance variables */

	public $DataManager;

	public $DataMember;

	public $Condition;

	public $References;

	/* Magic methods */

	public function __construct(\Framework\Route\Pattern $Pattern, array $Params) {
		parent::__construct($Pattern, $Params);

		$this->RegExp = '/^(?<'.$this->Name.'>'.$Params['RegExp'].')\\//';

		$this->DataManager = $Params['DataManager'];

		$this->DataMember = $Params['DataMember'];

		if(array_key_exists('Condition', $Params)) {
			$this->Condition = $Params['Condition'];
		}
		else {
			$this->Condition = new \Framework\Database\Conditions\IsEqualTo([
				'ColumnA' => new \Framework\Database\Columns\Reference([
					'Source' => $this->DataManager,
					'Name' => $this->DataMember
				]),
				'ColumnB' => new \Framework\Database\Columns\Value([
					'Variable' => 'Parameter'
				])
			]);
		}

		$this->References = $Params['References'] ?? null;
	}

	public function __debugInfo() {
		return array_merge(
			parent::__debugInfo(),
			[
				'DataManager' => $this->DataManager,
				'DataMember' => $this->DataMember,
				'Condition' => $this->Condition,
				'References' => $this->References
			]
		);
	}

	/* Instance ASegment methods */

	protected function Match»ValidateParams(array $Params, array &$NewParams) : bool {
		$object = \Framework\GetDataManager($this->DataManager)->Find(
			[
				'Condition' => $this->Condition
			],
			[
				'References' => $this->References,
				'Variables' => [
					'Locale' => $this->Pattern->LocaleId,
					'Parameter' => $NewParams[$this->Name]
				]
			]
		);

		if($object === null) {
			return false;
		}

		$NewParams[$this->Name] = $object;

		return true;
	}

	public function CreateUrl(array $Params) {
		$param = $Params[$this->Name] ?? null;

		if($param === null) {
			if($this->IsOptional) {
				return null;
			}
			else {
				throw new \Framework\RuntimeException(
					'Missing parameter for segment.',
					[
						'Route' => $this->Pattern->Route->Id,
						'LocaleId' => $this->Pattern->LocaleId,
						'Segment' => $this->Name,
						'Params' => $Params
					]
				);
			}
		}

		if($param instanceof \Framework\DataType) {
			$object = $param;
		}
		else {
			$object = \Framework\GetDataManager($this->DataManager)->Find(
				[
					'Condition' => new \Framework\Database\Conditions\IsEqualTo([
						'ColumnA' => new \Framework\Database\Columns\Reference([
							'Source' => $this->DataManager,
							'Name' => 'Id'
						]),
						'ColumnB' => $param
					])
				],
				[
					'References' => $this->References,
					'Variables' => [
						'Locale' => $this->Pattern->LocaleId
					]
				]
			);

			if($object === null) {
				if($this->IsOptional) {
					return null;
				}
				else {
					throw new \Framework\RuntimeException(
						'Unable to create URL, missing object for segment.',
						[
							'Route' => $this->Pattern->Route->Id,
							'LocaleId' => $this->Pattern->LocaleId,
							'Segment' => $this->Name,
							'Params' => $Params
						]
					);
				}
			}
		}

		foreach(explode('.', $this->DataMember) as $key) {
			$object = $object->{$key};
		}

		return $object.'/';
	}

	public function TryCreateUrl(array $Params, string &$Url = null) : bool {
		$param = $Params[$this->Name] ?? null;

		if($param === null) {
			if($this->IsOptional) {
				$Url = null;

				return true;
			}
			else {
				return false;
			}
		}

		if($param instanceof \Framework\DataType) {
			$object = $param;
		}
		else {
			$object = \Framework\GetDataManager($this->DataManager)->Find(
				[
					'Condition' => new \Framework\Database\Conditions\IsEqualTo([
						'ColumnA' => new \Framework\Database\Columns\Reference([
							'Source' => $this->DataManager,
							'Name' => 'Id'
						]),
						'ColumnB' => $param
					])
				],
				[
					'References' => $this->References,
					'Variables' => [
						'Locale' => $this->Pattern->LocaleId
					]
				]
			);

			if($object === null) {
				if($this->IsOptional) {
					$Url = null;

					return true;
				}
				else {
					return false;
				}
			}
		}

		foreach(explode('.', $this->DataMember) as $key) {
			$object = $object->{$key};
		}

		$Url = $object.'/';

		return true;
	}
}
?>