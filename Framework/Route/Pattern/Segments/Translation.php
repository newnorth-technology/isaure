<?
namespace Framework\Route\Pattern\Segments;

class Translation extends \Framework\Route\Pattern\ASegment {
	/* Instance variables */

	public $Translation = null;

	/* Magic methods */

	public function __construct(\Framework\Route\Pattern $Pattern, array $Params) {
		parent::__construct($Pattern, $Params);

		if($this->Pattern->LocaleId === null) {
			$this->RegExp = null;
		}
		else if(\Framework\TryGetTranslation($this->Pattern->LocaleId, 'Routes.'.$this->Pattern->Route->Id.'.'.$this->Name, $this->Translation)) {
			$this->RegExp = '/^(?<'.$this->Name.'>'.$this->Translation.')\\//';
		}
	}

	public function __debugInfo() {
		return array_merge(
			parent::__debugInfo(),
			[
				'Translation' => $this->Translation,
			]
		);
	}

	/* Instance ASegment methods */

	protected function Match»ValidateParams(array $Params, array &$NewParams) : bool {
		return true;
	}

	public function CreateUrl(array $Params) {
		if($this->Translation === null) {
			if($this->IsOptional) {
				return null;
			}
			else {
				throw new \Framework\RuntimeException(
					'Unable to create URL, missing translation for route pattern segment.',
					[
						'Route' => $this->Pattern->Route->Id,
						'LocaleId' => $this->Pattern->LocaleId,
						'Segment' => $this->Name,
						'Translation' => 'Routes.'.$this->Pattern->Route->Id.'.'.$this->Name,
					]
				);
			}
		}
		else {
			return $this->Translation.'/';
		}
	}

	public function TryCreateUrl(array $Params, string &$Url = null) : bool {
		if($this->Translation === null) {
			if($this->IsOptional) {
				$Url = null;

				return true;
			}
			else {
				return false;
			}
		}
		else {
			$Url = $this->Translation.'/';

			return true;
		}
	}
}
?>