<?
namespace Framework\Route\Pattern\Segments;

class Constant extends \Framework\Route\Pattern\ASegment {
	/* Instance variables */

	public $Value = null;

	/* Magic methods */

	public function __construct(\Framework\Route\Pattern $Pattern, array $Params) {
		parent::__construct($Pattern, $Params);

		$this->RegExp = '/^'.$Params['Value'].'\\//';

		$this->Value = $Params['Value'];
	}

	public function __debugInfo() {
		return array_merge(
			parent::__debugInfo(),
			[
				'Value' => $this->Value,
			]
		);
	}

	/* Instance ASegment methods */

	protected function Match»ValidateParams(array $Params, array &$NewParams) : bool {
		if($this->Name !== null) {
			$NewParams[$this->Name] = $this->Value;
		}

		return true;
	}

	public function CreateUrl(array $Params) {
		return $this->Value.'/';
	}

	public function TryCreateUrl(array $Params, string &$Url = null) : bool {
		$Url = $this->Value.'/';

		return true;
	}
}
?>