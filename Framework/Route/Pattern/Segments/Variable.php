<?
namespace Framework\Route\Pattern\Segments;

class Variable extends \Framework\Route\Pattern\ASegment {
	/* Instance variables */

	public $ValidateMethod = null;

	/* Magic methods */

	public function __construct(\Framework\Route\Pattern $Pattern, array $Params) {
		parent::__construct($Pattern, $Params);

		$this->RegExp = '/^(?<'.$this->Name.'>'.$Params['RegExp'].')\\//';

		$this->ValidateMethod = $Params['ValidateMethod'] ?? $this->ValidateMethod;
	}

	public function __debugInfo() {
		return [
			'Route' => $this->Pattern->Route->Id,
			'LocaleId' => $this->Pattern->LocaleId,
			'Name' => $this->Name,
			'IsOptional' => $this->IsOptional,
			'RegExp' => $this->RegExp,
			'ValidateMethod' => $this->ValidateMethod,
		];
	}

	/* Instance ASegment methods */

	protected function Match»ValidateParams(array $Params, array &$NewParams) : bool {
		if($this->ValidateMethod !== null) {
			foreach($NewParams as $key => &$value) {
				if(!call_user_func_array($this->ValidateMethod['Method'], [$key, &$value, $this->ValidateMethod['Params'] ?? [], $this->Pattern->LocaleId, $Params])) {
					return false;
				}
			}
		}

		return true;
	}

	public function CreateUrl(array $Params) {
		$param = $Params[$this->Name] ?? null;

		if($param === null) {
			if($this->IsOptional) {
				return null;
			}
			else {
				throw new \Framework\RuntimeException(
					'Unable to create URL, missing parameter for segment.',
					[
						'Route' => $this->Pattern->Route->Id,
						'LocaleId' => $this->Pattern->LocaleId,
						'Segment' => $this->Name,
						'Params' => $Params,
					]
				);
			}
		}

		return $param.'/';
	}

	public function TryCreateUrl(array $Params, string &$Url = null) : bool {
		$param = $Params[$this->Name] ?? null;

		if($param === null) {
			if($this->IsOptional) {
				$Url = null;

				return true;
			}
			else {
				return false;
			}
		}

		$Url = $param.'/';

		return true;
	}
}
?>