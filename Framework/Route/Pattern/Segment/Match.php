<?
namespace Framework\Route\Pattern\Segment;

class Match {
	/* Instance variables */

	public $Url;

	public $rest_url;

	public $Params;

	/* Magic methods */

	public function __construct(string $Url = null, string $rest_url = null, array $Params) {
		$this->Url = isset($Url[0]) ? $Url : null;

		$this->rest_url = isset($rest_url[0]) ? $rest_url : null;

		$this->Params = $Params;
	}

	public function __debugInfo() {
		return [
			'Url' => $this->Url,
			'rest_url' => $this->rest_url,
			'Params' => $this->Params,
		];
	}
}
?>