<?
namespace Framework\Route\Pattern;

abstract class ASegment {
	/* Instance variables */

	public $Pattern;

	public $Name = null;

	public $IsOptional = false;

	public $RegExp = null;

	/* Magic methods */

	public function __construct(\Framework\Route\Pattern $Pattern, array $Params) {
		$this->Pattern = $Pattern;

		$this->Name = $Params['Name'] ?? $this->Name;

		$this->IsOptional = $Params['IsOptional'] ?? $this->IsOptional;
	}

	public function __debugInfo() {
		return [
			'Route' => $this->Pattern->Route->Id,
			'LocaleId' => $this->Pattern->LocaleId,
			'Name' => $this->Name,
			'IsOptional' => $this->IsOptional,
			'RegExp' => $this->RegExp,
		];
	}

	/* Instance methods */

	public function Match(string $Url, array $Params, \Framework\Route\Pattern\Segment\Match &$Match = null) : bool {
		if($this->RegExp === null) {
			return false;
		}

		if(preg_match($this->RegExp, $Url, $params) !== 1) {
			return false;
		}

		foreach($params as $key => $value) {
			if(is_int($key)) {
				unset($params[$key]);
			}
		}

		if(!$this->Match»ValidateParams($Params, $params)) {
			return false;
		}

		$Match = new \Framework\Route\Pattern\Segment\Match(
			$Url,
			preg_replace($this->RegExp, '', $Url),
			$params
		);

		return true;
	}

	protected abstract function Match»ValidateParams(array $Params, array &$NewParams) : bool;

	public abstract function CreateUrl(array $Params);

	public abstract function TryCreateUrl(array $Params, string &$Url = null) : bool;
}
?>