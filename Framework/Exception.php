<?
namespace Framework;

class Exception extends \Exception {
	/* Instance variables */

	public $Type;

	public $Data;

	public $InnerException;

	/* Magic methods */

	public function __construct($Type, $Message, $Data = [], \Exception $InnerException = null) {
		parent::__construct($Message);

		$this->Type = $Type;

		$this->Data = $Data;

		$this->InnerException = $InnerException;
	}
}
?>