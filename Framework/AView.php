<?
namespace Framework;

abstract class AView {
	/* Instance variables */

	protected $Translations = [];

	/* Magic methods */

	public abstract function __construct(array $Params);

	/* Instance life cycle methods */

	public abstract function Render(\Framework\Model $Model, bool $Return, array $Params = []);

	/* Instance translations methods */

	public function GetTranslation(string $Path, string $DefaultTranslation = null) {
		return $this->Translations[$Path] ?? $DefaultTranslation;
	}

	public function TryGetTranslation(string $Path, string &$Translation = null) : bool {
		if(isset($this->Translations[$Path])) {
			$Translation = $this->Translations[$Path];

			return true;
		}
		else {
			return false;
		}
	}

	public function SetTranslation(string $Path, $Translation) {
		$this->Translations[$Path] = $Translation;
	}

	public function IsTranslationSet(string $Path) : bool {
		return isset($this->Translations[$Path]);
	}

	public function IsTranslationNotSet(string $Path) : bool {
		return !isset($this->Translations[$Path]);
	}

	public function IsTranslationEmpty(string $Path) : bool {
		return !isset($this->Translations[$Path][0]);
	}

	public function IsTranslationNotEmpty(string $Path) : bool {
		return isset($this->Translations[$Path][0]);
	}
}
?>