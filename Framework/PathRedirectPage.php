<?
namespace Framework;

class PathRedirectPage extends \Framework\Controllers\AHtml {
	/* Instance variables */

	public function Execute() {
		\Framework\Router::RedirectPath(
			\Framework\GetTrailData('Path'),
			\Framework\GetTrailData('Locale', null),
			\Framework\GetTrailData('Params', []),
			\Framework\GetTrailData('QueryString', [])
		);
	}
}
?>