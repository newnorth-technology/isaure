<?
namespace Framework;

class BoolDataMember extends \Framework\ADynamicDataMember {
	/* Static methods */

	public static function ParseFromDbValue(\Framework\BoolDataMember $DataMember = null, $Value) {
		/*{IF:DEBUG}*/

		if($Value === null) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, value may not be null.'
			);
		}

		if(!is_bool($Value) && !is_int($Value) && !is_string($Value)) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, invalid value type.',
				['Value' => $Value]
			);
		}

		if(is_string($Value)) {
			if($Value !== '0' && $Value !== '1') {
				throw new \Framework\RuntimeException(
					'Failed to parse data member value, invalid string value.',
					['Value' => $Value]
				);
			}
		}

		if(is_int($Value)) {
			if($Value !== 0 && $Value !== 1) {
				throw new \Framework\RuntimeException(
					'Failed to parse data member value, invalid int value.',
					['Value' => $Value]
				);
			}
		}

		/*{ENDIF}*/

		return (bool)$Value;
	}

	public static function ParseValue(\Framework\BoolDataMember $DataMember = null, $Value) {
		/*{IF:DEBUG}*/

		if($Value === null) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, value may not be null.'
			);
		}

		if(!is_bool($Value)) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, invalid value type.',
				['Value' => $Value]
			);
		}

		/*{ENDIF}*/

		return $Value;
	}

	public static function ParseToDbValue(\Framework\BoolDataMember $DataMember = null, $Value) {
		return $Value;
	}
}
?>