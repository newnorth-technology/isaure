<?
namespace Framework;

class Discord {
	/* Static methods */

	public static function PostMessage($pv_webhook_id, $pv_message) {
		$handle = curl_init(\Framework\GetConfig('Discord.Webhooks.'.$pv_webhook_id));

		curl_setopt($handle, CURLOPT_POST, 1);

		curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode(['content' => $pv_message]));

		curl_setopt($handle, CURLOPT_FOLLOWLOCATION, 1);

		curl_setopt($handle, CURLOPT_HEADER, 0);

		curl_setopt($handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

		curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);

		curl_exec($handle);
	}
}
?>