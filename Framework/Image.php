<?
namespace Framework;

class Image {
	/* Instance variables */

	public $Source;

	public $Width;

	public $Height;

	public $Alpha;

	/* Instance magic methods */

	public function __construct(array $Params) {
		$this->Alpha = $Params['Alpha'];

		if(array_key_exists('Path', $Params)) {
			$this->Source = imagecreatefrompng($Params['Path']);

			$this->Width = imagesx($this->Source);

			$this->Height = imagesy($this->Source);
		}
		else {
			$this->Source = imagecreatetruecolor($Params['Width'], $Params['Height']);

			$this->Width = $Params['Width'];

			$this->Height = $Params['Height'];
		}

		if($this->Alpha) {
			imagealphablending($this->Source, false);

			imagesavealpha($this->Source, true);
		}

		if(!array_key_exists('Path', $Params) && array_key_exists('Color', $Params)) {
			$this->Fill([
				'Color' => $Params['Color']
			]);
		}
	}

	public function __destruct() {
		imagedestroy($this->Source);
	}

	public function __clone() {
		$original = $this->Source;

		$clone = imagecreatetruecolor($this->Width, $this->Height);

		imagecopy($clone, $original, 0, 0, 0, 0, $this->Width, $this->Height);

		$this->Source = $clone;
	}

	/* Instance methods */

	public function GetPixel(array $Params = []) {
		$color = imagecolorat($this->Source, $Params['X'], $Params['Y']);

		$r = ($color & 0x00FF0000) >> 16;

		$g = ($color & 0x0000FF00) >> 8;

		$b = ($color & 0x000000FF) >> 0;

		$a = 1.0 - (($color & 0xFF000000) >> 24) / 0x7F;

		return [$r, $g, $b, $a];
	}

	public function SetPixel(array $Params = []) {
		$r = $Params['Color'][0];

		$g = $Params['Color'][1];

		$b = $Params['Color'][2];

		if($this->Alpha) {
			$a = 0x7F - $Params['Color'][3] * 0x7F;
		}

		imagesetpixel(
			$this->Source,
			$Params['X'],
			$Params['Y'],
			$this->Alpha ? imagecolorallocatealpha($this->Source, $r, $g, $b, $a) : imagecolorallocate($this->Source, $r, $g, $b)
		);
	}

	public function Fill(array $Params = []) {
		$r = $Params['Color'][0];

		$g = $Params['Color'][1];

		$b = $Params['Color'][2];

		if($this->Alpha) {
			$a = 0x7F - $Params['Color'][3] * 0x7F;
		}

		imagefill(
			$this->Source,
			$Params['X'] ?? 0,
			$Params['Y'] ?? 0,
			$this->Alpha ? imagecolorallocatealpha($this->Source, $r, $g, $b, $a) : imagecolorallocate($this->Source, $r, $g, $b)
		);
	}

	public function DrawRect(array $Params = []) {
		$r = $Params['FillColor'][0];

		$g = $Params['FillColor'][1];

		$b = $Params['FillColor'][2];

		if($this->Alpha) {
			$a = 0x7F - $Params['FillColor'][3] * 0x7F;
		}

		imagefilledrectangle(
			$this->Source,
			$Params['X'],
			$Params['Y'],
			$Params['X'] + $Params['Width'],
			$Params['Y'] + $Params['Height'],
			$this->Alpha ? imagecolorallocatealpha($this->Source, $r, $g, $b, $a) : imagecolorallocate($this->Source, $r, $g, $b)
		);
	}

	public function DrawImage(array $Params = []) {
		imagecopy(
			$this->Source,
			$Params['Image']->Source,
			$Params['TargetX'] ?? 0,
			$Params['TargetY'] ?? 0,
			$Params['SourceX'] ?? 0,
			$Params['SourceY'] ?? 0,
			$Params['SourceWidth'] ?? $Params['Image']->Width,
			$Params['SourceHeight'] ?? $Params['Image']->Height
		);
	}

	public function DrawLine(array $Params = []) {
		$r = $Params['Color'][0];

		$g = $Params['Color'][1];

		$b = $Params['Color'][2];

		if($this->Alpha) {
			$a = 0x7F - $Params['Color'][3] * 0x7F;
		}

		imageline($this->Source,
			$Params['X'],
			$Params['Y'],
			$Params['X2'],
			$Params['Y2'],
			$this->Alpha ? imagecolorallocatealpha($this->Source, $r, $g, $b, $a) : imagecolorallocate($this->Source, $r, $g, $b)
		);
	}

	public function DrawEllipse(array $Params = []) {
		$r = $Params['Color'][0];

		$g = $Params['Color'][1];

		$b = $Params['Color'][2];

		if($this->Alpha) {
			$a = 0x7F - $Params['Color'][3] * 0x7F;
		}

		imagefilledellipse($this->Source,
			$Params['X'],
			$Params['Y'],
			$Params['Width'],
			$Params['Height'],
			$this->Alpha ? imagecolorallocatealpha($this->Source, $r, $g, $b, $a) : imagecolorallocate($this->Source, $r, $g, $b)
		);
	}

	public function DrawText(array $Params = []) {
		$r = $Params['Color'][0];

		$g = $Params['Color'][1];

		$b = $Params['Color'][2];

		if($this->Alpha) {
			$a = 0x7F - $Params['Color'][3] * 0x7F;
		}

		imagettftext($this->Source,
			$Params['FontSize'],
			$Params['Degrees'],
			$Params['X'],
			$Params['Y'],
			$this->Alpha ? imagecolorallocatealpha($this->Source, $r, $g, $b, $a) : imagecolorallocate($this->Source, $r, $g, $b),
			$Params['Font'],
			$Params['Text']
		);
	}

	public function TextBoundingBox(array $Params = []) {
		return imagettfbbox(
			$Params['FontSize'],
			$Params['Degrees'],
			$Params['Font'],
			$Params['Text']
		);
	}

	public function ApplyNoise(array $Params = []) {
		if(array_key_exists('Mask', $Params)) {
			if(array_key_exists('Monochromatic', $Params) && $Params['Monochromatic']) {
				for($y = 0; $y < $this->Height; ++$y) {
					for($x = 0; $x < $this->Width; ++$x) {
						$color = $this->GetPixel([
							'X' => $x,
							'Y' => $y
						]);

						$strength = $Params['Mask']->GetPixel([
							'X' => $x,
							'Y' => $y
						]);

						$noise = rand(-$strength[0], $strength[0]);

						$color[0] = max(0, min(255, $color[0] + $noise));

						$color[1] = max(0, min(255, $color[1] + $noise));

						$color[2] = max(0, min(255, $color[2] + $noise));

						$this->SetPixel([
							'X' => $x,
							'Y' => $y,
							'Color' => $color
						]);
					}
				}
			}
			else {
				for($y = 0; $y < $this->Height; ++$y) {
					for($x = 0; $x < $this->Width; ++$x) {
						$color = $this->GetPixel([
							'X' => $x,
							'Y' => $y
						]);

						$strength = $Params['Mask']->GetPixel([
							'X' => $x,
							'Y' => $y
						]);

						$noise = rand(-$strength[0], $strength[0]);

						$color[0] = max(0, min(255, $color[0] + $noise));

						$noise = rand(-$strength[1], $strength[1]);

						$color[1] = max(0, min(255, $color[1] + $noise));

						$noise = rand(-$strength[2], $strength[2]);

						$color[2] = max(0, min(255, $color[2] + $noise));

						$this->SetPixel([
							'X' => $x,
							'Y' => $y,
							'Color' => $color
						]);
					}
				}
			}
		}
		else if(array_key_exists('Strength', $Params)) {
			if(array_key_exists('Monochromatic', $Params) && $Params['Monochromatic']) {
				for($y = 0; $y < $this->Height; ++$y) {
					for($x = 0; $x < $this->Width; ++$x) {
						$color = $this->GetPixel([
							'X' => $x,
							'Y' => $y
						]);

						$noise = rand(-$Params['Strength'], $Params['Strength']);

						$color[0] = max(0, min(255, $color[0] + $noise));

						$color[1] = max(0, min(255, $color[1] + $noise));

						$color[2] = max(0, min(255, $color[2] + $noise));

						$this->SetPixel([
							'X' => $x,
							'Y' => $y,
							'Color' => $color
						]);
					}
				}
			}
			else {
				for($y = 0; $y < $this->Height; ++$y) {
					for($x = 0; $x < $this->Width; ++$x) {
						$color = $this->GetPixel([
							'X' => $x,
							'Y' => $y
						]);

						$noise = rand(-$Params['Strength'], $Params['Strength']);

						$color[0] = max(0, min(255, $color[0] + $noise));

						$noise = rand(-$Params['Strength'], $Params['Strength']);

						$color[1] = max(0, min(255, $color[1] + $noise));

						$noise = rand(-$Params['Strength'], $Params['Strength']);

						$color[2] = max(0, min(255, $color[2] + $noise));

						$this->SetPixel([
							'X' => $x,
							'Y' => $y,
							'Color' => $color
						]);
					}
				}
			}
		}
	}

	public function HorizontalBoxBlur(array $Params = []) {
		$source = clone $this;

		for($y = 0; $y < $this->Height; ++$y) {
			for($x = 0; $x < $this->Width; ++$x) {
				$sum = [0, 0, 0, 0];

				for($i = -$Params['Strength']; $i <= $Params['Strength']; ++$i) {
					if($x + $i < 0) {
						$color = $source->GetPixel([
							'X' => 0,
							'Y' => $y
						]);
					}
					else if($this->Width <= $x + $i) {
						$color = $source->GetPixel([
							'X' => $this->Width - 1,
							'Y' => $y
						]);
					}
					else {
						$color = $source->GetPixel([
							'X' => $x + $i,
							'Y' => $y
						]);
					}

					$sum[0] += $color[0];

					$sum[1] += $color[1];

					$sum[2] += $color[2];

					$sum[3] += $color[3];
				}

				$sum[0] /= $Params['Strength'] * 2 + 1;

				$sum[1] /= $Params['Strength'] * 2 + 1;

				$sum[2] /= $Params['Strength'] * 2 + 1;

				$sum[3] /= $Params['Strength'] * 2 + 1;

				$this->SetPixel([
					'X' => $x,
					'Y' => $y,
					'Color' => $sum
				]);
			}
		}
	}

	public function VerticalBoxBlur(array $Params = []) {
		$source = clone $this;

		for($y = 0; $y < $this->Height; ++$y) {
			for($x = 0; $x < $this->Width; ++$x) {
				$sum = [0, 0, 0, 0];

				for($i = -$Params['Strength']; $i <= $Params['Strength']; ++$i) {
					if($y + $i < 0) {
						$color = $source->GetPixel([
							'X' => $x,
							'Y' => 0
						]);
					}
					else if($this->Height <= $y + $i) {
						$color = $source->GetPixel([
							'X' => $x,
							'Y' => $this->Height - 1
						]);
					}
					else {
						$color = $source->GetPixel([
							'X' => $x,
							'Y' => $y + $i
						]);
					}

					$sum[0] += $color[0];

					$sum[1] += $color[1];

					$sum[2] += $color[2];

					$sum[3] += $color[3];
				}

				$sum[0] /= $Params['Strength'] * 2 + 1;

				$sum[1] /= $Params['Strength'] * 2 + 1;

				$sum[2] /= $Params['Strength'] * 2 + 1;

				$sum[3] /= $Params['Strength'] * 2 + 1;

				$this->SetPixel([
					'X' => $x,
					'Y' => $y,
					'Color' => $sum
				]);
			}
		}
	}

	public function GetPngSource(array $Params = []) : string {
		ob_start();

		imagepng(
			$this->Source,
			null,
			$Params['Quality'] ?? 0,
			$Params['Filters'] ?? PNG_NO_FILTER
		);

		return ob_get_clean();
	}
}
?>