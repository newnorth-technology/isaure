<?
namespace Framework;

class StaticJsonDataMember extends \Framework\AStaticDataMember {
	/* Static methods */

	public static function ParseFromDbValue(\Framework\StaticJsonDataMember $DataMember = null, $Value) {
		/*{IF:DEBUG}*/

		if($Value === null) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, value may not be null.',
				['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name]
			);
		}

		if(!is_string($Value)) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, invalid value type.',
				['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name, 'Value' => $Value]
			);
		}

		/*{ENDIF}*/

		return json_decode($Value, true);
	}

	public static function ParseValue(\Framework\StaticJsonDataMember $DataMember = null, $Value) {
		return $Value;
	}

	public static function ParseToDbValue(\Framework\StaticJsonDataMember $DataMember = null, $Value) {
		return json_encode($Value);
	}
}
?>