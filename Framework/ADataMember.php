<?
namespace Framework;

abstract class ADataMember {
	/* instance variables */

	public $DataManager;

	public $Alias;

	public $Name;

	public $UseLogInsert = false;

	public $UseLogUpdate = false;

	public $UseLogDelete = false;

	/* instance methods */

	public function __construct(\Framework\ADataManager $DataManager, string $Alias, array $Params) {
		$this->DataManager = $DataManager;

		$this->Alias = $Alias;

		if(array_key_exists('Name', $Params)) {
			$this->Name = $Params['Name'];

			/*{IF:DEBUG}*/

			unset($Params['Name']);

			/*{ENDIF}*/
		}
		else {
			$this->Name = $Alias;
		}

		if(array_key_exists('UseLogInsert', $Params)) {
			$this->UseLogInsert = $Params['UseLogInsert'];

			/*{IF:DEBUG}*/

			unset($Params['UseLogInsert']);

			/*{ENDIF}*/
		}

		if(array_key_exists('UseLogUpdate', $Params)) {
			$this->UseLogUpdate = $Params['UseLogUpdate'];

			/*{IF:DEBUG}*/

			unset($Params['UseLogUpdate']);

			/*{ENDIF}*/
		}

		if(array_key_exists('UseLogDelete', $Params)) {
			$this->UseLogDelete = $Params['UseLogDelete'];

			/*{IF:DEBUG}*/

			unset($Params['UseLogDelete']);

			/*{ENDIF}*/
		}

		/*{IF:DEBUG}*/

		if(0 < count($Params)) {
			throw new \Framework\RuntimeException(
				'Unable to parse params, trailing data found.',
				['Data manager' => $this->DataManager->Id, 'Data member' => $this->Name, 'Data' => $Params]
			);
		}

		/*{ENDIF}*/
	}

	public abstract function Set(int $Id, $Value);

	public abstract function Increment(int $Id, $Amount);

	public abstract function Decrement(int $Id, $Amount);

	private function Log($Id, $Value, $Method) {
		/*
		$Query = new \Framework\DbInsertQuery();

		if($this->DataManager->Database === null) {
			$Query->Source = '`'.$this->DataManager->Table.'»Log`';
		}
		else {
			$Query->Source = '`'.$this->DataManager->Database.'`.`'.$this->DataManager->Table.'»Log`';
		}

		$Query->AddColumn('`PrimaryKey`');

		$Query->AddValue($Id);

		$Query->AddColumn('`Method`');

		$Query->AddValue('"'.$Method.'"');

		$Query->AddColumn('`Column`');

		$Query->AddValue('"'.$this->Alias.'"');

		$Query->AddColumn('`Value`');

		$Query->AddValue($Value);

		$Query->AddColumn('`System`');

		$Query->AddValue('"'.$GLOBALS['Transaction']->System.'"');

		$Query->AddColumn('`Source`');

		$Query->AddValue('"'.json_encode($GLOBALS['Transaction']->Source).'"');

		$Query->AddColumn('`Time`');

		$Query->AddValue($_SERVER['REQUEST_TIME']);

		return $this->DataManager->Connection->Insert($Query);
		*/
	}

	public function LogUpdate($Id, $Value) {
		return $this->Log($Id, $Value, 'UPDATE');
	}

	public function LogInsert($Id, $Value) {
		return $this->Log($Id, $Value, 'INSERT');
	}

	public function LogDelete($Id, $Value) {
		return $this->Log($Id, $Value, 'DELETE');
	}

	public function is_between($pv_from, $pv_to) : \Framework\Database\Conditions\IsBetween {
		return new \Framework\Database\Conditions\IsBetween(['Column' => $this, 'From' => $pv_from, 'To' => $pv_to]);
	}

	public function is_equal_to($pv_column) : \Framework\Database\Conditions\IsEqualTo {
		return new \Framework\Database\Conditions\IsEqualTo(['ColumnA' => $this, 'ColumnB' => $pv_column]);
	}

	public function is_greater_than($pv_column) : \Framework\Database\Conditions\IsGreaterThan {
		return new \Framework\Database\Conditions\IsGreaterThan(['ColumnA' => $this, 'ColumnB' => $pv_column]);
	}

	public function is_less_than($pv_column) : \Framework\Database\Conditions\IsLessThan {
		return new \Framework\Database\Conditions\IsLessThan(['ColumnA' => $this, 'ColumnB' => $pv_column]);
	}

	public function is_like($pv_column) : \Framework\Database\Conditions\IsLike {
		return new \Framework\Database\Conditions\IsLike(['ColumnA' => $this, 'ColumnB' => $pv_column]);
	}

	public function is_null() : \Framework\Database\Conditions\IsNull {
		return new \Framework\Database\Conditions\IsNull(['Column' => $this]);
	}

	public function is_not_null() : \Framework\Database\Conditions\IsNotNull {
		return new \Framework\Database\Conditions\IsNotNull(['Column' => $this]);
	}

	public function sort(string $pv_direction = null) : \Framework\Database\Sort {
		return new \Framework\Database\Sort(['Column' => $this, 'Direction' => $pv_direction]);
	}
}
?>