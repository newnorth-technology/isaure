<?
namespace Framework;

abstract class AController {
	/* Static methods */

	public static function Instantiate(\Framework\AController $Parent = null, string $Path, string $Name = null, array $Params = []) : \Framework\AController {
		/*{IF:DEBUG}*/

		if(!\Framework\IsConfigSet('Controllers.'.$Path)) {
			throw new \Framework\RuntimeException(
				'Unable to instantiate controller, declaration not found.',
				['Path' => $Path, 'Name' => $Name, 'Params' => $Params]
			);
		}

		/*{ENDIF}*/

		$Params = MergeArrays(\Framework\GetConfig('Controllers.'.$Path), $Params);

		$alternativeIds = [];

		if($Parent !== null && $Name !== null) {
			foreach($Parent->AlternativeIds as $alternativeId) {
				$alternativeIds[] = $alternativeId.'»'.$Name;
			}
		}

		$id = $Path;

		$alternativeIds[] = $Path;

		while(isset($Params['Template'])) {
			$Path = \Framework\NormalizePath($Path, $Params['Template']);

			unset($Params['Template']);

			$alternativeIds[] = $Path;

			/*{IF:DEBUG}*/

			if(!\Framework\IsConfigSet('Controllers.'.$Path)) {
				throw new \Framework\RuntimeException(
					'Unable to instantiate controller, template declaration not found.',
					['Path' => $Path, 'Name' => $Name, 'Params' => $Params]
				);
			}

			/*{ENDIF}*/

			$Params = MergeArrays(\Framework\GetConfig('Controllers.'.$Path), $Params);
		}

		$delimiter = strrpos($Path, '/');

		if($delimiter === false) {
			$name = $Name ?? $Path;

			$class = $Path;

			$Params['Directory'] = $directory = '';

			$Params['File'] = $file = $Path.'.php';
		}
		else {
			$name = $Name ?? substr($Path, $delimiter + 1);

			$class = str_replace('/', '\\', $Path);

			$Params['Directory'] = $directory = substr($Path, 0, $delimiter + 1);

			$Params['File'] = $file = substr($Path, $delimiter + 1).'.php';
		}

		if(self::Instantiate»LoadClass($directory, $file, $class)) {
			if(!class_exists($class, false)) {
				throw new \Framework\RuntimeException(
					'Unable to instantiate controller, class not found.',
					['Directory' => $directory, 'File' => $file, 'Class' => $class]
				);
			}
		}
		else {
			throw new \Framework\RuntimeException(
				'Unable to instantiate controller, file not found.',
				['Directory' => $directory, 'File' => $file, 'Class' => $class]
			);
		}

		$path = $Path;

		while(isset($Params['Extends'])) {
			$path = \Framework\NormalizePath($path, $Params['Extends']);

			unset($Params['Extends']);

			$alternativeIds[] = $path;

			$delimiter = strrpos($path, '/');

			if($delimiter === false) {
				$base = $path;

				$directory = '';

				$file = $path.'.php';
			}
			else {
				$base = str_replace('/', '\\', $path);

				$directory = substr($path, 0, $delimiter + 1);

				$file = substr($path, $delimiter + 1).'.php';
			}

			if(self::Instantiate»LoadClass($directory, $file, $base)) {
				if(!class_exists($base, false)) {
					throw new \Framework\RuntimeException(
						'Unable to instantiate controller, base class not found.',
						[
							'Directory' => $directory,
							'File' => $file,
							'Class' => $base
						]
					);
				}
			}
			else {
				throw new \Framework\RuntimeException(
					'Unable to instantiate controller, base file not found.',
					[
						'Directory' => $directory,
						'File' => $file,
						'Class' => $base
					]
				);
			}

			$Params = MergeArrays(\Framework\GetConfig('Controllers.'.$path, []), $Params);
		}

		$Params = NormalizeArray($Params, false);

		if(isset($Params['Actions'])) {
			$actions = $Params['Actions'];

			unset($Params['Actions']);

			$actions = NormalizeArray($actions, false);
		}
		else {
			$actions = [];
		}

		if(isset($Params['Controls'])) {
			$controls = $Params['Controls'];

			unset($Params['Controls']);

			$controls = NormalizeArray($controls, false);
		}
		else {
			$controls = [];
		}

		$Params = NormalizeArray($Params);

		$Params['Id'] = $id;

		$Params['AlternativeIds'] = $alternativeIds;

		$Params['Name'] = $name;

		$Params['Directory'] = $directory;

		$Params['File'] = $file;

		$controller = new $class($Params);

		if(isset($Params['Parent'])) {
			$parent = self::Instantiate(
				$controller,
				$Params['Parent'],
				null,
				[
					'IsControl' => $controller->IsControl,
					'IsTemplate' => $controller->IsTemplate,
					'<Model' => $Params['ParentModel'] ?? []
				]
			);

			$controller->SetParent($parent);

			$parent->SetChild($controller);
		}

		foreach($actions as $name => $params) {
			$controller->CreateAction($params['Action'], $name, $params);
		}

		foreach($controls as $name => $params) {
			$controller->CreateControl($params['Controller'], $name, $params);
		}

		return $controller;
	}

	private static function Instantiate»LoadClass(string & $pv_directory, string $pv_file, string $pv_class) : bool {
		foreach(\Framework\GetConfig('System.Directories.Controllers', ['']) as $directory) {
			if(file_exists($directory.$pv_directory.$pv_file)) {
				if(!class_exists($pv_class, false)) {
					@include($directory.$pv_directory.$pv_file);
				}

				$pv_directory = $directory.$pv_directory;

				return true;
			}
		}

		foreach($GLOBALS['modules'] as $module) {
			$path = $module->directory.'controllers/'.$pv_directory.$pv_file;

			if(file_exists($path)) {
				if(!class_exists($pv_class, false)) {
					@include($path);
				}

				$pv_directory = $module->directory.'controllers/'.$pv_directory;

				return true;
			}
		}

		return false;
	}

	/* Instance variables */

	public $Directory;

	public $File;

	public $Id;

	public $AlternativeIds;

	public $Name;

	public $Parent = null;

	public $Child = null;

	public $Owner = null;

	public $IsControl;

	public $IsTemplate;

	public $Actions;

	public $Controls;

	public $Model;

	public $View = null;

	public $Stage = 0;

	/* Magic methods */

	public function __construct(array $Params) {
		$this->Directory = $Params['Directory'];

		$this->File = $Params['File'];

		$this->Id = $Params['Id'];

		$this->AlternativeIds = $Params['AlternativeIds'];

		$this->Name = $Params['Name'];

		$this->IsControl = $Params['IsControl'] ?? false;

		$this->IsTemplate = $Params['IsTemplate'] ?? false;

		$this->Actions = new \Framework\AController\Actions();

		$this->Controls = new \Framework\AController\Controls();

		$this->Model = new \Framework\Model($Params);

		if($this->View !== null) {
			$this->SetView($this->View);
		}
	}

	public function __debugInfo() {
		return [
			'Id' => $this->Id,
			'Name' => $this->Name,
			'Parent' => $this->Parent === null ? null : $this->Parent->Id,
			'Child' => $this->Child === null ? null : $this->Child->Id
		];
	}

	/* Instance life cycle methods */

	public function Initialize() {
		$this->Stage = STAGE_INITIALIZE;

		$this->on_initialize();

		if(!$this->IsControl && $this->Child !== null) {
			$this->Child->Initialize();
		}

		$this->Controls->Initialize();

		if($this->IsControl && $this->Parent !== null) {
			$this->Parent->Initialize();
		}

		$this->Stage = STAGE_INITIALIZED;

		$this->on_initialized();
	}

	public function on_initialize() {}

	public function on_initialized() {}

	public function Load() {
		$this->Stage = STAGE_LOAD;

		$this->on_load();

		if(!$this->IsControl && $this->Child !== null) {
			$this->Child->Load();
		}

		$this->Controls->Load();

		if($this->IsControl && $this->Parent !== null) {
			$this->Parent->Load();
		}

		$this->Stage = STAGE_LOADED;

		$this->on_loaded();
	}

	public function on_load() {}

	public function on_loaded() {}

	public function Execute() {
		$this->Stage = STAGE_EXECUTE;

		$this->on_execute();

		if(!$this->IsControl && $this->Child !== null) {
			$this->Child->Execute();
		}

		$this->Actions->Execute($this, $this->Model);

		$this->Controls->Execute();

		if($this->IsControl && $this->Parent !== null) {
			$this->Parent->Execute();
		}

		$this->Stage = STAGE_EXECUTED;

		$this->on_executed();
	}

	public function on_execute() {}

	public function on_executed() {}

	public function Render() {
		$output = null;

		$this->Stage = STAGE_RENDER;

		if(!$this->IsControl && $this->Child !== null) {
			$output = $this->Child->Render();
		}

		$this->Controls->Render();

		if($this->View !== null) {
			$output = $this->View->Render($this->Model, true);

			if($this->Parent !== null && $this->Parent->View !== null) {
				$this->Parent->View->SetTranslation('Controllers.'.$this->Parent->Id.'.Child', $output);
			}
			else if($this->Owner !== null && $this->Owner->View !== null) {
				$this->Owner->View->SetTranslation('Controllers.'.$this->Owner->Id.'.Controls.'.$this->Name, $output);
			}
		}

		if($this->IsControl && $this->Parent !== null) {
			$output = $this->Parent->Render();
		}

		$this->Stage = STAGE_RENDERED;

		return $output;
	}

	/* Instance action methods */

	public function AddAction($Name, \Framework\Action $Action) {
		$this->Actions->Items[$Name] = $Action;
	}

	public function RemoveAction($Name) {
		$this->Actions->Items[$Name]->Destroy();

		unset($this->Actions->Items[$Name]);
	}

	public function GetAction($Name) {
		return $this->Actions->Items[$Name];
	}

	public function HasAction($Name) {
		return isset($this->Actions->Items[$Name]);
	}

	public function IsActionExecuted($Name) {
		return $this->Actions->Items[$Name]->IsExecuted;
	}

	/* Pre formatter methods */

	public function PreFormatters»Trim($Parameters) {
		eval($Parameters['Variable'].' = trim('.$Parameters['Variable'].');');
	}

	/* Instance validator methods */

	public function Validators»IsBetweenExclusive($Parameters) {
		return eval('return $Parameters[\'Min\'] < '.$Parameters['Variable'].' && '.$Parameters['Variable'].' < $Parameters[\'Max\'];');
	}

	public function Validators»IsBetweenInclusive($Parameters) {
		return eval('return $Parameters[\'Min\'] <= '.$Parameters['Variable'].' && '.$Parameters['Variable'].' <= $Parameters[\'Max\'];');
	}

	public function Validators»IsEmailAddress($Parameters) {
		return eval('return 0 < preg_match(\'/^([a-zA-Z0-9_.-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]+)?$/\', '.$Parameters['Variable'].');');
	}

	public function Validators»IsFalse($Parameters) {
		return eval('return !('.$Parameters['Expression'].');');
	}

	public function Validators»IsFloat($Parameters) {
		return eval('return is_numeric('.$Parameters['Variable'].');');
	}

	public function Validators»IsInt($Parameters) {
		return eval('return ctype_digit('.$Parameters['Variable'].');');
	}

	public function Validators»IsTrue($Parameters) {
		return eval('return ('.$Parameters['Expression'].');');
	}

	public function Validators»MaxLength($Parameters) {
		return eval('return isset('.$Parameters['Variable'].'[$Parameters[\'Max\']]);');
	}

	public function Validators»MinLength($Parameters) {
		return eval('return isset('.$Parameters['Variable'].'[$Parameters[\'Min\']]);');
	}

	public function Validators»NotEmpty($Parameters) {
		return eval('return isset('.$Parameters['Variable'].'[0]);');
	}

	/* Instance methods */

	public function SetParent(\Framework\AController $Parent) {
		$this->Parent = $Parent;

		if($this->View !== null) {
			$this->View->SetTranslation('Parent', '%[["Controllers.'.implode('.%$0%"%,$1...%],["Controllers.', $this->Parent->AlternativeIds).'.%$0%"%,$1...%]]%');
		}
	}

	public function SetChild(\Framework\AController $Child) {
		$this->Child = $Child;

		if($this->View !== null) {
			$this->View->SetTranslation('Child', '%[["Controllers.'.implode('.%$0%"%,$1...%],["Controllers.', $this->Child->AlternativeIds).'.%$0%"%,$1...%]]%');
		}
	}

	public function SetOwner(\Framework\AController $Owner) {
		$this->Owner = $Owner;

		if($this->View !== null) {
			$this->View->SetTranslation('Owner', '%[["Controllers.'.implode('.%$0%"%,$1...%],["Controllers.', $this->Owner->AlternativeIds).'.%$0%"%,$1...%]]%');
		}

		if(STAGE_INITIALIZE < $Owner->Stage) {
			$this->Initialize();
		}

		if(STAGE_LOAD < $Owner->Stage) {
			$this->Load();
		}

		if(STAGE_EXECUTE < $Owner->Stage) {
			$this->Execute();
		}
	}

	public function SetView(string $ViewType) {
		$this->View = new $ViewType(['Directory' => $this->Directory, 'File' => $this->File]);

		$this->View->SetTranslation('Controller', '%[["Controllers.'.implode('.%$0%"%,$1...%],["Controllers.', $this->AlternativeIds).'.%$0%"%,$1...%]]%');

		$this->View->SetTranslation('Controls', '%["Controllers.'.$this->Id.'.Controls.%$0%"%,$1...%]%');

		$this->View->SetTranslation('Controllers.'.$this->Id.'.Id', $this->Id);

		$this->View->SetTranslation('Controllers.'.$this->Id.'.Name', $this->Name);

		$this->View->SetTranslation('Route', '%["Routes.'.$GLOBALS['Route']->Id.'.%$0%"%,$1...%]%');
	}

	public function CreateAction(string $Path, string $Name, array $Params = []) : \Framework\AAction {
		if(isset($Path[0]) && $Path[0] === '/') {
			$path = substr($Path, 1);

			$action = AAction::Instantiate($this, $path, $Name, $Params);
		}
		else {
			foreach($this->AlternativeIds as $id) {
				$path = \Framework\NormalizePath($id, $Path);

				$action = AAction::Instantiate($this, $path, $Name, $Params);

				if($action !== null) {
					break;
				}
			}
		}

		if($action === null) {
			throw new \Framework\RuntimeException(
				'Unable to create action.',
				[
					'Controller' => $this->Id,
					'Path' => $Path
				]
			);
		}
		else {
			$this->Actions->Add($action);

			return $action;
		}
	}

	public function CreateControl(string $Path, string $Name, array $Params = []) : \Framework\AController {
		foreach($this->AlternativeIds as $id) {
			$path = \Framework\NormalizePath($id, $Path);

			if(\Framework\IsConfigSet('Controllers.'.$path)) {
				break;
			}
			else {
				$path = null;
			}
		}

		/*{IF:DEBUG}*/

		if($path === null) {
			throw new \Framework\RuntimeException(
				'Unable to instantiate controller, declaration not found.',
				['Path' => $Path, 'Name' => $Name, 'Params' => $Params]
			);
		}

		/*{ENDIF}*/

		$control = self::Instantiate(
			$this,
			$path,
			$Name,
			MergeArrays(
				[
					'IsControl' => true
				],
				$Params
			)
		);

		$parent = $control;

		while($parent->Parent !== null) {
			$parent->Name = $parent->Id;

			$parent->SetOwner($this);

			$parent = $parent->Parent;
		}

		$parent->Name = $Name;

		$parent->SetOwner($this);

		$this->Controls->Add($control);

		return $control;
	}

	public function CloneControl(string $Name = null, string $NewName, array $Params = []) : \Framework\AController {
		if($Name === null) {
			return $this->CreateControl(
				'/'.$this->Id,
				$NewName,
				MergeArrays(
					[
						'IsControl' => true,
						'IsTemplate' => false,
						'Model' => get_object_vars($this->Model)
					],
					$Params
				)
			);
		}

		if(!$this->Controls->TryGet($Name, $control)) {
			throw new \Framework\RuntimeException(
				'Unable to clone control, control not found.',
				[
					'Controller' => $this->Id,
					'Control' => $Name
				]
			);
		}

		return $this->CreateControl(
			'/'.$control->Id,
			$NewName,
			MergeArrays(
				[
					'IsControl' => true,
					'IsTemplate' => false,
					'Model' => get_object_vars($control->Model)
				],
				$Params
			)
		);
	}

	public function RemoveControl(string $Name) {
		return $this->Controls->Remove($Name);
	}

	public function GetControl(string $Name) : \Framework\AController {
		return $this->Controls->Get($Name);
	}

	public function TryGetControl(string $Name, \Framework\AController &$Control) : bool {
		return $this->Controls->TryGet($Name, $Control);
	}

	public function ContainsControl(string $Name) : bool {
		return $this->Controls->Contains($Name);
	}

	public function SetControlData(string $Key, $Default) {
		$method = 'GetControlData»'.$this->Name.'»'.$Key;

		if(method_exists($this->Owner, $method)) {
			$this->Model->$Key = $this->Owner->$method();
		}
		else if(!isset($this->Model->$Key)) {
			$this->Model->$Key = $Default;
		}
	}

	public function TryGetRelController(string $Path, \Framework\AController &$Controller = null) : bool {
		$controller = $this;

		if($Path[0] === '/') {
			while($controller->Owner !== null) {
				$controller = $controller->Owner;
			}

			$path = substr($Path, 1);
		}

		foreach(explode('/', $Path) as $part) {
			if($part === '.') {
				continue;
			}
			else if($part === '..') {
				if($controller->Owner === null) {
					return false;
				}

				$controller = $controller->Owner;
			}
			else if($controller->Child !== null && $controller->Child->Name === $part) {
				$controller = $controller->Child;
			}
			else if(!$controller->TryGetControl($part, $controller)) {
				return false;
			}
		}

		$Controller = $controller;

		return true;
	}
}
?>