<?
namespace Framework;

class DataType implements \JsonSerializable {
	/* Instance variables */

	public $DataManager;

	/* Magic methods */

	public function __construct(\Framework\ADataManager $DataManager, string $DataKey = null, array $Data, array $Extras = null) {
		$this->DataManager = $DataManager;

		foreach($Data as $key => $value) {
			$key = explode('.', $key);

			if(isset($key[1])) {
				if($key[0] !== $DataKey) {
					continue;
				}
				else {
					$key = $key[1];
				}
			}
			else if($DataKey !== null) {
				continue;
			}
			else {
				$key = $key[0];
			}

			if(isset($DataManager->DataMembers[$key])) {
				$dataMember = $DataManager->DataMembers[$key];

				$this->$key = $dataMember->ParseFromDbValue($dataMember, $value);
			}
			else if(isset($Extras[$key])) {
				$this->$key = call_user_func('\\Framework\\Static'.$Extras[$key].'DataMember::ParseFromDbValue', null, $value);
			}
			else {
				throw new \Framework\RuntimeException(
					'Unable to parse value, data member not found.',
					['Key' => $key, 'Value' => $value]
				);
			}
		}
	}

	public function __debugInfo() {
		$variables = get_object_vars($this);

		unset($variables['DataManager']);

		return $variables;
	}

	/* Instance JsonSerializable methods */

	public function jsonSerialize() {
		$data = [];

		foreach($this->DataManager->DataMembers as $key => $dataMember) {
			if(isset($this->$key)) {
				$data[$key] = $this->$key;
			}
		}

		return $data;
	}

	/* Instance methods */

	public function CreateInsertQuery() {
		$columns = [];

		$values = [];

		foreach($this->DataManager->DataMembers as $dataMember) {
			$columns[] = $dataMember;

			$values[] = $this->{$dataMember->Name};
		}

		return new \Framework\Database\InsertQuery([
			'Target' => new \Framework\Database\Target(['DataManager' => $this->DataManager]),
			'Columns' => new \Framework\Database\Columns($columns),
			'Values' => new \Framework\Database\Values($values)
		]);
	}

	public function Set(string $DataMember, $Value) {
		if(!isset($this->DataManager->DataMembers[$DataMember])) {
			throw new RuntimeException(
				'Failed to set data member value, data member not found.',
				[
					'Data member' => $DataMember
				]
			);
		}

		$dataMember = $this->DataManager->DataMembers[$DataMember];

		$value = $dataMember->ParseValue($dataMember, $Value);

		if(method_exists($this, 'OnBeforeSet»'.$DataMember)) {
			if(!$this->{'OnBeforeSet»'.$DataMember}($value)) {
				return false;
			}
		}

		if($this->{$dataMember->Alias} === $value) {
			return false;
		}

		$dataMember->Set($this->{$this->DataManager->PrimaryKey}, $value);

		$this->{$dataMember->Alias} = $value;

		if(method_exists($this, 'OnAfterSet»'.$DataMember)) {
			$this->{'OnAfterSet»'.$DataMember}($value);
		}

		return true;
	}

	public function Increment(string $DataMember, $Amount = 1) {
		if(!isset($this->DataManager->DataMembers[$DataMember])) {
			throw new RuntimeException(
				'Failed to increment data member value, data member not found.',
				[
					'Data member' => $DataMember
				]
			);
		}

		$dataMember = $this->DataManager->DataMembers[$DataMember];

		$dataMember->Increment($this->{$this->DataManager->PrimaryKey}, $Amount);

		$this->{$dataMember->Alias} += $Amount;

		return true;
	}

	public function Decrement(string $DataMember, $Amount = 1) {
		if(!isset($this->DataManager->DataMembers[$DataMember])) {
			throw new RuntimeException(
				'Failed to decrement data member value, data member not found.',
				[
					'Data member' => $DataMember
				]
			);
		}

		$dataMember = $this->DataManager->DataMembers[$DataMember];

		$dataMember->Decrement($this->{$this->DataManager->PrimaryKey}, $Amount);

		$this->{$dataMember->Alias} -= $Amount;

		return true;
	}

	public function TryOnDelete() : bool {
		foreach($this->DataManager->References as $reference) {
			if(!$reference->TryOnDelete($this)) {
				return false;
			}
		}

		return true;
	}

	public function OnDelete() {
		foreach($this->DataManager->References as $reference) {
			$reference->OnDelete($this);
		}

		return true;
	}

	public function OnDeleted() {
		foreach($this->DataManager->References as $reference) {
			$reference->OnDeleted($this);
		}
	}

	public function HasDataMember(string $DataMember) : bool {
		return array_key_exists($DataMember, $this->DataManager->DataMembers);
	}

	public function HasReference(string $Reference) : bool {
		return array_key_exists($Reference, $this->DataManager->References);
	}

	public function GetReference(string $Reference) : \Framework\ADataManager\Reference {
		return $this->DataManager->References[$Reference];
	}

	public function Insert(string $Reference, $Query = null, $Params = null) {
		return $this->DataManager->References[$Reference]->Insert($this, $Query, $Params);
	}

	public function Delete(string $Reference, $Query = null, $Params = null) {
		return $this->DataManager->References[$Reference]->Delete($this, $Query, $Params);
	}

	public function Find(string $Reference, $Query = null, array $References = null) {
		return $this->DataManager->References[$Reference]->Find($this, $Query, $References);
	}

	public function TryFind(\Framework\DataType &$Result = null, string $Reference, $Query = null, array $References = null) : bool {
		return $this->DataManager->References[$Reference]->TryFind($Result, $this, $Query, $References);
	}

	public function FindById(string $Reference, int $Id, array $References = null) {
		return $this->DataManager->References[$Reference]->Find(
			$this,
			[
				'Condition' => new \Framework\Database\Conditions\IsEqualTo([
					'ColumnA' => $this->DataManager->References[$Reference]->TargetDataManager->DataMembers['Id'],
					'ColumnB' => $Id
				])
			],
			$References
		);
	}

	public function TryFindById(\Framework\DataType &$Result = null, string $Reference, int $Id, array $References = null) : bool {
		return $this->DataManager->References[$Reference]->TryFind(
			$Result,
			$this,
			[
				'Condition' => new \Framework\Database\Conditions\IsEqualTo([
					'ColumnA' => $this->DataManager->References[$Reference]->TargetDataManager->DataMembers['Id'],
					'ColumnB' => $Id
				])
			],
			$References
		);
	}

	public function FindTranslation(string $Locale, array $References = null) {
		return $this->DataManager->References['Translations']->Find(
			$this,
			[
				'Condition' => new \Framework\Database\Conditions\IsEqualTo([
					'ColumnA' => $this->DataManager->References['Translations']->TargetDataManager->DataMembers['Locale'],
					'ColumnB' => $Locale
				])
			],
			$References
		);
	}

	public function TryFindTranslation(\Framework\DataType &$Result = null, string $Locale, array $References = null) : bool {
		return $this->DataManager->References['Translations']->TryFind(
			$Result,
			$this,
			[
				'Condition' => new \Framework\Database\Conditions\IsEqualTo([
					'ColumnA' => $this->DataManager->References['Translations']->TargetDataManager->DataMembers['Locale'],
					'ColumnB' => $Locale
				])
			],
			$References
		);
	}

	public function FindAll(string $Reference, $Query = null, array $References = null) : array {
		return $this->DataManager->References[$Reference]->FindAll($this, $Query, $References);
	}

	public function TryFindAll(array &$Result = null, string $Reference, $Query = null, array $References = null) : bool {
		return $this->DataManager->References[$Reference]->TryFindAll($Result, $this, $Query, $References);
	}

	public function Count(string $Reference, $Query = null, array $References = null) : int {
		return $this->DataManager->References[$Reference]->Count($this, $Query, $References);
	}

	public function SumFloat($Column, string $Reference, $Query = null, array $References = null) : float {
		return $this->DataManager->References[$Reference]->SumFloat($this, $Column, $Query, $References);
	}

	public function Contains(string $Reference, $Query = null, array $References = null) : bool {
		return 0 < $this->DataManager->References[$Reference]->Count($this, $Query, $References);
	}

	public function DoesNotContain(string $Reference, $Query = null, array $References = null) : bool {
		return $this->DataManager->References[$Reference]->Count($this, $Query, $References) === 0;
	}
}
?>