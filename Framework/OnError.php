<?
namespace Framework;

function OnError($Type, $Message, $File, $Line, $Data) {
	throw new \Framework\RuntimeException($Message, $Data);

	if(0 < ob_get_level()) {
		ob_end_clean();
	}

	if(GetConfig('System.ShowErrors', true)) {
		echo '<pre>';

		echo '<b>ERROR</b>', "\n", '------------------------------', "\n";

		echo '<b>Message:</b>', "\n", $Message, "\n\n";

		echo '<b>File:</b>', "\n", utf8_encode($File), "\n\n";

		echo '<b>Line:</b>', "\n", $Line, "\n\n";

		if(isset($Variables)) {
			echo '<b>DATA</b>', "\n", '------------------------------', "\n";

			foreach($Variables as $key => $value) {
				if(is_array($value)) {
					echo '<b>', $key, ':</b>', "\n", print_r($value, true), "\n";
				}
				else if(is_object($value)) {
					echo '<b>', $key, ':</b>', "\n", print_r($value, true), "\n";
				}
				else {
					echo '<b>', $key, ':</b>', "\n", $value, "\n\n";
				}
			}
		}

		$backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);

		array_shift($backtrace);

		foreach($backtrace as $i => $trace) {
			echo '<b>TRACE #', $i + 1, '</b>', "\n", '------------------------------', "\n";

			if(isset($trace['file'])) {
				echo '<b>File:</b>', "\n", $trace['file'], "\n\n";
			}

			if(isset($trace['line'])) {
				echo '<b>Line:</b>', "\n", $trace['line'], "\n\n";
			}

			if(isset($trace['class'])) {
				echo '<b>Function:</b>', "\n", $trace['class'].$trace['type'].$trace['function'], "\n\n";
			}
			else {
				echo '<b>Function:</b>', "\n", $trace['function'], "\n\n";
			}
		}

		echo '</pre>';
	}

	exit();
}
?>