<?
namespace Framework;

class FloatDataMember extends \Framework\ADynamicDataMember {
	/* Static methods */

	public static function ParseFromDbValue(\Framework\FloatDataMember $DataMember = null, $Value) {
		/*{IF:DEBUG}*/

		if($Value === null) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, value may not be null.',
				['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name]
			);
		}

		if(!is_float($Value) && !is_int($Value) && !is_string($Value)) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, invalid value type.',
				['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name, 'Value' => $Value]
			);
		}

		/*{ENDIF}*/

		return (float)$Value;
	}

	public static function ParseValue(\Framework\FloatDataMember $DataMember = null, $Value) {
		/*{IF:DEBUG}*/

		if($Value === null) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, value may not be null.',
				['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name]
			);
		}

		if(!is_float($Value) && !is_int($Value)) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, invalid value type.',
				['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name, 'Value' => $Value]
			);
		}

		/*{ENDIF}*/

		return $Value;
	}

	public static function ParseToDbValue(\Framework\FloatDataMember $DataMember = null, $Value) {
		return $Value;
	}
}
?>