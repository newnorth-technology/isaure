<?
namespace Framework;

class NullableStringDataMember extends \Framework\ADynamicNullableDataMember {
	/* Static methods */

	public static function ParseFromDbValue(\Framework\NullableStringDataMember $DataMember = null, $Value) {
		/*{IF:DEBUG}*/

		if($Value !== null && !is_string($Value)) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, invalid value type.',
				['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name, 'Value' => $Value]
			);
		}

		/*{ENDIF}*/

		return $Value;
	}

	public static function ParseValue(\Framework\NullableStringDataMember $DataMember = null, $Value) {
		/*{IF:DEBUG}*/

		if($Value !== null && !is_string($Value)) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, invalid value type.',
				['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name, 'Value' => $Value]
			);
		}

		/*{ENDIF}*/

		return $Value;
	}

	public static function ParseToDbValue(\Framework\NullableStringDataMember $DataMember = null, $Value) {
		return $Value;
	}
}
?>