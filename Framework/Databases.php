<?
namespace Framework;

class Databases {
	/* Static variables */

	private static $Items = [];

	/* Static create methods */

	private static function Create(string $Name) : \Framework\Database\AConnection {
		if(\Framework\TryGetConfig($data, 'Databases.'.$Name)) {
			if(!isset($data['Type'])) {
				throw new \Framework\ConfigException(
					'Database connection type not set.',
					[
						'Name' => $Name
					]
				);
			}
			else if(!class_exists($data['Type'], false)) {
				throw new \Framework\ConfigException(
					'Database connection type not found.',
					[
						'Name' => $Name,
						'Type' => $data['Type']
					]
				);
			}
			else {
				$Database = new $data['Type']($Name, $data);

				\Framework\Databases::$Items[$Name] = $Database;

				return $Database;
			}
		}
		else {
			throw new \Framework\ConfigException(
				'Database connection not found.',
				[
					'Name' => $Name
				]
			);
		}
	}

	/* Static get methods */

	public static function GetOrCreate(string $Path) : \Framework\Database\AConnection {
		return \Framework\Databases::$Items[$Path] ?? \Framework\Databases::Create($Path);
	}
}

function GetDatabase(string $Alias) : \Framework\Database\AConnection {
	return \Framework\Databases::GetOrCreate($Alias);
}
?>