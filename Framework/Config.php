<?
namespace Framework;

class Config {
	/* Static variables */

	private static $Data = [
		'Controllers' => [
			'Framework/Controllers/HtmlAction' => [],
			'Framework/Controllers/JsonAction' => [],
			'Framework/Controls/AjaxImageUploadControl' => [],
			'Framework/Controls/ButtonControl' => [],
			'Framework/Controls/CheckBoxControl' => [],
			'Framework/Controls/DropDownListControl' => [],
			'Framework/Controls/EmailAddressBoxControl' => [],
			'Framework/Controls/FileUploadControl' => [],
			'Framework/Controls/HiddenControl' => [],
			'Framework/Controls/ItemTextBoxControl' => [],
			'Framework/Controls/LocaleDropDownListControl' => ['Extends' => '/Framework/Controls/DropDownListControl'],
			'Framework/Controls/PasswordBoxControl' => [],
			'Framework/Controls/RadioButtonControl' => [],
			'Framework/Controls/ResetControl' => [],
			'Framework/Controls/SubmitButtonControl' => [],
			'Framework/Controls/SuggestionListControl' => [],
			'Framework/Controls/TextAreaBoxControl' => [],
			'Framework/Controls/TextBoxControl' => [],
		],
		'Translations' => [
			'' => [
				'Controllers.Framework/Controls/EmailAddressBoxControl.Placeholder' => '',
				'Controllers.Framework/Controls/ItemTextBoxControl.Placeholder' => '',
				'Controllers.Framework/Controls/PasswordBoxControl.Placeholder' => '',
				'Controllers.Framework/Controls/TextAreaBoxControl.Placeholder' => '',
				'Controllers.Framework/Controls/TextBoxControl.Placeholder' => '',
			]
		]
	];

	/* Static initialization methods */

	public static function Initialize(array $FilePaths, array $Data) {
		/*{IF:BENCHMARK}*/

		\Framework\Benchmark::StartProcess(['Initialize config']);

		/*{ENDIF}*/

		self::Initialize»Load($FilePaths);

		self::$Data = NormalizeLeftMergeArrays(self::$Data, $Data);

		self::Initialize»Compile(self::$Data);

		/*{IF:DEBUG}*/

		self::Initialize»Validate();

		/*{ENDIF}*/

		/*{IF:BENCHMARK}*/

		\Framework\Benchmark::FinishProcess();

		/*{ENDIF}*/
	}

	private static function Initialize»Load(array $FilePaths) {
		foreach($FilePaths as $filePath) {
			if(($fileData = @file_get_contents($filePath)) === false) {
				throw new ConfigException('Failed to open "Config" file.', ['File path' => $filePath]);
			}
			else if(($fileData = json_decode($fileData, true)) === null) {
				throw new ConfigException('Failed to parse "Config" file.', ['File path' => $filePath, 'Error code' => json_last_error(), 'Error message' => json_last_error_msg()]);
			}
			else {
				self::$Data = NormalizeLeftMergeArrays(self::$Data, $fileData);
			}
		}
	}

	private static function Initialize»Compile(array &$Compilation, string $Key = null, array $Value = null) {
		if($Key === null) {
			foreach($Compilation as $key => $value) {
				if(is_array($value)) {
					self::Initialize»Compile($Compilation, $key, $value);
				}
			}
		}
		else {
			foreach($Value as $key => $value) {
				$key = $Key.'.'.$key;

				$Compilation[$key] = $value;

				if(is_array($value)) {
					self::Initialize»Compile($Compilation, $key, $value);
				}
			}
		}
	}

	public static function apply(array $pv_config) {
		self::apply»merge(self::$Data, $pv_config);

		self::apply»compile(null, $pv_config, self::$Data);
	}

	private static function apply»merge(array & $pv_target, array $pv_source) {
		foreach($pv_source as $key => $value) {
			if(is_int($key)) {
				$pv_target[] = $value;
			}
			else if(array_key_exists($key, $pv_target) && is_array($pv_target[$key]) && is_array($value)) {
				self::apply»merge($pv_target[$key], $value);
			}
			else {
				$pv_target[$key] = $value;
			}
		}
	}

	private static function apply»compile(string $pv_key = null, array $pv_new_value, array $pv_merged_value) {
		if($pv_key === null) {
			foreach($pv_new_value as $key => $value) {
				if(is_array($value)) {
					self::apply»compile($key, $value, $pv_merged_value[$key]);
				}
			}
		}
		else {
			foreach($pv_new_value as $key => $value) {
				self::$Data[$pv_key.'.'.$key] = $pv_merged_value[$key];

				if(is_array($value)) {
					self::apply»compile($pv_key.'.'.$key, $value, $pv_merged_value[$key]);
				}
			}
		}
	}

	/*{IF:DEBUG}*/

	private static function Initialize»Validate() {
		if(!isset(self::$Data['Locales'])) {
			throw new ConfigException('Invalid configuration, "Locales" section is not defined.');
		}

		if(isset(self::$Data['Actions'])) {
			self::Initialize»Validate»Actions(self::$Data['Actions']);
		}

		if(!isset(self::$Data['Routes'])) {
			throw new ConfigException('Invalid configuration, "Routes" section is not defined.');
		}

		if(isset(self::$Data['DataManagers'])) {
			self::Initialize»Validate»DataManagers(self::$Data['DataManagers']);
		}

		self::Initialize»Validate»Routes(self::$Data['Routes']);
	}

	private static function Initialize»Validate»Actions(array $Params) {
		foreach($Params as $id => $params) {
			if(isset($params['Variables'])) {
				self::Initialize»Validate»Actions»Variables($id, $params['Variables']);
			}

			if(isset($params['PreValidators'])) {
				self::Initialize»Validate»Actions»PreValidators($id, $params['PreValidators']);
			}
		}
	}

	private static function Initialize»Validate»Actions»Variables(string $ActionId, array $Params) {
		foreach($Params as $name => $params) {
			if(!is_array($params)) {
				throw new ConfigException(
					'Invalid configuration, action variables params is not an object.',
					[
						'Action' => $ActionId,
						'Variable' => $name
					]
				);
			}

			if(!isset($params['Source'])) {
				throw new \Framework\ConfigException(
					'Invalid configuration, source not defined in action variable.',
					[
						'Action' => $ActionId,
						'Variable' => $name
					]
				);
			}
		}
	}

	private static function Initialize»Validate»Actions»PreValidators(string $ActionId, array $Params) {
		foreach($Params as $name => $params) {
			if(!is_array($params)) {
				throw new ConfigException(
					'Invalid configuration, action pre validators params is not an object.',
					[
						'Action' => $ActionId,
						'Pre validator' => $name
					]
				);
			}

			if(!isset($params['Method'])) {
				throw new \Framework\ConfigException(
					'Invalid configuration, method not defined in action pre validator.',
					[
						'Action' => $ActionId,
						'Pre validator' => $name
					]
				);
			}
		}
	}

	private static function Initialize»Validate»DataManagers(array $Params) {
		foreach($Params as $id => $params) {
			if(isset($params['DataMembers'])) {
				if(!is_array($params['DataMembers'])) {
					throw new ConfigException(
						'Invalid configuration, \'DataMembers\' in data manager is not an object.',
						['Data manager' => $id, 'DataMembers' => $params['DataMembers']]
					);
				}

				self::Initialize»Validate»DataManagers»DataMembers($id, $params['DataMembers']);
			}

			if(isset($params['References'])) {
				if(!is_array($params['References'])) {
					throw new ConfigException(
						'Invalid configuration, \'References\' in data manager is not an object.',
						['Data manager' => $id, 'References' => $params['References']]
					);
				}

				self::Initialize»Validate»DataManagers»References($id, $params['References']);
			}
		}
	}

	private static function Initialize»Validate»DataManagers»DataMembers(string $DataManagerId, array $Params) {
		foreach($Params as $id => $params) {
			if(!is_array($params)) {
				throw new ConfigException(
					'Invalid configuration, data member is not an object.',
					['Data manager' => $DataManagerId, 'Data member' => $id]
				);
			}

			if(!isset($params['Type'])) {
				throw new ConfigException(
					'Invalid configuration, type not defined for data member.',
					['Data manager' => $DataManagerId, 'Data member' => $id]
				);
			}

			if(!method_exists(__CLASS__, 'Initialize»Validate»DataManagers»DataMembers»'.$params['Type'])) {
				throw new ConfigException(
					'Invalid configuration, invalid type for data member.',
					['Data manager' => $DataManagerId, 'Data member' => $id, 'Type' => $params['Type']]
				);
			}

			call_user_func('\Framework\Config::Initialize»Validate»DataManagers»DataMembers»'.$params['Type'], $DataManagerId, $id, $params);
		}
	}

	private static function Initialize»Validate»DataManagers»DataMembers»Bool(string $DataManagerId, string $DataMemberId, array $Params) {

	}

	private static function Initialize»Validate»DataManagers»DataMembers»Enum(string $DataManagerId, string $DataMemberId, array $Params) {
		if(!isset($Params['Values'])) {
			throw new ConfigException(
				'Invalid configuration, values not defined for enum data member.',
				['Data manager' => $DataManagerId, 'Data member' => $DataMemberId]
			);
		}

		if(!is_array($Params['Values'])) {
			throw new ConfigException(
				'Invalid configuration, values is not an array for enum data member.',
				['Data manager' => $DataManagerId, 'Data member' => $DataMemberId, 'Values' => $Params['Values']]
			);
		}
	}

	private static function Initialize»Validate»DataManagers»DataMembers»Float(string $DataManagerId, string $DataMemberId, array $Params) {

	}

	private static function Initialize»Validate»DataManagers»DataMembers»Int(string $DataManagerId, string $DataMemberId, array $Params) {

	}

	private static function Initialize»Validate»DataManagers»DataMembers»Json(string $DataManagerId, string $DataMemberId, array $Params) {

	}

	private static function Initialize»Validate»DataManagers»DataMembers»String(string $DataManagerId, string $DataMemberId, array $Params) {

	}

	private static function Initialize»Validate»DataManagers»DataMembers»NullableBool(string $DataManagerId, string $DataMemberId, array $Params) {

	}

	private static function Initialize»Validate»DataManagers»DataMembers»NullableEnum(string $DataManagerId, string $DataMemberId, array $Params) {
		if(!isset($Params['Values'])) {
			throw new ConfigException(
				'Invalid configuration, values not defined for enum data member.',
				['Data manager' => $DataManagerId, 'Data member' => $DataMemberId]
			);
		}

		if(!is_array($Params['Values'])) {
			throw new ConfigException(
				'Invalid configuration, values is not an array for enum data member.',
				['Data manager' => $DataManagerId, 'Data member' => $DataMemberId, 'Values' => $Params['Values']]
			);
		}
	}

	private static function Initialize»Validate»DataManagers»DataMembers»NullableFloat(string $DataManagerId, string $DataMemberId, array $Params) {

	}

	private static function Initialize»Validate»DataManagers»DataMembers»NullableInt(string $DataManagerId, string $DataMemberId, array $Params) {

	}

	private static function Initialize»Validate»DataManagers»DataMembers»NullableJson(string $DataManagerId, string $DataMemberId, array $Params) {

	}

	private static function Initialize»Validate»DataManagers»DataMembers»NullableString(string $DataManagerId, string $DataMemberId, array $Params) {

	}

	private static function Initialize»Validate»DataManagers»DataMembers»StaticBool(string $DataManagerId, string $DataMemberId, array $Params) {

	}

	private static function Initialize»Validate»DataManagers»DataMembers»StaticEnum(string $DataManagerId, string $DataMemberId, array $Params) {
		if(!isset($Params['Values'])) {
			throw new ConfigException(
				'Invalid configuration, values not defined for enum data member.',
				['Data manager' => $DataManagerId, 'Data member' => $DataMemberId]
			);
		}

		if(!is_array($Params['Values'])) {
			throw new ConfigException(
				'Invalid configuration, values is not an array for enum data member.',
				['Data manager' => $DataManagerId, 'Data member' => $DataMemberId, 'Values' => $Params['Values']]
			);
		}
	}

	private static function Initialize»Validate»DataManagers»DataMembers»StaticFloat(string $DataManagerId, string $DataMemberId, array $Params) {

	}

	private static function Initialize»Validate»DataManagers»DataMembers»StaticInt(string $DataManagerId, string $DataMemberId, array $Params) {

	}

	private static function Initialize»Validate»DataManagers»DataMembers»StaticJson(string $DataManagerId, string $DataMemberId, array $Params) {

	}

	private static function Initialize»Validate»DataManagers»DataMembers»StaticString(string $DataManagerId, string $DataMemberId, array $Params) {

	}

	private static function Initialize»Validate»DataManagers»DataMembers»StaticNullableBool(string $DataManagerId, string $DataMemberId, array $Params) {

	}

	private static function Initialize»Validate»DataManagers»DataMembers»StaticNullableEnum(string $DataManagerId, string $DataMemberId, array $Params) {
		if(!isset($Params['Values'])) {
			throw new ConfigException(
				'Invalid configuration, values not defined for enum data member.',
				['Data manager' => $DataManagerId, 'Data member' => $DataMemberId]
			);
		}

		if(!is_array($Params['Values'])) {
			throw new ConfigException(
				'Invalid configuration, values is not an array for enum data member.',
				['Data manager' => $DataManagerId, 'Data member' => $DataMemberId, 'Values' => $Params['Values']]
			);
		}
	}

	private static function Initialize»Validate»DataManagers»DataMembers»StaticNullableFloat(string $DataManagerId, string $DataMemberId, array $Params) {

	}

	private static function Initialize»Validate»DataManagers»DataMembers»StaticNullableInt(string $DataManagerId, string $DataMemberId, array $Params) {

	}

	private static function Initialize»Validate»DataManagers»DataMembers»StaticNullableJson(string $DataManagerId, string $DataMemberId, array $Params) {

	}

	private static function Initialize»Validate»DataManagers»DataMembers»StaticNullableString(string $DataManagerId, string $DataMemberId, array $Params) {

	}

	private static function Initialize»Validate»DataManagers»References(string $DataManagerId, array $Params) {
		foreach($Params as $name => $params) {
			if(!is_array($params)) {
				throw new ConfigException(
					'Invalid configuration, reference params is not an object.',
					[
						'Data manager' => $DataManagerId,
						'Data reference' => $name
					]
				);
			}

			if(!isset($params['TargetDataManager'])) {
				throw new ConfigException(
					'Invalid configuration, target data manager not defined in reference.',
					[
						'Data manager' => $DataManagerId,
						'Data reference' => $name
					]
				);
			}
		}
	}

	private static function Initialize»Validate»Routes(array $Data) {
		if(!isset($Data['/'])) {
			throw new \Framework\ConfigException(
				'Invalid configuration, root route is not defined.'
			);
		}

		foreach($Data as $path => $params) {
			if(!isset($path[0])) {
				throw new \Framework\ConfigException(
					'Invalid configuration, route path is empty.',
					[
						'Route path' => $path,
						'Params' => $params
					]
				);
			}

			if($path[0] !== '/') {
				throw new \Framework\ConfigException(
					'Invalid configuration, route path does not start with a "/".',
					[
						'Route path' => $path,
						'Params' => $params
					]
				);
			}

			if($path[strlen($path) - 1] !== '/') {
				throw new \Framework\ConfigException(
					'Invalid configuration, route path does not end with a "/".',
					[
						'Route path' => $path,
						'Route params' => $params
					]
				);
			}

			if(isset($params['Pattern'])) {
				self::Initialize»Validate»Routes»Pattern($path, $params['Pattern']);
			}
		}
	}

	private static function Initialize»Validate»Routes»Pattern(string $RoutePath, array $Params) {
		foreach($Params as $segment) {
			if(!isset($segment['Type'])) {
				throw new ConfigException(
					'Invalid configuration, "Type" is not defined on pattern segment.',
					[
						'Route path' => $RoutePath,
						'Segment params' => $segment
					]
				);
			}
		}
	}

	/*{ENDIF}*/

	/* Static methods */

	public static function Get(string $Path, $DefaultValue = null) {
		return self::$Data[$Path] ?? $DefaultValue;
	}

	public static function TryGet(string $Path, &$Value = null) : bool {
		if(isset(self::$Data[$Path])) {
			$Value = self::$Data[$Path];

			return true;
		}
		else {
			return false;
		}
	}

	public static function Set(string $Key, $Value) {
		self::$Data[$Key] = $Value;
	}

	public static function IsSet(string $Key) : bool {
		return isset(self::$Data[$Key]);
	}

	public static function IsNotSet(string $Key) : bool {
		return !isset(self::$Data[$Key]);
	}

	public static function IsEmpty(string $Key) : bool {
		return !isset(self::$Data[$Key][0]);
	}

	public static function IsNotEmpty(string $Key) : bool {
		return isset(self::$Data[$Key][0]);
	}
}

function GetConfig(string $Key, $DefaultData = null) {
	return \Framework\Config::Get($Key, $DefaultData);
}

function TryGetConfig(&$Data, string $Key) : bool {
	return \Framework\Config::TryGet($Key, $Data);
}

function SetConfig(string $Key, $Data) {
	\Framework\Config::Set($Key, $Data);
}

function IsConfigSet(string $Key) : bool {
	return \Framework\Config::IsSet($Key);
}

function IsConfigNotSet(string $Key) : bool {
	return \Framework\Config::IsNotSet($Key);
}

function IsConfigEmpty(string $Key) : bool {
	return \Framework\Config::IsEmpty($Key);
}

function IsConfigNotEmpty(string $Key) : bool {
	return \Framework\Config::IsNotEmpty($Key);
}
?>