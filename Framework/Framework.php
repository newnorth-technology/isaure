<?
namespace f {
	function array_merge(array $pv_target, array ... $pv_sources) : array {
		foreach($pv_sources as $source) {
			foreach($source as $key => $value) {
				if(is_int($key)) {
					$pv_target[] = $value;
				}
				else if(!isset($key[0])) {
					$pv_target[''] = $value;
				}
				else if($key[0] === '<') {
					$key = substr($key, 1);

					$pv_target[$key] = \f\array_merge($pv_target[$key] ?? [], $value);
				}
				else if($key[0] === '>') {
					$key = substr($key, 1);

					$pv_target[$key] = \f\array_merge($value, $pv_target[$key] ?? []);
				}
				else if($key[0] === '-') {
					$pv_target[$key] = null;
				}
				else {
					$pv_target[$key] = $value;
				}
			}
		}

		return $pv_target;
	}

	function array_merge_after(array $pv_array, $pv_key, $pv_value) : array {
		$index = array_search($pv_key, array_keys($pv_array));

		return array_slice($pv_array, 0, $index + 1) + $pv_value + array_slice($pv_array, $index + 1);
	}

	function get_manager(string $pv_alias) : \Framework\ADataManager {
		/*{IF:DEBUG}*/

		if(!isset(\Framework\DataManagers::$Items[$pv_alias])) {
			throw new \Framework\RuntimeException('Manager not found.', ['pv_alias' => $pv_alias]);
		}

		/*{ENDIF}*/

		return \Framework\DataManagers::$Items[$pv_alias];
	}

	function try_get_manager(string $pv_alias, \Framework\ADataManager &$pv_manager = null) : bool {
		if(isset(\Framework\DataManagers::$Items[$pv_alias])) {
			$pv_manager = \Framework\DataManagers::$Items[$pv_alias];

			return true;
		}
		else {
			return false;
		}
	}

	function manager_exists(string $pv_alias) : bool {
		return isset(\Framework\DataManagers::$Items[$pv_alias]);
	}

	function get_member(string $pv_manager_alias, string $pv_member_alias) : \Framework\ADataMember {
		/*{IF:DEBUG}*/

		if(!isset(\Framework\DataManagers::$Items[$pv_manager_alias])) {
			throw new \Framework\RuntimeException('Manager not found.', ['pv_manager_alias' => $pv_manager_alias]);
		}

		if(!isset(\Framework\DataManagers::$Items[$pv_manager_alias]->DataMembers[$pv_member_alias])) {
			throw new \Framework\RuntimeException('Member not found.', ['pv_manager_alias' => $pv_manager_alias, 'pv_member_alias' => $pv_member_alias]);
		}

		/*{ENDIF}*/

		return \Framework\DataManagers::$Items[$pv_manager_alias]->DataMembers[$pv_member_alias];
	}

	class config {
		/* instance variables */

		public $data = [];

		/* instance methods */

		public function load(string $pv_path) {
			if(!file_exists($pv_path)) {
				throw new \Framework\ConfigException(
					'Config file not found.',
					['path' => $pv_path]
				);
			}

			$config = @file_get_contents($pv_path);

			if($config === false) {
				throw new \Framework\ConfigException(
					'Failed to open config file.',
					['path' => $pv_path]
				);
			}

			$config = json_decode($config, true);

			if($config === null) {
				throw new \Framework\ConfigException(
					'Failed to parse config file.',
					['path' => $pv_path, 'error code' => json_last_error(), 'error message' => json_last_error_msg()]
				);
			}

			$this->apply($config);
		}

		public function apply(array $pv_config) {
			$this->apply»merge($this->data, $pv_config);

			$this->apply»compile(null, $pv_config, $this->data);
		}

		private function apply»merge(array & $pv_target, array $pv_source) {
			foreach($pv_source as $key => $value) {
				if(is_int($key)) {
					$pv_target[] = $value;
				}
				else if(array_key_exists($key, $pv_target) && is_array($pv_target[$key]) && is_array($value)) {
					$this->apply»merge($pv_target[$key], $value);
				}
				else {
					$pv_target[$key] = $value;
				}
			}
		}

		private function apply»compile(string $pv_key = null, array $pv_new_value, array $pv_merged_value) {
			if($pv_key === null) {
				foreach($pv_new_value as $key => $value) {
					if(is_array($value)) {
						$this->apply»compile($key, $value, $pv_merged_value[$key]);
					}
				}
			}
			else {
				foreach($pv_new_value as $key => $value) {
					$this->data[$pv_key.'.'.$key] = $pv_merged_value[$key];

					if(is_array($value)) {
						$this->apply»compile($pv_key.'.'.$key, $value, $pv_merged_value[$key]);
					}
				}
			}
		}
	}

	$modules = [];

	class module {
		/* static methods */

		public static function load(string $pv_path) {
			$modules = self::load»search($pv_path);

			foreach($modules as $module) {
				$config_path = $module['directory'].'module.json';

				if(!file_exists($config_path)) {
					throw new \Framework\ConfigException(
						'Module config file not found.',
						['path' => $config_path]
					);
				}

				$config = @file_get_contents($config_path);

				if($config === false) {
					throw new \Framework\ConfigException(
						'Failed to open module config file.',
						['path' => $config_path]
					);
				}

				$config = json_decode($config, true);

				if($config === null) {
					throw new \Framework\ConfigException(
						'Failed to parse module config file.',
						['path' => $config_path, 'error code' => json_last_error(), 'error message' => json_last_error_msg()]
					);
				}

				if(!file_exists($config_path)) {
					throw new \Framework\ConfigException(
						'Module config file not found.',
						['path' => $config_path]
					);
				}

				$script_path = $module['directory'].'module.php';

				if(!file_exists($script_path)) {
					throw new \Framework\ConfigException(
						'Module script file not found.',
						['path' => $script_path]
					);
				}

				if(!@include($script_path)) {
					throw new \Framework\ConfigException(
						'Failed to include module script file.',
						['path' => $script_path]
					);
				}

				$module = new \f\module($module['directory'], $module['namespace'], $module['name']);

				foreach($config ?? [] as $file) {
					$module->config->load($module->directory.$file);
				}

				\Framework\Config::apply($module->config->data);

				$GLOBALS['modules'][] = $module;
			}
		}

		private static function load»search(string $pv_path) {
			$result = [];

			$paths = [['\\', $pv_path]];

			for($i = 0; $i < count($paths); ++$i) {
				$path = $paths[$i];

				$directories = scandir($path[1]);

				foreach($directories as $directory) {
					if($directory === '.' || $directory === '..') {
						continue;
					}

					if(file_exists($path[1].$directory.'/module.json')) {
						$result[] = ['directory' => $path[1].$directory.'/', 'namespace' => $path[0], 'name' => $directory];
					}
					else {
						$paths[] = [$path[0].$directory.'\\', $path[1].$directory.'/'];
					}
				}
			}

			return $result;
		}

		/* instance variables */

		public $id;

		public $directory;

		public $namespace;

		public $name;

		public $config;

		/* magic methods */

		public function __construct(string $pv_directory, string $pv_namespace, string $pv_name) {
			$this->id = $pv_namespace.$pv_name;

			$this->directory = $pv_directory;

			$this->namespace = $pv_namespace;

			$this->name = $pv_name;

			$this->config = new \f\config();
		}
	}

	function has_module(string $pv_id) : bool {
		foreach($GLOBALS['modules'] as $module) {
			if($module->id === $pv_id) {
				return true;
			}
		}

		return false;
	}
}

namespace Framework {
	/* Compilation mode */

	/*{IF:RELEASE}*/
	define('MODE', 'RELEASE');
	/*{ELIF:DEBUG}*/
	define('MODE', 'DEBUG');
	/*{ELIF:BENCHMARK}*/
	define('MODE', 'BENCHMARK');
	/*{ENDIF}*/

	/* Life cycle stage definitions */

	define('STAGE_INITIALIZE', 1);

	define('STAGE_INITIALIZED', 2);

	define('STAGE_LOAD', 3);

	define('STAGE_LOADED', 4);

	define('STAGE_EXECUTE', 5);

	define('STAGE_EXECUTED', 6);

	define('STAGE_RENDER', 7);

	define('STAGE_RENDERED', 8);
}

require('Exception.php');
require('ConfigException.php');
require('RuntimeException.php');
require('RerouteException.php');
require('RedirectException.php');
require('ErrorHandler.php');
require('Logger.php');
require('Emailer.php');
require('Translator.php');
require('Routes.php');

require('Email.php');

require('Translations.php');

require('DataType.php');

require('AAction.php');

require('AAction/Variables.php');

require('AAction/PreValidator.php');

require('AAction/PreValidators.php');

require('Benchmark.php');

require('Benchmark/Data.php');

require('Benchmark/Process.php');

require('DataManagers.php');

require('Databases.php');

require('Config.php');

require('Locale.php');

require('OnError.php');

require('OnException.php');

require('OnShutdown.php');

require('Param.php');

require('Route.php');

require('RouteValidators.php');

require('RouteValidator.php');

require('Route/Match.php');

require('Route/Pattern.php');

require('Route/Pattern/Match.php');

require('Route/Pattern/ASegment.php');

require('Route/Pattern/Segment/Match.php');

require('Route/Pattern/Segments/Constant.php');

require('Route/Pattern/Segments/DataManager.php');

require('Route/Pattern/Segments/Translation.php');

require('Route/Pattern/Segments/Variable.php');

require('Router.php');

require('Run.php');

require('Transaction.php');

require('Image.php');

/* Models */

require('Model.php');

/* Views */

require('AView.php');

require('Views/Css.php');

require('Views/Html.php');

require('Views/ImagePng.php');

require('Views/Js.php');

require('Views/Json.php');

require('Views/ZipArchive.php');

/* Database */

require('Database/AColumn.php');

require('Database/ACondition.php');

require('Database/AConnection.php');

require('Database/MySqlConnection.php');

require('Database/AResult.php');

require('Database/MySqlResult.php');

require('Database/Change.php');

require('Database/Changes.php');

require('Database/Columns/Add.php');

require('Database/Columns/Condition.php');

require('Database/Columns/Config.php');

require('Database/Columns/Count.php');

require('Database/Columns/Expression.php');

require('Database/Columns/Max.php');

require('Database/Columns/Min.php');

require('Database/Columns/Random.php');

require('Database/Columns/Reference.php');

require('Database/Columns/Subtract.php');

require('Database/Columns/Sum.php');

require('Database/Columns/Value.php');

require('Database/Columns.php');

require('Database/Conditions/All.php');

require('Database/Conditions/Any.php');

require('Database/Conditions/Expression.php');

require('Database/Conditions/IsBetween.php');

require('Database/Conditions/IsEmpty.php');

require('Database/Conditions/IsEqualTo.php');

require('Database/Conditions/IsFalse.php');

require('Database/Conditions/IsGreaterThan.php');

require('Database/Conditions/IsGreaterThanOrEqualTo.php');

require('Database/Conditions/IsIn.php');

require('Database/Conditions/IsLessThan.php');

require('Database/Conditions/IsLessThanOrEqualTo.php');

require('Database/Conditions/IsLike.php');

require('Database/Conditions/IsNotBetween.php');

require('Database/Conditions/IsNotEqualTo.php');

require('Database/Conditions/IsNotEmpty.php');

require('Database/Conditions/IsNotIn.php');

require('Database/Conditions/IsNotLike.php');

require('Database/Conditions/IsNotNull.php');

require('Database/Conditions/IsNull.php');

require('Database/Conditions/IsTrue.php');

require('Database/DeleteQuery.php');

require('Database/Group.php');

require('Database/Groups.php');

require('Database/InsertQuery.php');

require('Database/Join.php');

require('Database/Joins.php');

require('Database/Limit.php');

require('Database/SelectQuery.php');

require('Database/Sort.php');

require('Database/Sorts.php');

require('Database/Source.php');

require('Database/Sources.php');

require('Database/Target.php');

require('Database/Targets.php');

require('Database/UpdateQuery.php');

require('Database/Values.php');

/* Data manager */

require('ADataManager.php');

require('ADataMember.php');

require('ADynamicDataMember.php');

require('ADynamicNullableDataMember.php');

require('AStaticDataMember.php');

require('AStaticNullableDataMember.php');

require('BoolDataMember.php');

require('EnumDataMember.php');

require('FloatDataMember.php');

require('IntDataMember.php');

require('JsonDataMember.php');

require('StringDataMember.php');

require('NullableBoolDataMember.php');

require('NullableEnumDataMember.php');

require('NullableFloatDataMember.php');

require('NullableIntDataMember.php');

require('NullableJsonDataMember.php');

require('NullableStringDataMember.php');

require('StaticBoolDataMember.php');

require('StaticEnumDataMember.php');

require('StaticFloatDataMember.php');

require('StaticIntDataMember.php');

require('StaticJsonDataMember.php');

require('StaticStringDataMember.php');

require('StaticNullableBoolDataMember.php');

require('StaticNullableEnumDataMember.php');

require('StaticNullableFloatDataMember.php');

require('StaticNullableIntDataMember.php');

require('StaticNullableJsonDataMember.php');

require('StaticNullableStringDataMember.php');

require('ADataManager/Reference.php');

require('ADataManager/Slave.php');

require('ConnectionDataManager.php');

require('NewConnectionDataManager.php');

require('StandardDataManager.php');

require('NewStandardDataManager.php');

require('TranslationDataManager.php');

require('NewTranslationDataManager.php');

/* Controller */

require('AController.php');

require('AController/Actions.php');

require('AController/Controls.php');

require('Controllers/ACss.php');

require('Controllers/AHtml.php');

require('Controllers/AImagePng.php');

require('Controllers/AJs.php');

require('Controllers/AJson.php');

require('Controllers/AZipArchive.php');

require('Controllers/HtmlAction.php');

require('Controllers/JsonAction.php');

require('ActionController.php');

/* Controls */

require('Controls/AjaxImageUploadControl.php');

require('Controls/ButtonControl.php');

require('Controls/CheckBoxControl.php');

require('Controls/DropDownListControl.php');

require('Controls/EmailAddressBoxControl.php');

require('Controls/FileUploadControl.php');

require('Controls/HiddenControl.php');

require('Controls/ItemTextBoxControl.php');

require('Controls/LocaleDropDownListControl.php');

require('Controls/PasswordBoxControl.php');

require('Controls/RadioButtonControl.php');

require('Controls/ResetControl.php');

require('Controls/SubmitButtonControl.php');

require('Controls/SuggestionListControl.php');

require('Controls/TextAreaBoxControl.php');

require('Controls/TextBoxControl.php');

/* Libraries */

require('Discord.php');

require('HTML/HTML.php');

namespace Framework {
	function FormatNumber(float $Number, int $Decimals) : string {
		return number_format($Number, $Decimals, $GLOBALS['Locale']->DecimalPoint, $GLOBALS['Locale']->ThousandsSeparator);
	}

	function BuildHttpQueryString(array $Query) {
		foreach($Query as $key => &$value) {
			if(is_int($key)) {
				$value = urlencode($value);
			}
			else if(is_bool($value)) {
				$value = urlencode($key).'='.($value ? '1' : '0');
			}
			else {
				$value = urlencode($key).'='.urlencode($value);
			}
		}

		return implode('&', $Query);
	}

	function InArrayFunc(array $Haystack, $Needle, callable $Function) : bool {
		foreach($Haystack as $item) {
			if(call_user_func($Function, $item, $Needle)) {
				return true;
			}
		}

		return false;
	}

	function AllInArrayFunc(array $Haystack, array $Needle, callable $Function) : bool {
		foreach($Needle as $b) {
			$isFound = false;

			foreach($Haystack as $a) {
				if(call_user_func($Function, $a, $b)) {
					$isFound = true;

					break;
				}
			}

			if(!$isFound) {
				return false;
			}
		}

		return true;
	}

	function AnyInArrayFunc(array $Haystack, array $Needle, callable $Function) : bool {
		foreach($Needle as $b) {
			foreach($Haystack as $a) {
				if(call_user_func($Function, $a, $b)) {
					return true;
				}
			}
		}

		return false;
	}

	function FindInArrayFunc(array $Haystack, $Needle, callable $Function) {
		foreach($Haystack as $item) {
			if(call_user_func($Function, $item, $Needle)) {
				return $item;
			}
		}

		return null;
	}

	function NormalizeArray(array $Array, bool $Recursive = true) : array {
		$keys = array_keys($Array);

		rsort($keys);

		if($Recursive) {
			foreach($keys as $key) {
				if(is_int($key)) {
					continue;
				}

				if(!isset($key[0])) {
					continue;
				}

				$value = $Array[$key];

				if($key[0] === '-') {
					unset($Array[$key]);

					$key = substr($key, 1);

					if(isset($key[0]) && $key[0] === '-') {
						$Array[$key] = null;
					}
					else {
						unset($Array[$key]);
					}
				}
				else if($key[0] === '<') {
					unset($Array[$key]);

					$key = substr($key, 1);

					if(is_array($value)) {
						if(isset($Array[$key]) && is_array($Array[$key])) {
							$Array[$key] = NormalizeLeftMergeArrays($Array[$key], $value);
						}
						else {
							$Array[$key] = NormalizeArray($value);
						}
					}
					else {
						$Array[$key] = $value;
					}
				}
				else if($key[0] === '>') {
					unset($Array[$key]);

					$key = substr($key, 1);

					if(is_array($value)) {
						if(isset($Array[$key]) && is_array($Array[$key])) {
							$Array[$key] = NormalizeRightMergeArrays($Array[$key], $value);
						}
						else {
							$Array[$key] = NormalizeArray($value);
						}
					}
					else {
						$Array[$key] = $value;
					}
				}
				else if(is_array($value)) {
					$Array[$key] = NormalizeArray($value);
				}
			}
		}
		else {
			foreach($keys as $key) {
				if(is_int($key)) {
					continue;
				}

				if(!isset($key[0])) {
					continue;
				}

				$value = $Array[$key];

				if($key[0] === '-') {
					unset($Array[$key]);

					$key = substr($key, 1);

					if(isset($key[0]) && $key[0] === '-') {
						$Array[$key] = null;
					}
					else {
						unset($Array[$key]);
					}
				}
				else if($key[0] === '<') {
					unset($Array[$key]);

					$key = substr($key, 1);

					if(is_array($value)) {
						if(isset($Array[$key]) && is_array($Array[$key])) {
							$Array[$key] = MergeArrays($Array[$key], $value);
						}
						else {
							$Array[$key] = $value;
						}
					}
					else {
						$Array[$key] = $value;
					}
				}
				else if($key[0] === '>') {
					unset($Array[$key]);

					$key = substr($key, 1);

					if(is_array($value)) {
						if(isset($Array[$key]) && is_array($Array[$key])) {
							$Array[$key] = MergeArrays($value, $Array[$key]);
						}
						else {
							$Array[$key] = $value;
						}
					}
					else {
						$Array[$key] = $value;
					}
				}
			}
		}

		return $Array;
	}

	function NormalizeLeftMergeArrays(array $Target, array $Source) : array {
		$keys = array_keys($Source);

		sort($keys);

		foreach($keys as $key) {
			if(!is_int($key)) {
				continue;
			}

			$value = $Source[$key];

			if(is_array($value)) {
				$Target[] = NormalizeArray($value);
			}
			else {
				$Target[] = $value;
			}
		}

		return NormalizeMergeArrays($Target, $Source);
	}

	function NormalizeRightMergeArrays(array $Target, array $Source) : array {
		$keys = array_keys($Source);

		rsort($keys);

		foreach($keys as $key) {
			if(!is_int($key)) {
				continue;
			}

			$value = $Source[$key];

			if(is_array($value)) {
				array_unshift($Target, NormalizeArray($value));
			}
			else {
				array_unshift($Target, $value);
			}
		}

		return NormalizeMergeArrays($Target, $Source);
	}

	function NormalizeMergeArrays(array $Target, array $Source) : array {
		$keys = array_keys($Source);

		rsort($keys);

		foreach($keys as $key) {
			if(is_int($key)) {
				continue;
			}

			$value = $Source[$key];

			if(!isset($key[0])) {
				if(is_array($value)) {
					$Target[''] = NormalizeArray($value);
				}
				else {
					$Target[''] = $value;
				}
			}
			else if($key[0] === '-') {
				$key = substr($key, 1);

				if(isset($key[0]) && $key[0] === '-') {
					$Target[$key] = null;
				}
				else {
					unset($Target[$key]);
				}
			}
			else if($key[0] === '<') {
				$key = substr($key, 1);

				if(is_array($value)) {
					$Target[$key] = NormalizeLeftMergeArrays($Target[$key] ?? [], $value);
				}
				else {
					$Target[$key] = $value;
				}
			}
			else if($key[0] === '>') {
				$key = substr($key, 1);

				if(is_array($value)) {
					$Target[$key] = NormalizeRightMergeArrays($Target[$key] ?? [], $value);
				}
				else {
					$Target[$key] = $value;
				}
			}
			else if(is_array($value)) {
				$Target[$key] = NormalizeArray($value);
			}
			else {
				$Target[$key] = $value;
			}
		}

		return $Target;
	}

	function MergeArrays(array $Target, array ... $Sources) : array {
		foreach($Sources as $source) {
			foreach($source as $key => $value) {
				if(is_int($key)) {
					$Target[] = $value;
				}
				else if(!isset($key[0])) {
					$Target[''] = $value;
				}
				else if($key[0] === '<') {
					$Target[$key] = MergeArrays($Target[$key] ?? [], $value);
				}
				else if($key[0] === '>') {
					$Target[$key] = MergeArrays($value, $Target[$key] ?? []);
				}
				else if($key[0] === '-') {
					$Target[$key] = null;
				}
				else {
					$Target[$key] = $value;
				}
			}
		}

		return $Target;
	}

	function NormalizePath(string $BasePath, string $RelativePath) : string {
		if($RelativePath[0] === '/') {
			return substr($RelativePath, 1);
		}

		$path = $BasePath;

		foreach(explode('/', $RelativePath) as $part) {
			if($part === '.') {
				continue;
			}
			else if($part === '..') {
				$delimiter = strrpos($path, '/');

				if($delimiter === false) {
					$path = '';
				}
				else {
					$path = substr($path, 0, $delimiter);
				}
			}
			else if(isset($path[0])) {
				$path .= '/'.$part;
			}
			else {
				$path = $part;
			}
		}

		return $path;
	}

	function StrStartsWith(string $Haystack, string $Needle) : bool {
		return substr($Haystack, 0, strlen($Needle)) === $Needle;
	}

	function StrEndsWith(string $Haystack, string $Needle) : bool {
		return substr($Haystack, -strlen($Needle)) === $Needle;
	}
}

namespace Framework {
	require('PHPMailer'.'/'.'PHPMailer.php');

	require_once('SendGrid'.'/'.'sendgrid-php.php');
}
?>