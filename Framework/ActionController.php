<?
namespace Framework;

class ActionController extends \Framework\AController {
	/* Instance life cycle methods */

	public function Execute() {
		parent::Execute();

		$this->Model->WasSuccessful = null;

		$this->Model->SuccessMessages = [];

		$this->Model->ErrorMessages = [];

		foreach($this->Actions->Items as $action) {
			if($action->WasSuccessful === true) {
				$this->Model->WasSuccessful = $this->Model->WasSuccessful ?? true;

				$this->Model->SuccessMessages = array_merge($this->Model->SuccessMessages, $action->Messages);
			}
			else if($action->WasSuccessful === false) {
				$this->Model->WasSuccessful = false;

				$this->Model->ErrorMessages = array_merge($this->Model->ErrorMessages, $action->Messages);
			}
		}

		if($this->Model->WasSuccessful === true) {
			http_response_code(200);
		}
		else if($this->Model->WasSuccessful === false) {
			http_response_code(400);
		}
		else {
			http_response_code(404);
		}
	}
}
?>