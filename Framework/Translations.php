<?
namespace Framework;

class Translations {
	/* Instance variables */

	private static $Items = [];

	/* Instance initialize methods */

	public static function Initialize() {
		/*{IF:BENCHMARK}*/

		\Framework\Benchmark::StartProcess(['Initialize translations']);

		/*{ENDIF}*/

		$translations = \Framework\GetConfig('Translations', []);

		self::$Items = self::Initialize»Compile($translations);

		/*{IF:BENCHMARK}*/

		\Framework\Benchmark::FinishProcess();

		/*{ENDIF}*/
	}

	private static function Initialize»Compile(array $Translations) : array {
		$compilation = [];

		foreach($Translations as $locale => $translations) {
			$compilation[$locale] = self::Initialize»CompileLocale($translations);
		}

		return $compilation;
	}

	private static function Initialize»CompileLocale(array $Translations) : array {
		$compilation = [];

		foreach($Translations as $key => $translation) {
			if(is_array($translation)) {
				self::Initialize»CompileTranslation($compilation, $key, $translation);
			}
			else {
				$compilation[$key] = $translation;
			}
		}

		return $compilation;
	}

	private static function Initialize»CompileTranslation(array &$Compilation, string $Path, array $Translation) {
		foreach($Translation as $key => $translation) {
			$path = $Path.'.'.$key;

			if(is_array($translation)) {
				self::Initialize»CompileTranslation($Compilation, $path, $translation);
			}
			else {
				$Compilation[$path] = $translation;
			}
		}
	}

	/* Instance get methods */

	public static function Get(string $LocaleId = null, string $Path, string $DefaultTranslation = null) {
		global $Locale;

		return self::$Items[$LocaleId ?? $Locale->Id][$Path] ?? $DefaultTranslation;
	}

	public static function TryGet(string $LocaleId = null, string $Path, string &$Translation = null) : bool {
		global $Locale;

		if(isset(self::$Items[$LocaleId ?? $Locale->Id][$Path])) {
			$Translation = self::$Items[$LocaleId ?? $Locale->Id][$Path];

			return true;
		}
		else {
			return false;
		}
	}

	public static function Set(string $LocaleId = null, string $Path, $Translation) {
		global $Locale;

		self::$Items[$LocaleId ?? $Locale->Id][$Path] = $Translation;
	}

	public static function IsSet(string $LocaleId = null, string $Path) : bool {
		global $Locale;

		return isset(self::$Items[$LocaleId ?? $Locale->Id][$Path]);
	}

	public static function IsNotSet(string $LocaleId = null, string $Path) : bool {
		global $Locale;

		return !isset(self::$Items[$LocaleId ?? $Locale->Id][$Path]);
	}

	public static function IsEmpty(string $LocaleId = null, string $Path) : bool {
		global $Locale;

		return !isset(self::$Items[$LocaleId ?? $Locale->Id][$Path][0]);
	}

	public static function IsNotEmpty(string $LocaleId = null, string $Path) : bool {
		global $Locale;

		return isset(self::$Items[$LocaleId ?? $Locale->Id][$Path][0]);
	}
}

function GetTranslation(string $LocaleId = null, string $Path, string $DefaultTranslation = null) {
	return \Framework\Translations::Get($LocaleId, $Path, $DefaultTranslation);
}

function TryGetTranslation(string $LocaleId = null, string $Path, string &$Translation = null) : bool {
	return \Framework\Translations::TryGet($LocaleId, $Path, $Translation);
}

function SetTranslation(string $LocaleId = null, string $Path, string $Translation) {
	\Framework\Translations::Set($LocaleId, $Path, $Translation);
}

function IsTranslationSet(string $LocaleId = null, string $Path) : bool {
	return \Framework\Translations::IsSet($LocaleId, $Path);
}

function IsTranslationNotSet(string $LocaleId = null, string $Path) : bool {
	return \Framework\Translations::IsNotSet($LocaleId, $Path);
}

function IsTranslationEmpty(string $LocaleId = null, string $Path) : bool {
	return \Framework\Translations::IsEmpty($LocaleId, $Path);
}

function IsTranslationNotEmpty(string $LocaleId = null, string $Path) : bool {
	return \Framework\Translations::IsNotEmpty($LocaleId, $Path);
}
?>