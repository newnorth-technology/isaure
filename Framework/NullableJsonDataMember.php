<?
namespace Framework;

class NullableJsonDataMember extends \Framework\ADynamicNullableDataMember {
	/* Static methods */

	public static function ParseFromDbValue(\Framework\NullableJsonDataMember $DataMember = null, $Value) {
		if($Value === null) {
			return null;
		}

		/*{IF:DEBUG}*/

		if(!is_string($Value)) {
			throw new \Framework\RuntimeException(
				'Failed to parse data member value, invalid value type.',
				['DataManager' => $DataMember->DataManager->Id, 'DataMember' => $DataMember->Name, 'Value' => $Value]
			);
		}

		/*{ENDIF}*/

		return json_decode($Value, true);
	}

	public static function ParseValue(\Framework\NullableJsonDataMember $DataMember = null, $Value) {
		return $Value;
	}

	public static function ParseToDbValue(\Framework\NullableJsonDataMember $DataMember = null, $Value) {
		if($Value === null) {
			return null;
		}

		return json_encode($Value);
	}
}
?>