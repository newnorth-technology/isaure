<?
namespace Framework;

class RerouteException extends \Exception {
	/* Instance variables */

	public $LocaleId;

	public $Path;

	public $Params;

	/* Magic methods */

	public function __construct(string $LocaleId = null, array $Path, array $Params) {
		$this->LocaleId = $LocaleId;

		$this->Path = $Path;

		$this->Params = $Params;
	}
}
?>