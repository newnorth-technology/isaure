<?
namespace Framework;

function Run(array $pv_params) {
	global $Route;

	mysqli_report(MYSQLI_REPORT_STRICT);

	session_start();

	set_error_handler('\\Framework\\OnError');

	set_exception_handler('\\Framework\\OnException');

	register_shutdown_function('\\Framework\\OnShutdown');

	\Framework\Config::Initialize($pv_params['ConfigFiles'] ?? [], $pv_params['Config'] ?? []);

	foreach($pv_params['modules'] ?? [] as $path) {
		\f\module::load($path);
	}

	if(\Framework\TryGetConfig($timezone, 'System.Timezone')) {
		date_default_timezone_set($timezone);
	}

	$GLOBALS['Objects'] = [];

	\Framework\DataManagers::Initialize();

	\Framework\Translations::Initialize();

	\Framework\Routes::Initialize();

	$GLOBALS['Transaction'] = new \Framework\Transaction(null, $_SERVER['SERVER_NAME'], ['Uri' => $_SERVER['REQUEST_URI']]);

	\Framework\Benchmark::StartProcess(['Initializers (stage 1)']);

	if(array_key_exists('Initializers', $pv_params)) {
		foreach($pv_params['Initializers'] as $initializer) {
			if($initializer['Stage'] === 1) {
				call_user_func($initializer['Method'], $initializer['Params'] ?? []);
			}
		}
	}

	foreach($GLOBALS['modules'] as $module) {
		$function = $module->namespace.$module->name.'\\on_initialize';

		if(function_exists($function)) {
			call_user_func($function, $module);
		}
	}

	\Framework\Benchmark::FinishProcess();

	\Framework\Benchmark::StartProcess(['Parse URL']);

	try {
		Run»ParseUrl();
	}
	catch(\Framework\RedirectException $Exception) {
		header('Location: '.$Exception->Url);

		exit();
	}

	\Framework\Benchmark::FinishProcess();

	\Framework\Benchmark::StartProcess(['Initializers (stage 2)']);

	if(array_key_exists('Initializers', $pv_params)) {
		foreach($pv_params['Initializers'] as $initializer) {
			if($initializer['Stage'] === 2) {
				call_user_func($initializer['Method'], $initializer['Params'] ?? []);
			}
		}
	}

	\Framework\Benchmark::FinishProcess();

	\Framework\Benchmark::StartProcess(['Main loop']);

	while(true) {
		try
		{
			$Stage = 0;

			$controller = \Framework\AController::Instantiate(null, $Route->Controller, null, []);

			$root = $controller;

			while($root->Parent !== null) {
				$root = $root->Parent;
			}

			\Framework\Benchmark::StartProcess(['Initializers (stage 3)']);

			if(array_key_exists('Initializers', $pv_params)) {
				foreach($pv_params['Initializers'] as $initializer) {
					if($initializer['Stage'] === 3) {
						call_user_func($initializer['Method'], $initializer['Params'] ?? []);
					}
				}
			}

			\Framework\Benchmark::FinishProcess();

			\Framework\Benchmark::StartProcess(['Controller' => $root->Id]);

			$root->Initialize();

			\Framework\Benchmark::FinishProcess();

			\Framework\Benchmark::StartProcess(['Controller' => $root->Id]);

			$root->Load();

			\Framework\Benchmark::FinishProcess();

			\Framework\Benchmark::StartProcess(['Controller' => $root->Id]);

			$root->Execute();

			\Framework\Benchmark::FinishProcess();

			if(isset($_GET['__BENCHMARK__'])) {
				header('Content-Type: text/html');
			}
			else if($Route->ContentType !== null) {
				header('Content-Type: '.$Route->ContentType);
			}
			else if($controller->View->ContentType !== null) {
				header('Content-Type: '.$controller->View->ContentType);
			}

			\Framework\Benchmark::StartProcess(['Controller' => $root->Id]);

			$output = $root->Render();

			\Framework\Benchmark::FinishProcess();

			if(isset($_GET['__BENCHMARK__'])) {
				\Framework\Benchmark::Print();
			}
			else if($Route->IsAjaxRequest) {
				if(isset($_GET['RequestId'])) {
					echo $_GET['RequestId'], '$', $output;
				}
				else {
					echo $output;
				}
			}
			else {
				echo $output;
			}

			break;
		}
		catch(\Framework\RedirectException $Exception) {
			header('Location: '.$Exception->Url);

			exit();
		}
		catch(\Framework\RerouteException $Exception) {
			if($Exception->LocaleId !== null) {
				$GLOBALS['Locale'] = new \Framework\Locale(\Framework\GetConfig('Locales.'.$Exception->LocaleId));

				$GLOBALS['Locale']->Activate();
			}

			$GLOBALS['Path'] = $Exception->Path;

			$GLOBALS['Route'] = $Exception->Path[count($Exception->Path) - 1];

			$GLOBALS['Params'] = $Exception->Params;

			if($GLOBALS['Route']->Controller === null) {
				header('HTTP/1.0 404 Not Found');

				die('HTTP/1.0 404 Not Found');
			}

			continue;
		}
		catch(\Exception $Exception) {
			throw $Exception;
			/*\Framework\ErrorHandler::HandleException($Exception);

			return true;*/
		}
	}

	\Framework\Benchmark::FinishProcess();
}

function Run»ParseUrl() {
	if(isset($_SERVER['REQUEST_URI'])) {
		$url = urldecode($_SERVER['REQUEST_URI']);

		$url = preg_replace('/\\?.*?$/', '', $url);

		$url = isset($url[1]) ? substr($url, 1) : null;
	}
	else {
		$url = null;
	}

	/*{IF:DEBUG}*/
	if(array_key_exists('debug_route', $_GET)) {
		if(array_key_exists('Locale', $_SESSION)) {
			if(\Framework\ParseUrl($url, $_SESSION['Locale'], $path, $params)) {
				die();
			}
			else if(!\Framework\ParseUrl($url, $locale, $path, $params)) {
				die();
			}
			else {
				die();
			}
		}
		else if(!\Framework\ParseUrl($url, $locale, $path, $params)) {
			die();
		}
		else {
			die();
		}
	}
	else if(array_key_exists('Locale', $_SESSION)) {
		if(\Framework\ParseUrl($url, $_SESSION['Locale'], $path, $params)) {
			$locale = $_SESSION['Locale'];
		}
		else if(!\Framework\ParseUrl($url, $locale, $path, $params)) {
			if(isset($_GET['__BENCHMARK__'])) {
				\Framework\Benchmark::Print();

				die();
			}
			else {
				header('HTTP/1.0 404 Not Found');

				die('HTTP/1.0 404 Not Found');
			}
		}
	}
	else if(!\Framework\ParseUrl($url, $locale, $path, $params)) {
		if(isset($_GET['__BENCHMARK__'])) {
			\Framework\Benchmark::Print();

			die();
		}
		else {
			header('HTTP/1.0 404 Not Found');

			die('HTTP/1.0 404 Not Found');
		}
	}
	/*{ELSE}*/
	if(array_key_exists('Locale', $_SESSION)) {
		if(\Framework\ParseUrl($url, $_SESSION['Locale'], $path, $params)) {
			$locale = $_SESSION['Locale'];
		}
		else if(!\Framework\ParseUrl($url, $locale, $path, $params)) {
			if(isset($_GET['__BENCHMARK__'])) {
				\Framework\Benchmark::Print();

				die();
			}
			else {
				header('HTTP/1.0 404 Not Found');

				die('HTTP/1.0 404 Not Found');
			}
		}
	}
	else if(!\Framework\ParseUrl($url, $locale, $path, $params)) {
		if(isset($_GET['__BENCHMARK__'])) {
			\Framework\Benchmark::Print();

			die();
		}
		else {
			header('HTTP/1.0 404 Not Found');

			die('HTTP/1.0 404 Not Found');
		}
	}
	/*{ENDIF}*/

	if($locale === null) {
		if(!\Framework\TryGetConfig($locale, 'System.Defaults.Locale')) {
			throw new \Framework\ConfigException('Locale not set.');
		}
	}

	$GLOBALS['Locale'] = new \Framework\Locale(\Framework\GetConfig('Locales.'.$locale));

	$GLOBALS['Locale']->Activate();

	$GLOBALS['Path'] = $path;

	$GLOBALS['Route'] = $GLOBALS['Path'][count($path) - 1];

	$GLOBALS['Params'] = $params;

	if($GLOBALS['Route']->Controller === null) {
		if(isset($_GET['__BENCHMARK__'])) {
			\Framework\Benchmark::Print();

			die();
		}
		else {
			header('HTTP/1.0 404 Not Found');

			die('HTTP/1.0 404 Not Found');
		}
	}
}
?>