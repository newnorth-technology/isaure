<?
namespace Framework;

abstract class ADynamicNullableDataMember extends \Framework\ADataMember {
	/* Magic methods */

	public function __construct(\Framework\ADataManager $DataManager, string $Name, array $Params) {
		parent::__construct($DataManager, $Name, $Params);
	}

	/* Instance methods */

	public function Set(int $Id, $Value) {
		$this->DataManager->Update([
			'Changes' => [
				[
					'ColumnA' => [
						'Type' => 'Reference',
						'Source' => $this->DataManager->Id,
						'Name' => $this->Name
					],
					'ColumnB' => [
						'Type' => 'Value',
						'Value' => $this->ParseToDbValue($this, $Value)
					]
				]
			],
			'Condition' => [
				'Type' => 'IsEqualTo',
				'ColumnA' => [
					'Type' => 'Reference',
					'Source' => $this->DataManager->Id,
					'Name' => $this->DataManager->PrimaryKey
				],
				'ColumnB' => [
					'Type' => 'Value',
					'Value' => $Id
				]
			]
		]);

		if($this->DataManager->UseLog && $this->UseLogUpdate) {
			$this->LogUpdate($Id, $Value);
		}
	}

	public function Increment(int $Id, $Amount) {
		$this->DataManager->Update([
			'Changes' => [
				[
					'ColumnA' => [
						'Type' => 'Reference',
						'Source' => $this->DataManager->Id,
						'Name' => $this->Name
					],
					'ColumnB' => [
						'Type' => 'Add',
						'ColumnA' => [
							'Type' => 'Reference',
							'Source' => $this->DataManager->Id,
							'Name' => $this->Name
						],
						'ColumnB' => [
							'Type' => 'Value',
							'Value' => $Amount
						]
					]
				]
			],
			'Condition' => [
				'Type' => 'IsEqualTo',
				'ColumnA' => [
					'Type' => 'Reference',
					'Source' => $this->DataManager->Id,
					'Name' => $this->DataManager->PrimaryKey
				],
				'ColumnB' => [
					'Type' => 'Value',
					'Value' => $Id
				]
			]
		]);

		if($this->DataManager->UseLog && $this->UseLogUpdate) {
			$this->LogUpdate($Id, '+'.$Amount);
		}
	}

	public function Decrement(int $Id, $Amount) {
		$this->DataManager->Update([
			'Changes' => [
				[
					'ColumnA' => [
						'Type' => 'Reference',
						'Source' => $this->DataManager->Id,
						'Name' => $this->Name
					],
					'ColumnB' => [
						'Type' => 'Subtract',
						'ColumnA' => [
							'Type' => 'Reference',
							'Source' => $this->DataManager->Id,
							'Name' => $this->Name
						],
						'ColumnB' => [
							'Type' => 'Value',
							'Value' => $Amount
						]
					]
				]
			],
			'Condition' => [
				'Type' => 'IsEqualTo',
				'ColumnA' => [
					'Type' => 'Reference',
					'Source' => $this->DataManager->Id,
					'Name' => $this->DataManager->PrimaryKey
				],
				'ColumnB' => [
					'Type' => 'Value',
					'Value' => $Id
				]
			]
		]);

		if($this->DataManager->UseLog && $this->UseLogUpdate) {
			$this->LogUpdate($Id, '-'.$Amount);
		}
	}
}
?>