<?
namespace Framework;

class StandardDataManager extends \Framework\ADataManager {
	/* Instance variables */

	public $UseTranslation;

	/* Magic methods */

	public function __construct(string $Id, string $Name, array $DataType, array $Params) {
		$this->UseTranslation = $Params['UseTranslation'] ?? false;

		parent::__construct($Id, $Name, $DataType, $Params);
	}

	/* Instance initialization methods */

	public function Initialize() {

	}

	public function Initialize»DataMembers() {
		$this->DataMembers['Id'] = [
			'Type' => 'StaticInt',
			'UseLogInsert' => true,
			'UseLogUpdate' => false,
			'UseLogDelete' => true
		];

		parent::Initialize»DataMembers();
	}

	public function Initialize»References() {
		if($this->UseTranslation) {
			$this->References['Translation'] = [
				'TargetDataManager' => $this->Id.'»Translation',
				'SelectQuery' => [
					'Condition' => new \Framework\Database\Conditions\All([
						'Conditions' => [
							new \Framework\Database\Conditions\IsEqualTo([
								'ColumnA' => new \Framework\Database\Columns\Reference(['Source' => $this->Id.'»Translation', 'Name' => 'ParentId']),
								'ColumnB' => new \Framework\Database\Columns\Reference(['Source' => $this->Id, 'Name' => 'Id'])
							]),
							new \Framework\Database\Conditions\IsEqualTo([
								'ColumnA' => new \Framework\Database\Columns\Reference(['Source' => $this->Id.'»Translation', 'Name' => 'Locale']),
								'ColumnB' => new \Framework\Database\Columns\Value(['Variable' => 'Locale'])
							])
						]
					])
				]
			];

			$this->References['Translations'] = [
				'TargetDataManager' => $this->Id.'»Translation',
				'OnDeleteAction' => 'Delete',
				'SelectQuery' => [
					'Condition' => new \Framework\Database\Conditions\IsEqualTo([
						'ColumnA' => new \Framework\Database\Columns\Reference(['Source' => $this->Id.'»Translation', 'Name' => 'ParentId']),
						'ColumnB' => new \Framework\Database\Columns\Value(['Variable' => $this->Id.'.Id'])
					])
				]
			];
		}

		parent::Initialize»References();
	}

	public function Initialize»InsertQuery(array $Params = null) {
		$this->InsertQuery = new \Framework\Database\InsertQuery([
			'Target' => new \Framework\Database\Target([
				'DataManager' => $this
			])
		]);
	}

	public function Initialize»UpdateQuery(array $Params = null) {
		$this->UpdateQuery = new \Framework\Database\UpdateQuery([
			'Sources' => new \Framework\Database\Sources([
				$this->Id => new \Framework\Database\Source([
					'DataManager' => $this,
					'Alias' => $this->Id
				])
			])
		]);
	}

	public function Initialize»SelectQuery(array $Params = null) {
		$this->SelectQuery = new \Framework\Database\SelectQuery([
			'Columns' => $this->DataMembers,
			'Sources' => new \Framework\Database\Sources([
				$this->Id => new \Framework\Database\Source([
					'DataManager' => $this,
					'Alias' => $this->Id
				])
			]),
			'Condition' => $Params['Condition'] ?? null
		]);
	}

	/* Instance find methods */

	public function FindById(int $Id, array $Params = null) {
		return $this->Find(
			[
				'Condition' => new \Framework\Database\Conditions\IsEqualTo([
					'ColumnA' => $this->DataMembers['Id'],
					'ColumnB' => $Id
				])
			],
			$Params
		);
	}

	public function TryFindById(&$Item, int $Id, array $Params = null) : bool {
		$Item = $this->Find(
			[
				'Condition' => new \Framework\Database\Conditions\IsEqualTo([
					'ColumnA' => $this->DataMembers['Id'],
					'ColumnB' => $Id
				])
			],
			$Params
		);

		return $Item !== null;
	}

	/* Instance contains methods */

	public function ContainsById(int $Id, array $Params = null) : bool {
		return $this->Contains(
			[
				'Condition' => new \Framework\Database\Conditions\IsEqualTo([
					'ColumnA' => $this->DataMembers['Id'],
					'ColumnB' => $Id
				])
			],
			$Params
		);
	}
}
?>