<?
namespace Framework;

class Route {
	/* Instance variables */

	public $Id;

	public $Name;

	public $Parent;

	public $Children = [];

	public $Controller;

	public $Patterns = [];

	public $Params = [];

	public $Protocol;

	public $Port;

	public $Method;

	public $Validators = null;

	public $ContentType;

	public $IsAjaxRequest;

	/* Magic methods */

	public function __construct(array $Params) {
		$this->Id = $Params['Id'];

		$this->Name = $Params['Name'] ?? null;

		$this->Parent = $Params['Parent'] ?? null;

		$this->Controller = $Params['Controller'] ?? null;

		if(isset($this->Name[1])) {
			$params = $Params['Pattern'] ?? [['Type' => 'Constant', 'Value' => str_replace('_', '-', substr($this->Name, 0, -1))]];

			$this->Patterns[''] = new \Framework\Route\Pattern($this, null, $params);

			foreach(\Framework\GetConfig('Locales') as $locale) {
				$this->Patterns[$locale['Id']] = new \Framework\Route\Pattern($this, $locale['Id'], $params);
			}
		}
		else {
			$params = $Params['Pattern'] ?? [];

			$this->Patterns[''] = new \Framework\Route\Pattern($this, null, $params);

			foreach(\Framework\GetConfig('Locales') as $locale) {
				$this->Patterns[$locale['Id']] = new \Framework\Route\Pattern($this, $locale['Id'], $params);
			}
		}

		$this->Params = $Params['Params'] ?? [];

		$this->Protocol = $Params['Protocol'] ?? null;

		$this->Port = $Params['Port'] ?? null;

		$this->Method = $Params['Method'] ?? null;

		if(array_key_exists('Validators', $Params)) {
			$this->Validators = new \Framework\RouteValidators($Params['Validators']);
		}

		$this->ContentType = $Params['ContentType'] ?? null;

		$this->IsAjaxRequest = $Params['IsAjaxRequest'] ?? false;
	}

	public function __debugInfo() {
		return [
			'Id' => $this->Id,
			'Controller' => $this->Controller,
			'Patterns' => $this->Patterns,
			'Protocol' => $this->Protocol,
			'Port' => $this->Port,
			'Method' => $this->Method
		];
	}

	/* Instance methods */

	public function Match(string $pv_url = null, string $pv_locale_id = null, array $pv_params, array &$pv_matches) : bool {
		/*{IF:DEBUG}*/
		if(array_key_exists('debug_route', $_GET)) {
			echo '<div><b>route::match(', $pv_url === null ? 'null' : '"'.$pv_url.'"', ', ', $pv_locale_id === null ? 'null' : '"'.$pv_locale_id.'"', ', [...], [...])</b></div>';

			echo '<div style="padding-left: 2rem;">';

			echo '<div>- id: "', $this->Id, '"</div>';

			$matches = [];

			$match_count = 0;

			if($pv_locale_id === null) {
				foreach($this->Patterns as $pattern) {
					$match_count += $pattern->Match($pv_url, $pv_params, $matches);
				}
			}
			else {
				$match_count += $this->Patterns[$pv_locale_id]->Match($pv_url, $pv_params, $matches);

				$match_count += $this->Patterns['']->Match($pv_url, $pv_params, $matches);
			}

			foreach($matches as $match) {
				$pv_matches[] = new \Framework\Route\Match($pv_url, $match->rest_url, $pv_locale_id, $match->Params);
			}

			echo '<div>- match_count: ', $match_count, '</div>';

			echo '</div>';

			return 0 < $match_count;
		}
		else {
			$matches = [];

			$match_count = 0;

			if($pv_locale_id === null) {
				foreach($this->Patterns as $pattern) {
					$match_count += $pattern->Match($pv_url, $pv_params, $matches);
				}
			}
			else {
				$match_count += $this->Patterns[$pv_locale_id]->Match($pv_url, $pv_params, $matches);

				$match_count += $this->Patterns['']->Match($pv_url, $pv_params, $matches);
			}

			foreach($matches as $match) {
				$pv_matches[] = new \Framework\Route\Match($pv_url, $match->rest_url, $pv_locale_id, $match->Params);
			}

			return 0 < $match_count;
		}
		/*{ELSE}*/
		$matches = [];

		$match_count = 0;

		if($pv_locale_id === null) {
			foreach($this->Patterns as $pattern) {
				$match_count += $pattern->Match($pv_url, $pv_params, $matches);
			}
		}
		else {
			$match_count += $this->Patterns[$pv_locale_id]->Match($pv_url, $pv_params, $matches);

			$match_count += $this->Patterns['']->Match($pv_url, $pv_params, $matches);
		}

		foreach($matches as $match) {
			$pv_matches[] = new \Framework\Route\Match($pv_url, $match->rest_url, $pv_locale_id, $match->Params);
		}

		return 0 < $match_count;
		/*{ENDIF}*/
	}

	public function ParseUrl(string $pv_url = null, string &$pv_locale_id = null, array &$pv_path = null, array &$pv_params = null) : bool {
		/*{IF:DEBUG}*/
		if(array_key_exists('debug_route', $_GET)) {
			echo '<div><b>route["', $this->Id, '"]::parse_url(', $pv_url === null ? 'null' : '"'.$pv_url.'"', ', ', $pv_locale_id === null ? 'null' : '"'.$pv_locale_id.'"', ', [...], [...])</b></div>';

			echo '<div style="padding-left: 2rem;">';

			$matches = [];

			if(!$this->Match($pv_url, $pv_locale_id, $pv_params ?? [], $matches)) {
				return false;
			}

			foreach($matches as $match) {
				foreach($this->Children as $child) {
					$localeId = $match->LocaleId;

					$params = array_merge($pv_params ?? [], $this->Params, $match->Params);

					if($this->Validators === null || $this->Validators->Validate($params)) {
						if($child->ParseUrl($match->rest_url, $localeId, $path, $params)) {
							$pv_locale_id = $localeId;

							$pv_path = array_merge([$this], $path);

							$pv_params = $params;

							return true;
						}
					}
				}

				$params = array_merge($pv_params ?? [], $this->Params, $match->Params);

				if($this->Validators === null || $this->Validators->Validate($params)) {
					if($match->rest_url === null) {
						if($this->Protocol !== null && preg_match('/^'.$this->Protocol.'$/', $_SERVER['SERVER_PROTOCOL']) !== 1) {
							continue;
						}

						if($this->Port !== null && preg_match('/^'.$this->Port.'$/', $_SERVER['SERVER_PORT']) !== 1) {
							continue;
						}

						if($this->Method !== null && preg_match('/^'.$this->Method.'$/', $_SERVER['REQUEST_METHOD']) !== 1) {
							continue;
						}

						$pv_locale_id = $match->LocaleId;

						$pv_path = [$this];

						$pv_params = $params;

						return true;
					}
				}
			}

			return false;

			echo '</div>';
		}
		else {
			$matches = [];

			if(!$this->Match($pv_url, $pv_locale_id, $pv_params ?? [], $matches)) {
				return false;
			}

			foreach($matches as $match) {
				foreach($this->Children as $child) {
					$localeId = $match->LocaleId;

					$params = array_merge($pv_params ?? [], $this->Params, $match->Params);

					if($this->Validators === null || $this->Validators->Validate($params)) {
						if($child->ParseUrl($match->rest_url, $localeId, $path, $params)) {
							$pv_locale_id = $localeId;

							$pv_path = array_merge([$this], $path);

							$pv_params = $params;

							return true;
						}
					}
				}

				$params = array_merge($pv_params ?? [], $this->Params, $match->Params);

				if($this->Validators === null || $this->Validators->Validate($params)) {
					if($match->rest_url === null) {
						if($this->Protocol !== null && preg_match('/^'.$this->Protocol.'$/', $_SERVER['SERVER_PROTOCOL']) !== 1) {
							continue;
						}

						if($this->Port !== null && preg_match('/^'.$this->Port.'$/', $_SERVER['SERVER_PORT']) !== 1) {
							continue;
						}

						if($this->Method !== null && preg_match('/^'.$this->Method.'$/', $_SERVER['REQUEST_METHOD']) !== 1) {
							continue;
						}

						$pv_locale_id = $match->LocaleId;

						$pv_path = [$this];

						$pv_params = $params;

						return true;
					}
				}
			}

			return false;
		}
		/*{ELSE}*/
		$matches = [];

		if(!$this->Match($pv_url, $pv_locale_id, $pv_params ?? [], $matches)) {
			return false;
		}

		foreach($matches as $match) {
			foreach($this->Children as $child) {
				$localeId = $match->LocaleId;

				$params = array_merge($pv_params ?? [], $this->Params, $match->Params);

				if($this->Validators === null || $this->Validators->Validate($params)) {
					if($child->ParseUrl($match->rest_url, $localeId, $path, $params)) {
						$pv_locale_id = $localeId;

						$pv_path = array_merge([$this], $path);

						$pv_params = $params;

						return true;
					}
				}
			}

			$params = array_merge($pv_params ?? [], $this->Params, $match->Params);

			if($this->Validators === null || $this->Validators->Validate($params)) {
				if($match->rest_url === null) {
					if($this->Protocol !== null && preg_match('/^'.$this->Protocol.'$/', $_SERVER['SERVER_PROTOCOL']) !== 1) {
						continue;
					}

					if($this->Port !== null && preg_match('/^'.$this->Port.'$/', $_SERVER['SERVER_PORT']) !== 1) {
						continue;
					}

					if($this->Method !== null && preg_match('/^'.$this->Method.'$/', $_SERVER['REQUEST_METHOD']) !== 1) {
						continue;
					}

					$pv_locale_id = $match->LocaleId;

					$pv_path = [$this];

					$pv_params = $params;

					return true;
				}
			}
		}

		return false;
		/*{ENDIF}*/
	}

	public function CreateUrl(string $LocaleId = null, array $Params = null, array $QueryString = null) : string {
		if($this->Controller === null) {
			throw new \Framework\RuntimeException(
				'Unable to create URL, controller is null.',
				[
					'Route' => $this->Id
				]
			);
		}

		$localeId = $LocaleId ?? $GLOBALS['Locale']->Id ?? $_SESSION['Locale']->Id ?? \Framework\GetConfig('System.Defaults.Locale');

		$params = array_merge($GLOBALS['Params'] ?? [], $Params ?? []);

		$url = $this->CreateUrl»Recursive($localeId, $params);

		if($QueryString === null || count($QueryString) === 0) {
			return $url;
		}
		else {
			return $url.'?'.\Framework\BuildHttpQueryString($QueryString);
		}
	}

	private function CreateUrl»Recursive(string $LocaleId, array $Params) : string {
		if($this->Parent === null) {
			return '/'.$this->Patterns[$LocaleId]->CreateUrl($Params);
		}
		else {
			return $this->Parent->CreateUrl»Recursive($LocaleId, $Params).$this->Patterns[$LocaleId]->CreateUrl($Params);
		}
	}

	public function TryCreateUrl(string $LocaleId = null, array $Params = null, array $QueryString = null, string &$Url = null) : bool {
		if($this->Controller === null) {
			return false;
		}

		$localeId = $LocaleId ?? $GLOBALS['Locale']->Id ?? $_SESSION['Locale']->Id ?? \Framework\GetConfig('System.Defaults.Locale');

		$params = array_merge($GLOBALS['Params'] ?? [], $Params ?? []);

		if(!$this->TryCreateUrl»Recursive($localeId, $params, $url)) {
			return false;
		}

		if($QueryString === null || count($QueryString) === 0) {
			$Url = $url;
		}
		else {
			$Url = $url.'?'.\Framework\BuildHttpQueryString($QueryString);
		}

		return true;
	}

	private function TryCreateUrl»Recursive(string $LocaleId = null, array $pv_params = [], string &$Url = null) : bool {
		if(!$this->Patterns[$LocaleId]->TryCreateUrl($pv_params, $url)) {
			return false;
		}

		if($this->Parent === null) {
			$Url = '/'.$url;

			return true;
		}

		if(!$this->Parent->TryCreateUrl»Recursive($LocaleId, $pv_params, $parentUrl)) {
			return false;
		}

		$Url = $parentUrl.$url;

		return true;
	}

	public function IsEligible(bool $pv_recursive = false) : bool {
		if($pv_recursive && $this->Parent !== null && !$this->Parent->IsEligible($pv_recursive)) {
			return false;
		}

		if($this->Validators !== null && !$this->Validators->Validate([])) {
			return false;
		}

		return true;
	}
}
?>