<?
namespace Framework\AAction;

class PreValidators implements \ArrayAccess {
	/* Instance variables */

	public $Items = [];

	/* Magic methods */

	public function __construct(array &$Params = null) {
		if($Params !== null) {
			foreach($Params as $params) {
				$this->Items[] = new \Framework\AAction\PreValidator($params);
			}
		}
	}

	/* Instance ArrayAccess methods */

	public function offsetSet($Key, $Value) {
		return $this->Items[$Key] = $Value;
	}

	public function offsetExists($Key) {
		return isset($this->Items[$Key]);
	}

	public function offsetUnset($Key) {
		throw new \Framework\NotImplementedException();
	}

	public function &offsetGet($Key) {
		return $this->Items[$Key];
	}

	/* Instance initialization methods */

	public function Validate(\Framework\AController $Controller, \Framework\AAction $Action) : bool {
		foreach($this->Items as $item) {
			if(!$item->Validate($Controller, $Action)) {
				return false;
			}
		}

		return true;
	}
}
?>