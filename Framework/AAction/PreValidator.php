<?
namespace Framework\AAction;

class PreValidator  {
	/* Instance variables */

	public $Controller;

	public $Method;

	public $Params;

	/* Magic methods */

	public function __construct(array &$Params = null) {
		$this->Controller = $Params['Controller'] ?? null;

		$this->Method = 'Validate»'.$Params['Method'];

		$this->Params = $Params['Params'] ?? [];
	}

	/* Instance initialization methods */

	public function Validate(\Framework\AController $Controller, \Framework\AAction $Action) : bool {
		return $this->Validate»GetObject($Controller, $Action)->{$this->Method}($this->Params);
	}

	private function Validate»GetObject(\Framework\AController $Controller, \Framework\AAction $Action) {
		if($this->Controller === null) {
			return $Action;
		}

		if($Controller->TryGetRelController($this->Controller, $object)) {
			return $object;
		}

		throw new \Framework\RuntimeException(
			'Unable to validate, controller not found.',
			[
				'Controller' => $Controller->Id,
				'Action' => $Action->Name,
				'Path' => $this->Controller,
			]
		);
	}
}
?>