<?
namespace Framework\AAction;

class Variables implements \ArrayAccess {
	/* Instance variables */

	public $Items = [];

	public $Config;

	/* Magic methods */

	public function __construct(array &$Params = null) {
		$this->Config = $Params ?? [];
	}

	/* Instance ArrayAccess methods */

	public function offsetSet($Key, $Value) {
		return $this->Items[$Key] = $Value;
	}

	public function offsetExists($Key) {
		return isset($this->Items[$Key]);
	}

	public function offsetUnset($Key) {
		throw new \Framework\NotImplementedException();
	}

	public function &offsetGet($Key) {
		return $this->Items[$Key];
	}

	/* Instance initialization methods */

	public function Initialize(\Framework\AController $Controller, \Framework\Model $Model) {
		foreach($this->Config as $name => &$params) {
			$params['IsTemplate'] = (($params['IsTemplate'] ?? false) === true);

			if($params['IsTemplate']) {
				$params['TemplateCount'] = eval('return intval('.$params['TemplateCount'].');');
			}
			else {
				$params['TemplateCount'] = 0;
			}

			$params['IsTranslation'] = (($params['IsTranslation'] ?? false) === true);

			if($params['IsTranslation']) {
				foreach(\Framework\GetConfig('Locales') as $locale) {
					if($this->Initialize»IsVariableSet($params['Source'], $locale['Id'])) {
						$this->Items[$locale['Id']][$name] = $this->Initialize»GetVariableValue($params['Source'], $locale['Id']);
					}
					else if(array_key_exists('DefaultValue', $params)) {
						$this->Items[$locale['Id']][$name] = $params['DefaultValue'];
					}
					else {
						$this->Items[$locale['Id']][$name] = null;
					}
				}
			}
			else if($this->Initialize»IsVariableSet($params['Source'])) {
				$this->Items[$name] = $this->Initialize»GetVariableValue($params['Source']);
			}
			else if(array_key_exists('DefaultValue', $params)) {
				$this->Items[$name] = $params['DefaultValue'];
			}
			else {
				$this->Items[$name] = null;
			}
		}
	}

	private function Initialize»IsVariableSet(string $pv_source, string $pv_locale = null) {
		if($pv_locale === null) {
			return eval('return isset('.$pv_source.');');
		}
		else {
			return eval('return isset('.$pv_source.'[\''.$pv_locale.'\']);');
		}
	}

	private function Initialize»GetVariableValue(string $pv_source, string $pv_locale = null) {
		if($pv_locale === null) {
			return eval('return '.$pv_source.';');
		}
		else {
			return eval('return '.$pv_source.'[\''.$pv_locale.'\'];');
		}
	}

	/* Instance methods */

	public function IsVariableTemplate(string $pv_key) : bool {
		return $this->Config[$pv_key]['IsTemplate'];
	}

	public function GetVariableTemplateCount(string $pv_key) : int {
		return $this->Config[$pv_key]['TemplateCount'];
	}

	public function IsVariableTranslation(string $pv_key) : bool {
		return $this->Config[$pv_key]['IsTranslation'];
	}
}
?>