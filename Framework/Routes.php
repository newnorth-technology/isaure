<?
namespace Framework;

class Routes {
	/* Static variables */

	private static $Items = [];

	/* Static initialize methods */

	public static function Initialize() {
		/*{IF:BENCHMARK}*/

		\Framework\Benchmark::StartProcess(['Initialize routes']);

		/*{ENDIF}*/

		$params = \Framework\GetConfig('Routes', []);

		ksort($params);

		Routes::Initialize»Items($params);

		/*{IF:BENCHMARK}*/

		\Framework\Benchmark::FinishProcess();

		/*{ENDIF}*/
	}

	private static function Initialize»Items(array $Params) {
		$params = $Params['/'];

		$params['Id'] = '/';

		$params['Name'] = '/';

		$params['Parent'] = null;

		$route = new \Framework\Route($params);

		Routes::$Items['/'] = $route;

		unset($Params['/']);

		foreach($Params as $path => $params) {
			$params['Id'] = $path;

			$delimiter = strrpos($path, '/', -2);

			$params['Name'] = substr($path, $delimiter + 1);

			$params['Parent'] = Routes::$Items[substr($path, 0, $delimiter + 1)] ?? null;

			$route = new \Framework\Route($params);

			Routes::$Items[$params['Id']] = $route;

			$params['Parent']->Children[$params['Name']] = $route;
		}
	}

	/* Static get methods */

	public static function GetRoute(string $Path) : \Framework\Route {
		return Routes::$Items[$Path];
	}

	public static function TryGetRoute(string $Path, \Framework\Route &$Route = null) : bool {
		if(isset(Routes::$Items[$Path])) {
			$Route = Routes::$Items[$Path];

			return true;
		}
		else {
			return false;
		}
	}

	public static function GetRelRoute(\Framework\Route $Base = null, string $Path) : \Framework\Route {
		$route = $Base ?? $GLOBALS['Route'];

		if($Path === '') {
			return $route;
		}

		if($Path[0] === '/') {
			return Routes::GetRoute($Path);
		}

		foreach(explode('/', $Path) as $path) {
			if($path === '' || $path === '.') {
				continue;
			}
			else if($path === '..') {
				$route = $route->Parent;

				if($route === null) {
					throw new \Framework\RuntimeException(
						'Parent route not found.',
						[
							'Base route' => $Base,
							'Path' => $Path,
						]
					);
				}
			}
			else {
				$route = $route->Children[$path.'/'] ?? null;

				if($route === null) {
					throw new \Framework\RuntimeException(
						'Child route not found.',
						[
							'Base route' => $Base->Id,
							'Path' => $Path,
							'Sub path' => $path.'/',
						]
					);
				}
			}
		}

		return $route;
	}

	public static function TryGetRelRoute(\Framework\Route $Base = null, string $Path, \Framework\Route &$Route = null) {
		$route = $Base ?? $GLOBALS['Route'];

		if($Path === '') {
			$Route = $route;

			return true;
		}

		if($Path[0] === '/') {
			return Routes::TryGetRoute($Path, $Route);
		}

		foreach(explode('/', $Path) as $path) {
			if($path === '.') {
				continue;
			}
			else if($path === '..') {
				$route = $route->Parent;

				if($route === null) {
					return false;
				}
			}
			else {
				$route = $route->Children[$path] ?? null;

				if($route === null) {
					return false;
				}
			}
		}

		$Route = $route;

		return true;
	}
}

function GetRoute(string $Path) : \Framework\Route {
	return \Framework\Routes::GetRoute($Path);
}

function TryGetRoute(string $Path, \Framework\Route &$Route = null) : bool {
	return \Framework\Routes::TryGetRoute($Path, $Route);
}

function GetRelRoute(\Framework\Route $Base = null, string $Path) : \Framework\Route {
	return \Framework\Routes::GetRelRoute($Base, $Path);
}

function TryGetRelRoute(\Framework\Route $Base = null, string $Path, \Framework\Route &$Route = null) {
	return \Framework\Routes::TryGetRelRoute($Base, $Path, $Route);
}

function ParseUrl(string $Url = null, string &$LocaleId = null, array &$Path = null, array &$Params = null) : bool {
	return \Framework\Routes::GetRoute('/')->ParseUrl($Url, $LocaleId, $Path, $Params);
}

function CreateUrl(string $Path = '', string $LocaleId = null, array $Params = null, array $QueryString = null) : string {
	if(isset($Path[0]) && $Path[0] === '/') {
		return \Framework\Routes::GetRoute($Path)->CreateUrl($LocaleId, $Params, $QueryString);
	}
	else {
		return \Framework\Routes::GetRelRoute($GLOBALS['Route'], $Path)->CreateUrl($LocaleId, $Params, $QueryString);
	}
}

function TryCreateUrl(string &$Url = null, string $Path = '', string $LocaleId = null, array $Params = null, array $QueryString = null) : string {
	if(isset($Path[0]) && $Path[0] === '/') {
		return \Framework\Routes::TryGetRoute($Path, $route) && $route->TryCreateUrl($LocaleId, $Params, $QueryString, $Url);
	}
	else {
		return \Framework\Routes::TryGetRelRoute($GLOBALS['Route'], $Path, $route) && $route->TryCreateUrl($LocaleId, $Params, $QueryString, $Url);
	}
}

function Reroute(string $Path = '', string $LocaleId = null, array $Params = null) {
	$url = \Framework\CreateUrl($Path, $LocaleId, $Params);

	$url = substr($url, 1);

	\Framework\ParseUrl($url, $localeId, $path, $params);

	$params = array_merge($Params ?? [], $params);

	throw new \Framework\RerouteException($localeId, $path, $params);
}

function RedirectPath(string $Path = '', string $LocaleId = null, array $Params = null, array $pv_qs = null) {
	if(array_key_exists('RequestId', $_GET)) {
		if($pv_qs === null) {
			$pv_qs = ['RequestId' => $_GET['RequestId']];
		}
		else {
			$pv_qs['RequestId'] = $_GET['RequestId'];
		}
	}

	foreach($_GET as $key => $value) {
		if($key[0] === '_') {
			if($pv_qs === null) {
				$pv_qs = [$key => $_GET[$key]];
			}
			else {
				$pv_qs[$key] = $_GET[$key];
			}
		}
	}

	$url = \Framework\CreateUrl($Path, $LocaleId, $Params, $pv_qs);

	throw new \Framework\RedirectException($url);
}

function RedirectUrl(string $Url = null, array $QueryString = []) {
	$url = $Url ?? $_SERVER['REQUEST_URI'];

	if($QueryString === null || count($QueryString) === 0) {
		throw new \Framework\RedirectException($url);
	}
	else if(strpos($url, '?') === false) {
		throw new \Framework\RedirectException($url.'?'.\Framework\BuildHttpQueryString($QueryString));
	}
	else {
		throw new \Framework\RedirectException($url.'&'.\Framework\BuildHttpQueryString($QueryString));
	}
}
?>