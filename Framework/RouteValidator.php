<?
namespace Framework;

class RouteValidator {
	/* Instance variables */

	public $Method = [];

	public $Params = [];

	/* Magic methods */

	public function __construct(array $Params) {
		$this->Method = $Params['Method'];

		$this->Params = $Params['Params'];
	}

	public function __debugInfo() {
		return [
			'Method' => $this->Method,
			'Params' => $this->Params
		];
	}

	/* Instance methods */

	public function Validate($pv_params) : bool {
		return call_user_func($this->Method, array_merge($this->Params, $pv_params));
	}
}
?>